﻿Shader "Unlit/LivingGridShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }


#define S(a, b, t) smoothstep(a, b, t)

            float DistLine(float2 p, float2 a, float2 b)
            {
 	            float2 pa = p-a;
                float2 ba = b-a;
                float t = clamp(dot(pa, ba) / dot (ba, ba), 0., 1.);
                return length (pa - ba*t);
            }

            // single random number
            float N21(float2 p)
            {
 	            p = frac(p*float2(233.34, 851.73));
                p += dot(p, p+23.45);
                return frac(p.x * p.y);
            }

            // 2 random numbers
            float2 N22(float2 p)
            {
 	            float n = N21(p);
                return float2(n, N21(p+n));
            }

            float2 GetPos(float2 id, float2 offset)
            {
                float2 n = N22(id + offset) * _Time.y;		// noise
                return offset + sin(n) *.4;
            }

            float Line(float2 p, float2 a, float2 b)
            {
 	            float d = DistLine(p, a, b);
                //float m = S(.02, .01, d);			// 'cut out' thickness of the line
                float m = S(.01, .005, d);			// 'cut out' thickness of the line
    
                float d2 = length(a-b);
                m *= S(1.2, .8, d2) *.5 + S(.05, .03, abs(d2-.75));		// don't draw line if > 1.2, fade in between 1.2 and .8, .8 fully visible.  flash when line is a certain length
    
                return m;
            }

            float Layer(float2 uv)
            {
                float m = .0;
                float2 gv = frac(uv) - .5;		// 0,0 is centre of grid
                float2 id = floor(uv);		    // integer component
    
                float2 p[9];					// for adjacent cells (4 is middle one)
    
                int i = 0;
                for (float y=-1.; y<=1.; y++)
                {
    	            for (float x=-1.; x<=1.; x++)
    	            {
        	            p[i++] = GetPos(id, float2(x, y));
    	            }
                }
    
                float sparkleTime = _Time.y * .01;
                for (int i=0; i<9; i++)
                {
                    m += Line(gv, p[4], p[i]);
        
                    float2 j = (p[i] - gv) * 40.;			// sparkle size
                    float sparkle = 1./dot(j, j);
        
                    m += sparkle * (sin(sparkleTime + frac(p[i].x) * 20.) *.5 + .5);
                }
    
                // 4 extra lines to cross adjacent cells
                m += Line(gv, p[1], p[3]);
                m += Line(gv, p[1], p[5]);
                m += Line(gv, p[5], p[7]);
                m += Line(gv, p[7], p[3]);
    
                return m;
            }


            // 'main'

            fixed4 frag (v2f i) : SV_Target
            {
                float2 uv = i.uv;               // already adjusted for resolution and aspect ratio

                float m = Layer(uv * 10.);					    // grid size

                float rotationTime = _Time.y * .02;
                float layerTime = _Time.y * .05;
                float colourTime = _Time.y * 5.;

                // rotation
                float s = sin(rotationTime);
                float c = cos(rotationTime);
                float2x2 rot = float2x2(c, -s, s, c);

                uv = mul(uv, rot);

                // 4 layers
                for (float i=0.; i<=1.; i+= 1.4/4.)	
                {
                    float z = frac(i + layerTime);
                    float size = lerp(5., .5, z);
                    float fade = S(0., .5, z) * S(1., .8, z);	// fade layers in/out (ie. not pop) (in over first 50% of their movement, out over last 20%)
                    m += Layer(uv * size + i * 20.) * fade;		// offset each layer
                }
    
                // cycle thru colours
                float3 base = sin(colourTime * float3(.345, .456, .567)) * .4 + .6;
 
                float3 col = m * base;	
    
                //if (gv.x >.48 || gv.y > .48) col = vec3(1,0,0);		// red grid line

                // Output to screen
                return float4(col, 1.0);
            }
 
            ENDCG
        }
    }
}
