﻿Shader "Unlit/GridUniverseShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }


#define S(a, b, t) smoothstep(a, b, t)

            float DistLine(float2 p, float2 a, float2 b)
            {
 	            float2 pa = p-a;
                float2 ba = b-a;
                float t = clamp(dot(pa, ba) / dot (ba, ba), 0., 1.);
                return length (pa - ba*t);
            }

            // single random number
            float N21(float2 p)
            {
 	            p = frac(p*float2(233.34, 851.73));
                p += dot(p, p+23.45);
                return frac(p.x * p.y);
            }

            // 2 random numbers
            float2 N22(float2 p)
            {
 	            float n = N21(p);
                return float2(n, N21(p+n));
            }

            float2 GetPos(float2 id, float2 offset)
            {
                float2 n = N22(id + offset) * _Time.y;		// noise
                return offset + sin(n) *.4;
            }

            float Line(float2 p, float2 a, float2 b)
            {
 	            float d = DistLine(p, a, b);
                float m = S(.02, .01, d);
                m*= S(1.2, .8, length(a-b));		// don't draw line if > 1.2, fade in between 1.2 and .8, .8 fully visible
                return m;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float2 uv = i.uv; // (fragCoord - .5 * iResolution.xy)/iResolution.y;		// adjusted for resolution and aspect ratio

                float m = 0.;
    
                uv *=5.;					// grid size
    
                float2 gv = frac(uv)-.5;		// 0,0 is centre of grid
                float2 id =  floor(uv);		// integer component
    
                float2 p[9];					// for adjacent cells (4 is middle one)
    
                int adj = 0;
                for (float y=-1.; y<=1.; y++)
                {
    	            for (float x=-1.; x<=1.; x++)
    	            {
        	            p[adj++] = GetPos(id, float2(x, y));
    	            }
                }
    
                float t = _Time.y*2.;
                for (int i=0; i<9; i++)
                {
                    m += Line(gv, p[4], p[i]);
        
                    float2 j = (p[i]-gv) * 20.;
                    float sparkle = 1./dot(j, j);
        
                    m += sparkle*(sin(t+p[i].x*10.) *.5+.5);
                }
    
                // 4 extra lines to cross adjacent cells
                m += Line(gv, p[1], p[3]);
                m += Line(gv, p[1], p[5]);
                m += Line(gv, p[5], p[7]);
                m += Line(gv, p[7], p[3]);
    
                float3 col = (m);	
    
                //if (gv.x >.48 || gv.y > .48) col = float3(1,0,0);		// red grid line

                // Output to screen
                return float4(col,1.0);
            }


            ENDCG
        }
    }
}
