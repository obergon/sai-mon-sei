﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Handle screen touches
// Mouse clicks emulate touches
public class TouchInput : MonoBehaviour
{
	private Vector2 startPosition;
    private Vector2 moveDirection;
    private Vector2 endPosition;

    private void Update()
    {
        //// track a single touch
        //if (Input.touchCount > 0)
        //{
        //    Touch touch = Input.GetTouch(0);

        //    // handle finger movements based on TouchPhase
        //    switch (touch.phase)
        //    {
        //        case TouchPhase.Began:
        //            startPosition = touch.position;
        //            SaimonseiEvents.OnTouchStart?.Invoke(startPosition);
        //            break;

        //        case TouchPhase.Moved:
        //            moveDirection = touch.position - startPosition;
        //            SaimonseiEvents.OnTouchMove?.Invoke(moveDirection, touch.position);
        //            break;

        //        case TouchPhase.Ended:
        //            endPosition = touch.position;
        //            SaimonseiEvents.OnTouchEnd?.Invoke(endPosition);
        //            break;

        //        case TouchPhase.Stationary:			// touching but hasn't moved
        //            break;

        //        case TouchPhase.Canceled:			// system cancelled touch
        //            break;
        //    }
        //}

        // emulate touch with mouse
        if (Input.GetMouseButtonDown(0))
        {
            startPosition = Input.mousePosition;
            SaimonseiEvents.OnTouchStart?.Invoke(startPosition);
            //SaimonseiEvents.OnDebug?.Invoke("GetMouseButtonDown!");
        }
        else if (Input.GetMouseButton(0))
        {
            Vector2 mousePosition = Input.mousePosition;
            moveDirection = mousePosition - startPosition;
            SaimonseiEvents.OnTouchMove?.Invoke(moveDirection, mousePosition);
        }
        else if (Input.GetMouseButtonUp(0))
        {
            endPosition = Input.mousePosition;
            SaimonseiEvents.OnTouchEnd?.Invoke(endPosition);
        }
    }
}
