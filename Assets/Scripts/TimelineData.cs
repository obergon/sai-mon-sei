﻿using System;
using System.IO;
using MLAPI.Serialization;
using MLAPI.Serialization.Pooled;
using System.Runtime.InteropServices;


// data used by FMODPlayer and streamed over the network for RPC calls
[Serializable]
[StructLayout(LayoutKind.Sequential)]
public class TimelineData : IBitWritable
{
    public int currentBeat;
    public int currentBar;
    public int currentTempo;
    public int timelinePosition;        // ms

    public int instrumentNumber;
    public int noteNumber;
    public int noteDuration;            // 1/4 notes


    public void Read(Stream stream)
    {
        using (PooledBitReader reader = PooledBitReader.Get(stream))
        {
            currentBeat = reader.ReadInt32Packed();
            currentBar = reader.ReadInt32Packed();
            currentTempo = reader.ReadInt32Packed();
            timelinePosition = reader.ReadInt32Packed();
            instrumentNumber = reader.ReadInt32Packed();
            noteNumber = reader.ReadInt32Packed();
            noteDuration = reader.ReadInt32Packed();
        }
    }

    public void Write(Stream stream)
    {
        using (PooledBitWriter writer = PooledBitWriter.Get(stream))
        {
            writer.WriteInt32Packed(currentBeat);
            writer.WriteInt32Packed(currentBar);
            writer.WriteInt32Packed(currentTempo);
            writer.WriteInt32Packed(timelinePosition);
            writer.WriteInt32Packed(instrumentNumber);
            writer.WriteInt32Packed(noteNumber);
            writer.WriteInt32Packed(noteDuration);
        }
    }
}

//private void Start()
//{

//	// Tells the MLAPI how to serialize and deserialize TimelineData in the future.
//	SerializationManager.RegisterSerializationHandlers<FMODPlayer.TimelineData>((Stream stream, FMODPlayer.TimelineData instance) =>
//	{
//		// This delegate gets ran when the MLAPI want's to serialize a TimelineData type to the stream.
//		using (PooledBitWriter writer = PooledBitWriter.Get(stream))
//		{
//			writer.WriteObjectPacked(instance);
//		}
//	}, (Stream stream) =>
//	{
//		// This delegate gets ran when the MLAPI want's to deserialize a TimelineData type from the stream.
//		using (PooledBitReader reader = PooledBitReader.Get(stream))
//		{
//			return new FMODPlayer.TimelineData(reader.ReadObjectPacked());
//		}
//	});
//}