﻿
using System;
using System.Collections.Generic;
using UnityEngine;


// player name / stats
[CreateAssetMenu(fileName = "PlayerProfile", menuName = "Player/Profile")]
public class PlayerProfile : ScriptableObject
{
	public string PlayerName;              // set on firebase db playerprofile save (and can be set in inspector)

	public DateTime FirstPlay;
	public DateTime LastPlay;

	public int PlayCount;

	// JSON for leaderboard
	public string ToJson(bool prettyPrint)
	{
		return JsonUtility.ToJson(this, prettyPrint);
	}
}
