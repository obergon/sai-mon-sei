﻿
//--------------------------------------------------------------------
//
// Timeline markers can be implicit - such as beats and bars. Or they 
// can be explicity placed by sound designers, in which case they have 
// a sound designer specified name attached to them.
//
// Timeline markers are useful for syncing game events to sound
// events.
//
//--------------------------------------------------------------------

using System;
using System.Runtime.InteropServices;
using UnityEngine;

using FMOD.Studio;
using FMODUnity;
using System.Collections.Generic;



// handles the playing of an FMOD track comprising a backing track 'event', with timeline markers
// that trigger up to 4 instrument 'events'
public class FMODPlayer : MonoBehaviour
{
	//public string SongTitle;

	[SerializeField]
	[EventRef]
	private string backingTrack;        // aka SongEventName

	[SerializeField]
	[EventRef]
	private string instrument1;

	[SerializeField]
	[EventRef]
	private string instrument2;

	[SerializeField]
	[EventRef]
	private string instrument3;

	[SerializeField]
	[EventRef]
	private string instrument4;

	private EventInstance backingTrackEvent;

	private EventInstance instrument1Event;
	private EventInstance instrument2Event;
	private EventInstance instrument3Event;
	private EventInstance instrument4Event;

	public string SongEventName { get { return backingTrack; } }
	public int NumInstruments { get; private set; }
	public List<string> SongInstruments { get; private set; }

	// Variables that are modified in the callback need to be part of a separate class.
	// This class needs to be 'blittable' otherwise it can't be pinned in memory.
	[StructLayout(LayoutKind.Sequential)]
	public class TimelineInfo
	{
		public EVENT_CALLBACK_TYPE type;
		public FMOD.StringWrapper markerName = new FMOD.StringWrapper();

		public TimelineData timelineData = new TimelineData();
		public IntPtr fmodPlayerRef;
	}

	// backing track
	private TimelineInfo backingInfo;
	private GCHandle backingTrackHandle;
	private GCHandle fmodPlayerHandle;
	private EVENT_CALLBACK backingCallback;
	private EventDescription descriptionCallback;

	private const string PitchParam = "ACCURACY";
	private const string NoteParam = "NOTE";

	private int trackLength;         // ms
	private int timelineMarkerCount = 0;        // incremented by FMOD timeline markers

	private TrackManager.PlayMode playMode;
	private DateTime trackPlayStartTime;

	private int currentInstrumentNumber = 0;        // 0 == all instruments
    private int restBeats = 4;                      // breathing space OnTrackEnd


	private void OnEnable()
	{
		SaimonseiEvents.OnTrackPlay += OnTrackPlay;
		SaimonseiEvents.OnTimelineMarker += OnTimelineMarker;
		SaimonseiEvents.OnTimelineBeat += OnTimelineBeat;

		SaimonseiEvents.OnPlayNote += OnPlayNote;
		SaimonseiEvents.OnStopNote += OnStopNote;

		InitBackingTrack();
		InitInstruments();
	}

	private void OnDisable()
	{
		SaimonseiEvents.OnTrackPlay -= OnTrackPlay;
		SaimonseiEvents.OnTimelineMarker -= OnTimelineMarker;
		SaimonseiEvents.OnTimelineBeat -= OnTimelineBeat;

		SaimonseiEvents.OnPlayNote -= OnPlayNote;
		SaimonseiEvents.OnStopNote -= OnStopNote;

		Cleanup();
	}


    protected void InitBackingTrack()
	{
		if (string.IsNullOrEmpty(backingTrack))    // eg. story mode
			return;

		backingInfo = new TimelineInfo();

		// Explicitly create the delegate object and assign it to a member so it doesn't get freed
		// by the garbage collected while it's being used
		backingCallback = new EVENT_CALLBACK(BackingEventCallback);
		backingTrackEvent = RuntimeManager.CreateInstance(backingTrack);

		backingTrackEvent.setParameterByName(PitchParam, 0.5f);

		// Pin the class that will store the data modified during the callback
		backingTrackHandle = GCHandle.Alloc(backingInfo, GCHandleType.Pinned);

		fmodPlayerHandle = GCHandle.Alloc(this);
		backingInfo.fmodPlayerRef = (IntPtr) fmodPlayerHandle;

		// Pass the object through the userdata of the instance
		backingTrackEvent.setUserData(GCHandle.ToIntPtr(backingTrackHandle));

		backingTrackEvent.setCallback(backingCallback, EVENT_CALLBACK_TYPE.TIMELINE_BEAT | EVENT_CALLBACK_TYPE.TIMELINE_MARKER);

		backingTrackEvent.getDescription(out descriptionCallback);
		descriptionCallback.getLength(out trackLength);
		//Debug.Log("InitBackingTrack: " + SongEventName + " trackLength: " + trackLength);

		SaimonseiEvents.OnBackingTrackCreated?.Invoke(SongEventName, trackLength);
	}


	protected void InitInstruments()
	{
		NumInstruments = 0;
		SongInstruments = new List<string>();

		if (!string.IsNullOrEmpty(instrument1))
		{
			NumInstruments++;
			instrument1Event = RuntimeManager.CreateInstance(instrument1);
			SongInstruments.Add(PlayerInstrumentName(1));
		}

		if (!string.IsNullOrEmpty(instrument2))
		{
			NumInstruments++;
			instrument2Event = RuntimeManager.CreateInstance(instrument2);
			SongInstruments.Add(PlayerInstrumentName(2));
		}

		if (!string.IsNullOrEmpty(instrument3))
		{
			NumInstruments++;
			instrument3Event = RuntimeManager.CreateInstance(instrument3);
			SongInstruments.Add(PlayerInstrumentName(3));
		}

		if (!string.IsNullOrEmpty(instrument4))
		{
			NumInstruments++;
			instrument4Event = RuntimeManager.CreateInstance(instrument4);
			SongInstruments.Add(PlayerInstrumentName(4));
		}

		SaimonseiEvents.OnInitInstruments?.Invoke(SongEventName, SongInstruments);
	}


	private void OnTimelineBeat(string songEventName, TimelineInfo timelineInfo, bool backingTrackStarted, TrackManager.PlayMode playMode)
	{
		if (SongEventName != songEventName)
			return;

		//Debug.Log("FMODPlayer.OnTimelineBeat: timelinePosition = " + timelineInfo.timelinePosition + ", trackLength = " + trackLength);

		if (timelineInfo.timelineData.timelinePosition >= trackLength)
		{
			SaimonseiEvents.OnTrackEnd?.Invoke(SongEventName, playMode, timelineInfo, restBeats);
		}
	}

	private void OnPlayNote(string songEventName, int instrumentNumber, int noteNumber)
	{
		//Debug.Log("FMODPlayer.OnPlayNote: " + songEventName + ", instrumentNumber = " + instrumentNumber + ", noteNumber = " + noteNumber);

		if (SongEventName != songEventName)
			return;

		PlayNote(instrumentNumber, noteNumber);
	}

	private void OnStopNote(string songEventName, int instrumentNumber, int noteNumber)
	{
		if (SongEventName != songEventName)
			return;

		StopNote(instrumentNumber, noteNumber);
	}

	// play through backingTrackEvent, firing instrument events according to timeline markers
	// and capture timeline data (temp, signature, note start and duration data)
	private void OnTrackPlay(string songEventName, TrackManager.PlayMode mode, int instrumentNumber)
	{
		if (songEventName != SongEventName)
			return;

		currentInstrumentNumber = instrumentNumber;

		//Debug.Log("FMODPlayer.OnTrackPlay: " + songEventName + " currentInstrumentNumber = " + currentInstrumentNumber + ", mode = " + mode);

		playMode = mode;

		backingTrackEvent.start();          // contains markers

		trackPlayStartTime = DateTime.Now;
		timelineMarkerCount = 0;
		SaimonseiEvents.OnBackingTrackStart?.Invoke(SongEventName, mode, trackPlayStartTime);
	}

	public EventInstance GetInstrumentEventByNumber(int instrumentNumber)
	{
		switch (instrumentNumber)
		{
			case 1:
				return instrument1Event;
			case 2:
				return instrument2Event;
			case 3:
				return instrument3Event;
			case 4:
				return instrument4Event;
		}

		return backingTrackEvent;       // TODO: how to return null struct?
	}

	public string GetInstrumentNameByNumber(int instrumentNumber)
	{
		switch (instrumentNumber)
		{
			case 1:
				return instrument1;
			case 2:
				return instrument2;
			case 3:
				return instrument3;
			case 4:
				return instrument4;
		}

		return "Unknown instrument: " + instrumentNumber;
	}

	protected void PlayNote(int instrumentNumber, int noteNumber)
	{
		var instrumentEvent = GetInstrumentEventByNumber(instrumentNumber);

		//Debug.Log("PlayNote: playMode = " + playMode + " instrumentNumber = " + instrumentNumber + " noteNumber = " + GetInstrumentNameByNumber(instrumentNumber) + " noteNumber = " + noteNumber);

		instrumentEvent.setParameterByName(NoteParam, noteNumber);
		instrumentEvent.start();
	}


	protected void StopNote(int instrumentNumber, int noteNumber)
	{
		var instrumentEvent = GetInstrumentEventByNumber(instrumentNumber);

		instrumentEvent.setParameterByName(NoteParam, 0);
	}

	private void OnTimelineMarker(string songEventName, TimelineInfo timelineInfo)
	{
		if (songEventName != SongEventName)
			return;

		if (currentInstrumentNumber > 0 && timelineInfo.timelineData.instrumentNumber != currentInstrumentNumber)
			return;

		//Debug.Log("OnTimelineMarker: noteNumber: " + timelineInfo.timelineData.noteNumber + " noteDuration: " + timelineInfo.timelineData.noteDuration + " timelinePosition: " + timelineInfo.timelineData.timelinePosition);
		if (playMode == TrackManager.PlayMode.PlayAll || playMode == TrackManager.PlayMode.PlayInstrument)
			PlayNote(timelineInfo.timelineData.instrumentNumber, timelineInfo.timelineData.noteNumber);
	}

	[AOT.MonoPInvokeCallback(typeof(EVENT_CALLBACK))]
	private static FMOD.RESULT BackingEventCallback(EVENT_CALLBACK_TYPE type, EventInstance instance, IntPtr parameterPtr)
	{
		// Retrieve the user data
		IntPtr timelineInfoPtr;
		FMOD.RESULT result = instance.getUserData(out timelineInfoPtr);

		if (result != FMOD.RESULT.OK)
		{
			Debug.LogError("Timeline Callback error: " + result);
		}
		else if (timelineInfoPtr != IntPtr.Zero)
		{
			// Get the object to store beat and marker details
			GCHandle timelineHandle = GCHandle.FromIntPtr(timelineInfoPtr);
			TimelineInfo timelineInfo = (TimelineInfo)timelineHandle.Target;
			TimelineData timelineData = timelineInfo.timelineData;

			GCHandle fmodPlayerLocalHandle = GCHandle.FromIntPtr(timelineInfo.fmodPlayerRef);
			FMODPlayer fmodPlayer = (FMODPlayer) fmodPlayerLocalHandle.Target;

			timelineInfo.type = type;

			switch (type)
			{
				case EVENT_CALLBACK_TYPE.TIMELINE_BEAT:
					{
						var parameter = (TIMELINE_BEAT_PROPERTIES)Marshal.PtrToStructure(parameterPtr, typeof(TIMELINE_BEAT_PROPERTIES));
						timelineData.currentBeat = parameter.beat;
						timelineData.currentBar = parameter.bar;
						timelineData.currentTempo = (int)parameter.tempo;
						timelineData.timelinePosition = parameter.position;

                        SaimonseiEvents.OnTimelineBeat?.Invoke(fmodPlayer.SongEventName, timelineInfo, fmodPlayer.timelineMarkerCount > 0, fmodPlayer.playMode);

						//Debug.Log("TIMELINE_BEAT: beat: " + timelineInfo.currentBeat + " bar: " + timelineInfo.currentBar + " tempo: " + timelineInfo.currentTempo + " time: " + timelineInfo.timelinePosition);
					}
					break;

				case EVENT_CALLBACK_TYPE.TIMELINE_MARKER:
					{
						var parameter = (TIMELINE_MARKER_PROPERTIES)Marshal.PtrToStructure(parameterPtr, typeof(TIMELINE_MARKER_PROPERTIES));
						timelineInfo.markerName = parameter.name;
						timelineData.timelinePosition = parameter.position;

						//Debug.Log("BackingEventCallback: " + timelineInfo);

						if (ParseMarkerLabel(timelineInfo.markerName, out timelineInfo.timelineData.instrumentNumber, out timelineInfo.timelineData.noteNumber, out timelineInfo.timelineData.noteDuration))
                            fmodPlayer.timelineMarkerCount++;

						//Debug.Log("TIMELINE_MARKER: noteNumber: " + timelineInfo.noteNumber + " noteDuration: " + timelineInfo.noteDuration + " time: " + timelineInfo.timelinePosition);
						SaimonseiEvents.OnTimelineMarker?.Invoke(fmodPlayer.SongEventName, timelineInfo);
					}
					break;
			}
		}

		return FMOD.RESULT.OK;
	}


	//private void OnDestroy()
	protected void Cleanup()
	{
		if (string.IsNullOrEmpty(backingTrack))     // eg. story mode
			return;

		backingTrackEvent.setUserData(IntPtr.Zero);
		backingTrackEvent.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
		backingTrackEvent.release();

		backingTrackHandle.Free();

		if (!string.IsNullOrEmpty(instrument1))
		{
			instrument1Event.setUserData(IntPtr.Zero);
			instrument1Event.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
			instrument1Event.release();
		}

		if (!string.IsNullOrEmpty(instrument2))
		{
			instrument2Event.setUserData(IntPtr.Zero);
			instrument2Event.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
			instrument2Event.release();
		}

		if (!string.IsNullOrEmpty(instrument3))
		{
			instrument3Event.setUserData(IntPtr.Zero);
			instrument3Event.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
			instrument3Event.release();
		}

		if (!string.IsNullOrEmpty(instrument4))
		{
			instrument4Event.setUserData(IntPtr.Zero);
			instrument4Event.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
			instrument4Event.release();
		}
	}


	private static bool ParseMarkerLabel(string marker, out int instrumentNumber, out int noteNumber, out int duration)
	{
		instrumentNumber = 0;     // all notes muted
		noteNumber = 0;     // all notes muted
		duration = 0;

		string[] split = marker.Split('_');

		if (split.Length < 3)
		{
			Debug.LogError("ParseMarkerLabel: invalid marker format " + split.Length + " (instr_note_duration)");
			return false;
		}

		if (!Int32.TryParse(split[0], out instrumentNumber))
		{
			Debug.LogError("ParseMarkerLabel: invalid instrumentNumber '" + split[0] + "'");
			return false;
		}

		if (!Int32.TryParse(split[1], out noteNumber))
		{
			Debug.LogError("ParseMarkerLabel: invalid noteNumber '" + split[1] + "'");
			return false;
		}

		if (!Int32.TryParse(split[2], out duration))
		{
			Debug.LogError("ParseMarkerLabel: invalid duration '" + split[2] + "'");
			return false;
		}

		return true;
	}

	public string PlayerInstrumentName(int instrumentNumber)
	{
		string instrumentName;          // formatted

		if (instrumentNumber == 0)
			instrumentName = "All Instruments";
		else
		{
			string fullInstrumentName = GetInstrumentNameByNumber(instrumentNumber);

			instrumentName = "";

			string[] instrSplit = fullInstrumentName.Split('_');

			if (instrSplit.Length == 2)
				instrumentName += instrSplit[1];
			else
				instrumentName = fullInstrumentName;       // full event name
		}

		return instrumentName;
	}
}
