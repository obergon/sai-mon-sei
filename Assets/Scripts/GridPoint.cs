﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


// class to contain a grid point object (eg. image) and its x,y grid coordinates
// instantiated to make grid in play area
public class GridPoint
{
	public GridDot gridDot { get; private set; }
	public Vector2 gridCoords { get; private set; }          // x/y coordinates
	//public Vector2 DotPosition { get; private set; }            // localPosition
	public Color DotColour { get; private set; }

	private float Transparency = 0.25f;             // coloured circle (dot)

	private float StartScale = 0.075f;
	private float MaxExpandScale = 3.75f;
	private float MaxExpandTime = 0.2f;           // seconds
	private float ResetTime = 0.15f;             // seconds

	public Color TransColour { get { return new Color(DotColour.r, DotColour.g, DotColour.b, Transparency); } }

    private Image GridDotImage {  get { return gridDot.GetComponent<Image>(); } }

	private LTDescr expandTween;
	private LTDescr colourTween;

	public GridPoint(GridDot dot, Vector2 coords, Color colour, Material material)
	{
		gridDot = dot;
		if (gridDot == null)
			Debug.Log("GridPoint: null gridDot!!");

		//DotPosition = position;
		gridCoords = coords;

		SetColour(colour);
		DotColour = colour;
	}

	public void SetColour(Color colour)
	{
		gridDot.SetColour(colour);
	}


	public void Expand(float durationMs, bool disableOnReset, bool sparklesOnly = false)
	{
		StopExpand();

		gridDot.gameObject.SetActive(true);
		gridDot.transform.localScale = disableOnReset ? Vector2.zero : new Vector2(StartScale, StartScale);
		GridDotImage.color = TransColour;
		GridDotImage.enabled = !sparklesOnly;

		if (! sparklesOnly)
		{
			var duration = durationMs / 1000f;                  // seconds
			var targetScale = (duration >= MaxExpandTime) ? MaxExpandScale : MaxExpandScale * (duration / MaxExpandTime);

			expandTween = LeanTween.scale(gridDot.gameObject, Vector2.one * targetScale, duration)
								.setEase(LeanTweenType.easeOutCirc) // .easeOutQuart)
								.setOnComplete(() => { ResetScale(disableOnReset); });
		}

		gridDot.ExpandSparkles(durationMs, disableOnReset);
	}

	// expand, waiting for ResetScale (eg. OnTouchUp)
	public void Expand()
	{
		StopExpand();

		gridDot.transform.localScale = new Vector2(StartScale, StartScale);
		GridDotImage.color = TransColour;

		expandTween = LeanTween.scale(gridDot.gameObject, Vector2.one * MaxExpandScale, MaxExpandTime)
								.setEase(LeanTweenType.easeOutCirc);

		gridDot.ExpandSparkles();
	}

	public void ResetScale(bool disableOnReset)
	{
		StopExpand();

		if (gridDot == null)
			Debug.Log("ResetScale: null gridDot!!");

		var resetScale = disableOnReset ? Vector2.zero : new Vector2(StartScale, StartScale);

		LeanTween.scale(gridDot.gameObject, resetScale, ResetTime)
					.setEase(LeanTweenType.easeInCirc);
					//.setOnComplete(() =>
					//    {
					//	    //if (disableOnReset)
     //     //                      gridDot.gameObject.SetActive(false);
					//    });

        gridDot.ResetSparkleScale(disableOnReset);
    }

	private void StopExpand()
	{
		if (expandTween != null)
		{
			LeanTween.cancel(expandTween.id);
			expandTween = null;
		}

		if (colourTween != null)
		{
			LeanTween.cancel(colourTween.id);
			colourTween = null;
		}

		gridDot.StopSparkleExpand();
	}


    // static function to get nearest gridPoint to touchPosition
	public static GridPoint GetNearestGridPoint(GridPoint[,] gridPoints, Vector2 touchPosition, float maxTouchDistance, out float distanceFromGridPoint)
	{
		GridPoint nearest = null;
		float nearestDistance = 0f;

		bool firstGridPoint = true;

		foreach (var gridPoint in gridPoints)
		{
			//var touchDistance = Vector2.Distance(touchPosition, gridPoint.gridDot.transform.position);

			Vector2 worldPosition = Camera.main.ScreenToWorldPoint(touchPosition);
			var touchDistance = Vector2.Distance(worldPosition, gridPoint.gridDot.transform.position);
			//Debug.Log("GetNearestGridPoint: touchPosition = " + touchPosition + " gridPoint position = " + (Vector2)gridPoint.gridDot.transform.position + " distance = " + touchDistance);

			if (touchDistance > maxTouchDistance)
				continue;

			// if touch position nearer to this gridPoint than current nearest, record it
			if (firstGridPoint || touchDistance < nearestDistance)
			{
				nearest = gridPoint;
				nearestDistance = touchDistance;

				firstGridPoint = false;
			}
		}

		distanceFromGridPoint = nearestDistance;
		return nearest;
	}
}
