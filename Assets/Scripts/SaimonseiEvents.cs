﻿using System;
using System.Collections.Generic;
using UnityEngine;


public static class SaimonseiEvents
{
	// Game mode

	public delegate void OnGameModeSelectedDelegate(ModeManager.GameMode mode);
	public static OnGameModeSelectedDelegate OnGameModeSelected;

	// Touch/mouse input

	public delegate void OnTouchStartDelegate(Vector2 position);
	public static OnTouchStartDelegate OnTouchStart;

	public delegate void OnTouchMoveDelegate(Vector2 direction, Vector2 position);
	public static OnTouchMoveDelegate OnTouchMove;

	public delegate void OnTouchEndDelegate(Vector2 position);
	public static OnTouchEndDelegate OnTouchEnd;

	// Track play / record / playback cycle

	public delegate void OnSongClickedDelegate(string songTitle, string songEventName);
	public static OnSongClickedDelegate OnSongClicked;

	public delegate void OnSongSelectedDelegate(string songTitle, string songEventName, PlayArea.PlayMode playMode, int gridWidth, int gridHeight, float tolerance);
	public static OnSongSelectedDelegate OnSongSelected;

	public delegate void OnBackingTrackCreatedDelegate(string songEventName, int length);
	public static OnBackingTrackCreatedDelegate OnBackingTrackCreated;

	public delegate void OnInitInstrumentsDelegate(string songEventName, List<string> songInstruments);
	public static OnInitInstrumentsDelegate OnInitInstruments;

	public delegate void OnBackingTrackStartDelegate(string songEventName, TrackManager.PlayMode mode, DateTime trackPlayStartTime);
	public static OnBackingTrackStartDelegate OnBackingTrackStart;

	public delegate void OnTimelineBeatDelegate(string songEventName, FMODPlayer.TimelineInfo timelineInfo, bool backingTrackStarted, TrackManager.PlayMode playMode);
	public static OnTimelineBeatDelegate OnTimelineBeat;

	public delegate void OnPlayDelegate();
	public static OnPlayDelegate OnPlay;            // user clicks play button

	public delegate void OnPlayEnabledDelegate(bool enabled);
	public static OnPlayEnabledDelegate OnPlayEnabled;            // play button / tap to play

	public delegate void OnPlayCycleStartDelegate(ModeManager.GameMode gameMode);
	public static OnPlayCycleStartDelegate OnPlayCycleStart;            // user clicks play button - leaderboard and story implementations

	public delegate void OnPlayCycleDoneDelegate(string songEventName, List<InstrumentScore> instrumentScores);
	public static OnPlayCycleDoneDelegate OnPlayCycleDone;

	public delegate void OnPlayNoteDelegate(string songEventName, int instrumentNumber, int noteNumber);
	public static OnPlayNoteDelegate OnPlayNote;

	public delegate void OnStopNoteDelegate(string songEventName, int instrumentNumber, int noteNumber);
	public static OnStopNoteDelegate OnStopNote;

	public delegate void OnDudNoteDelegate();
	public static OnDudNoteDelegate OnDudNote;

	public delegate void OnTrackPlayDelegate(string songEventName, TrackManager.PlayMode mode, int instrumentNumber);
	public static OnTrackPlayDelegate OnTrackPlay;

	public delegate void OnInstrumentChangedDelegate(string songEventName, int instrumentNumber, string playerInstrumentName, string shortInstrumentName, int numGridPoints);
	public static OnInstrumentChangedDelegate OnInstrumentChanged;

	public delegate void OnTrackEndDelegate(string songEventName, TrackManager.PlayMode mode, FMODPlayer.TimelineInfo timelineInfo, int restBeats);			// track reached end of timeline
	public static OnTrackEndDelegate OnTrackEnd;

	public delegate void OnTrackStopDelegate(string songEventName);					// stopped by user
	public static OnTrackStopDelegate OnTrackStop;

	// Scoring

	public delegate void OnScoreUpdatedDelegate(InstrumentScore score, ScoreKeeper.ScoreRating rating);
	public static OnScoreUpdatedDelegate OnScoreUpdated;

	public delegate void OnFinalScoreDelegate(bool failed);
	public static OnFinalScoreDelegate OnFinalScore;                            // pass or fail

	public delegate void OnSongScoreLookupDelegate(string songTitle, SongScore songScore);  
	public static OnSongScoreLookupDelegate OnSongScoreLookup;

	public delegate void OnNewPersonalBestDelegate(SongScore songScore);
	public static OnNewPersonalBestDelegate OnNewPersonalBest;

	public delegate void OnNewInstrumentPBDelegate(SongScore songScore, InstrumentScore instrumentScore);
	public static OnNewInstrumentPBDelegate OnNewInstrumentPB;

	public delegate void OnFinalGroupScoreDelegate(SongScore songScore);           // multiplayer
	public static OnFinalGroupScoreDelegate OnFinalGroupScore;

    public delegate void OnSaveGroupSongScoreDelegate(string songName, List<InstrumentScore> instrumentScores); // multiplayer
	public static OnSaveGroupSongScoreDelegate OnSaveGroupSongScore;

	// Player

	public delegate void OnSavePlayerNameDelegate(string playerName, PlayerProfile profile);
	public static OnSavePlayerNameDelegate OnSavePlayerName;

	public delegate void OnPlayerNameSaveResultDelegate(string playerName, bool success);
	public static OnPlayerNameSaveResultDelegate OnPlayerNameSaveResult;

	public delegate void OnLookupPlayerNameDelegate(string playerName);
	public static OnLookupPlayerNameDelegate OnLookupPlayerName;

	public delegate void OnSavePlayerStatsDelegate(PlayerProfile profile);
	public static OnSavePlayerStatsDelegate OnSavePlayerStats;

	public delegate void OnPlayerStatsSaveResultDelegate(string playerName, bool success);
	public static OnPlayerStatsSaveResultDelegate OnPlayerStatsSaveResult;

	public delegate void OnLookupPlayerResultDelegate(string playerName, bool found);
	public static OnLookupPlayerResultDelegate OnLookupPlayerResult;

	public delegate void OnPlayerNameChangedDelegate(NetworkPlayer player, string playerName);
	public static OnPlayerNameChangedDelegate OnPlayerNameChanged;

	public delegate void OnPlayerNumberChangedDelegate(NetworkPlayer player, int playerNumber);
	public static OnPlayerNumberChangedDelegate OnPlayerNumberChanged;

	// Leaderboard

	public delegate void OnLeaderboardUpdatedDelegate(SongScore songScore, InstrumentScore instrumentScore, bool success);
	public static OnLeaderboardUpdatedDelegate OnLeaderboardUpdated;

	public delegate void OnToggleLeaderboardDelegate(LeaderboardUI.LeaderboardType view);
	public static OnToggleLeaderboardDelegate OnToggleLeaderboard;

	public delegate void OnShowLeaderboardDelegate(LeaderboardUI.LeaderboardType view, bool show);
	public static OnShowLeaderboardDelegate OnShowLeaderboard;

	public delegate void OnLookupSongScoresDelegate(string songName);
	public static OnLookupSongScoresDelegate OnLookupSongScores;

	public delegate void OnSongScoresRetrievedDelegate(string songName, List<LeaderboardInstrPlayerScores> instrumentScores);
	public static OnSongScoresRetrievedDelegate OnSongScoresRetrieved;

	public delegate void OnLookupPlayerScoresDelegate(string playerName);
	public static OnLookupPlayerScoresDelegate OnLookupPlayerScores;

	public delegate void OnPlayerScoresRetrievedDelegate(string playerName, List<LeaderboardSongInstrScores> songScores);
	public static OnPlayerScoresRetrievedDelegate OnPlayerScoresRetrieved;

	public delegate void OnLookupGroupScoresDelegate(string hostName, string clientNames);
	public static OnLookupGroupScoresDelegate OnLookupGroupScores;

	public delegate void OnGroupScoresRetrievedDelegate(string hostName, string clientNames, List<LeaderboardHostSongScores> songScores);
	public static OnGroupScoresRetrievedDelegate OnGroupScoresRetrieved;

	public delegate void OnLookupGroupSongScoresDelegate(string hostName, string clientNames, string songName);
	public static OnLookupGroupSongScoresDelegate OnLookupGroupSongScores;

	public delegate void OnGroupSongScoresRetrievedDelegate(string hostName, string clientNames, string songName, List<LeaderboardInstrPlayerScores> instrScores);
	public static OnGroupSongScoresRetrievedDelegate OnGroupSongScoresRetrieved;

	public delegate void OnSaveGroupSongScoreResultDelegate(string songName, List<InstrumentScore> instrumentScores, bool success);
	public static OnSaveGroupSongScoreResultDelegate OnSaveGroupSongScoreResult;

	// UI

	public delegate void OnStartNewGameDelegate(string songEventName, PlayArea.PlayMode mode);
	public static OnStartNewGameDelegate OnStartNewGame;                    // user clicks song button

	public delegate void OnInitGridDelegate(GridPoint[,] grid);
	public static OnInitGridDelegate OnInitGrid;

	public delegate void OnStatusUpdatedDelegate(string status, string prompt, TrackManager.PlayMode mode);
	public static OnStatusUpdatedDelegate OnStatusUpdated;

	public delegate void OnNextPlayerCountdownDelegate(int countFrom, string nextPlayer, Action onZero);
	public static OnNextPlayerCountdownDelegate OnNextPlayerCountdown;

	public delegate void OnStartMenuDelegate();
	public static OnStartMenuDelegate OnStartMenu;

	public delegate void OnMainMenuDelegate();
	public static OnMainMenuDelegate OnMainMenu;            // core leaderboard game

	//public delegate void OnActivateMistDelegate(bool on);
	//public static OnActivateMistDelegate OnActivateMist;

	public delegate void OnCreateRippleDelegate(Vector2 worldPosition, Ripple.RippleType touchType);
	public static OnCreateRippleDelegate OnCreateRipple;

	public delegate void OnTouchScoreDelegate(Vector2 worldPosition, TrackNote note, InstrumentScore instrumentScore, ScoreKeeper.ScoreRating rating);
	public static OnTouchScoreDelegate OnTouchScore;

	public delegate void OnDebugDelegate(string message);
	public static OnDebugDelegate OnDebug;

	// FMOD timeline markers

	public delegate void OnTimelineMarkerDelegate(string songEventName, FMODPlayer.TimelineInfo timelineInfo);
	public static OnTimelineMarkerDelegate OnTimelineMarker;

	// Turn management

	public delegate void OnStartSongServerDelegate(string songTitle, string songEventName);
	public static OnStartSongServerDelegate OnStartSongServer;                      // for NetworkPlayer

	public delegate void OnStartCycleServerDelegate();
	public static OnStartCycleServerDelegate OnStartCycleServer;                    // for NetworkPlayer

    public delegate void OnStartTouchClientDelegate(Vector2 touchPosition);
    public static OnStartTouchClientDelegate OnStartTouchClient;                    // for NetworkPlayer

    public delegate void OnEndTouchClientDelegate(Vector2 touchPosition);
    public static OnEndTouchClientDelegate OnEndTouchClient;                        // for NetworkPlayer

    public delegate void OnPlayNoteLocalDelegate(Vector2 randomCoords, TimelineData timelineData);
	public static OnPlayNoteLocalDelegate OnPlayTimelineNoteLocal;                  // for NetworkPlayer

	public delegate void OnPlayNoteServerDelegate(Vector2 randomCoords, TimelineData timelineData);
	public static OnPlayNoteServerDelegate OnPlayTimelineNoteServer;                // for NetworkPlayer

    // Networking - Discovery

	public delegate void OnHostGameDelegate();
	public static OnHostGameDelegate OnHostGame;

	public delegate void OnJoinGameDelegate(string ipAddress);
	public static OnJoinGameDelegate OnJoinGame;

	public delegate void OnLeaveGameDelegate(ulong clientID);
	public static OnLeaveGameDelegate OnLeaveGame;

	public delegate void OnHostEndedGameDelegate();
	public static OnHostEndedGameDelegate OnHostEndedGame;

	public delegate void OnNetworkPlayerEnteredGameDelegate(NetworkPlayer player);
	public static OnNetworkPlayerEnteredGameDelegate OnNetworkPlayerEnteredGame;

	public delegate void OnNetworkPlayerLeftGameDelegate(ulong clientID);
	public static OnNetworkPlayerLeftGameDelegate OnNetworkPlayerLeftGame;

	public delegate void OnHostIPDelegate(string hostIP);
	public static OnHostIPDelegate OnHostIP;

	public delegate void OnDiscoveryListenCountdownDelegate(int elapsedSeconds, int timeoutSeconds);
	public static OnDiscoveryListenCountdownDelegate OnDiscoveryListenCountdown;

	public delegate void OnDiscoveryListenTimeoutDelegate();
	public static OnDiscoveryListenTimeoutDelegate OnDiscoveryListenTimeout;

	// Networking - Play

	public delegate void OnSongSelectedNetworkDelegate(string songTitle, string songEventName);
	public static OnSongSelectedNetworkDelegate OnSongSelectedNetwork;            // song selected

	public delegate void OnStartPlayCycleNetworkDelegate();
	public static OnStartPlayCycleNetworkDelegate OnStartPlayCycleNetwork;        // 'play' button clicked

	public delegate void OnCurrentPlayerChangedDelegate(int newCurrentPlayerNum);
	public static OnCurrentPlayerChangedDelegate OnCurrentPlayerChanged;

	public delegate void OnTouchStartNetworkDelegate(Vector2 position);
	public static OnTouchStartNetworkDelegate OnTouchStartNetwork;

	public delegate void OnTouchEndNetworkDelegate(Vector2 position);
	public static OnTouchEndNetworkDelegate OnTouchEndNetwork;

	public delegate void OnAnimateNoteNetworkDelegate(Vector2 randomCoords, TimelineData timelineData);
	public static OnAnimateNoteNetworkDelegate OnAnimateNoteNetwork;

	// Story Mode

	public delegate void OnUpdateLandHealthDelegate(float health, bool increment);
	public static OnUpdateLandHealthDelegate OnUpdateLandHealth;

	public delegate void OnLandHealthChangedDelegate(float newHealth, float landMaxHealth);
	public static OnLandHealthChangedDelegate OnLandHealthChanged;

	public delegate void OnRestartGameDelegate();
	public static OnRestartGameDelegate OnRestartGame; 

	public delegate void OnEnterRealmDelegate(StoryRealm realm, int realmIndex, int realmCount);
	public static OnEnterRealmDelegate OnEnterRealm;

	public delegate void OnSkipRealmDelegate();
	public static OnSkipRealmDelegate OnSkipRealm;

	public delegate void OnSkipRealmLayerDelegate();
	public static OnSkipRealmLayerDelegate OnSkipRealmLayer;
	
	public delegate void OnEnterLayersDelegate();
	public static OnEnterLayersDelegate OnEnterLayers;

	public delegate void OnHideGoddessDelegate();
	public static OnHideGoddessDelegate OnHideGoddess;

    public delegate void OnPlayCurrentRealmLayerDelegate();
	public static OnPlayCurrentRealmLayerDelegate OnPlayCurrentRealmLayer;

	public delegate void OnInstrumentAcquiredDelegate(string text);
	public static OnInstrumentAcquiredDelegate OnInstrumentAcquired;

	public delegate void OnInstrumentTimeOutDelegate();
	public static OnInstrumentTimeOutDelegate OnInstrumentTimeOut;

	public delegate void OnRealmStateChangedDelegate(Goddess.RealmState newState, bool skipping);
	public static OnRealmStateChangedDelegate OnRealmStateChanged;

	public delegate void OnRealmRevealStateChangedDelegate(RealmReveal.RevealState newState, StoryRealm realm, bool skippingTouchstones);
	public static OnRealmRevealStateChangedDelegate OnRealmRevealStateChanged;

	public delegate void OnRealmImageRevealedDelegate(int layerIndex, bool skipping);
	public static OnRealmImageRevealedDelegate OnRealmImageRevealed;

	public delegate void OnWaitForLayerChoiceDelegate(List<LayerChoice> layerChoices);
	public static OnWaitForLayerChoiceDelegate OnWaitForLayerChoice;

	public delegate void OnRealmLayerChosenDelegate(LayerChoice chosenLayer);
	public static OnRealmLayerChosenDelegate OnRealmLayerChosen;

	public delegate void OnRealmRevealCaptureDelegate(int countdownTime);
	public static OnRealmRevealCaptureDelegate OnRealmRevealCapture;

	public delegate void OnRealmRevealCountdownDelegate(int counter, int finalCountdown);
	public static OnRealmRevealCountdownDelegate OnRealmRevealCountdown;

	public delegate void OnEnterRealmLayerDelegate(StoryRealm realm, RealmLayer layer, Vector3[] gridCorners);
	public static OnEnterRealmLayerDelegate OnEnterRealmLayer;

	public delegate void OnAllRealmLayersRevealedDelegate();
	public static OnAllRealmLayersRevealedDelegate OnAllRealmLayersRevealed;

	public delegate void OnLearnRealmSongFailedDelegate();
	public static OnLearnRealmSongFailedDelegate OnLearnRealmSongFailed;        // exceeded pass score

	public delegate void OnStoryPlayQuitDelegate();
	public static OnStoryPlayQuitDelegate OnStoryPlayQuit;

	public delegate void OnHealthCrystalTouchedDelegate(HealthCrystal crystal);
	public static OnHealthCrystalTouchedDelegate OnHealthCrystalTouched;

	public delegate void OnHealthCrystalsPocketedDelegate(int numCrystalsPocketed, int maxCrystals);
	public static OnHealthCrystalsPocketedDelegate OnHealthCrystalsPocketed;

	public delegate void OnCrystalsInPocketUpdatedDelegate(int totalCrystalsInPocket, int increment, bool reset, bool spent);
	public static OnCrystalsInPocketUpdatedDelegate OnCrystalsInPocketUpdated;

	// Testing

	public delegate void OnCompleteAllRealmsDelegate();
	public static OnCompleteAllRealmsDelegate OnCompleteAllRealms;

	public delegate void OnGameResetDelegate();
	public static OnGameResetDelegate OnGameReset;

	// Memes

	public delegate void OnToggleMemesDelegate(bool isOn);
	public static OnToggleMemesDelegate OnToggleMemes;

	public delegate void OnStartMemesDelegate();
	public static OnStartMemesDelegate OnStartMemes;

	public delegate void OnStopMemesDelegate(bool onTouch);
	public static OnStopMemesDelegate OnStopMemes;

    public delegate void OnMemeShowingDelegate(MemeUI meme);
	public static OnMemeShowingDelegate OnMemeShowing;

	public delegate void OnMemeHiddenDelegate();
	public static OnMemeHiddenDelegate OnMemeHidden;

	public delegate void OnMemeCountUpdatedDelegate(int memeCount);     // memes showing
	public static OnMemeCountUpdatedDelegate OnMemeCountUpdated;

	public delegate void OnMemeLikedDelegate(MemeUI meme);
	public static OnMemeLikedDelegate OnMemeLiked;

	public delegate void OnMemeDislikedDelegate(MemeUI meme);
	public static OnMemeDislikedDelegate OnMemeDisliked;

	public delegate void OnMemeSharedDelegate(MemeUI meme);
	public static OnMemeSharedDelegate OnMemeShared;

	public delegate void OnMemeCancelledDelegate(MemeUI meme);
	public static OnMemeCancelledDelegate OnMemeCancelled;

	public delegate void OnMemeMissedDelegate(MemeUI meme);
	public static OnMemeMissedDelegate OnMemeMissed;

	// Player Ageing

	public delegate void OnStartAgingDelegate();
	public static OnStartAgingDelegate OnStartAgeing;

	public delegate void OnStopAgeingDelegate();
	public static OnStopAgeingDelegate OnStopAgeing;

	public delegate void OAgeingStartedDelegate();
	public static OAgeingStartedDelegate OnAgeingStarted;       // not currently used

	public delegate void OnAgeingStoppedDelegate();
	public static OnAgeingStoppedDelegate OnAgeingStopped;      // not currently used

	public delegate void OnIncrementAgeDelegate(float increment, float updateTime);
	public static OnIncrementAgeDelegate OnIncrementAge;
}

