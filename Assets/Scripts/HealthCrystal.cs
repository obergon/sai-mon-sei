﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthCrystal : MonoBehaviour
{
    public List<ParticleSystem> Sparkles;
	public Button CrystalButton;

	private float StartScale = 1f;
	private float MaxExpandScale = 5f;
	private float MaxExpandTime = 0.25f;           // seconds
	private float ResetTime = 0.2f;             // seconds

	private LTDescr sparkleExpandTween;

	private Vector3 currentSparkleScale;

	public int CrystalNumber { get; private set; }      // for 'pocketing'


    private void OnEnable()
    {
		CrystalButton.onClick.AddListener(OnCrystalButtonClicked);
	}

    private void OnDisable()
	{
		CrystalButton.onClick.RemoveListener(OnCrystalButtonClicked);
	}


	public void SetNumber(int number)
	{
		CrystalNumber = number;
	}

	public void SetColour(Color colour)
	{
		GetComponent<Image>().color = colour;

		foreach (var sparkle in Sparkles)
		{
			var main = sparkle.main;
			main.startColor = colour;
		}
	}

	private void OnCrystalButtonClicked()
	{
		SaimonseiEvents.OnHealthCrystalTouched?.Invoke(this);
	}


	private void SetSparkleScale(Vector3 scale)
	{
		currentSparkleScale = scale;

		foreach (var sparkle in Sparkles)
		{
			sparkle.transform.localScale = scale;
		}
	}

	public void ExpandSparkles(float durationMs, bool disableOnReset)
	{
		StopSparkleExpand();
		transform.localScale = new Vector2(StartScale, StartScale);

		var duration = durationMs / 1000f;                  // seconds
		var targetScale = (duration >= MaxExpandTime) ? MaxExpandScale : MaxExpandScale * (duration / MaxExpandTime);

		sparkleExpandTween = LeanTween.value(gameObject, Vector3.one * StartScale, Vector3.one * targetScale, duration)
										.setEase(LeanTweenType.easeOutQuart)
							            .setOnUpdate((Vector3 x) => { SetSparkleScale(x); })
							            .setOnComplete(() => { ResetSparkleScale(disableOnReset); });
	}

	//// expand, waiting for ResetScale (eg. OnTouchUp)
	//public void ExpandSparkles()
	//{
	//	StopSparkleExpand();
	//	transform.localScale = new Vector2(StartScale, StartScale);

	//	sparkleExpandTween = LeanTween.value(gameObject, Vector3.one * StartScale, Vector3.one * MaxExpandScale, MaxExpandTime)
	//							.setEase(LeanTweenType.easeOutQuart)
	//							.setOnUpdate((Vector3 x) => { SetSparkleScale(x); });
	//}

	public void ResetSparkleScale(bool disableOnReset)
	{
		StopSparkleExpand();

		LeanTween.value(gameObject, currentSparkleScale, Vector3.one * StartScale, ResetTime)
								.setEase(LeanTweenType.easeInQuart)
								.setOnUpdate((Vector3 x) => { SetSparkleScale(x); })
								.setOnComplete(() =>
                                        {
                                            if (disableOnReset) gameObject.SetActive(false);
                                        });
    }

	public void StopSparkleExpand()
	{
		if (sparkleExpandTween != null)
		{
			LeanTween.cancel(sparkleExpandTween.id);
			sparkleExpandTween = null;
		}
	}

	public void PulseSparkles(float pulseScale, float pulseTime, int pulseCount = 1) //, Action onComplete = null)
    {
        foreach (var sparkle in Sparkles)
        {
			LeanTween.scale(sparkle.gameObject, Vector3.one * pulseScale, pulseTime)
						.setEase(LeanTweenType.easeOutBack)
						.setLoopPingPong(pulseCount);
						//.setOnComplete(() => { onComplete?.Invoke(); });
		}
	}
}
