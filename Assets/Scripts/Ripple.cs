﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ripple : MonoBehaviour
{
    public Image RippleImage;

	private const float ripplePulseScale = 5f;
	private const float rippleScaleTime = 0.5f;

	private const float rippleResetTime = 1f;

	private const float rippleFadeTime = 0.45f;
	//private const float fadeDelayTime = 0.05f;


	public enum RippleType
	{
		Valid,
		Invalid,
		Score,
        RealmReveal
	}


	public void Pulse(Color colour, float speedFactor = 1f, float scaleFactor = 1f)
	{
		Vector3 rippleScale = Vector3.one * scaleFactor;
		//RippleImage.transform.localScale = rippleScale;
		RippleImage.transform.localScale = Vector3.zero;

		RippleImage.color = colour;

		LeanTween.value(RippleImage.gameObject, colour, Color.clear, rippleFadeTime * speedFactor)
							.setEase(LeanTweenType.easeInCubic)
							//.setDelay(fadeDelayTime)
							.setOnUpdate((Color col) => RippleImage.color = col);

		LeanTween.scale(RippleImage.gameObject, rippleScale * ripplePulseScale, rippleScaleTime * speedFactor)
							.setEase(LeanTweenType.easeOutQuad)
							.setOnComplete(() =>
							{
								Reset();
							});
	}


	public void Reset()
	{
		LeanTween.scale(RippleImage.gameObject, Vector2.zero, rippleResetTime)
					.setEase(LeanTweenType.easeOutSine)
					.setOnComplete(() =>
					{
						gameObject.SetActive(false);        // effectively pooled again
						transform.position = Vector3.zero;
					});
	}
}
