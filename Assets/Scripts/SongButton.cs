﻿using System;
using System.Collections;
using System.Collections.Generic;
using FMODUnity;
using UnityEngine;
using UnityEngine.UI;


public class SongButton : MonoBehaviour
{
	[SerializeField]
	[EventRef]
	private string SongEventName;

	public string SongTitle;
	public Button PlaySongButton;


	private void OnEnable()
	{
		PlaySongButton.onClick.AddListener(OnPlaySong);
	}

	private void OnDisable()
	{
		PlaySongButton.onClick.RemoveListener(OnPlaySong);
	}

	private void OnPlaySong()
	{
		SaimonseiEvents.OnSongClicked?.Invoke(SongTitle, SongEventName);
	}
}
