﻿using System;
using System.Collections;
using System.Collections.Generic;
using FMODUnity;
using UnityEngine;
using UnityEngine.UI;


public class LeaderboardButton : MonoBehaviour
{
	public Text Score;
	public Button LBButton;


	private void OnEnable()
	{
		LBButton.onClick.AddListener(OnLBScoreClick);
	}


	private void OnDisable()
	{
		LBButton.onClick.RemoveListener(OnLBScoreClick);
	}

	private void OnLBScoreClick()
	{
		//SaimonseiEvents.OnLBScoreClicked?.Invoke();
	}
}
