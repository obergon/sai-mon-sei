﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using System.Threading.Tasks;
using System.Linq;

public class LeaderboardManager : MonoBehaviour
{
	public PlayerProfile PlayerProfile;

    private DatabaseReference databaseRoot;

	public static string GroupNameSeparator = "|";      // used to separate group player names

	// remove all firebase illegal chars
	public static string CleanPlayerName(string playerName)
	{
		string cleanPlayerName = playerName.Replace(".", "");
		cleanPlayerName = cleanPlayerName.Replace("$", "");
		cleanPlayerName = cleanPlayerName.Replace("#", "");
		cleanPlayerName = cleanPlayerName.Replace("[", "");
		cleanPlayerName = cleanPlayerName.Replace("]", "");
		cleanPlayerName = cleanPlayerName.Replace("/", "");

		// char reserved for multiplayer group key separator
		cleanPlayerName = cleanPlayerName.Replace(GroupNameSeparator, "");          

		return cleanPlayerName;
	}

	private void OnEnable()
    {
        SaimonseiEvents.OnLookupPlayerName += OnLookupPlayerName;
        SaimonseiEvents.OnSavePlayerName += OnSavePlayerName;
		SaimonseiEvents.OnSavePlayerStats += OnSavePlayerStats;
		SaimonseiEvents.OnLookupSongScores += OnLookupSongScores;
		SaimonseiEvents.OnLookupPlayerScores += OnLookupPlayerScores;
		SaimonseiEvents.OnLookupGroupScores += OnLookupGroupScores;
		SaimonseiEvents.OnLookupGroupSongScores += OnLookupGroupSongScores;
		SaimonseiEvents.OnNewInstrumentPB += OnNewInstrumentPB;
		SaimonseiEvents.OnSaveGroupSongScore += OnSaveGroupSongScore;         
	}

    private void OnDisable()
    {
		SaimonseiEvents.OnLookupPlayerName -= OnLookupPlayerName;
		SaimonseiEvents.OnSavePlayerName -= OnSavePlayerName;
		SaimonseiEvents.OnSavePlayerStats -= OnSavePlayerStats;
		SaimonseiEvents.OnLookupSongScores -= OnLookupSongScores;
		SaimonseiEvents.OnLookupPlayerScores -= OnLookupPlayerScores;
		SaimonseiEvents.OnLookupGroupScores -= OnLookupGroupScores;
		SaimonseiEvents.OnLookupGroupSongScores -= OnLookupGroupSongScores;
		SaimonseiEvents.OnNewInstrumentPB -= OnNewInstrumentPB;
		SaimonseiEvents.OnSaveGroupSongScore -= OnSaveGroupSongScore;
	}

    private void Start()
    {
        // set the DB url before calling into the realtime database
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://sai-mon-sei.firebaseio.com/");

        // get the root reference location of the database
        databaseRoot = FirebaseDatabase.DefaultInstance.RootReference;
    }


	private void OnLookupPlayerScores(string playerName)
	{
		RetrievePlayerScores(playerName);
	}

	private void OnLookupSongScores(string songName)
	{
		RetrieveSongScores(songName);
	}

	private void OnLookupGroupScores(string hostName, string clientNames)
	{
		RetrieveGroupScores(hostName, clientNames);
	}

	private void OnLookupGroupSongScores(string hostName, string clientNames, string songName)
	{
		RetrieveGroupSongScores(hostName, clientNames, songName);
	}


	// post instrument score to leaderboard
	private void OnNewInstrumentPB(SongScore songScore, InstrumentScore instrumentScore)
    {
        if (!string.IsNullOrEmpty(PlayerProfile.PlayerName))
        {
            // atomic updates of new instrument personal best in song/player/group scores
            Dictionary<string, object> childUpdates = new Dictionary<string, object>();

			string songPlayerScore = string.Format("{0}/{1}/{2}", songScore.SongName, instrumentScore.InstrumentName, PlayerProfile.PlayerName);
            childUpdates["/songs/" + songPlayerScore] = instrumentScore.PersonalBest;

            string playerSongScore = string.Format("{0}/{1}/{2}", PlayerProfile.PlayerName, songScore.SongName, instrumentScore.InstrumentName);
            childUpdates["/players/" + playerSongScore] = instrumentScore.PersonalBest;

			databaseRoot.UpdateChildrenAsync(childUpdates).ContinueWith(task =>
			{
				if (task.IsCompleted)
				{
					//Debug.Log("OnNewInstrumentPB: success!");
					SaimonseiEvents.OnLeaderboardUpdated?.Invoke(songScore, instrumentScore, true);
				}
				else
				{
					Debug.Log("OnNewInstrumentPB: error = " + task.Exception.Message);
					SaimonseiEvents.OnLeaderboardUpdated?.Invoke(songScore, instrumentScore, false);
				}
			}, TaskScheduler.FromCurrentSynchronizationContext());     // task continuation on UI thread
		}
	}


	// each player updates db with host/group song scores for the instruments they played
	private void OnSaveGroupSongScore(string songName, List<InstrumentScore> instrumentScores)
	{
		if (!NetworkManager.IsMultiPlayerGame)
			return;

		if (string.IsNullOrEmpty(PlayerProfile.PlayerName))
			return;

		Dictionary<string, object> childUpdates = new Dictionary<string, object>();

        foreach (var instrScore in instrumentScores)
        {
			string hostSongScore = string.Format("{0}/{1}/{2}/{3}/{4}/{5}", NetworkManager.GameHost.PlayerName.Value, "hosted", NetworkManager.GroupPlayersKey, songName, instrScore.InstrumentName, instrScore.PlayerName);
			childUpdates["/players/" + hostSongScore] = instrScore.TotalScore;

			databaseRoot.UpdateChildrenAsync(childUpdates).ContinueWith(task =>
			{
				if (task.IsCompleted)
				{
                    //Debug.Log("OnSaveGroupSongScore: " + hostSongScore + " success!");
					SaimonseiEvents.OnSaveGroupSongScoreResult?.Invoke(songName, instrumentScores, true);
				}
				else
				{
					Debug.Log("OnSaveGroupSongScore: error = " + task.Exception.Message);
					SaimonseiEvents.OnSaveGroupSongScoreResult?.Invoke(songName, instrumentScores, false);
				}
			}, TaskScheduler.FromCurrentSynchronizationContext());     // task continuation on UI thread
		}
	}


	private void OnSavePlayerName(string playerName, PlayerProfile profile)
	{
		if (!string.IsNullOrEmpty(playerName))
		{
			profile.PlayerName = playerName;

			Dictionary<string, object> childUpdates = new Dictionary<string, object>();
			childUpdates["/playerprofiles/" + playerName] = profile.PlayerName;

			databaseRoot.UpdateChildrenAsync(childUpdates).ContinueWith(task =>
			{
				if (task.IsCompleted)
				{
					Debug.Log("OnSavePlayerName: " + playerName + " success!");
					SaimonseiEvents.OnPlayerNameSaveResult?.Invoke(playerName, true);
				}
				else
				{
					Debug.Log("OnSavePlayerName: error = " + task.Exception.Message);
					SaimonseiEvents.OnPlayerNameSaveResult?.Invoke(playerName, false);
				}
			}, TaskScheduler.FromCurrentSynchronizationContext());     // task continuation on UI thread
		}
	}

	private void OnSavePlayerStats(PlayerProfile profile)
	{
		if (!string.IsNullOrEmpty(profile.PlayerName))
		{
            // atomic updates of player stats
			Dictionary<string, object> childUpdates = new Dictionary<string, object>();

			string playCount = string.Format("{0}/{1}", profile.PlayerName, "PlayCount");
			string firstPlay = string.Format("{0}/{1}", profile.PlayerName, "FirstPlay");
			string lastPlay = string.Format("{0}/{1}", profile.PlayerName, "LastPlay");

			childUpdates["/playerprofiles/" + playCount] = profile.PlayCount;
			childUpdates["/playerprofiles/" + firstPlay] = profile.FirstPlay.ToUniversalTime().ToString();
			childUpdates["/playerprofiles/" + lastPlay] = profile.LastPlay.ToUniversalTime().ToString();

			databaseRoot.UpdateChildrenAsync(childUpdates).ContinueWith(task =>
			{
				if (task.IsCompleted)
				{
					Debug.Log("OnSavePlayerStats: " + profile.PlayerName + " success!");
					SaimonseiEvents.OnPlayerStatsSaveResult?.Invoke(profile.PlayerName, true);
				}
				else
				{
					Debug.Log("OnSavePlayerStats: error = " + task.Exception.Message);
					SaimonseiEvents.OnPlayerStatsSaveResult?.Invoke(profile.PlayerName, false);
				}
			}, TaskScheduler.FromCurrentSynchronizationContext());     // task continuation on UI thread
		}
	}


	// gets a list of the instruments within the song, each with a list of player scores
	private void RetrieveSongScores(string songTitle, int maxScores = 100)	
	{
		var songsNode = databaseRoot.Child("songs").Child(songTitle).LimitToFirst(maxScores);

		songsNode.GetValueAsync().ContinueWith(task => {

			if (task.IsCompleted)
			{
				if (task.Result == null)        // songName does not exist
				{
					Debug.Log("song: " + songTitle + " does not exist");
				}
				else
				{
					DataSnapshot snapshot = task.Result;
					var instrumentScores = snapshot.Children;

					var songScores = new List<LeaderboardInstrPlayerScores>();

					// add each entry to the top of the list to reverse the sort order to descending
					foreach (var score in instrumentScores)
					{
						var songScore = new LeaderboardInstrPlayerScores { Instrument = score.Key };
						var playerScores = score.Children;

						foreach (var playerScore in playerScores)
						{
							songScore.PlayerScores.Add(new LeaderboardPlayerScore { Player = playerScore.Key, Score = Convert.ToInt32(playerScore.Value) });
						}

						songScores.Insert(0, songScore);
					}
					SaimonseiEvents.OnSongScoresRetrieved?.Invoke(songTitle, songScores);
				}
			}
			else
			{
				Debug.Log("RetrieveSongScores: error: " + task.Exception.Message);
			}
		}, TaskScheduler.FromCurrentSynchronizationContext());		// task continuation on UI thread
	}

    // make sure name is clean of any illegal characters
    private void OnLookupPlayerName(string playerName)
    {
		if (string.IsNullOrEmpty(playerName))
			return;

		var cleanPlayerName = CleanPlayerName(playerName);
		var profileNode = databaseRoot.Child("playerprofiles").Child(cleanPlayerName);

		profileNode.GetValueAsync().ContinueWith(task => {

			if (task.IsCompleted)
			{
				if (task.Result == null || task.Result.Value == null)        // playerName does not exist
				{
					Debug.Log("OnLookupPlayerName: " + cleanPlayerName + " not found!");
					SaimonseiEvents.OnLookupPlayerResult?.Invoke(cleanPlayerName, false);
				}
				else
				{
					Debug.Log("OnLookupPlayerName: " + cleanPlayerName + " found");
					SaimonseiEvents.OnLookupPlayerResult?.Invoke(cleanPlayerName, true);
				}
			}
		}, TaskScheduler.FromCurrentSynchronizationContext());     // task continuation on UI thread

	}

	// gets a list of the songs for the player, each with a list of instrument scores
	private void RetrievePlayerScores(string playerName)
	{
		if (string.IsNullOrEmpty(playerName))
		{
			return;
		}

		var playerNode = databaseRoot.Child("players").Child(playerName);

		playerNode.GetValueAsync().ContinueWith(task => {

			if (task.IsCompleted)
			{
				if (task.Result == null || task.Result.Value == null)        // playerName does not exist
				{
					Debug.Log("player: " + playerName + " does not exist");
				}
				else
				{
					DataSnapshot snapshot = task.Result;
					var songScores = snapshot.Children;

					var instrumentScores = new List<LeaderboardSongInstrScores>();

					// add each entry to the top of the list to reverse the sort order to descending
					foreach (var score in songScores)
					{
						var playerScore = new LeaderboardSongInstrScores { Song = score.Key };
						var instrScores = score.Children;

						foreach (var instrScore in instrScores)
						{
							playerScore.InstrumentScores.Add(new LeaderboardInstrumentScore { Instrument = instrScore.Key, Score = Convert.ToInt32(instrScore.Value) });
						}

						instrumentScores.Insert(0, playerScore);
					}

					SaimonseiEvents.OnPlayerScoresRetrieved?.Invoke(playerName, instrumentScores);
				}
			}
			else
			{
				Debug.Log("RetrievePlayerScores: error = " + task.Exception.Message);
			}
		}, TaskScheduler.FromCurrentSynchronizationContext());     // task continuation on UI thread
	}

	// gets a list of instrument player scores for a given song
	private void RetrieveGroupSongScores(string hostName, string clientNames, string songName)
	{
		if (string.IsNullOrEmpty(hostName))
		{
			return;
		}

		var playerNode = databaseRoot.Child("players").Child(hostName).Child("hosted").Child(clientNames).Child(songName);

		playerNode.GetValueAsync().ContinueWith(task => {

			if (task.IsCompleted)
			{
				var songScores = new List<LeaderboardInstrPlayerScores>();

				if (task.Result == null || task.Result.Value == null)        // host/group does not exist
				{
					Debug.Log("Host group: " + hostName + "/" + clientNames + " does not exist");
					SaimonseiEvents.OnGroupSongScoresRetrieved?.Invoke(hostName, clientNames, songName, songScores);       // empty score list
				}
				else
				{
					DataSnapshot snapshot = task.Result;
					var instrScores = snapshot.Children;

					// scores for each instrument in song
					foreach (var instrScore in instrScores)
					{
						var instrPlayerScores = new List<LeaderboardPlayerScore>();
						var playerScores = instrScore.Children;

						// scores for each player of instrument
						foreach (var playerScore in playerScores)
						{
							//Debug.Log("RetrieveHostScores: instrument: " + instrScore.Key + " player: " + playerScore.Key + " score: " + Convert.ToInt32(playerScore.Value));
							instrPlayerScores.Add(new LeaderboardPlayerScore { Player = playerScore.Key, Score = Convert.ToInt32(playerScore.Value) });
						}

						songScores.Add(new LeaderboardInstrPlayerScores { Instrument = instrScore.Key, PlayerScores = instrPlayerScores });
					}

					SaimonseiEvents.OnGroupSongScoresRetrieved?.Invoke(hostName, clientNames, songName, songScores);
				}
			}
			else
			{
				Debug.Log("RetrieveHostScores: error = " + task.Exception.Message);
			}
		}, TaskScheduler.FromCurrentSynchronizationContext());     // task continuation on UI thread
	}


	// gets a list of the groups player has hosted, with songs for each group, each with a list of instrument player scores
	private void RetrieveGroupScores(string hostName, string clientNames)
	{
		if (string.IsNullOrEmpty(hostName))
		{
			return;
		}

		var playerNode = databaseRoot.Child("players").Child(hostName).Child("hosted").Child(clientNames);

		//if (!string.IsNullOrEmpty(songName))
		//	playerNode.Child(songName);

		playerNode.GetValueAsync().ContinueWith(task => {

			if (task.IsCompleted)
			{
				var hostScores = new List<LeaderboardHostSongScores>();

				if (task.Result == null || task.Result.Value == null)        // host/group does not exist
				{
					Debug.Log("Host group: " + hostName + "/" + clientNames + " does not exist");
					SaimonseiEvents.OnGroupScoresRetrieved?.Invoke(hostName, clientNames, hostScores);       // empty score list
				}
				else
				{
					DataSnapshot snapshot = task.Result;
					var songScores = snapshot.Children;

					// add each entry to the top of the list to reverse the sort order to descending
					foreach (var score in songScores)
					{
						//Debug.Log("RetrieveHostScores: song " + score.Key);

						var songScore = new LeaderboardHostSongScores { Host = hostName, Song = score.Key, GroupKey = clientNames };
						var instrScores = score.Children;

						// scores for each instrument in song
						foreach (var instrScore in instrScores)
						{
							//Debug.Log("RetrieveHostScores: instrument " + instrScore.Key);

							var instrPlayerScores = new List<LeaderboardPlayerScore>();
							var playerScores = instrScore.Children;

							// scores for each player of instrument
							foreach (var playerScore in playerScores)
							{
								//Debug.Log("RetrieveHostScores: player " + playerScore.Key + " score: " + Convert.ToInt32(playerScore.Value));
								instrPlayerScores.Add(new LeaderboardPlayerScore { Player = playerScore.Key, Score = Convert.ToInt32(playerScore.Value) });
							}

							songScore.InstrScores.Add(new LeaderboardInstrPlayerScores { Instrument = instrScore.Key, PlayerScores = instrPlayerScores });
						}

						hostScores.Insert(0, songScore);
					}
					SaimonseiEvents.OnGroupScoresRetrieved?.Invoke(hostName, clientNames, hostScores);
				}
			}
			else
			{
				Debug.Log("RetrieveHostScores: error = " + task.Exception.Message);
			}
		}, TaskScheduler.FromCurrentSynchronizationContext());     // task continuation on UI thread
	}

	//private void GetSong(string songName)
	//{
	//	if (string.IsNullOrEmpty(songName))
	//	{
	//		return;
	//	}

	//	var playerNode = databaseRoot.Child("songs").Child(songName);
	//	playerNode.GetValueAsync().ContinueWith(task => {

	//		if (task.IsCompleted)
	//		{
	//			if (task.Result == null)
	//			{
	//				Debug.Log("song: " + songName + " does not exist");
	//			}
	//			else
	//			{
	//				DataSnapshot snapshot = task.Result;
	//				var playerProfile = JsonUtility.FromJson<LeaderboardInstrPlayerScores>(snapshot.GetRawJsonValue());

	//				Debug.Log("song: " + songName + " exists");
	//			}
	//		}
	//		else
	//		{
	//			Debug.Log("GetSong: error = " + task.Exception.Message);
	//		}
	//	});
	//}
}



// leaderboard entry for instrument/player (for song scores)
[Serializable] 
public class LeaderboardInstrPlayerScores
{
	public string Instrument = "";
	public List<LeaderboardPlayerScore> PlayerScores = new List<LeaderboardPlayerScore>();

    // score for given player
    public LeaderboardPlayerScore GetPlayerScore(string playerName)
    {
        return PlayerScores.Where(x => x.Player == playerName).FirstOrDefault();
    }

	// best (lowest) score for this instrument
	public LeaderboardPlayerScore GetLowestScore()
	{
		LeaderboardPlayerScore lowest = null;

		foreach (var playerScore in PlayerScores)
		{
			if (playerScore.Score == InstrumentScore.MaxScore)
				continue;

			if (lowest == null || playerScore.Score < lowest.Score)
				lowest = playerScore;
		}

		return lowest;
	}
}

[Serializable]
public class LeaderboardPlayerScore
{
	public string Player = "";
	public int Score = 0;
}


// leaderboard entry for host, group and song (for host scores)
[Serializable]
public class LeaderboardHostSongScores
{
	public string Host = "";
	public string GroupKey = "";        // client names (alphabetic)
	public string Song = "";
	public List<LeaderboardInstrPlayerScores> InstrScores = new List<LeaderboardInstrPlayerScores>();
}


// leaderboard entry for song/instrument (for player scores)
[Serializable]
public class LeaderboardSongInstrScores
{
	public string Song = "";
	public List<LeaderboardInstrumentScore> InstrumentScores = new List<LeaderboardInstrumentScore>();
}

[Serializable]
public class LeaderboardInstrumentScore
{
	public string Instrument = "";
	public int Score = 0;
}

