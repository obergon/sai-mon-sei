﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;


public class LeaderboardUI : MonoBehaviour
{
    public Image LeaderboardPanel;
    public Text LeaderboardHeader;

    public Transform LeaderboardContents;           // ScrollView content

    public GameObject EntryPrefab;
    public GameObject GroupPrefab;                  // multiplayer

    public int TopScoreCount = 100;

    public PlayerProfile PlayerProfile;

    private string selectedSong;
    private bool leaderboardShowing = false;

    public enum LeaderboardType
    {
		None,
        Song,
        Player,
        Group,          // multiplayer group, keyed in db by host player name + group player's (clients) names
        GroupSong       // multiplayer group, keyed in db by host player name + group player's (clients) names + song title
    }

    public LeaderboardType LeaderboardView;     // single player
    public LeaderboardType GroupView;           // multiplayer

    private float resultsPauseTime = 0.2f;
    private int playerBoardCount = 10;       // for animation / audio
    private int songBoardCount = 10;         // for animation / audio

  

    private void OnEnable()
    {
        SaimonseiEvents.OnSongSelected += OnSongSelected;
        SaimonseiEvents.OnToggleLeaderboard += OnToggleLeaderboard;
        SaimonseiEvents.OnShowLeaderboard += OnShowLeaderboard;
        SaimonseiEvents.OnSongScoresRetrieved += OnSongScoresRetrieved;
        SaimonseiEvents.OnPlayerScoresRetrieved += OnPlayerScoresRetrieved;
        SaimonseiEvents.OnGroupScoresRetrieved += OnGroupScoresRetrieved;
        SaimonseiEvents.OnGroupSongScoresRetrieved += OnGroupSongScoresRetrieved;
        SaimonseiEvents.OnLeaderboardUpdated += OnLeaderboardUpdated;
    }

    private void OnDisable()
    {
        SaimonseiEvents.OnSongSelected -= OnSongSelected;
        SaimonseiEvents.OnToggleLeaderboard -= OnToggleLeaderboard;
        SaimonseiEvents.OnShowLeaderboard -= OnShowLeaderboard;
        SaimonseiEvents.OnSongScoresRetrieved -= OnSongScoresRetrieved;
        SaimonseiEvents.OnPlayerScoresRetrieved -= OnPlayerScoresRetrieved;
        SaimonseiEvents.OnGroupScoresRetrieved -= OnGroupScoresRetrieved;
        SaimonseiEvents.OnGroupSongScoresRetrieved -= OnGroupSongScoresRetrieved;
        SaimonseiEvents.OnLeaderboardUpdated -= OnLeaderboardUpdated;
    }


	private void OnSongSelected(string songTitle, string songEventName, PlayArea.PlayMode playMode, int gridWidth, int gridHeight, float tolerance)
	{
        selectedSong = songTitle;
	}

	private void OnToggleLeaderboard(LeaderboardType view)
    {
        OnShowLeaderboard(view, !leaderboardShowing);
    }


    private void OnShowLeaderboard(LeaderboardType view, bool show)
    {
        if (NetworkManager.IsMultiPlayerGame)
        {
            if (view != GroupView)
                return;
        }
        else
        {
            if (view != LeaderboardView)
                return;
        }

        leaderboardShowing = show;

        if (leaderboardShowing)
        {
            switch (view)
            {
                case LeaderboardType.None:
                    break;

                case LeaderboardType.Song:
                    LeaderboardHeader.text = selectedSong + " Top " + TopScoreCount;
                    SaimonseiEvents.OnLookupSongScores?.Invoke(selectedSong);   // retrieve from db & enable after clearing existing entries
                    break;

                case LeaderboardType.Player:
                    LeaderboardHeader.text = PlayerProfile.PlayerName + "'s Top " + TopScoreCount;
                    SaimonseiEvents.OnLookupPlayerScores?.Invoke(PlayerProfile.PlayerName);   // retrieve from db & enable after clearing existing entries
                    break;

                case LeaderboardType.Group:
                    LeaderboardHeader.text = "Group Leaderboard";
                    SaimonseiEvents.OnLookupGroupScores?.Invoke(NetworkManager.GameHost.PlayerName.Value, NetworkManager.GroupPlayersKey);   // retrieve from db & enable after clearing existing entries
                    break;

                case LeaderboardType.GroupSong:
                    LeaderboardHeader.text = selectedSong + " Group Leaderboard";
                    SaimonseiEvents.OnLookupGroupSongScores?.Invoke(NetworkManager.GameHost.PlayerName.Value, NetworkManager.GroupPlayersKey, selectedSong);   // retrieve from db & enable after clearing existing entries
                    break;
            }
        }
        else
            LeaderboardPanel.gameObject.SetActive(false);
    }

	// scores for a single song
    private void OnSongScoresRetrieved(string songTitle, List<LeaderboardInstrPlayerScores> instrumentScores)
    {
        if (songTitle != selectedSong)
            return;

        if (instrumentScores == null)
            return;

        if (! leaderboardShowing)
            return;

        ClearEntries();
        StartCoroutine(PopulateSongLeaderboard(instrumentScores, songBoardCount));
    }

	// scores for a single player
    private void OnPlayerScoresRetrieved(string playerName, List<LeaderboardSongInstrScores> songScores)
    {
        if (playerName != PlayerProfile.PlayerName)
            return;

        if (songScores == null)
            return;

        if (!leaderboardShowing)
            return;

        ClearEntries();
        StartCoroutine(PopulatePlayerLeaderboard(songScores));
    }

    // scores for all songs for the host / group
    private void OnGroupScoresRetrieved(string hostName, string clientNames, List<LeaderboardHostSongScores> songScores)
    {
        if (songScores == null)
            return;

        if (!leaderboardShowing)
            return;

        ClearEntries();
        StartCoroutine(PopulateGroupLeaderboard(songScores));
    }

    // scores for given song for the host / group
    private void OnGroupSongScoresRetrieved(string hostName, string clientNames, string songName, List<LeaderboardInstrPlayerScores> songScores)
    {
        if (songScores == null)
            return;

        if (!leaderboardShowing)
            return;

        ClearEntries();
        StartCoroutine(PopulateGroupSongLeaderboard(songName, songScores));
    }

    private IEnumerator PopulateGroupLeaderboard(List<LeaderboardHostSongScores> songScores)
    {
        LeaderboardPanel.gameObject.SetActive(true);
        float initAngle = 90f;
        int scoreCount = 0;

        // list of songs played by this group
        foreach (var songScore in songScores)
        {
            //var sortedScores = songScore.InstrScores.OrderBy(x => x.Instrument).Take(TopScoreCount).ToList();      // default ascending

            // list of instruments in each song
            foreach (var instrScore in songScore.InstrScores)
            {
                // list of player scores for each instrument
                foreach (var playerScore in instrScore.PlayerScores)
                {
                    yield return new WaitForSeconds(resultsPauseTime);

                    var prefab = NetworkManager.IsMultiPlayerGame ? GroupPrefab : EntryPrefab;
                    var newEntry = Instantiate(prefab, LeaderboardContents);
                    LeaderboardEntry entry = newEntry.GetComponent<LeaderboardEntry>();

                    if (++scoreCount <= songBoardCount)
                    {
                        entry.transform.localRotation = Quaternion.Euler(new Vector2(initAngle, 0f));     // ie. not visible   
                        entry.FlipIn(initAngle, true);
                    }

                    entry.SongTitle.text = songScore.Song.ToLower();

                    entry.Instrument.text = instrScore.Instrument.ToLower();
                    entry.Player.text = playerScore.Player;
                    entry.Score.text = playerScore.Score.ToString();

                    // hilight 'you'
                    if (playerScore.Player == PlayerProfile.PlayerName)
                        entry.LBButton.GetComponent<Image>().color = Color.green;
                }
            }
        }
        yield return null;
    }

    private IEnumerator PopulateGroupSongLeaderboard(string songName, List<LeaderboardInstrPlayerScores> songScores)
    {
        LeaderboardPanel.gameObject.SetActive(true);
        float initAngle = 90f;
        int scoreCount = 0;

        // list of instruments in each song
        foreach (var instrScore in songScores)
        {
            // list of player scores for each instrument
            foreach (var playerScore in instrScore.PlayerScores)
            {
                yield return new WaitForSeconds(resultsPauseTime);

                var prefab = NetworkManager.IsMultiPlayerGame ? GroupPrefab : EntryPrefab;
                var newEntry = Instantiate(prefab, LeaderboardContents);
                LeaderboardEntry entry = newEntry.GetComponent<LeaderboardEntry>();

                if (++scoreCount <= songBoardCount)
                {
                    entry.transform.localRotation = Quaternion.Euler(new Vector2(initAngle, 0f));     // ie. not visible   
                    entry.FlipIn(initAngle, true);
                }

                entry.SongTitle.text = songName.ToLower();

                entry.Instrument.text = instrScore.Instrument.ToLower();
                entry.Player.text = playerScore.Player;
                entry.Score.text = playerScore.Score.ToString();

                // hilight 'you'
                if (playerScore.Player == PlayerProfile.PlayerName)
                    entry.LBButton.GetComponent<Image>().color = Color.green;
            }
        }

        yield return null;
    }

    private IEnumerator PopulateSongLeaderboard(List<LeaderboardInstrPlayerScores> instrumentScores, int visibleCount)
    {
        LeaderboardPanel.gameObject.SetActive(true);
        float initAngle = 90f;
        int scoreCount = 0;

        foreach (var instrumentScore in instrumentScores)
        {
            var sortedScores = instrumentScore.PlayerScores.OrderBy(x => x.Score).Take(TopScoreCount).ToList();      // default ascending

            foreach (LeaderboardPlayerScore playerScore in sortedScores)
            {
                yield return new WaitForSeconds(resultsPauseTime);

                var prefab = NetworkManager.IsMultiPlayerGame ? GroupPrefab : EntryPrefab;
                var newEntry = Instantiate(prefab, LeaderboardContents);
                LeaderboardEntry entry = newEntry.GetComponent<LeaderboardEntry>();

                if (++scoreCount <= songBoardCount)
                {
                    entry.transform.localRotation = Quaternion.Euler(new Vector2(initAngle, 0f));     // ie. not visible   
                    entry.FlipIn(initAngle, true);
                }

                entry.Instrument.text = instrumentScore.Instrument.ToLower();
                entry.Player.text = playerScore.Player;
                entry.Score.text = playerScore.Score.ToString();

                // hilight 'you'
                if (playerScore.Player == PlayerProfile.PlayerName)
                    entry.LBButton.GetComponent<Image>().color = Color.green;
            }
        }
        yield return null;
    }


    private IEnumerator PopulatePlayerLeaderboard(List<LeaderboardSongInstrScores> songScores)
    {
        LeaderboardPanel.gameObject.SetActive(true);
        float initAngle = 90f;
        int scoreCount = 0;

        foreach (var songScore in songScores)
        {
            var sortedScores = songScore.InstrumentScores.OrderBy(x => x.Instrument).Take(TopScoreCount).ToList();      // default ascending

            foreach (LeaderboardInstrumentScore instrScore in sortedScores)
            {
                yield return new WaitForSeconds(resultsPauseTime);

                var prefab = NetworkManager.IsMultiPlayerGame ? GroupPrefab : EntryPrefab;
                var newEntry = Instantiate(prefab, LeaderboardContents);
                LeaderboardEntry entry = newEntry.GetComponent<LeaderboardEntry>();

                if (++scoreCount <= playerBoardCount)
                {
                    entry.transform.localRotation = Quaternion.Euler(new Vector2(initAngle, 0f));     // ie. not visible
                    entry.FlipIn(initAngle, true);
                }

                entry.SongTitle.text = songScore.Song;
                entry.Instrument.text = instrScore.Instrument.ToLower();
                entry.Score.text = instrScore.Score.ToString();
            }
        }

        yield return null;
    }

    private void OnLeaderboardUpdated(SongScore songScore, InstrumentScore instrumentScore, bool success)
    {
        if (leaderboardShowing && songScore.SongName == selectedSong && success)
        {
            SaimonseiEvents.OnLookupSongScores?.Invoke(songScore.SongName);
        }
    }

    private void ClearEntries()
    {
        foreach (Transform entry in LeaderboardContents)
        {
            Destroy(entry.gameObject);
        }
    }
}
