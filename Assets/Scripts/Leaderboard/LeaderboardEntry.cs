﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class LeaderboardEntry : MonoBehaviour
{
    public Text SongTitle;
    public Text Instrument;
    public Text Group;
    public Text Player;
    public Text Score;

    public Button LBButton;

    [Header("Audio")]
    public AudioSource LeaderboardAudioSource;
    public AudioClip FlipAudio;
    public AudioClip FlippedAudio;

    private float flipTime = 0.3f;


    private void OnEnable()
    {
        LBButton.onClick.AddListener(OnLBEntryClick);
    }

    private void OnDisable()
    {
        LBButton.onClick.RemoveListener(OnLBEntryClick);
    }


    public void FlipIn(float initAngle, bool audio)
    {
        //if (audio && FlipAudio != null)
        //    AudioSource.PlayClipAtPoint(FlippedAudio, Vector3.zero);

        LeanTween.rotateAround(LBButton.GetComponent<RectTransform>(), Vector3.left, 180f + initAngle, flipTime)
                    .setEaseInCubic()
                    .setOnComplete(() =>
                    {
                        if (audio)
                            PlaySFX(FlippedAudio);
                        //if (FlippedAudio != null)
                        //    AudioSource.PlayClipAtPoint(FlippedAudio, Vector3.zero);

                        LeanTween.rotateAround(LBButton.GetComponent<RectTransform>(), Vector3.left, 180f, flipTime)
                                    .setEaseOutCubic()
                                    .setOnComplete(() =>
                                    {
                                        if (audio) // && FlippedAudio != null)
                                            PlaySFX(FlippedAudio);
                                        //AudioSource.PlayClipAtPoint(FlippedAudio, Vector3.zero);
                                    });
                    }
                    );
    }


    private void OnLBEntryClick()
    {
        //SaimonseiEvents.OnLeaderboardEntryClicked?.Invoke();
    }

    private void PlaySFX(AudioClip sfx)
    {
        if (sfx == null)
            return;

        LeaderboardAudioSource.clip = sfx;
        LeaderboardAudioSource.Play();
    }
}
