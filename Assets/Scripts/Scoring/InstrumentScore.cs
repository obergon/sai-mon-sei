﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using MLAPI.Serialization;
using System.IO;
using MLAPI.Serialization.Pooled;


// recorded in a list in SongScore SO
// and used in a NetworkedDictionary for multiplayer
[Serializable]
public class InstrumentScore : IBitWritable
{
	public string SongTitle = "";

	public int InstrumentNumber;
	public string InstrumentName = "";

	//public bool PlayedInstrument;           // multiplayer
	public int PlayerNumber;                // multiplayer
	public string PlayerName = "";          // multiplayer

	public int NumGridPoints;               // difficulty factor

	public int Rank;

	public int TargetNoteCount;
	public int RecordedNoteCount;

	public int DudNoteCount;
	//public int DudNoteScore;
	public int UnrecordedCount;             // missing note numbers

	public int TimingDifference;         // ms, note timeline start position
	public int DurationDifference;         // ms, note duration
	public int LocationDifference;         // distance from grid points

	public int PersonalBest;
	public bool isNewPB;


	public const int MaxScore = 9999999;       // if score == 0 (ie. no input/effort)
	//public const int DudNotePenalty = 1000;    

	public int TotalScore
	{
		get
		{
			int score = TimingDifference + DurationDifference + LocationDifference + (DudNoteCount * SongScore.DudNotePenalty);
			//score *= DudNoteCount+1;
			//score /= (NumGridPoints > 0) ? NumGridPoints : 1;		// TODO: as long as grid size sliders are on MainMenu

			return (score == 0) ? MaxScore : score;
		}
	}

	public int StoryTotalScore
	{
		get
		{
			int score = TimingDifference + DurationDifference + LocationDifference + (DudNoteCount * SongScore.DudNotePenalty);
			return (score == 0) ? MaxScore : score;
		}
	}

	public string FormattedResults
	{
		get { return string.Format("Rank #{0}\nPlayer: {1}\n{2}\nSong notes: {3}\nRecorded: {4}\nMissed: {5}\nTiming: {6}\nDuration: {7}\nPosition: {8}\nDifficulty: {9}\nTotal: {10}",
								  Rank,
                                  PlayerName,
								  InstrumentName,
								  TargetNoteCount,
								  RecordedNoteCount,
								  DudNoteCount,
								  TimingDifference,
								  DurationDifference,
								  LocationDifference,
                                  NumGridPoints,
								  (TotalScore == MaxScore) ? "N/A" : TotalScore.ToString()); }
	}


	public void Read(Stream stream)
	{
		using (PooledBitReader reader = PooledBitReader.Get(stream))
		{
			SongTitle = reader.ReadStringPacked().ToString();

			InstrumentNumber = reader.ReadInt32Packed();
			InstrumentName = reader.ReadStringPacked().ToString();

			//PlayedInstrument = reader.ReadBool();
			PlayerNumber = reader.ReadInt32Packed();
			PlayerName = reader.ReadStringPacked().ToString();
			NumGridPoints = reader.ReadInt32Packed();

			Rank = reader.ReadInt32Packed();

			TargetNoteCount = reader.ReadInt32Packed();
			RecordedNoteCount = reader.ReadInt32Packed();

			DudNoteCount = reader.ReadInt32Packed();
			//DudNoteScore = reader.ReadInt32Packed();
			UnrecordedCount = reader.ReadInt32Packed();

			TimingDifference = reader.ReadInt32Packed();
			DurationDifference = reader.ReadInt32Packed();
			LocationDifference = reader.ReadInt32Packed();

			PersonalBest = reader.ReadInt32Packed();
			isNewPB = reader.ReadBool();
		}
	}

	public void Write(Stream stream)
	{
		using (PooledBitWriter writer = PooledBitWriter.Get(stream))
		{
			writer.WriteStringPacked(SongTitle);

			writer.WriteInt32Packed(InstrumentNumber);
			writer.WriteStringPacked(InstrumentName);

			//writer.WriteBool(PlayedInstrument);
			writer.WriteInt32Packed(PlayerNumber);
			writer.WriteStringPacked(PlayerName);
			writer.WriteInt32Packed(NumGridPoints);

			writer.WriteInt32Packed(Rank);

			writer.WriteInt32Packed(TargetNoteCount);
			writer.WriteInt32Packed(RecordedNoteCount);

			writer.WriteInt32Packed(DudNoteCount);
			//writer.WriteInt32Packed(DudNoteScore);
			writer.WriteInt32Packed(UnrecordedCount);

			writer.WriteInt32Packed(TimingDifference);
			writer.WriteInt32Packed(DurationDifference);
			writer.WriteInt32Packed(LocationDifference);

			writer.WriteInt32Packed(PersonalBest);
			writer.WriteBool(isNewPB);
		}
	}

	public string ToJson(bool prettyPrint)
	{
		return JsonUtility.ToJson(this, prettyPrint);
	}
}
