﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ScoreKeeper : MonoBehaviour
{
	public List<SongScore> SongScores;

	public static int RunningScore = 0;         // running total score during recording mode

	private string selectedSongTitle;
	private string selectedSongEventName;

	private PlayArea.PlayMode playMode;

	public enum ScoreRating
	{
		None,
		Nice,
		Perfect,
		Final
	}

	private void OnEnable()
	{
		SaimonseiEvents.OnSongSelected += OnSongSelected;
	}

	private void OnDisable()
	{
		SaimonseiEvents.OnSongSelected -= OnSongSelected;
	}

	private void OnSongSelected(string songTitle, string songEventName, PlayArea.PlayMode playMode, int gridWidth, int gridHeight, float tolerance)
	{
		selectedSongTitle = songTitle;
		selectedSongEventName = songEventName;

		this.playMode = playMode;
		LookupSongScore();
	}

	private SongScore LookupSongScore()
	{
		if (string.IsNullOrEmpty(selectedSongTitle))
		{
			Debug.LogError("LookupSongScore: a song has not been selected!!");
			return null;
		}

		var score = SongScores.Where(x => x.SongName == selectedSongTitle).FirstOrDefault();

		//var score = (playMode == PlayArea.PlayMode.LearnSong) ?
		//		SongScores.Where(x => x.SongName == selectedSongTitle).FirstOrDefault() :
		//              RealmScores.Where(x => x.SongName == selectedSongTitle).FirstOrDefault();

		if (score != null)
			SaimonseiEvents.OnSongScoreLookup?.Invoke(selectedSongTitle, score);

		return score;
	}


	public List<InstrumentScore> CalculateInstrumentScores(int numInstruments, List<int> instrumentsPlayed, int numGridPoints, Track targetTrack, Track recordedTrack)
	{
		var songScore = LookupSongScore();

		if (songScore == null)
		{
			Debug.LogError("CalculateInstrumentScores: SongScore for " + selectedSongTitle + " not found!!");
			return null;
		}

		return songScore.CalculateInstrumentResults(numInstruments, instrumentsPlayed, numGridPoints, targetTrack, recordedTrack);
	}


	// each note score on touch end
	public InstrumentScore ScoreInstrument(int instrumentNumber, int numGridPoints, Track targetTrack, Track recordedTrack, out ScoreRating rating)
	{
		var songScore = LookupSongScore();
		rating = ScoreRating.None;

		if (songScore == null)
		{
			Debug.LogError("ScoreInstrument: SongScore for " + selectedSongTitle + " not found!!");
			return null;
		}

		return songScore.ScoreInstrument(instrumentNumber, numGridPoints, targetTrack, recordedTrack, out rating);
	}

	// each note score on touch end
	public InstrumentScore ScoreStoryNote(int noteIndex, Track targetTrack, Track recordedTrack, out ScoreRating rating)
	{
		var songScore = LookupSongScore();
		rating = ScoreRating.None;

		if (songScore == null)
		{
			Debug.LogError("ScoreStoryNote: SongScore for " + selectedSongTitle + " not found!!");
			return null;
		}

		return songScore.ScoreStoryNote(noteIndex, targetTrack, recordedTrack, out rating);
	}
}
