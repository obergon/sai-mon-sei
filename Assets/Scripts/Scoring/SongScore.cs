﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using FMODUnity;
using UnityEngine;


[CreateAssetMenu(fileName = "SongScore", menuName = "Scores/SongScore")]
public class SongScore : ScriptableObject
{
	public string SongName;             // set in inspector

	[SerializeField]
	[EventRef]
	public string SongEventName;        // set in inspector

	public List<InstrumentScore> InstrumentScores;

	private const int PerfectScore = 1000;      // <=
	private const int NiceScore = 2000;         // <=
	private const int ScoreLocationFactor = 100;   
	public static int DudNotePenalty = 1000;   


	private void OnEnable()
	{
		SaimonseiEvents.OnPlayCycleStart += OnPlayCycleStart;
		SaimonseiEvents.OnInstrumentChanged += OnInstrumentChanged;
	}

    private void OnDisable()
	{
		SaimonseiEvents.OnPlayCycleStart -= OnPlayCycleStart;
		SaimonseiEvents.OnInstrumentChanged -= OnInstrumentChanged;
	}

	// create a new score entry if not aleady present
	private void OnInstrumentChanged(string songEventName, int instrumentNumber, string playerInstrumentName, string shortInstrumentName, int numGridPoints)
	{
		if (SongEventName != songEventName)
			return;

		if (instrumentNumber == 0)
			return;

		if (LookupInstrumentScore(instrumentNumber) == null)
			AddInstrumentScore(songEventName, instrumentNumber, shortInstrumentName, numGridPoints);
	}

	private void AddInstrumentScore(string songEventName, int instrumentNumber, string instrumentName, int numGridPoints)
	{
		InstrumentScore newInstrumentScore = new InstrumentScore
        {
            SongTitle = SongName,
            InstrumentNumber = instrumentNumber,
            NumGridPoints = numGridPoints,
            InstrumentName = instrumentName
		};

		InstrumentScores.Add(newInstrumentScore);
	}

	// may be null!
	private InstrumentScore LookupInstrumentScore(int instrumentNumber)
	{
		return InstrumentScores.Where(x => x.InstrumentNumber == instrumentNumber).FirstOrDefault();
	}

	public List<InstrumentScore> CalculateInstrumentResults(int numInstruments, List<int> instrumentsPlayed, int numGridPoints, Track targetTrack, Track recordedTrack)
	{
		for (int i = 0; i < numInstruments; i++)
		{
			ScoreKeeper.ScoreRating rating;
			var instrumentScore = ScoreInstrument(i + 1, numGridPoints, targetTrack, recordedTrack, out rating);
		}

		// calculate rankings
		var sortedScores = InstrumentScores.OrderBy(x => x.TotalScore).ToList();      // default ascending

		int rank = 1;
		bool newPB = false;

		foreach (var score in sortedScores)
		{
			var instrumentResult = LookupInstrumentScore(score.InstrumentNumber);

			// multiplayer only - new PB only for instrument(s) local player played
			//score.PlayedInstrument = instrumentsPlayed != null ? instrumentsPlayed.Contains(score.InstrumentNumber) : true;
			//Debug.Log("CalculateInstrumentResults InstrumentNumber =" + score.InstrumentNumber);

			if (instrumentResult != null)
			{
				instrumentResult.Rank = rank;
				rank++;

                if (NetworkManager.LocalPlayerPlayedInstrument(score.InstrumentNumber))
				//if (score.PlayedInstrument)
				{
					if (score.RecordedNoteCount == 0 || score.TotalScore == InstrumentScore.MaxScore)
						score.isNewPB = false;
					else                        // lower score is better
						score.isNewPB = score.PersonalBest == 0 || score.TotalScore < score.PersonalBest;

					// hi-score
					if (score.isNewPB)
					{
						newPB = true;
						score.PersonalBest = score.TotalScore;
						SaimonseiEvents.OnNewInstrumentPB?.Invoke(this, score);     // eg. for leaderboard
					}
				}
				else
				{
					newPB = false;
					score.isNewPB = false;
				}
			}
		}

		if (newPB)
			SaimonseiEvents.OnNewPersonalBest?.Invoke(this);

        // update networked instrument scores with the scores for the instrument(s) this player played
        if (NetworkManager.IsMultiPlayerGame)
		    SaimonseiEvents.OnFinalGroupScore?.Invoke(this);       // for multiplayer networked var (for UI)

		return InstrumentScores;
	}


	// each note score on touch end
	public InstrumentScore ScoreInstrument(int instrumentNumber, int numGridPoints, Track targetTrack, Track recordedTrack, out ScoreKeeper.ScoreRating rating)
	{
		var instrumentScore = LookupInstrumentScore(instrumentNumber);
		rating = ScoreKeeper.ScoreRating.None;

		if (instrumentScore == null)
			return null;

		instrumentScore.NumGridPoints = numGridPoints;
		instrumentScore.TargetNoteCount = 0;
		instrumentScore.TimingDifference = 0;
		instrumentScore.DurationDifference = 0;
		instrumentScore.LocationDifference = 0;
		instrumentScore.UnrecordedCount = 0;

		instrumentScore.RecordedNoteCount = 0;
		instrumentScore.DudNoteCount = 0;

		var instrumentPlayer = NetworkManager.GetPlayerByInstrument(instrumentScore.InstrumentNumber);
		if (instrumentPlayer != null)
		{
			instrumentScore.PlayerNumber = instrumentPlayer.PlayerNumber.Value;
			instrumentScore.PlayerName = instrumentPlayer.PlayerName.Value;
		}

		int ratingScore = 0;

		foreach (var targetNote in targetTrack.Notes)
		{
			if (targetNote.InstrumentNumber != instrumentScore.InstrumentNumber)
				continue;

			instrumentScore.TargetNoteCount++;

			// calculate score for each target note (timing of note start and note duration)
			TrackNote recordedNoteLookup;
			if ((recordedNoteLookup = GetRecordedNoteNumber(recordedTrack, targetNote.InstrumentNumber, targetNote.NoteNumber)) != null)
				CalculateNoteScore(targetNote, recordedNoteLookup, instrumentScore, ref ratingScore);
			else
				instrumentScore.UnrecordedCount++;      // missing note number
		}

		foreach (var recordedNote in recordedTrack.Notes)
		{
			if (recordedNote.InstrumentNumber != instrumentScore.InstrumentNumber)
				continue;

			instrumentScore.RecordedNoteCount++;

			if (recordedNote.IsDudNote)
				instrumentScore.DudNoteCount++;
		}

        if (ratingScore > 0)
		    rating = GetRating(ratingScore);

		return instrumentScore;
	}


	// increment running score (instrumentScore)
	private void CalculateNoteScore(TrackNote targetNote, TrackNote recordedNote, InstrumentScore instrumentScore, ref int ratingScore)
	{
		var timingScore = Mathf.Abs(targetNote.TimelinePosition - recordedNote.TimelinePosition);

		var durationScore = Mathf.Abs(targetNote.NoteDurationMs - recordedNote.NoteDurationMs);
		var locationScore = (int)(recordedNote.TouchStartDistance * ScoreLocationFactor);
		var dudNoteScore = recordedNote.IsDudNote ? DudNotePenalty : 0;

		instrumentScore.TimingDifference += timingScore;
		instrumentScore.DurationDifference += durationScore;
		instrumentScore.LocationDifference += locationScore;

		ratingScore += (timingScore + durationScore + locationScore + dudNoteScore);

		//var timingScore = recordedNote.TimelinePosition - targetNote.TimelinePosition;
		//string earlyLate = (recordedNote.TimelinePosition - recordedNote.TimelinePosition < 0) ? "EARLY" : "LATE";

		//Debug.Log("ScoreNote: " + targetNote.InstrumentNumber + " note: " + targetNote.NoteNumber + " " + earlyLate + ", timingScore = " + Mathf.Abs(timingScore) + ", durationScore = "
		//						+ durationScore + ", locationScore = " + locationScore + ", ratingScore = " + ratingScore);
	}


	// story mode only
	// each note scored discretely on touch end
	public InstrumentScore ScoreStoryNote(int noteIndex, Track targetTrack, Track recordedTrack, out ScoreKeeper.ScoreRating rating)
	{
		var instrumentScore = new InstrumentScore();
		rating = ScoreKeeper.ScoreRating.None;

		instrumentScore.RecordedNoteCount = 1;

		if (noteIndex > targetTrack.Notes.Count - 1)
		{
            Debug.Log("ScoreStoryNote: noteIndex " + noteIndex + " not in targetTrack");
            instrumentScore.DudNoteCount = 1;
		}
		else
		{
			//Debug.Log("ScoreStoryNote: recordedTrack.Notes.Count " + recordedTrack.Notes.Count);
			TrackNote targetNote = targetTrack.Notes[noteIndex];
			TrackNote recordedNote = recordedTrack.Notes[noteIndex];
			int ratingScore = 0;

			if (recordedNote.IsDudNote)
				instrumentScore.DudNoteCount = 1;

			// calculate score for the target note (timing of note start and note duration)
			CalculateStoryNoteScore(targetNote, recordedNote, instrumentScore, ref ratingScore);

            string earlyLate = recordedNote.TimelinePosition < targetNote.TimelinePosition ? "EARLY" : "LATE";
            Debug.Log("ScoreStoryNote - noteIndex: " + noteIndex + " " + earlyLate + ", TimingDifference = " + instrumentScore.TimingDifference + ", DurationDifference = "
                                    + instrumentScore.DurationDifference + ", LocationDifference = " + instrumentScore.LocationDifference + ", ratingScore = " + ratingScore);

            if (ratingScore > 0)
			    rating = GetRating(ratingScore);
	    }

		return instrumentScore;
	}

	//// story mode only
	//// each note scored discretely on touch end
	//public InstrumentScore ScoreStoryNote(int noteIndex, Track targetTrack, Track recordedTrack, out ScoreKeeper.ScoreRating rating)
	//{
	//	var instrumentScore = new InstrumentScore();
	//	rating = ScoreKeeper.ScoreRating.None;

	//	if (noteIndex > targetTrack.Notes.Count - 1)
	//	{
	//		Debug.Log("ScoreStoryNote: targetTrack noteIndex " + noteIndex + " not valid!");
	//		instrumentScore.DudNoteCount = 1;
	//		return instrumentScore;
	//	}

 //       if (noteIndex > recordedTrack.Notes.Count - 1)
	//	{
	//		Debug.Log("ScoreStoryNote: recordedTrack noteIndex " + noteIndex + " not valid!");
	//		instrumentScore.DudNoteCount = 1;
	//		return instrumentScore;
	//	}

	//	//Debug.Log("ScoreStoryNote: recordedTrack.Notes.Count " + recordedTrack.Notes.Count);
	//	TrackNote targetNote = targetTrack.Notes[noteIndex];
	//	TrackNote recordedNote = recordedTrack.Notes[noteIndex];

	//	int ratingScore = 0;

	//	instrumentScore.RecordedNoteCount++;

	//	if (recordedNote.IsDudNote)
	//		instrumentScore.DudNoteCount = 1;

	//	// calculate score for the target note (timing of note start and note duration)
	//	CalculateStoryNoteScore(targetNote, recordedNote, instrumentScore, ref ratingScore);


	//	//string earlyLate = recordedNote.TimelinePosition < targetNote.TimelinePosition ? "EARLY" : "LATE";
	//	//Debug.Log("ScoreStoryNote: " + instrumentNumber + " noteIndex: " + noteIndex + " " + earlyLate + ", TimingDifference = " + instrumentScore.TimingDifference + ", DurationDifference = "
	//	//						+ instrumentScore.DurationDifference + ", LocationDifference = " + instrumentScore.LocationDifference + ", DudNotePenalty = " + (instrumentScore.DudNoteCount * SongScore.DudNotePenalty) + ", ratingScore = " + ratingScore);

	//	if (ratingScore > 0)
	//		rating = GetRating(ratingScore);

	//	return instrumentScore;
	//}

	// each note scored discretely
	private void CalculateStoryNoteScore(TrackNote targetNote, TrackNote recordedNote, InstrumentScore instrumentScore, ref int ratingScore)
	{
		var timingScore = Mathf.Abs(targetNote.TimelinePosition - recordedNote.TimelinePosition);

		var durationScore = Mathf.Abs(targetNote.NoteDurationMs - recordedNote.NoteDurationMs);
		var locationScore = (int)(recordedNote.TouchStartDistance * ScoreLocationFactor);

		instrumentScore.TimingDifference = timingScore;
		instrumentScore.DurationDifference = durationScore;
		instrumentScore.LocationDifference = locationScore;

		ratingScore = instrumentScore.StoryTotalScore;

		//var timingScore = recordedNote.TimelinePosition - targetNote.TimelinePosition;
		//string earlyLate = (recordedNote.TimelinePosition - recordedNote.TimelinePosition < 0) ? "EARLY" : "LATE";

		//Debug.Log("CalculateNoteScore: " + targetNote.InstrumentNumber + " note: " + targetNote.NoteNumber + " " + earlyLate + ", timingScore = " + Mathf.Abs(timingScore) + ", durationScore = "
		//						+ durationScore + ", locationScore = " + locationScore + ", ratingScore = " + ratingScore);
	}

	private ScoreKeeper.ScoreRating GetRating(int ratingScore)
	{
		//Debug.Log("GetRating: " + ratingScore);

		if (ratingScore <= PerfectScore)
			return ScoreKeeper.ScoreRating.Perfect;

        if (ratingScore <= NiceScore)
			return ScoreKeeper.ScoreRating.Nice;

		return ScoreKeeper.ScoreRating.None;
	}


	private void OnPlayCycleStart(ModeManager.GameMode gameMode)
	{
		// reset instrument new PB flags
		foreach (var instrument in InstrumentScores)
		{
			instrument.isNewPB = false;
		}
	}

	private TrackNote GetRecordedNoteNumber(Track recordedTrack, int instrumentNumber, int noteNumber)
	{
		return recordedTrack.Notes.Where(x => x.InstrumentNumber == instrumentNumber && x.NoteNumber == noteNumber).FirstOrDefault();
	}


	// JSON for leaderboard
	public string ToJson(bool prettyPrint)
	{
		return JsonUtility.ToJson(this, prettyPrint);
	}
}
