﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// enables the selected song's TrackManager
public class SongManager : MonoBehaviour
{
	public List<TrackManager> SongList;

	private void OnEnable()
	{
		SaimonseiEvents.OnSongSelected += OnSongSelected;
		SaimonseiEvents.OnTrackStop += OnTrackStop;
	}

	private void OnDisable()
	{
		SaimonseiEvents.OnSongSelected -= OnSongSelected;
		SaimonseiEvents.OnTrackStop -= OnTrackStop;
	}

	private void Start()
	{
		//DisableAllSongs();
	}


	private void OnSongSelected(string songTitle, string songEventName, PlayArea.PlayMode playMode, int gridWidth, int gridHeight, float tolerance)
	{
		foreach (var song in SongList)
		{
			song.gameObject.SetActive(song.SongEventName == songEventName);
		}
	}

	private void DisableAllSongs()
	{
		foreach (var song in SongList)
		{
			song.gameObject.SetActive(false);
		}
	}

	private void OnTrackStop(string songEventName)
	{
		DisableAllSongs();
	}
}
