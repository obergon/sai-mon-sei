﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GridDot : MonoBehaviour
{
    public List<ParticleSystem> Sparkles;

	private float StartScale = 1f;
	private float MaxExpandScale = 5f;
	private float MaxExpandTime = 0.25f;           // seconds
	private float ResetTime = 0.2f;             // seconds

	private LTDescr sparkleExpandTween;

	private Vector3 currentSparkleScale;


	public void SetColour(Color colour)
	{
		GetComponent<Image>().color = colour;

		foreach (var sparkle in Sparkles)
		{
			var main = sparkle.main;
			main.startColor = colour;
		}
	}

	public void SetSparkleScale(Vector3 scale)
	{
		currentSparkleScale = scale;

		foreach (var sparkle in Sparkles)
		{
			sparkle.transform.localScale = scale;
		}
	}

	public void ExpandSparkles(float durationMs, bool fromZero)
	{
		StopSparkleExpand();

		var startScale = fromZero ? Vector3.zero : new Vector3(StartScale, StartScale);
		transform.localScale = startScale;

		var duration = durationMs / 1000f;                  // seconds
		var targetScale = (duration >= MaxExpandTime) ? MaxExpandScale : MaxExpandScale * (duration / MaxExpandTime);

		//sparkleExpandTween = LeanTween.value(gameObject, Vector3.one * StartScale, Vector3.one * targetScale, duration)
		sparkleExpandTween = LeanTween.value(gameObject, startScale, Vector3.one * targetScale, duration)
										.setEase(LeanTweenType.easeOutQuart)
							            .setOnUpdate((Vector3 x) => { SetSparkleScale(x); })
							            .setOnComplete(() => { ResetSparkleScale(fromZero); });
	}

	// expand, waiting for ResetScale (eg. OnTouchUp)
	public void ExpandSparkles()
	{
		StopSparkleExpand();
		transform.localScale = new Vector2(StartScale, StartScale);

		sparkleExpandTween = LeanTween.value(gameObject, Vector3.one * StartScale, Vector3.one * MaxExpandScale, MaxExpandTime)
								.setEase(LeanTweenType.easeOutQuart)
								.setOnUpdate((Vector3 x) => { SetSparkleScale(x); });
	}

	public void ResetSparkleScale(bool toZero)
	{
		StopSparkleExpand();

        var targetScale = toZero ? Vector3.zero : Vector3.one * StartScale;

		LeanTween.value(gameObject, currentSparkleScale, targetScale, ResetTime)
								.setEase(LeanTweenType.easeInQuart)
								.setOnUpdate((Vector3 x) => { SetSparkleScale(x); });
    }

	public void StopSparkleExpand()
	{
		if (sparkleExpandTween != null)
		{
			LeanTween.cancel(sparkleExpandTween.id);
			sparkleExpandTween = null;
		}
	}

    public void PulseSparkles(float pulseScale = 4f, float pulseTime = 0.25f)
    {
        foreach (var sparkle in Sparkles)
        {
            LeanTween.scale(sparkle.gameObject, Vector3.one * pulseScale, pulseTime)
                        .setEase(LeanTweenType.easeOutBack)
                        .setLoopPingPong(1);
        }
    }
}
