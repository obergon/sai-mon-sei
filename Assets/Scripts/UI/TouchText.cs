﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TouchText : MonoBehaviour
{
	public Text Message;
	public Text Score;

	private const float scorePulseScale = 2f;
	private const float messagePulseScale = 3f;
	private const float textPulseTime = 0.3f;
	private const float textDelayTime = 0.1f;
	private const float scoreMoveTime = 1f;


	private const float destroyDelayTime = 1f;

	private Text gameScoreText;
	private int runningScore;


	public void ScoreFeedback(Color colour, string message, int runningScore, int scoreIncrement, Text gameScore)
	{
		if (string.IsNullOrEmpty(message) && scoreIncrement <= 0)        // nothing to show!
		{
			Destroy(gameObject, destroyDelayTime);
			return;
		}

		Message.color = colour;
		Score.color = colour;
		Message.transform.localScale = Vector3.zero;
		Score.transform.localScale = Vector3.zero;

		gameScoreText = gameScore;
		this.runningScore = runningScore;

		Message.text = string.IsNullOrEmpty(message) ? "" : message;
		Score.text = scoreIncrement > 0 ? scoreIncrement.ToString() : "";

		// if there is a score, pulse it, update the game score text, pulse the message if present and then destroy object
		if (Score.text != "")
			PulseScore();
		// else just pulse the message and then destroy the object
		else if (Message.text != "")
			PulseMessage(true);
	}

    private void PulseScore()
	{
		if (Score.text == "")
			return;

		LeanTween.scale(Score.gameObject, Vector3.one * scorePulseScale, textPulseTime)
								.setEase(LeanTweenType.easeOutQuart)
								.setDelay(textDelayTime)
								.setOnComplete(() =>
								{
									UpdateGameScore();      // moves to game score + destroys object on completion
									PulseMessage(false);
	                            });
    }

	private void UpdateGameScore()
	{
		// shrink score
		LeanTween.scale(Score.gameObject, Vector3.zero, scoreMoveTime)
					            .setEase(LeanTweenType.easeInQuart);

		if (gameScoreText == null)
			return;

		// move score to game score
		LeanTween.move(Score.gameObject, gameScoreText.transform.position, scoreMoveTime)
							    .setEase(LeanTweenType.easeInQuart)
							    .setOnComplete(() =>
							    {
									// update game score
									gameScoreText.transform.localScale = Vector3.one;

									LeanTween.scale(gameScoreText.gameObject, Vector3.one * scorePulseScale, textPulseTime)
							                .setEase(LeanTweenType.easeInQuart)
							                .setOnComplete(() =>
							                {
								                gameScoreText.text = runningScore.ToString();

												LeanTween.scale(gameScoreText.gameObject, Vector3.one, textPulseTime * 0.5f)        // snap back
															.setEase(LeanTweenType.easeInQuart)
															.setOnComplete(() =>
															{
																Destroy(gameObject, destroyDelayTime);
															});
							                });
							    });
	}

	private void PulseMessage(bool destroyOnComplete)
    {
		if (Message.text == "")
			return;

		LeanTween.scale(Message.gameObject, Vector3.one * messagePulseScale, textPulseTime)
						        .setEase(LeanTweenType.easeOutQuart)
								.setDelay(textDelayTime)
								.setLoopPingPong(1)
						        .setOnComplete(() =>
						        {
									if (destroyOnComplete)      // if score present, object destroyed on game score update
										Destroy(gameObject, destroyDelayTime);
						        });
    }
}
