﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : UIPanel
{
    [Space]
    public Text PlayerName;

    public Image NameInputPanel;
    public InputField PlayerNameInput;
    public Button SubmitNameButton;
    public Text NameMessage;

    public GameObject SongSelector;
    public Transform SongContents;

    public Slider GridWidthSlider;		// width in dots
    public Text GridWidthValue;		

    public Slider GridHeightSlider;		// height in dots
    public Text GridHeightValue;

    public Slider ToleranceSlider;
    public Text ToleranceValue;

    public Button BackButton;
    public Button LeaderboardButton;

    //public AudioSource MenuMusic;
    //public AudioSource Wonderland;

    public AudioClip ErrorAudio;

    private int gridWidth;
    private int gridHeight;
    private float tolerance;

    private bool leaderboardShowing = false;

    public PlayerProfile PlayerProfile;


    private void OnEnable()
	{
        SaimonseiEvents.OnLookupPlayerResult += OnLookupPlayerResult;
        SaimonseiEvents.OnPlayerNameSaveResult += OnPlayerNameSaved;

        SaimonseiEvents.OnSongClicked += OnSongClicked;
        SaimonseiEvents.OnJoinGame += OnJoinGame;
        SaimonseiEvents.OnLeaveGame += OnLeaveGame;
        SaimonseiEvents.OnHostEndedGame += OnHostEndedGame;

        SaimonseiEvents.OnSongSelectedNetwork += StartSong;

        SaimonseiEvents.OnNetworkPlayerEnteredGame += OnNetworkPlayerEnteredGame;
        SaimonseiEvents.OnNetworkPlayerLeftGame += OnNetworkPlayerLeftGame;

        GridWidthSlider.onValueChanged.AddListener(OnGridWidthChanged);
        GridHeightSlider.onValueChanged.AddListener(OnGridHeightChanged);
        ToleranceSlider.onValueChanged.AddListener(OnToleranceChanged);

        PlayerNameInput.onValueChanged.AddListener(OnPlayerNameChanged);

        SubmitNameButton.onClick.AddListener(OnSubmitNameButtonClicked);
        LeaderboardButton.onClick.AddListener(OnLeaderboardButtonClicked);
        BackButton.onClick.AddListener(OnBackButtonClicked);

        GetPlayerName();
        InitSliderValues();

        //MenuMusic.Play();
    }

    private void OnDisable()
    {
        SaimonseiEvents.OnLookupPlayerResult -= OnLookupPlayerResult;
        SaimonseiEvents.OnPlayerNameSaveResult -= OnPlayerNameSaved;

        SaimonseiEvents.OnSongClicked -= OnSongClicked;
        SaimonseiEvents.OnJoinGame -= OnJoinGame;
        SaimonseiEvents.OnLeaveGame -= OnLeaveGame;
        SaimonseiEvents.OnHostEndedGame -= OnHostEndedGame;

        SaimonseiEvents.OnSongSelectedNetwork -= StartSong;

        SaimonseiEvents.OnNetworkPlayerEnteredGame -= OnNetworkPlayerEnteredGame;
        SaimonseiEvents.OnNetworkPlayerLeftGame -= OnNetworkPlayerLeftGame;

        GridWidthSlider.onValueChanged.RemoveListener(OnGridWidthChanged);
        GridHeightSlider.onValueChanged.RemoveListener(OnGridHeightChanged);
        ToleranceSlider.onValueChanged.RemoveListener(OnToleranceChanged);

        PlayerNameInput.onValueChanged.RemoveListener(OnPlayerNameChanged);

        SubmitNameButton.onClick.RemoveListener(OnSubmitNameButtonClicked);
        LeaderboardButton.onClick.RemoveListener(OnLeaderboardButtonClicked);
        BackButton.onClick.RemoveListener(OnBackButtonClicked);

        //MenuMusic.Stop();
        //Wonderland.Stop();
    }


    // init values (in case sliders are not changed)
    private void InitSliderValues()
	{
        OnGridWidthChanged(GridWidthSlider.value);
        OnGridHeightChanged(GridHeightSlider.value);
        OnToleranceChanged(ToleranceSlider.value);
    }

	// joined game as client - disable song buttons (only host can select song (ie. start the game)
    private void OnJoinGame(string ipAddress)
    {
        EnableSongs(false);
    }

    // enable song buttons
    private void OnLeaveGame(ulong clientID)
    {
        EnableSongs(true);
    }

    private void EnableSongs(bool enable)
    {
        foreach (Transform song in SongContents)
        {
            var songButton = song.GetComponent<SongButton>();
            songButton.PlaySongButton.interactable = enable;
        }
    }


    private void OnNetworkPlayerEnteredGame(NetworkPlayer player)
    {
        EnableSongs(NetworkManager.GetLocalPlayer.IsOwnedByServer && NetworkManager.IsValidPlayerCount);
    }

    private void OnNetworkPlayerLeftGame(ulong clientID)
    {
        EnableSongs(NetworkManager.GetLocalPlayer.IsOwnedByServer && (NetworkManager.IsValidPlayerCount || NetworkManager.PlayerCount == 0));
    }

    private void OnHostEndedGame()
    {
        //TODO: anything?
    }

    private void StartSong(string songTitle, string songEventName)
    {
        if (NetworkManager.IsMultiPlayerGame)
            NetworkManager.ResetCurrentPlayer();

        SaimonseiEvents.OnSongSelected?.Invoke(songTitle, songEventName, PlayArea.PlayMode.LearnSong, gridWidth, gridHeight, tolerance);
        SaimonseiEvents.OnStartNewGame?.Invoke(songEventName, PlayArea.PlayMode.LearnSong);

        bool canStartGame = NetworkManager.IsMultiPlayerGame ? NetworkManager.GetLocalPlayer.IsOwnedByServer : true;
        //SaimonseiEvents.OnStatusUpdated?.Invoke("[ Ready ]", (canStartGame ? "Hit Play to Start!" : (NetworkManager.IsMultiPlayerGame ? "Waiting for Host..." : "")), TrackManager.PlayMode.Waiting);
        SaimonseiEvents.OnStatusUpdated?.Invoke("[ Ready ]", (canStartGame ? "Tap to Start!" : (NetworkManager.IsMultiPlayerGame ? "Waiting for Host..." : "")), TrackManager.PlayMode.Waiting);

        if (PlayerProfile.PlayCount == 0)
            PlayerProfile.FirstPlay = DateTime.Now;

        PlayerProfile.PlayCount++;
        PlayerProfile.LastPlay = DateTime.Now;

        SaimonseiEvents.OnSavePlayerStats?.Invoke(PlayerProfile);
    }

    private void OnSongClicked(string songTitle, string songEventName)
    {
        if (NetworkManager.IsMultiPlayerGame)
        {
            if (NetworkManager.GetLocalPlayer.IsOwnedByServer)
                SaimonseiEvents.OnStartSongServer?.Invoke(songTitle, songEventName);     // server invokes RPC StartSong on all clients
        }
        else
            StartSong(songTitle, songEventName);
    }

    private void OnLeaderboardButtonClicked()
    {
        leaderboardShowing = !leaderboardShowing;
        SongSelector.SetActive(!leaderboardShowing);

        SaimonseiEvents.OnToggleLeaderboard?.Invoke(NetworkManager.IsMultiPlayerGame ?
                                            LeaderboardUI.LeaderboardType.Group : LeaderboardUI.LeaderboardType.Player);
    }


    private void OnBackButtonClicked()
    {
        SaimonseiEvents.OnStartMenu?.Invoke();
    }


    private void GetPlayerName()
    {
        if (!string.IsNullOrEmpty(PlayerProfile.PlayerName))     // name already entered - only recorded in PlayerProfile on successful db save
        {
            SetPlayerName();
        }
        else
        {
            NameInputPanel.gameObject.SetActive(true);
            LeaderboardButton.interactable = false;

            //EnableSongs(false);
        }
    }

    private void SetPlayerName()
    {
        NameInputPanel.gameObject.SetActive(false);
        LeaderboardButton.interactable = true;

        //EnableSongs(true);

        PlayerName.text = PlayerProfile.PlayerName;
    }

    private void OnSubmitNameButtonClicked()
    {
        string playerName = PlayerNameInput.text;

        if (string.IsNullOrEmpty(playerName))
            return;

        // check db for duplicate
        SaimonseiEvents.OnLookupPlayerName?.Invoke(playerName);
        SubmitNameButton.interactable = false;
    }

    // async callback
    private void OnLookupPlayerResult(string playerName, bool found)
    {
        if (!found)              // save player profile
        {
            SaimonseiEvents.OnSavePlayerName?.Invoke(playerName, PlayerProfile);
            NameMessage.text = "";
        }
        else
        {
            PlaySFX(ErrorAudio);
            //if (ErrorAudio != null)
            //    AudioSource.PlayClipAtPoint(ErrorAudio, Vector3.zero);

            NameMessage.text = "'" + playerName + "' is already taken!";
        }

        SubmitNameButton.interactable = true;
    }

    // async callback
    private void OnPlayerNameSaved(string playerName, bool success)
    {
        if (success)
        {
            PlayerName.text = playerName;
            PlayerProfile.PlayerName = playerName;

            SetPlayerName();
        }
    }


    private void OnGridWidthChanged(float value)
    {
        gridWidth = (int)value;
        GridWidthValue.text = gridWidth.ToString();
    }

    private void OnGridHeightChanged(float value)
    {
        gridHeight = (int)value;
        GridHeightValue.text = gridHeight.ToString();
    }

    private void OnToleranceChanged(float value)
    {
        tolerance = value;
        ToleranceValue.text = value.ToString();
    }


    // remove any unwanted chars (eg. firebase illegal), profanity, etc.
    private void OnPlayerNameChanged(string name)
    {
        var cleanPlayerName = LeaderboardManager.CleanPlayerName(name);
        if (cleanPlayerName != name)       // illegal chars stripped out
        {
            PlaySFX(ErrorAudio);
            //if (ErrorAudio != null)
            //    AudioSource.PlayClipAtPoint(ErrorAudio, Vector3.zero);

            PlayerNameInput.text = cleanPlayerName;
        }
    }


    public override void Show(bool visible)
    {
        base.Show(visible);

        if (visible)
        {
            if (!IsBGMPlaying)
                PlayBGM();
        }
        else
        {
            StopBGM();
        }
    }
}
