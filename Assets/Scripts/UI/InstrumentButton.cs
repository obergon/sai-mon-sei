﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


// button showing instrument, personal best score and player name (multiplayer)
// with results panel
public class InstrumentButton : MonoBehaviour
{
    public NetworkPlayer NetworkPlayer { get; set; }

	public Button Button;
	public Image ButtonImage;
	public Image HostImage;
	public Image TurnImage;    
	public Image LocalImage;

    [Space]
    public Text Instrument;
	public Text PlayerName;

    [Space]
    public Text PBPlayerName;       // multiplayer
    public Text PersonalBest;

    [Space]
    public Image ResultsPanel;
    public Text Rank;
    public Text TotalScore;
    public Text Timing;
    public Text Duration;
    public Text Location;
    public Text Notes;

    [Header("Audio")]
    public AudioSource ButtonAudioSource;
    public AudioClip FlipAudio;
    public AudioClip FlippedAudio;

    [Space]
    public int InstrumentNumber;
    public ulong ClientID;

    private float flipTime = 0.3f;

    private Color buttonColour;
    private Color fullColour;


    // rotate 360 around x axis
	public void Flip(bool flash)
	{
        PlaySFX(FlipAudio);
        //if (FlipAudio != null)
        //    AudioSource.PlayClipAtPoint(FlipAudio, Vector3.zero);

        var button = Button.GetComponent<RectTransform>();

        // rotate whole component - including (results)
        LeanTween.rotateAroundLocal(button, Vector3.left, 180f, flipTime)
                    .setEaseInCubic()
                    .setOnComplete(() =>
                    {
                        PlaySFX(FlippedAudio);
                        //if (FlippedAudio != null)
                        //    AudioSource.PlayClipAtPoint(FlippedAudio, Vector3.zero);

                        LeanTween.rotateAroundLocal(button, Vector3.left, 180f, flipTime)
                                    .setEaseOutCubic()
                                    .setOnComplete(() =>
                                    {
                                        if (flash)
                                            FlashButton();

                                        PlaySFX(FlippedAudio);
                                        //if (FlippedAudio != null)
                                        //    AudioSource.PlayClipAtPoint(FlippedAudio, Vector3.zero);
                                    });
                    });
    }

    public void FlipIn(float initAngle)
    {
        PlaySFX(FlipAudio);
        //if (FlipAudio != null)
        //    AudioSource.PlayClipAtPoint(FlipAudio, Vector3.zero);

        var button = Button.GetComponent<RectTransform>();

        LeanTween.rotateAroundLocal(button, Vector3.left, 180f + initAngle, flipTime)
                    .setEaseInCubic()
                    .setOnComplete(() =>
                    {
                        PlaySFX(FlippedAudio);
                        //if (FlippedAudio != null)
                        //    AudioSource.PlayClipAtPoint(FlippedAudio, Vector3.zero);

                        LeanTween.rotateAroundLocal(button, Vector3.left, 180f, flipTime)
                                    .setEaseOutCubic()
                                    .setOnComplete(() =>
                                    {
                                        FlashButton();
                                        PlaySFX(FlippedAudio);
                                        //if (FlippedAudio != null)
                                        //    AudioSource.PlayClipAtPoint(FlippedAudio, Vector3.zero);
                                    });
                    }
                    );
    }

    public void FlipInResults(float initAngle)
    {
        PlaySFX(FlipAudio);
        //if (FlipAudio != null)
        //    AudioSource.PlayClipAtPoint(FlipAudio, Vector3.zero);

        LeanTween.rotateAroundLocal(ResultsPanel.GetComponent<RectTransform>(), Vector3.left, 180f + initAngle, flipTime)
                    .setEaseInCubic()
                    .setOnComplete(() =>
                    {
                        //FlashButton();

                        PlaySFX(FlippedAudio);
                        //if (FlippedAudio != null)
                        //    AudioSource.PlayClipAtPoint(FlippedAudio, Vector3.zero);

                        LeanTween.rotateAroundLocal(ResultsPanel.GetComponent<RectTransform>(), Vector3.left, 180f, flipTime)
                                    .setEaseOutCubic()
                                    .setOnComplete(() =>
                                    {
                                        FlashButton();
                                        PlaySFX(FlippedAudio);
                                        //if (FlippedAudio != null)
                                        //    AudioSource.PlayClipAtPoint(FlippedAudio, Vector3.zero);
                                    });
                    }
                    );
    }

    public void FlipTurn(bool isTurn)
    {
        PlaySFX(FlipAudio);
        //if (FlipAudio != null)
        //    AudioSource.PlayClipAtPoint(FlipAudio, Vector3.zero);

        LeanTween.rotateAroundLocal(Button.GetComponent<RectTransform>(), Vector3.left, 180f, flipTime)
                    .setEaseInCubic()
                    .setOnComplete(() =>
                        {
                            TurnImage.enabled = isTurn;

                            if (isTurn)
                                FullColour();
                            else
                                SemiColour();

                            PlaySFX(FlippedAudio);
                            //if (FlippedAudio != null)
                            //    AudioSource.PlayClipAtPoint(FlippedAudio, Vector3.zero);

                            LeanTween.rotateAroundLocal(Button.GetComponent<RectTransform>(), Vector3.left, 180f, flipTime)
                                        .setEaseOutCubic()
                                        .setOnComplete(() =>
                                        {
                                            PlaySFX(FlippedAudio);
                                            //if (FlippedAudio != null)
                                            //    AudioSource.PlayClipAtPoint(FlippedAudio, Vector3.zero);
                                        });
                        }
                    );
    }


    public void SetColour(Color colour)
    {
        buttonColour = colour;
        fullColour = new Color(buttonColour.r, buttonColour.g, buttonColour.b, 1f);

        ButtonImage.color = buttonColour;
    }

    public void FlashButton()
    {
        LeanTween.value(Button.gameObject, buttonColour, fullColour, flipTime)
                    .setEaseInCubic()
                    .setOnUpdate((Color val) =>
                    {
                        ButtonImage.color = val;
                    })
                    .setOnComplete(() =>
                    {
                        SemiColour();
                    }
                    );
    }

    public void FullColour()
    {
        LeanTween.value(Button.gameObject, buttonColour, fullColour, flipTime)
                    .setEaseInCubic()
                    .setOnUpdate((Color val) =>
                    {
                        ButtonImage.color = val;
                    });
    }

    public void SemiColour()
    {
        LeanTween.value(Button.gameObject, fullColour, buttonColour, flipTime)
                    .setEaseOutCubic()
                    .setOnUpdate((Color val) =>
                    {
                        ButtonImage.color = val;
                    });
    }

    public void PlayerLeftGame()
    {
        Button.interactable = false;
        TurnImage.enabled = false;
    }


    private void PlaySFX(AudioClip sfx)
    {
        if (sfx == null)
            return;

        ButtonAudioSource.clip = sfx;
        ButtonAudioSource.Play();
    }
}
