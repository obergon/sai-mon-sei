﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;


/// <summary>
/// UI for game visualisation and player interaction
/// </summary>
public class PlayArea : UIPanel
{
	[Space]
	public GameObject UIPanel;

	[Header("Background")]
	public Image BackgroundTint;
	public Color BackgroundColour;
	public Color BeatPulseColour;
	public Image Blackout;
	public Color BlackoutColour;

	[Header("Buttons")]
	public Button PlayTrackButton;
	public Text PlayTrackText;
	public Button MainMenuButton;			// leave game
	public Button ConfirmLeaveButton;
	public float ConfirmTimeout = 3f;

	[Header("Leaderboard")]
	public Button LeaderboardButton;
	public Image LeaderboardPanel;

	[Header("Instruments")]
	public Image InstrumentPanel;              // list of InstrumentButtons 
	public Image InstrumentPanelHeader;        //'personal best' 
	public GameObject InstrumentPlayers;       // scroll view content
	public InstrumentButton InstrumentPrefab;       // for scroll view content

	[Header("Player Colours")]
	public Color LocalPlayerColour;
	public Color OtherPlayerColour;
	public Color WinnerColour;
	public Color NewPBColour;
	public Color PlayedResultsColour;        // for results - if instrument was played by this player
	public Color NotPlayedResultsColour;

    [Header("Audio")]
	public AudioClip DudNote;
    [Space]
	public AudioClip NewInstrumentAudio;
	public AudioClip StatusChangeAudio;
	public AudioClip NewHiScoreAudio;
	public AudioClip CountDownAudio;

	public AudioClip YourTurnAudio;
	public AudioClip TheirTurnAudio;

	public AudioClip GridPointAnimateAudio;
	public AudioClip GridPointArriveAudio;

	[Header("Song")]
	public Text SongTitle;
	public Text CurrentInstrument;

	[Header("Score")]
	public Image ScorePanel;
	public Text Score;
	public Image ResultsHeaders;

	[Header("Prompts")]
	public Text Status;
	public Text Prompt;

	[Header("Status Colours")]
	public Color StatusColour = Color.magenta;    
	public Color PlayColour = Color.green; 
	public Color RecordingColour = Color.red;
	public Color PlaybackColour = Color.yellow;

	[Header("Multiplayer")]
	public Text YourTurn;
	public Image YourTurnImage;

	public Text NextPlayerIn;
	public Text CountDown;

	[Header("Beat")]
	public Text CurrentTempo;
	public Text CurrentBeatBar;
	public Image BeatDot;

	[Header("Grid")]
	public Image GridBackground;
	public Image GridPanel;
	//public Slider PitchSlider;

    public List<Color> GridPointColours;
	private Color RandomGridPointColour { get { return GridPointColours[UnityEngine.Random.Range(0, GridPointColours.Count)]; } }

	public List<Material> GridPointMaterials;
	private Material RandomGridPointMaterial { get { return GridPointMaterials[UnityEngine.Random.Range(0, GridPointMaterials.Count)]; } }

	public GameObject GridDotPrefab;
	public GridPoint[,] GridPoints { get; private set; }        // 2 dimensional.. x/y

	public RipplePool RipplePool;
	public GameObject TouchTextPrefab;
    public GameObject TouchScores;          // container only

    public Color ValidRippleColour;
	public Color InvalidRippleColour;
	public Color ScoreRippleColour;
	public Color RealmRippleColour;

	public Color DudNoteColour;         // for score feedback text
	public Color ScoreColour;
	public Color NiceScoreColour;
	public Color PerfectScoreColour;

	public Color FeedbackColour = Color.cyan;

	public float BeatDotFlashTime = 0.5f;
	public Text DebugText;

	public PlayerProfile PlayerProfile;

	[Header("Story Mode")]
	public Image StoryPanel;                 // searching or learning
	public Text StoryText;                   // searching or learning
	public Color StoryPanelColour;
	public Color StoryTextColour;

	public Image GameOverPanel;                // land zero health
	public Text GameOverText;                // land zero health
	public Color GameOverPanelColour;
	public Color GameOverTextColour;

	public Text StoryTimeRemaining;
	public Text StoryCountDown;
	public Button StoryNextButton;          // story mode
	public Text StoryNextText;              // story mode

	public bool ShowStoryScores;

	[Space]
	[Header("Saved Data")]
	[Tooltip("Data saved between game sessions")]
	public SavedGameState SavedGameState;               // land health, pocket crystals, realm and layer attempts, completed flag, etc

	[Space]
	[Header("Skip Layers (if completed)")]
	public bool CanSkipLayers = true;        // skip to next layer
	public Button StorySkipButton;

	private bool completedLayer;                // previously completed

	private bool learnRealmSongFailed;
	private ModeManager.GameMode gameMode;      // leaderboard or story

	private string selectedSongEvent;
	private string selectedSongTitle;
	private string currentInstrumentName;

	private int trackLength;
	private float accuracyTolerance;
	private SongScore selectedSongScore;
	private float initInstrumentDelay = 0.3f;

	private const float pulseScale = 1.25f;
	private const float pulseTime = 0.25f;

	private const float scoreRippleTimeFactor = 0.75f;
	private const float scoreRippleScaleFactor = 0.75f;

	private const float textPulseScale = 1.15f;

	private float countdownPulseScale = 1.5f;
	private float countdownPulseTime = 0.2f;

	private const float resultsPauseTime = 0.5f;

	private const float backgroundAlpha = 0.1f;    // when pulsing to beat

	private const float gridAnimateTime = 0.75f;
	//private const float dotPulseFactor = 3f;   // grid point animation

	private bool leaderboardShowing = false;

	private const float minGridPointDelay = 0.05f;      // grid point animation
	private const float maxGridPointDelay = 0.5f;       // grid point animation
	private const float gridInitDelay = 1f;             // grid point animation

	private ulong clientID;

	private List<LeaderboardInstrPlayerScores> groupSongInstrPBScores = null;     // multiplayer only - list of leaderboard scores for song/group - for PB updates


	public Vector3[] GridCorners
	{
		get
		{
			Vector3[] gridCorners = new Vector3[4];
			GridPanel.rectTransform.GetWorldCorners(gridCorners);
			return gridCorners;   // world space corner coords
		}
	}

	public enum PlayMode
	{
        None,
        RevealLayers,           // realm layers
        LearnSong               // core game loop
	}

	private PlayMode currentPlayMode;
	private Goddess.RealmState realmState;      // Story Mode


	private void OnEnable()
	{
		SaimonseiEvents.OnGameModeSelected += OnGameModeSelected;

		SaimonseiEvents.OnSongSelected += OnSongSelected;
		SaimonseiEvents.OnStartNewGame += OnStartNewGame;

		SaimonseiEvents.OnPlay += StartPlayCycle;           // play button clicked

		SaimonseiEvents.OnStatusUpdated += OnStatusUpdated;
		SaimonseiEvents.OnSongScoreLookup += OnSongScoreLookup;
		SaimonseiEvents.OnNewPersonalBest += OnNewPersonalBest;

		SaimonseiEvents.OnBackingTrackCreated += OnBackingTrackCreated;
		SaimonseiEvents.OnInitInstruments += OnInitInstruments;

		SaimonseiEvents.OnTrackPlay += OnTrackPlay;
		SaimonseiEvents.OnTrackStop += OnTrackStop;
		SaimonseiEvents.OnTrackEnd += OnTrackEnd;
		SaimonseiEvents.OnCreateRipple += OnCreateRipple;
		SaimonseiEvents.OnTouchScore += OnTouchScore;

		SaimonseiEvents.OnInstrumentChanged += OnInstrumentChanged;
		SaimonseiEvents.OnPlayCycleDone += OnPlayCycleDone;

		SaimonseiEvents.OnTimelineBeat += OnTimelineBeat;
		SaimonseiEvents.OnDudNote += OnDudNote;

		SaimonseiEvents.OnNextPlayerCountdown += OnNextPlayerCountdown;

		SaimonseiEvents.OnNetworkPlayerEnteredGame += OnNetworkPlayerEnteredGame;
		SaimonseiEvents.OnNetworkPlayerLeftGame += OnNetworkPlayerLeftGame;
		SaimonseiEvents.OnHostEndedGame += OnHostEndedGame;

        SaimonseiEvents.OnStartPlayCycleNetwork += StartPlayCycle;       // 'play' button clicked (full circle)
		SaimonseiEvents.OnCurrentPlayerChanged += OnCurrentPlayerChanged;

        SaimonseiEvents.OnGroupSongScoresRetrieved += OnGroupSongScoresRetrieved;

        SaimonseiEvents.OnDebug += OnDebug;

		MainMenuButton.onClick.AddListener(OnMenuButtonClicked);
		ConfirmLeaveButton.onClick.AddListener(ConfirmLeaveButtonClicked);
		PlayTrackButton.onClick.AddListener(OnPlayTrackButtonClicked);
		LeaderboardButton.onClick.AddListener(OnLeaderboardButtonClicked);

		// story mode
		SaimonseiEvents.OnEnterRealmLayer += OnEnterRealmLayer;
		SaimonseiEvents.OnRealmStateChanged += OnRealmStateChanged;
		SaimonseiEvents.OnRealmRevealStateChanged += OnRealmRevealStateChanged;
        SaimonseiEvents.OnRealmRevealCountdown += OnRealmRevealCountdown;
        SaimonseiEvents.OnAllRealmLayersRevealed += OnAllRealmLayersRevealed;
		SaimonseiEvents.OnLearnRealmSongFailed += OnLearnRealmSongFailed;

		StoryNextButton.onClick.AddListener(OnStoryNextClicked);

		if (CanSkipLayers)
			StorySkipButton.onClick.AddListener(OnStorySkipClicked);

		//PitchSlider.onValueChanged.AddListener(OnPitchChanged);

		Show(false);
	}

    private void OnDisable()
	{
		SaimonseiEvents.OnGameModeSelected -= OnGameModeSelected;

		SaimonseiEvents.OnSongSelected -= OnSongSelected;
		SaimonseiEvents.OnStartNewGame -= OnStartNewGame;

		SaimonseiEvents.OnPlay -= StartPlayCycle;           // play button clicked

		SaimonseiEvents.OnStatusUpdated -= OnStatusUpdated;
		SaimonseiEvents.OnSongScoreLookup -= OnSongScoreLookup;
		SaimonseiEvents.OnNewPersonalBest -= OnNewPersonalBest;

		SaimonseiEvents.OnBackingTrackCreated -= OnBackingTrackCreated;
		SaimonseiEvents.OnInitInstruments -= OnInitInstruments;
		SaimonseiEvents.OnTrackPlay -= OnTrackPlay;
		SaimonseiEvents.OnTrackStop -= OnTrackStop;
		SaimonseiEvents.OnTrackEnd -= OnTrackEnd;
		SaimonseiEvents.OnCreateRipple -= OnCreateRipple;
		SaimonseiEvents.OnTouchScore -= OnTouchScore;

		SaimonseiEvents.OnInstrumentChanged -= OnInstrumentChanged;
		SaimonseiEvents.OnPlayCycleDone -= OnPlayCycleDone;

		SaimonseiEvents.OnTimelineBeat -= OnTimelineBeat;
		SaimonseiEvents.OnDudNote -= OnDudNote;

		SaimonseiEvents.OnNextPlayerCountdown -= OnNextPlayerCountdown;

		SaimonseiEvents.OnNetworkPlayerEnteredGame -= OnNetworkPlayerEnteredGame;
		SaimonseiEvents.OnNetworkPlayerLeftGame -= OnNetworkPlayerLeftGame;
		SaimonseiEvents.OnHostEndedGame -= OnHostEndedGame;

        SaimonseiEvents.OnStartPlayCycleNetwork -= StartPlayCycle;       // full circle
		SaimonseiEvents.OnCurrentPlayerChanged -= OnCurrentPlayerChanged;

        SaimonseiEvents.OnGroupSongScoresRetrieved -= OnGroupSongScoresRetrieved;

		SaimonseiEvents.OnDebug -= OnDebug;

		MainMenuButton.onClick.RemoveListener(OnMenuButtonClicked);
		ConfirmLeaveButton.onClick.RemoveListener(ConfirmLeaveButtonClicked);
		PlayTrackButton.onClick.RemoveListener(OnPlayTrackButtonClicked);
		LeaderboardButton.onClick.RemoveListener(OnLeaderboardButtonClicked);

		// story mode
		SaimonseiEvents.OnEnterRealmLayer -= OnEnterRealmLayer;
		SaimonseiEvents.OnRealmStateChanged -= OnRealmStateChanged;
		SaimonseiEvents.OnRealmRevealStateChanged -= OnRealmRevealStateChanged;
        SaimonseiEvents.OnRealmRevealCountdown -= OnRealmRevealCountdown;
        SaimonseiEvents.OnAllRealmLayersRevealed -= OnAllRealmLayersRevealed;
		SaimonseiEvents.OnLearnRealmSongFailed -= OnLearnRealmSongFailed;

		StoryNextButton.onClick.RemoveListener(OnStoryNextClicked);

		if (CanSkipLayers)
			StorySkipButton.onClick.RemoveListener(OnStorySkipClicked);

		//PitchSlider.onValueChanged.RemoveListener(OnPitchChanged);
	}

    public override void Show(bool visible)
	{
		UIPanel.SetActive(visible);
	}


	private void OnGameModeSelected(ModeManager.GameMode mode)
	{
		gameMode = mode;

		LeaderboardButton.gameObject.SetActive(gameMode == ModeManager.GameMode.SaiMonSei);
		GridBackground.gameObject.SetActive(gameMode == ModeManager.GameMode.SaiMonSei);

		SongTitle.gameObject.SetActive(gameMode == ModeManager.GameMode.SaiMonSei);       // realm UI (name) presists
		ScorePanel.gameObject.SetActive(gameMode == ModeManager.GameMode.SaiMonSei);
		InstrumentPanelHeader.gameObject.SetActive(gameMode == ModeManager.GameMode.SaiMonSei);

		StoryNextButton.gameObject.SetActive(gameMode == ModeManager.GameMode.StoryMode);
		StorySkipButton.gameObject.SetActive(gameMode == ModeManager.GameMode.StoryMode && CanSkipLayers);

		PlayTrackText.text = gameMode == ModeManager.GameMode.StoryMode ? "TAP TO PLAY!" : "PLAY!";
	}

	private void OnSongSelected(string songTitle, string songEventName, PlayMode playMode, int gridWidth, int gridHeight, float tolerance)
	{
		if (playMode != PlayMode.LearnSong)
			return;

		selectedSongEvent = songEventName;
		selectedSongTitle = songTitle;
		accuracyTolerance = tolerance;

		SongTitle.text = songTitle.ToUpper();

		InitGrid(gridWidth, gridHeight, false);
		InitUI();

		StartCoroutine(AnimateGrid());
	}

    private void OnStartNewGame(string songEventName, PlayMode mode)
	{
		SetPlayMode(mode);         // search or learn

		if (currentPlayMode != PlayMode.LearnSong)          // handled by RealmReveal
			return;

		ConfigPlayMode();
	}

	public void SetPlayMode(PlayMode mode)
	{
		currentPlayMode = mode;         // search or learn
	}

	private void InitUI()
	{
		BeatDot.enabled = false;
		CountDown.text = "";

		Prompt.text = "";
		Status.text = "";

		YourTurn.text = "";
		YourTurnImage.gameObject.SetActive(false);

		BackgroundTint.color = BackgroundColour;

		LeaderboardButton.interactable = !string.IsNullOrEmpty(PlayerProfile.PlayerName);
	}

	private void ConfigPlayMode()
	{
		bool learnSongMode = currentPlayMode == PlayMode.LearnSong;     // else reveal realm layers

		InstrumentPanel.gameObject.SetActive(learnSongMode && gameMode == ModeManager.GameMode.SaiMonSei);

		CurrentBeatBar.enabled = learnSongMode;
		CurrentInstrument.enabled = learnSongMode && gameMode == ModeManager.GameMode.SaiMonSei;

        if (! learnSongMode)
            EnablePlayButton(false);

		StoryPanel.gameObject.SetActive(false);     // enabled for reveal layers / learn song modes

		GameOverText.text = "";
		GameOverPanel.gameObject.SetActive(false);
    }


	public void InitRevealGrid(int searchGridWidth, int searchGridHeight, float tolerance)
	{
		if (currentPlayMode != PlayMode.RevealLayers)
			return;

		InitGrid(searchGridWidth, searchGridHeight, true);
		InitUI();

		ConfigPlayMode();
	}

	public void ResetRevealGrid()
	{
		foreach (var gridPoint in GridPoints)
		{
			gridPoint.ResetScale(true);
		}
	}

	public void FadeStory(string narrative, float storyTextTime, bool canPlay, Color textColour, Action onFade = null)
	{
		//Debug.Log("FadeStory: currentPlayMode = " + currentPlayMode + ", narrative = " + narrative);
		if (currentPlayMode != PlayMode.RevealLayers && currentPlayMode != PlayMode.LearnSong)
			return;

		StoryPanel.color = Color.clear;
		StoryText.color = Color.clear;

		//StoryText.text = "\"" + narrative + "\""; 
		StoryText.text = narrative; 

		StoryPanel.gameObject.SetActive(true);
		EnablePlayButton(false);

		if (!string.IsNullOrEmpty(narrative))
		{
			// fade in panel
			LeanTween.value(StoryPanel.gameObject, Color.clear, StoryPanelColour, storyTextTime)
						  .setEaseOutCirc()
						  .setOnUpdate((Color value) =>
						  {
							  StoryPanel.color = value;
						  });
		}

		// fade in text
		LeanTween.value(StoryText.gameObject, Color.clear, textColour, storyTextTime)
					.setEaseOutCirc()
					.setOnUpdate((Color value) =>
					{
						StoryText.color = value;
					})
					.setOnComplete(() =>
					{
						onFade?.Invoke();
						EnablePlayButton(canPlay);
					});
	}


	public void FadeGameOver(string narrative, float storyTextTime)
	{
		GameOverText.color = Color.clear;
		//GameOverText.text = "\"" + narrative + "\"";  
		GameOverText.text = narrative;  
		GameOverPanel.gameObject.SetActive(true);

		if (!string.IsNullOrEmpty(narrative))
		{
			// fade in panel
			LeanTween.value(GameOverPanel.gameObject, Color.clear, GameOverPanelColour, storyTextTime)
						  .setEaseOutCirc()
						  .setOnUpdate((Color value) =>
						  {
							  GameOverPanel.color = value;
						  });
		}

		// fade in text
		LeanTween.value(GameOverText.gameObject, Color.clear, GameOverTextColour, storyTextTime)
					.setEaseOutCirc()
					.setOnUpdate((Color value) =>
					{
						GameOverText.color = value;
					})
					.setOnComplete(() =>
					{
						//onFade?.Invoke();
						//EnablePlayButton(canPlay);
					});
	}

	private void OnSongScoreLookup(string songTitle, SongScore songScore)
	{
	    selectedSongScore = songScore;
	    UpdatePersonalBest(songScore, false);
	}

	private void OnNewPersonalBest(SongScore songScore)
	{
		selectedSongScore = songScore;
		UpdatePersonalBest(songScore, true);
	}

	private void UpdatePersonalBest(SongScore songScore, bool newPB)
	{
		selectedSongScore = songScore;

		if (songScore != null)
		{
			foreach (var instrument in songScore.InstrumentScores)
			{
				string instrumentPB = (instrument.PersonalBest > 0) ? instrument.PersonalBest.ToString() : "-";

                // update instrument player button
				if (instrument.isNewPB)
				{
					var instrumentButton = GetInstrumentButton(instrument.InstrumentNumber);
                    if (instrumentButton != null)
					    instrumentButton.PersonalBest.text = "[ " + instrumentPB + " ]";
				}
			}
		}

		if (newPB)
		{
			PlaySFX(NewHiScoreAudio);
		}
	}


    // set pb player and score
    // multiplayer only
    private void OnGroupSongScoresRetrieved(string hostName, string clientNames, string songName, List<LeaderboardInstrPlayerScores> instrScores)
    {
        groupSongInstrPBScores = instrScores;

		//Debug.Log("OnGroupSongScoresRetrieved: " + instrScores.Count); // + " instrumentInit = " + instrumentInit);

        //if (!instrumentInit)        // all buttons may not be created yet if InitInstruments not finished
            ShowGroupLeaderboardScores();
    }


    // set pb player and score
    // multiplayer only
    private IEnumerator ShowGroupPBScores()
	{
		var scores = NetworkManager.GroupInstrumentScores;     // scores from NetworkedDictionary for all players
		//Debug.Log("ShowGroupPBScores: " + scores.Count);

		foreach (Transform instrument in InstrumentPlayers.transform)     
		{
			yield return new WaitForSeconds(resultsPauseTime);

			var instrumentButton = instrument.GetComponent<InstrumentButton>();
			bool newPB = false;

			// get player score for this instrument
			foreach (var instrScore in scores)
			{
				if (instrScore.TotalScore == InstrumentScore.MaxScore)      // can't be new PB
					continue;

				if (instrScore.InstrumentNumber == instrumentButton.InstrumentNumber) 
				{
					//Debug.Log("ShowGroupPBScores " + instrScore.InstrumentNumber + ": " + instrScore.PlayerName + "/" + instrScore.TotalScore + " isNewPB = " + instrScore.isNewPB);

					if (instrScore.isNewPB)
					{
						newPB = true;
						instrumentButton.PersonalBest.text = "[ " + instrScore.TotalScore.ToString() + " ]";
						instrumentButton.PBPlayerName.text = instrScore.PlayerName;

						instrumentButton.Flip(true);
						instrumentButton.SetColour(NewPBColour);
					}
					break;      // next instrument button
				}
			}

			if (newPB) // && NewHiScoreAudio != null)
				PlaySFX(NewHiScoreAudio);
				//AudioSource.PlayClipAtPoint(NewHiScoreAudio, Vector3.zero);
		}

        yield return null;
	}

	// set up instrument/player buttons for all song instruments
	private void OnInitInstruments(string songEventName, List<string> songInstruments)
	{
		ClearInstrumentButtons();

        // no instrument buttons in story mode if only 1 instrument
        if (gameMode == ModeManager.GameMode.SaiMonSei || songInstruments.Count > 1)
		    StartCoroutine(InitInstruments(songInstruments));
	}

	// set up instrument/player buttons for all song instruments
	private IEnumerator InitInstruments(List<string> songInstruments)
	{
		//instrumentInit = true;

		float initAngle = 90f;       // ie. not visible    
		int instrumentNumber = 1;

		foreach (var instrument in songInstruments)
		{
			var instrumentButton = Instantiate(InstrumentPrefab, InstrumentPlayers.transform);     // scroll view content
            instrumentButton.Button.transform.localRotation = Quaternion.Euler(new Vector2(initAngle, 0f));     // ie. not visible          

            instrumentButton.InstrumentNumber = instrumentNumber++;
			instrumentButton.Instrument.text = instrument.ToUpper();  

			// show current personal best score
			if (selectedSongScore != null)
			{
				var instrumentScore = selectedSongScore.InstrumentScores.Where(x => x.InstrumentName == instrument).FirstOrDefault();

				if (NetworkManager.IsMultiPlayerGame)       // PB scores taken from group song leaderboard (see ShowGroupPBScores)
				{
					instrumentButton.PersonalBest.text = "-";
					instrumentButton.PBPlayerName.text = "...";
				}
				else
				{
					if (instrumentScore != null)
						instrumentButton.PersonalBest.text =
                                (instrumentScore.PersonalBest > 0 && instrumentScore.PersonalBest != InstrumentScore.MaxScore) ? instrumentScore.PersonalBest.ToString() : "-";
					else
						instrumentButton.PersonalBest.text = "-";
				}
			}

			if (!NetworkManager.IsMultiPlayerGame)
				instrumentButton.SetColour(LocalPlayerColour);

			yield return new WaitForSeconds(initInstrumentDelay / 4f);      // short pause between each button
		}

		if (NetworkManager.IsMultiPlayerGame)
			AttachInstrumentPlayers();          // must be after instrument buttons created. sets colour / group leaderboard PBs

		//instrumentInit = false;
		yield return StartCoroutine(FlipInInstruments(initAngle));
	}

	// multiplayer only - attach players to instruments
	// TODO: ensure no. players <= no. instruments (ie. can't have more players than instruments)
	private void AttachInstrumentPlayers()
	{
		if (! NetworkManager.IsMultiPlayerGame)
			return;

		int playerIndex = 0;

        // set player names / colours
		foreach (Transform instrument in InstrumentPlayers.transform)
		{
			if (playerIndex >= NetworkManager.PlayerCount)
				playerIndex = 0;            // player plays >1 instrument

			var instrumentButton = instrument.GetComponent<InstrumentButton>();
			var player = NetworkManager.NetworkPlayers[playerIndex];

			instrumentButton.NetworkPlayer = player;
			instrumentButton.ClientID = player.OwnerClientId;
			instrumentButton.PlayerName.text = player.PlayerName.Value;

			instrumentButton.SetColour(player.IsLocalPlayer ? LocalPlayerColour : OtherPlayerColour);
			instrumentButton.LocalImage.enabled = player.IsLocalPlayer;
			//instrumentButton.HostImage.enabled = player.IsOwnedByServer;

			if (player.IsLocalPlayer)
                clientID = player.OwnerClientId;

            playerIndex++;
		}

        // set leaderboard PB player and score for each instrument
		if (groupSongInstrPBScores != null)
			ShowGroupLeaderboardScores();
	}

    // leaderboard scores
	private void ShowGroupLeaderboardScores()
	{
		if (groupSongInstrPBScores == null)
			return;

		//Debug.Log("ShowGroupLeaderboardScores: " + InstrumentPlayers.transform.childCount);

		foreach (Transform instrument in InstrumentPlayers.transform)
		{
			var instrumentButton = instrument.GetComponent<InstrumentButton>();

			// get player scores for this instrument
			foreach (var instrScore in groupSongInstrPBScores)
			{
				if (instrScore.Instrument == instrumentButton.Instrument.text)      // don't have instrument number in LeaderboardInstrPlayerScores
				{
					var pbScore = instrScore.GetLowestScore();      // lowest player score for this instrument

					//Debug.Log("GetPBScore: " + pbScore == null ? "none" : (pbScore.Player + "/" + pbScore.Score.ToString()));

					instrumentButton.PersonalBest.text = (pbScore == null ? "-" : pbScore.Score.ToString());
					instrumentButton.PBPlayerName.text = (pbScore == null ? "..." : pbScore.Player);
					break;
				}
			}
		}
	}

	private IEnumerator FlipInInstruments(float initAngle)
	{
		foreach (Transform instrument in InstrumentPlayers.transform)
		{
			yield return new WaitForSeconds(initInstrumentDelay);

			var instrumentButton = instrument.GetComponent<InstrumentButton>();
			instrumentButton.FlipIn(initAngle);
		}

		yield return null;
	}


	private void ResetInstruments()
	{
		foreach (Transform instrument in InstrumentPlayers.transform)
		{
			var instrumentButton = instrument.GetComponent<InstrumentButton>();

			if (NetworkManager.IsMultiPlayerGame)
				instrumentButton.SetColour(instrumentButton.NetworkPlayer.IsLocalPlayer ? LocalPlayerColour : OtherPlayerColour);
			else
				instrumentButton.SetColour(LocalPlayerColour);
		}
	}

	private void ResetRunningScore()
	{
		Score.text = "";
		ScoreKeeper.RunningScore = 0;
	}

	private InstrumentButton GetInstrumentButton(int instrumentNumber)
	{
		if (InstrumentPlayers.transform.childCount < instrumentNumber)      // instrumentNumber indexed from 1
			return null;

		return InstrumentPlayers.transform.GetChild(instrumentNumber - 1).GetComponent<InstrumentButton>();
	}

	private void ClearInstrumentButtons()
	{
		foreach (Transform instrumentButton in InstrumentPlayers.transform)
		{
			Destroy(instrumentButton.gameObject);
		}
	}

	private void OnNetworkPlayerEnteredGame(NetworkPlayer player)
	{
		if (player.IsLocalPlayer)
		    clientID = player.OwnerClientId;
	}

	// disable / indicate player no longer in the game
	private void OnNetworkPlayerLeftGame(ulong clientID)
	{
		foreach (Transform instrument in InstrumentPlayers.transform)
		{
			var instrumentButton = instrument.GetComponent<InstrumentButton>();

			if (instrumentButton.NetworkPlayer.OwnerClientId == clientID)
			{
				instrumentButton.PlayerLeftGame();
				return;
			}
		}
	}


	private void OnStatusUpdated(string status, string prompt, TrackManager.PlayMode mode)
	{
		Status.text = status; 
		Status.color = StatusColour;

		//if (Prompt.text != prompt)
		//{
		//	Prompt.text = prompt;

		//	if (!string.IsNullOrEmpty(prompt))
		//	{
  //              PulseText(Prompt, pulseScale, pulseTime);

		//		PlaySFX(StatusChangeAudio);
		//    }
		//}

		switch (mode)
		{
			case TrackManager.PlayMode.Waiting:           // not player's turn
				break;

			case TrackManager.PlayMode.PlayAll:            // play original - all instruments
				Status.color = PlayColour;
				break;

			case TrackManager.PlayMode.PlayInstrument:     // play original - 1 instrument
				Status.color = PlayColour;
				break;

			case TrackManager.PlayMode.RecordInstrument:             // play backing track, record players' input
				Status.color = RecordingColour;
				break;

			case TrackManager.PlayMode.PlaybackInstrument:           // play player's recording - 1 instrument
				Status.color = PlaybackColour;
				break;

			case TrackManager.PlayMode.PlaybackAll:        // play player's recording - all instruments
				Status.color = PlaybackColour;
				break;

			case TrackManager.PlayMode.Done:                // next player / instrument / track
			default:
				break;
		}
	}

	private void OnTimelineBeat(string songEventName, FMODPlayer.TimelineInfo timelineInfo, bool backingTrackStarted, TrackManager.PlayMode playMode)
	{
		if (selectedSongEvent != songEventName)
			return;

		var timelineData = timelineInfo.timelineData;

		//Debug.Log("OnTimelineBeat: " + timelineInfo.timelinePosition + " / " + trackLength);

		if (!backingTrackStarted)       // ie. count-in
		{
		    CurrentBeatBar.text = timelineData.currentBeat.ToString();
	    }
		else if (timelineData.timelinePosition < trackLength)
		{
			CurrentBeatBar.text = timelineData.currentBeat.ToString() + " / " + (timelineData.currentBar - 1);       // first bar count-in not part of bar count
		}

		if (backingTrackStarted && (playMode == TrackManager.PlayMode.PlayAll || NetworkManager.IsLocalPlayersTurn))
		{
			TextBeat();
		}

		StartCoroutine(FlashBeatDot(BeatDotFlashTime));
		BackgroundBeat();
	}


	private void OnDudNote()
	{
		PlaySFX(DudNote);
	}

	private IEnumerator FlashBeatDot(float beatInterval)
	{
		BeatDot.enabled = true;
		yield return new WaitForSeconds(beatInterval / 4f);
		BeatDot.enabled = false;
	}

	public bool TouchedInsideGrid(Vector2 touchPosition)        // world position
	{
		Vector3[] gridCorners = GridCorners; // new Vector3[4]; // world space corner coords
		return !(Mathf.Abs(touchPosition.x) > Mathf.Abs(gridCorners[0].x) || Mathf.Abs(touchPosition.y) > Mathf.Abs(gridCorners[0].y));
	}

	private void OnCreateRipple(Vector2 worldPosition, Ripple.RippleType rippleType)
	{
		if (! TouchedInsideGrid(worldPosition))
			return;

		var ripple = RipplePool.GetRipple();
		ripple.transform.position = worldPosition;

        switch (rippleType)
        {
            case Ripple.RippleType.Valid:
                ripple.Pulse(ValidRippleColour);
                break;
            case Ripple.RippleType.Invalid:
                ripple.Pulse(InvalidRippleColour);
                break;
            case Ripple.RippleType.Score:
                ripple.Pulse(ScoreRippleColour, scoreRippleTimeFactor, scoreRippleScaleFactor);
                break;
            case Ripple.RippleType.RealmReveal:
                ripple.Pulse(RealmRippleColour, scoreRippleTimeFactor, scoreRippleScaleFactor);
                break;
            default:
				ripple.Pulse(InvalidRippleColour);
				break;
        }
    }

    private void OnTouchScore(Vector2 worldPosition, TrackNote recordedNote, InstrumentScore instrumentScore, ScoreKeeper.ScoreRating rating)
	{
		Vector3[] gridCorners = GridCorners;

		if (instrumentScore.TotalScore == InstrumentScore.MaxScore)
			return;

		// touch feedback only inside grid area
		if (! TouchedInsideGrid(worldPosition))  // Mathf.Abs(worldPosition.x) > Mathf.Abs(gridCorners[0].x) || Mathf.Abs(worldPosition.y) > Mathf.Abs(gridCorners[0].y))
			return;

		var newTouchText = Instantiate(TouchTextPrefab, TouchScores.transform);
		newTouchText.transform.position = worldPosition;

		var touchText = newTouchText.GetComponent<TouchText>();
		var scoreIncrement = instrumentScore.TotalScore - ScoreKeeper.RunningScore;

		bool hideStoryScores = (gameMode == ModeManager.GameMode.StoryMode && currentPlayMode == PlayMode.LearnSong && !ShowStoryScores);
		int incrementFeedback = hideStoryScores ? 0 : scoreIncrement;

		if (recordedNote.IsDudNote)
			touchText.ScoreFeedback(DudNoteColour, "DOH!", instrumentScore.TotalScore, incrementFeedback, Score);
		else
		{
			switch (rating)
			{
				case ScoreKeeper.ScoreRating.Nice:
					touchText.ScoreFeedback(NiceScoreColour, "NICE!", instrumentScore.TotalScore, incrementFeedback, Score);      // running total score
					break;

				case ScoreKeeper.ScoreRating.Perfect:
					touchText.ScoreFeedback(PerfectScoreColour, "PERFECT!!", instrumentScore.TotalScore, incrementFeedback, Score);      // running total score
					break;

				default:
					touchText.ScoreFeedback(ScoreColour, "", instrumentScore.TotalScore, incrementFeedback, Score);      // running total score
					break;
			}
		}

		ScoreKeeper.RunningScore = instrumentScore.TotalScore;
		SaimonseiEvents.OnScoreUpdated?.Invoke(instrumentScore, rating);
	}


	public void TouchFeedback(string text, Vector2 worldPosition)
	{
		var newTouchText = Instantiate(TouchTextPrefab, TouchScores.transform);
		newTouchText.transform.position = worldPosition;

		var touchText = newTouchText.GetComponent<TouchText>();
		touchText.ScoreFeedback(FeedbackColour, text, 0, 0, null);
	}


	private void TextBeat()
    {
		LeanTween.scale(Status.gameObject, Vector3.one * textPulseScale, BeatDotFlashTime / 4f)
			.setEase(LeanTweenType.easeOutQuart)
			.setLoopPingPong(1);

		LeanTween.scale(SongTitle.gameObject, Vector3.one * textPulseScale, BeatDotFlashTime / 4f)
			        .setEase(LeanTweenType.easeOutQuart)
			        .setLoopPingPong(1);
	}

	private void BackgroundBeat()
	{
		//var randomColour = RandomGridPointColour;
		//var backGroundColour = new Color(randomColour.r, randomColour.g, randomColour.b, backgroundAlpha);

		LeanTween.value(BackgroundTint.gameObject, BackgroundColour, BeatPulseColour, BeatDotFlashTime / 4f)
							.setEase(LeanTweenType.easeOutQuart)
							.setOnUpdate((Color col) => BackgroundTint.color = col)
							.setLoopPingPong(1);
	}


    // total 4-beat breathing space
	private IEnumerator EndTrackFade(float secondsPerBeat, int restBeats)
	{
		OnStatusUpdated("", "", TrackManager.PlayMode.Waiting);
		CurrentInstrument.text = "";
		CurrentBeatBar.text = "";

		// breathing space!
		yield return new WaitForSecondsRealtime(secondsPerBeat * (restBeats/2));

		Blackout.color = Color.clear;
		Blackout.gameObject.SetActive(true);

		LeanTween.value(Blackout.gameObject, Color.clear, BlackoutColour, secondsPerBeat * (restBeats/4))
					.setEase(LeanTweenType.easeOutQuart)
					.setOnUpdate((Color col) => Blackout.color = col)
					.setOnComplete(() =>
                        {
                            EnableGridDots(false);

							LeanTween.value(Blackout.gameObject, BlackoutColour, Color.clear, secondsPerBeat * (restBeats/4))
					                .setEase(LeanTweenType.easeInQuart)
					                .setOnUpdate((Color col) => Blackout.color = col)
									.setOnComplete(() =>
					                {
						                Blackout.gameObject.SetActive(false);
                                        EnableGridDots(true);
                                    });
						});

		yield return null;
	}

	// exit game
	private void OnMenuButtonClicked()
	{
		StartCoroutine(ConfirmLeaveTimer());
	}

	private void ConfirmLeaveButtonClicked()
	{
		SaimonseiEvents.OnShowLeaderboard?.Invoke(LeaderboardUI.LeaderboardType.Song, false);
		SaimonseiEvents.OnTrackStop?.Invoke(selectedSongEvent);

		SaimonseiEvents.OnLeaveGame?.Invoke(clientID);

		if (gameMode == ModeManager.GameMode.SaiMonSei)
			SaimonseiEvents.OnMainMenu?.Invoke();
		else        // story mode
			SaimonseiEvents.OnStoryPlayQuit?.Invoke();

		ClearAllResults();

		SongTitle.text = "";
		CurrentInstrument.text = "";
		CurrentBeatBar.text = "";
		CurrentTempo.text = "";

		EnablePlayButton(true);

		MainMenuButton.gameObject.SetActive(true);
		ConfirmLeaveButton.gameObject.SetActive(false);

		groupSongInstrPBScores = null;
	}

	private IEnumerator ConfirmLeaveTimer()
	{
		MainMenuButton.gameObject.SetActive(false);
		ConfirmLeaveButton.gameObject.SetActive(true);

		yield return new WaitForSeconds(ConfirmTimeout);

		MainMenuButton.gameObject.SetActive(true);
		ConfirmLeaveButton.gameObject.SetActive(false);

		yield return null;

	}

	private void OnPlayButtonClicked()
	{
		SaimonseiEvents.OnPlay?.Invoke();
	}

	// 'play' button clicked
	private void StartPlayCycle()
	{
		StoryPanel.gameObject.SetActive(false);

		GameOverText.text = "";
		GameOverPanel.gameObject.SetActive(false);

		if (gameMode == ModeManager.GameMode.StoryMode)     // handled by RealmReveal
		{
			learnRealmSongFailed = false;

			if (realmState == Goddess.RealmState.LandHealthZero)
			{
				//SaimonseiEvents.OnStoryPlayQuit?.Invoke();      // cleanup    // TODO: is this ok??
				SaimonseiEvents.OnRestartGame?.Invoke();        // reenter first realm
				return;
			}

			if (realmState != Goddess.RealmState.LearningRealmSong)
			    return;
		}

		SaimonseiEvents.OnShowLeaderboard?.Invoke(LeaderboardUI.LeaderboardType.Song, false);
		LeaderboardButton.interactable = false;

		InitUI();

		// play track after 1 bar count in
		SaimonseiEvents.OnPlayCycleStart?.Invoke(gameMode);

        ClearAllResults();
		EnableGridDots(true);
		EnablePlayButton(false);

		ResetInstruments();     // colours
		ResetRunningScore();
	}

	// on 'play' button clicked
	private void OnPlayTrackButtonClicked()
	{
		if (NetworkManager.IsMultiPlayerGame)
		{
			if (NetworkManager.GetLocalPlayer.IsOwnedByServer)      // only host can start play cycle
				SaimonseiEvents.OnStartCycleServer?.Invoke();     // server invokes RPC StartPlayCycle on all clients
		}
		else
			OnPlayButtonClicked();
	}

	//private void OnStopTrackButtonClicked()
	//{
	//	SaimonseiEvents.OnShowLeaderboard?.Invoke(false);
	//	SaimonseiEvents.OnTrackStop?.Invoke(selectedSongEvent);
	//}

	private void OnLeaderboardButtonClicked()
	{
		leaderboardShowing = !leaderboardShowing;

		Status.gameObject.SetActive(!leaderboardShowing);
		Prompt.gameObject.SetActive(!leaderboardShowing);
		CurrentInstrument.gameObject.SetActive(!leaderboardShowing);

		PlayTrackButton.interactable = !leaderboardShowing;
		MainMenuButton.interactable = !leaderboardShowing;

		SaimonseiEvents.OnToggleLeaderboard?.Invoke(NetworkManager.IsMultiPlayerGame ?
                                    LeaderboardUI.LeaderboardType.GroupSong : LeaderboardUI.LeaderboardType.Song);
	}

	private void OnBackingTrackCreated(string songEventName, int length)
	{
		//Debug.Log("OnBackingTrackCreated: " + length);
		trackLength = length;
	}

	private void OnTrackPlay(string songEventName, TrackManager.PlayMode mode, int instrumentNumber)
	{
		if (mode == TrackManager.PlayMode.PlaybackAll)
			YourTurn.text = "";

		if (mode == TrackManager.PlayMode.RecordInstrument)
			ResetRunningScore();
	}

	private void OnTrackStop(string songEventName)
	{
		BeatDot.enabled = false;
	}

	private void OnTrackEnd(string songEventName, TrackManager.PlayMode mode, FMODPlayer.TimelineInfo timelineInfo, int restBeats)
	{
		StartCoroutine(EndTrackFade(SecondsPerBeat(timelineInfo.timelineData), restBeats));
	}

    private float SecondsPerBeat(TimelineData timelineData)
    {
        return 60f / timelineData.currentTempo;
    }


    private void OnInstrumentChanged(string songEventName, int instrumentNumber, string playerInstrumentName, string shortInstrumentName, int numGridPoints)
	{
		if (selectedSongEvent != songEventName)
			return;

		CurrentInstrument.text = playerInstrumentName.ToUpper();          // was cleared

		if (currentInstrumentName == shortInstrumentName)       // no change of instrument
            return;

        currentInstrumentName = shortInstrumentName;
		ResetRunningScore();

		PulseText(CurrentInstrument, pulseScale, pulseTime);

		PlaySFX(NewInstrumentAudio);
		//if (NewInstrumentAudio != null)
		//	AudioSource.PlayClipAtPoint(NewInstrumentAudio, Vector3.zero);

        foreach (Transform instrument in InstrumentPlayers.transform)
        {
            var instrumentButton = instrument.GetComponent<InstrumentButton>();
			bool playersTurn = instrumentButton.InstrumentNumber == instrumentNumber;

			if (instrumentButton.TurnImage.enabled || playersTurn)
				instrumentButton.FlipTurn(playersTurn);
        }
	}


	private void OnPlayCycleDone(string songEventName, List<InstrumentScore> scores)
	{
		if (selectedSongEvent != songEventName)
			return;

		if (currentPlayMode == PlayMode.RevealLayers)        // TODO: check score before progressing to song learning
		{
			StoryNextButton.interactable = true;
			PulseButton(StoryNextButton, pulseScale, pulseTime);
		}
		else if (currentPlayMode == PlayMode.LearnSong)
		{
			//if (gameMode == ModeManager.GameMode.Leaderboard)
			SongResults(scores);

			if (gameMode == ModeManager.GameMode.StoryMode)
			{
				foreach (var score in scores)
				{
					SaimonseiEvents.OnScoreUpdated?.Invoke(score, ScoreKeeper.ScoreRating.Final);
				}
			}
		}

		RipplePool.ResetRipples();
	}

	private void SongResults(List<InstrumentScore> scores)
	{
		CurrentTempo.text = "";
		CurrentBeatBar.text = "";
		CurrentInstrument.text = "";
		YourTurn.text = "";

		ClearAllResults();
		EnablePlayButton(true);
		LeaderboardButton.interactable = !string.IsNullOrEmpty(PlayerProfile.PlayerName);
		BeatDot.enabled = false;

		ResetRunningScore();

		StartCoroutine(ShowResults(scores));
	}

    // reveal in rank order
	private IEnumerator ShowResults(List<InstrumentScore> instrumentScores)
	{
        EnableGridDots(false);

		if (gameMode == ModeManager.GameMode.SaiMonSei)
		{
			ResultsHeaders.gameObject.SetActive(true);
			float initAngle = 90f;

			if (!NetworkManager.IsMultiPlayerGame)         // use instrumentScores (all instruments played by local player... obviously)
			{
				var sortedScores = instrumentScores.OrderBy(x => x.Rank).ToList();      // default ascending

				foreach (var score in sortedScores)
				{
					yield return new WaitForSeconds(resultsPauseTime);

					var instrument = GetInstrumentButton(score.InstrumentNumber);

					if (instrument == null)         // instrument buttons not shown if only 1 instrument in story mode 
						continue;

					var instrumentButton = instrument.GetComponent<InstrumentButton>();
					var instrumentScore = instrumentScores.Where(x => x.InstrumentNumber == instrumentButton.InstrumentNumber).FirstOrDefault();

					if (instrumentScore != null)
						ShowInstrumentResults(instrumentButton, instrumentScore, initAngle);
				}
			}
			else        // get scores from NetworkPlayer that played each instrument
			{
				foreach (var score in instrumentScores)
				{
					yield return new WaitForSeconds(resultsPauseTime);

					var instrument = GetInstrumentButton(score.InstrumentNumber);
					var instrumentButton = instrument.GetComponent<InstrumentButton>();

					var player = NetworkManager.GetPlayerByInstrument(score.InstrumentNumber);
					if (player == null)
					{
						Debug.Log("ShowResults: " + score.InstrumentNumber + " has no player!");
						continue;
					}

					InstrumentScore groupScore = new InstrumentScore();

					if (player.GetInstrumentScore(score.InstrumentNumber, out groupScore))
						ShowInstrumentResults(instrumentButton, groupScore, initAngle);
				}

				StartCoroutine(ShowGroupPBScores());
			}
		}

		// allow progression to next realm only if 'passed' realm song
		if (gameMode == ModeManager.GameMode.StoryMode && currentPlayMode == PlayMode.LearnSong)        
            StoryNextButton.interactable = !learnRealmSongFailed;

		yield return null;
	}

    private void ShowInstrumentResults(InstrumentButton instrumentButton, InstrumentScore instrumentScore, float initAngle)
	{
		instrumentButton.ResultsPanel.transform.localRotation = Quaternion.Euler(new Vector2(initAngle, 0f));     // ie. not visible

		//instrumentButton.ResultsPanel.color = instrumentScore.PlayedInstrument ? PlayedResultsColour : NotPlayedResultsColour;
		instrumentButton.ResultsPanel.color = NetworkManager.LocalPlayerPlayedInstrument(instrumentScore.InstrumentNumber) ? PlayedResultsColour : NotPlayedResultsColour;

		instrumentButton.ResultsPanel.gameObject.SetActive(true);
		instrumentButton.FlipInResults(initAngle);

		//instrumentButton.Rank.text = "#" + instrumentScore.Rank.ToString();
		instrumentButton.TotalScore.text = instrumentScore.TotalScore == InstrumentScore.MaxScore ? "n/a" : instrumentScore.TotalScore.ToString();
		instrumentButton.Timing.text = instrumentScore.TimingDifference <= 0 ? "-" : instrumentScore.TimingDifference.ToString();
		instrumentButton.Duration.text = instrumentScore.DurationDifference <= 0 ? "-" : instrumentScore.DurationDifference.ToString();
		instrumentButton.Location.text = instrumentScore.LocationDifference <= 0 ? "-" : instrumentScore.LocationDifference.ToString();
		instrumentButton.Notes.text = instrumentScore.RecordedNoteCount + " / " + instrumentScore.TargetNoteCount;

		//if (instrumentScore.Rank == 1 && instrumentScore.TotalScore != InstrumentScore.MaxScore && scores.Count > 1)
		//{
		//	instrumentButton.SetColour(WinnerColour);
		//	instrumentButton.Flash();
		//}
		//else if (instrumentScore.isNewPB)
		if (instrumentScore.isNewPB && ! NetworkManager.IsMultiPlayerGame)
		{
			instrumentButton.SetColour(NewPBColour);
			instrumentButton.FlashButton();
		}
	}

	private void ClearAllResults()
	{
		foreach (Transform instrument in InstrumentPlayers.transform)
		{
			var instrumentButton = instrument.GetComponent<InstrumentButton>();
			instrumentButton.ResultsPanel.gameObject.SetActive(false);
		}

		ResultsHeaders.gameObject.SetActive(false);
	}

	private void ClearGridDots(int gridWidth, int gridHeight)
	{
		foreach (Transform child in GridPanel.transform)
		{
            if (child.GetComponent<GridDot>() != null)
			    Destroy(child.gameObject);
		}

		// size the grid point array
		GridPoints = new GridPoint[gridWidth, gridHeight];
	}

	private void EnableGridDots(bool enable)
	{
		foreach (Transform child in GridPanel.transform)
		{
			if (child.GetComponent<GridDot>() != null)
				child.gameObject.SetActive(enable);
		}
	}

	private void InitGrid(int gridWidth, int gridHeight, bool disable)
	{
		ClearGridDots(gridWidth, gridHeight);

		float playWidth = GridPanel.rectTransform.rect.width; 
		float playHeight = GridPanel.rectTransform.rect.height;

		float horizInterval = playWidth / (gridWidth + 1);
		float vertInterval = playHeight / (gridHeight + 1);

		// screen centre is 0,0
		float screenLeft = -playWidth / 2f;
		float screenBottom = -playHeight / 2f;

		// horizontal
		for (int x = 0; x < gridWidth; x++)
		{
			// vertical
			for (int y = 0; y < gridHeight; y++)
			{
				var position = new Vector2(screenLeft + (horizInterval * (x + 1)), screenBottom + (vertInterval * (y + 1)));

				var gridDotObj = Instantiate(GridDotPrefab, GridPanel.transform);
				var gridDot = gridDotObj.GetComponent<GridDot>();
				gridDotObj.transform.localPosition = position;

				if (disable)
				{
					gridDotObj.transform.localScale = Vector3.zero;
					gridDot.SetSparkleScale(Vector3.zero);
				}
				//gridDot.gameObject.SetActive(!disable);

				// cache the buttons in a 2-dim array
				var gridPoint = new GridPoint(gridDot, new Vector2(x, y), RandomGridPointColour, RandomGridPointMaterial);
				GridPoints[x, y] = gridPoint;
			}
		}

		SaimonseiEvents.OnInitGrid?.Invoke(GridPoints);
	}

	private IEnumerator AnimateGrid()
	{
		EnablePlayButton(false);

		// fade in grid background panel
		var gridColour = GridBackground.color;
		GridBackground.color = Color.clear;
		LeanTween.value(GridBackground.gameObject, Color.clear, gridColour, gridAnimateTime * 2f) // GridPoints.Length)
									.setEaseInQuart()
									.setOnUpdate((Color x) => { GridBackground.color = x; });

		// hide all grid points
		foreach (var point in GridPoints)
		{
			point.gridDot.gameObject.SetActive(false);
		}

		yield return new WaitForSeconds(gridInitDelay);     // short pause

		// warp in
		foreach (var point in GridPoints)
        {
			var endPosition = point.gridDot.transform.position;
			point.gridDot.transform.position = Vector3.zero;

			var endScale = point.gridDot.transform.localScale;
			point.gridDot.transform.localScale = Vector3.zero;

			point.gridDot.gameObject.SetActive(true);

			LeanTween.move(point.gridDot.gameObject, endPosition, gridAnimateTime)
                                .setEase(LeanTweenType.easeInQuart)
                                .setOnComplete(() =>
								{
									PlaySFX(GridPointArriveAudio);
									//if (GridPointArriveAudio != null)
									//	AudioSource.PlayClipAtPoint(GridPointArriveAudio, Vector3.zero);
								});

			LeanTween.scale(point.gridDot.gameObject, endScale, gridAnimateTime)
                                .setEase(LeanTweenType.easeOutBack)
								.setOnComplete(() =>
								{
									PulseGridPoint(point);
                                });

			PlaySFX(GridPointAnimateAudio);
			//if (GridPointAnimateAudio != null)
			//	AudioSource.PlayClipAtPoint(GridPointAnimateAudio, Vector3.zero);

			yield return new WaitForSeconds(UnityEngine.Random.Range(minGridPointDelay, maxGridPointDelay));
		}

		EnablePlayButton(true);
		yield return null;
    }


    private void PulseGridPoint(GridPoint point)
    {
		point.gridDot.PulseSparkles();  
    }


	private void EnablePlayButton(bool enable)
	{
		bool canPlay = NetworkManager.IsMultiPlayerGame ? NetworkManager.GetLocalPlayer.IsOwnedByServer : true;

		if (PlayTrackButton.interactable != (enable && canPlay))      // has changed
		{
		    PlayTrackButton.interactable = enable && canPlay;

			if (PlayTrackButton.interactable)
				PulseButton(PlayTrackButton, pulseScale, pulseTime);
		}

		StorySkipButton.interactable = CanSkipLayers && enable && canPlay && completedLayer && realmState != Goddess.RealmState.LandHealthZero;

		SaimonseiEvents.OnPlayEnabled?.Invoke(enable);
	}


	private void OnNextPlayerCountdown(int countFrom, string nextPlayer, Action onZero)
	{
		StartCoroutine(NextPlayerCountDown(countFrom, nextPlayer, onZero));
	}

	private IEnumerator NextPlayerCountDown(int countFrom, string nextPlayer, Action onZero)
	{
		//EnableGrid(false);

		if (gameMode == ModeManager.GameMode.SaiMonSei)
		{
			int countRemaining = countFrom;

			NextPlayerIn.text = string.Format("{0} in...", nextPlayer);

			while (countRemaining > 0)
			{
				CountDown.text = countRemaining.ToString();

				PlaySFX(CountDownAudio);
				//if (CountDownAudio != null)
				//	AudioSource.PlayClipAtPoint(CountDownAudio, Vector3.zero);

				PulseText(CountDown, countdownPulseScale, countdownPulseTime);

				yield return new WaitForSecondsRealtime(1f);
				countRemaining--;
			}
		}

		CountDown.text = "";
		NextPlayerIn.text = "";

		//EnableGrid(true);

		onZero?.Invoke();
		yield return null;
	}


	private void OnCurrentPlayerChanged(int newPlayerNum)
	{
		if (NetworkManager.IsMultiPlayerGame)
		{
			//Debug.Log("OnCurrentPlayerChanged: newPlayerNum = " + newPlayerNum);

			if (NetworkManager.IsLocalPlayersTurn)
				PlaySFX(YourTurnAudio);
				//AudioSource.PlayClipAtPoint(YourTurnAudio, Vector3.zero, 10f);
			else
				PlaySFX(TheirTurnAudio);
			    //AudioSource.PlayClipAtPoint(TheirTurnAudio, Vector3.zero, 10f);

			YourTurn.text = NetworkManager.IsLocalPlayersTurn ? "YOUR TURN!" : (NetworkManager.PlayersTurnName + "'s TURN...");
            if (NetworkManager.IsLocalPlayersTurn)
                PulseText(YourTurn, pulseScale, pulseTime);

			YourTurnImage.gameObject.SetActive(NetworkManager.IsLocalPlayersTurn);
		}
        else
        {
			YourTurn.text = "";
			YourTurnImage.gameObject.SetActive(false);
		}
	}

	private void OnHostEndedGame()
	{
		//ClearPlayerButtons();
	}

	private void PulseText(Text text, float scaleFactor, float scaleTime)
	{
		var startScale = text.transform.localScale;

		LeanTween.scale(text.gameObject, startScale * scaleFactor, scaleTime)
								.setEase(LeanTweenType.easeOutSine)
								.setOnComplete(() =>
								{
									LeanTween.scale(text.gameObject, startScale, scaleTime / 2f)	// TODO: magic number!
											.setEase(LeanTweenType.easeInSine);
								});
	}

	private void PulseButton(Button button, float scaleFactor, float scaleTime)
	{
		var startScale = button.transform.localScale;

		LeanTween.scale(button.gameObject, startScale * scaleFactor, scaleTime)
								.setEase(LeanTweenType.easeOutSine)
								.setOnComplete(() =>
								{
									LeanTween.scale(button.gameObject, Vector3.one, scaleTime / 2f)    // TODO: magic number!
											.setEase(LeanTweenType.easeInSine);
								});
	}


	// story mode

	private void OnEnterRealmLayer(StoryRealm realm, RealmLayer layer, Vector3[] gridCorners)
	{
		completedLayer = SavedGameState.RealmLayerCompleted(realm.RealmName, layer.VeilName);       // can skip if completed
	}

	private void OnRealmStateChanged(Goddess.RealmState newState, bool skipping)
	{
		realmState = newState;

        switch (newState)
        {
            case Goddess.RealmState.EnteredRealm:               // GoddessUI
            case Goddess.RealmState.StoryTold:                  // GoddessUI
                break;

            case Goddess.RealmState.LayerReveal:
                StoryNextText.text = "LEARN SONG...";
                StoryNextButton.interactable = false;
                break;

            case Goddess.RealmState.LearningRealmSong:
                StoryNextText.text = "NEXT REALM...";
                StoryNextButton.interactable = false;
                break;

            case Goddess.RealmState.RealmSongLearnt:
                StoryNextButton.interactable = false;
				break;

            case Goddess.RealmState.LandHealthZero:
				EnablePlayButton(true);
				StorySkipButton.interactable = false;
				break;
        }

        //DebugText.text = newState.ToString();
    }


    private void OnRealmRevealStateChanged(RealmReveal.RevealState newState, StoryRealm realm, bool skippingTouchstones)
	{
        switch (newState)
        {
            case RealmReveal.RevealState.Start:
                break;

            case RealmReveal.RevealState.PlaySequence:
				RipplePool.ResetRipples();
				EnablePlayButton(false);
                break;

            case RealmReveal.RevealState.Capture:
            case RealmReveal.RevealState.Touched:
				EnablePlayButton(false);
                break;

            case RealmReveal.RevealState.TimeOut:
				//RipplePool.ResetRipples();
				break;

			case RealmReveal.RevealState.Success:
				//RipplePool.ResetRipples();
				EnablePlayButton(false);        
				break;

			case RealmReveal.RevealState.Revealed:
				RipplePool.ResetRipples();
				EnablePlayButton(false);
                break;

			case RealmReveal.RevealState.WaitingPlayer:
				EnablePlayButton(false);
				break;

			case RealmReveal.RevealState.Fail:
				//RipplePool.ResetRipples();
				break;
        }
    }


	private void OnRealmRevealCountdown(int countRemaining, int displayAt)
    {
        if (countRemaining > 0 && countRemaining <= displayAt)
        {
			StartCoroutine(FlashBeatDot(BeatDotFlashTime));

			//StoryTimeRemaining.text = "Time Remaining:";
			//StoryCountDown.text = countRemaining.ToString();
			//PulseText(StoryCountDown, countdownPulseScale, countdownPulseTime);
		}
        else
        {
            //StoryTimeRemaining.text = "";
            //StoryCountDown.text = "";
        }
    }

    private void OnAllRealmLayersRevealed()
	{
		StoryNextButton.interactable = true;
		PulseButton(StoryNextButton, pulseScale, pulseTime);

		EnablePlayButton(false);
    }

	private void OnLearnRealmSongFailed()
	{
		learnRealmSongFailed = true;
	}

	private void OnStoryNextClicked()
	{
		if (gameMode == ModeManager.GameMode.SaiMonSei)       // safety net!
			return;

		switch (Goddess.CurrentRealmState)
		{
			case Goddess.RealmState.StoryTold:
				break;
			case Goddess.RealmState.LayerReveal:                // only if touchstones sucessfully played!
			    SaimonseiEvents.OnRealmStateChanged?.Invoke(Goddess.RealmState.LearningRealmSong, false);
				break;

			case Goddess.RealmState.LearningRealmSong:          // only if song successfully learnt!
				SaimonseiEvents.OnRealmStateChanged?.Invoke(Goddess.RealmState.RealmSongLearnt, false);
				break;
	    }
	}

	private void OnStorySkipClicked()
	{
		if (gameMode == ModeManager.GameMode.SaiMonSei)       // safety net!
			return;

		if (!CanSkipLayers)
			return;

		switch (Goddess.CurrentRealmState)
		{
			case Goddess.RealmState.EnteredRealm:
			case Goddess.RealmState.StoryTold:
			case Goddess.RealmState.LayerReveal:
				StoryPanel.gameObject.SetActive(false);     // hide story as if 'Play' clicked...
				SaimonseiEvents.OnSkipRealmLayer?.Invoke();
				break;

			case Goddess.RealmState.LearningRealmSong:
			//case Goddess.RealmState.RealmSongLearnt:
				SaimonseiEvents.OnRealmStateChanged?.Invoke(Goddess.RealmState.RealmSongLearnt, true);
				//OnStoryNextClicked();
				break;

			case Goddess.RealmState.RealmSongLearnt:
			case Goddess.RealmState.LandHealthZero:
				break;
		}
	}

	private void OnDebug(string message)
	{
		DebugText.text = message;
	}

    //private void OnPitchChanged(float pitch)
    //{
    //	PlayTrackEventEmitter.SetParameter(PitchParameter, pitch);
    //	PlayTrackParameterTrigger.TriggerParameters();
    //}

}