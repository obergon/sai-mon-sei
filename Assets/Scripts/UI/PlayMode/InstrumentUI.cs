﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InstrumentUI : MonoBehaviour
{
    public Button InstrumentButton;

    public void Enable(bool enable)
    {
        InstrumentButton.interactable = enable;
    }
}
