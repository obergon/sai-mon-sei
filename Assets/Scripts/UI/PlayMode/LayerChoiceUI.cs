﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LayerChoiceUI : MonoBehaviour
{
    public Text ChoiceText;
    public Button ChoiceButton;

    private LayerChoice layerChoice;

    private void OnEnable()
    {
        ChoiceButton.onClick.AddListener(OnLayerChosen);
    }

    private void OnDisable()
    {
        ChoiceButton.onClick.RemoveListener(OnLayerChosen);
    }

    // layer data (after instantiation)
    public void SetLayerChoice(LayerChoice layerChoice)
    {
        this.layerChoice = layerChoice;
        ChoiceText.text = layerChoice.ChoiceText.ToUpper();
    }

    private void OnLayerChosen()
    {
        SaimonseiEvents.OnRealmLayerChosen?.Invoke(layerChoice);
    }
}

