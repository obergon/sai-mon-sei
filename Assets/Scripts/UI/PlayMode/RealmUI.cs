﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class RealmUI : UIPanel
{
    [Space]
	public GameObject UIPanel;
	public Text RealmName;
	public Text RealmSongName;
	public Text RealmLayerName;
	public Text LearnSongText;

	[Space]
	public Image BackgroundBW;			// black and white background
	public Image Background;
	public Image FilterTint;

	[Header("Background fade in")]
	public float BackgroundAlphaStart = 0f;
	//private float BackgroundAlphaIncrement = 0.15f;

	public float BackgroundBWAlphaStart = 0.1f;
	public float BackgroundBWAlphaDecrement = 0.025f;

	[Space]
	public Image InstrumentCloud;
	public Image InstrumentImage;

	[Space]
	public Slider ProgressBar;
	public Image ProgressFill;
	public Image ProgressBackground;

	public Color ProgressFillColour;            // reveal layers
	public Color ProgressBackgroundColour;      // reveal layers

	//public Color ProgressFullColour;            // learning song

	public Color ScoreFillColour;				// learning song
	public Color ScoreBackgroundColour;         // learning song


	[Space]
	public GameObject LayerImagePrefab; // revealed via touchstone sequence - overlayed on background
	public AudioClip SuccessAudio;
	public AudioClip RevealedAudio;
	public AudioClip ProgressAudio;
	public AudioClip FailedAudio;       // when learning song - exceeded pass score

	public Color RevealColour;      // semi-transparent

	[Space]
	public Text LandHealthText;
	//public Slider LandHealthBar;
	public Image LandHealthFill;
	public Color LandSickColour;        
	public Color LandHealthyColour;

	[Space]
	public Image LandHealthCrystals;
	public HealthCrystal CrystalPrefab;
	public Gradient CrystalColours;
	public int MaxCrystals;

	[Space]
	public Image PocketImage;
	public Text PocketCrystals;
	public Text PocketCrystalsPrompt;              // prompt to tap again
	public Button PocketButton;
	public Text SlowTimePrompt;                    // slow time...

	[Space]
	public AudioClip CrystalAudio; 
	public AudioClip CrystalsUpAudio; 
	public AudioClip CrystalsDownAudio; 
	public AudioClip NoCrystalsAudio;
    public AudioClip CrystalsPocketFlashAudio;
	public int NumCrystalFlashes;

	[Space]
	[Header("Instruments")]
	public Image InstrumentsPanel;
	public List<Image> InstrumentBackgrounds = new List<Image>();
	public List<Image> InstrumentImages = new List<Image>();
	public List<ParticleSystem> InstrumentSparkles = new List<ParticleSystem>();

	public AudioClip NewInstrumentAudio;

	public Color InstrumentEmptyColour = Color.grey;
	public Color InstrumentFullColour = Color.cyan;
	public Color InstrumentHilightColour = Color.yellow;

	private float crystalInterval = 0.04f;       // pause between activating crystals
	private int activeCrystalCount = 0;

	[Space]
	[Header("Next Layer Choices")]
	public Image NextLayersPanel;               // enable/disable
	public Transform NextLayers;                // scroll rect content - parent for layer choice buttons
	public LayerChoiceUI LayerChoicePrefab;

	public AudioClip LayerChoicesAudio;
	public AudioClip LayerChoiceAudio;

	private float layerChoiceButtonDelay = 0.25f;
	private float layerChoiceFadeTime = 0.75f;

	[Space]
	public float LayerFadeTime;

	//public AudioClip LandHealthAudio;
	public AudioClip PocketAudio;

	public bool RealmSplashAtStart = false;          // during goddess visit - also means last layer represents restored state, so is not removed

	[Header("Saved Data")]
	[Tooltip("Data saved between game sessions")]
	public SavedGameState SavedGameState;               // land health, pocket crystals, realm and layer attempts, completed flag, etc

	private float landHealthyValue = 50f;           // changes colour

	private float progressUpdateTime = 0.5f;       // slider 
	private float progressPulseScale = 1.25f;
	private float progressPulseTime = 0.25f;

	private float crystalPulseScale = 2f;
	private float crystalPulseTime = 0.3f;

	private float crystalPocketScaleFactor = 1.2f;     // larger pulse when flashing for pocketing
	private float updatePocketCrystalInterval = 0.1f;      // when incrementing PocketCrystals text

	private float pocketPulseScale = 1.5f;
	private float pocketPulseTime = 0.25f;
	private float slowTimePromptTime = 1.5f;

	private float instrumentFadeTime = 0.5f;        // rapid in/out

	private int realmPassScore;
	private bool realmFailed;       // exceeded pass score

    private StoryRealm currentRealm;
    private List<Image> currentRealmLayers = new List<Image>();

    private LTDescr instrumentFadeTween;
    private float numCrystalFlashes;

    private int crystalsFlashingFrom;       
    private bool pocketingCrystals;

	private int currentRealmIndex = 0;


    private void OnEnable()
	{
		SaimonseiEvents.OnEnterRealm += OnEnterRealm;
		SaimonseiEvents.OnHideGoddess += OnHideGoddess;
		SaimonseiEvents.OnRealmStateChanged += OnRealmStateChanged;
		SaimonseiEvents.OnRealmRevealStateChanged += OnRealmRevealStateChanged;
		SaimonseiEvents.OnWaitForLayerChoice += OnWaitForLayerChoice;
		SaimonseiEvents.OnRealmLayerChosen += OnRealmLayerChosen;
		SaimonseiEvents.OnRealmRevealCapture += OnRealmRevealCapture;

		SaimonseiEvents.OnAllRealmLayersRevealed += OnAllRealmLayersRevealed;
		//SaimonseiEvents.OnEnterRealmLayer += OnEnterRealmLayer;
		SaimonseiEvents.OnPlayCycleStart += OnPlayCycleStart;

		SaimonseiEvents.OnScoreUpdated += OnScoreUpdated;
		SaimonseiEvents.OnLandHealthChanged += OnLandHealthChanged;
		SaimonseiEvents.OnHealthCrystalTouched += OnHealthCrystalTouched;
		SaimonseiEvents.OnCrystalsInPocketUpdated += OnCrystalsInPocketUpdated;

		SaimonseiEvents.OnSkipRealmLayer += OnSkipRealmLayer;
		SaimonseiEvents.OnStoryPlayQuit += OnStoryPlayQuit;
		SaimonseiEvents.OnRestartGame += OnRestartGame;

		PocketButton.onClick.AddListener(OnPocketClicked);
	}

	private void OnDisable()
	{
		SaimonseiEvents.OnEnterRealm -= OnEnterRealm;
		SaimonseiEvents.OnHideGoddess -= OnHideGoddess;
		SaimonseiEvents.OnRealmStateChanged -= OnRealmStateChanged;
		SaimonseiEvents.OnRealmRevealStateChanged -= OnRealmRevealStateChanged;
		SaimonseiEvents.OnWaitForLayerChoice -= OnWaitForLayerChoice;
		SaimonseiEvents.OnRealmLayerChosen -= OnRealmLayerChosen;
		SaimonseiEvents.OnRealmRevealCapture -= OnRealmRevealCapture;

		SaimonseiEvents.OnAllRealmLayersRevealed -= OnAllRealmLayersRevealed;
		//SaimonseiEvents.OnEnterRealmLayer -= OnEnterRealmLayer;
		SaimonseiEvents.OnPlayCycleStart -= OnPlayCycleStart;

		SaimonseiEvents.OnScoreUpdated -= OnScoreUpdated;
		SaimonseiEvents.OnLandHealthChanged -= OnLandHealthChanged;
		SaimonseiEvents.OnHealthCrystalTouched -= OnHealthCrystalTouched;
		SaimonseiEvents.OnCrystalsInPocketUpdated -= OnCrystalsInPocketUpdated;

		SaimonseiEvents.OnSkipRealmLayer -= OnSkipRealmLayer;
		SaimonseiEvents.OnStoryPlayQuit -= OnStoryPlayQuit;
		SaimonseiEvents.OnRestartGame -= OnRestartGame;

		PocketButton.onClick.RemoveListener(OnPocketClicked);
	}


    private void Start()
    {
		InitHealthCrystals();
		InitInstruments();
	}


	private void OnEnterRealm(StoryRealm realm, int realmIndex, int realmCount)
	{
		currentRealm = realm;
		currentRealmIndex = realmIndex;

		RealmName.text = realm.RealmName;
		RealmSongName.text = realm.RealmSongName;
		RealmName.color = realm.StoryColour;
		RealmSongName.color = realm.StoryColour;
		RealmLayerName.color = realm.StoryColour;

		Background.sprite = realm.RealmBackground;
		BackgroundBW.sprite = realm.RealmBackgroundBW;		// may be null!

		FilterTint.color = realm.FilterTint;

		realmPassScore = realm.PassScore;
		realmFailed = false;

		InstrumentsPanel.gameObject.SetActive(false);

		PocketCrystalsPrompt.enabled = false;
		SlowTimePrompt.enabled = false;

		HideRealmLayers();          // from previous realm
		DeleteLayerImages();        // from previous realm

		// start the mist for next realm...
		//SaimonseiEvents.OnActivateMist?.Invoke(true);

		//// create layer images (to be unveiled by touchstones)
		//if (realm.UnveilLayers)
		//{
		//  foreach (var layer in realm.RealmLayers)
		//  {
		//      CreateLayerImage(realm, layer);
		//  }
	    //}

		UpdateProgress(0);
		//ProgressBar.image.color = ProgressFillColour;       // reset

		ProgressFill.color = ProgressFillColour;
		ProgressBackground.color = ProgressBackgroundColour;

		// init background alpha
		var initColour = new Color(Background.color.r, Background.color.g, Background.color.b, BackgroundAlphaStart);
		Background.color = initColour;

		if (BackgroundBW != null)
		{
			var initColourBW = new Color(BackgroundBW.color.r, BackgroundBW.color.g, BackgroundBW.color.b, BackgroundBWAlphaStart);
			BackgroundBW.color = initColourBW;
		}

		realm.PlayBGM(true);
	}

	private void InitInstruments()
	{
		InstrumentsPanel.gameObject.SetActive(false);

		for (int i = 0; i < InstrumentImages.Count; i++)
		{
			InstrumentBackgrounds[i].color = InstrumentEmptyColour;
			InstrumentImages[i].enabled = false;
		}

		// enable as per saved game state
		//EnableInstruments();
	}

	// enable as per saved game state
	//private void EnableInstruments()
	//{
	//	for (int i = 0; i < SavedGameState.InstrumentsGained; i++)
	//	{
	//		InstrumentBackgrounds[i].color = InstrumentFullColour;
	//		InstrumentImages[i].enabled = true;
	//	}
	//}

	private void HilightCurrentInstrument()
	{
		for (int i = 0; i < InstrumentImages.Count; i++)
		{
			InstrumentImages[i].enabled = (i <= currentRealmIndex);

			if (i == currentRealmIndex)
			{
				InstrumentBackgrounds[i].color = InstrumentHilightColour;
				InstrumentSparkles[i].Play();

				PlaySFX(NewInstrumentAudio);
			}
			else
				InstrumentBackgrounds[i].color = InstrumentImages[i].enabled ? InstrumentFullColour : InstrumentEmptyColour;
		}
	}

	// 'fade in' alpha increase
	private void AlphaInBackground(StoryRealm realm, bool skipping, bool toFull = false, Action onAlphaIn = null)
	{
		int layerIndex = realm.CurrentLayerIndex;

		if (realm.CurrentLayer != null && (!realm.CurrentLayer.FadeInBackground || realm.CurrentLayer.FadeInAlpha <= 0))
			return;

		var backgroundColour = Background.color;
		float targetTrans;

		if (realm.CurrentLayer == null)     // eg. finale
			targetTrans = 1f;
		else
			targetTrans = backgroundColour.a + realm.CurrentLayer.FadeInAlpha;

		if (targetTrans > 1f || toFull)
			targetTrans = 1f;

		var targetColour = new Color(backgroundColour.r, backgroundColour.g, backgroundColour.b, targetTrans);

		// fade in background
		LeanTween.value(Background.gameObject, backgroundColour, targetColour, LayerFadeTime)
					  .setEaseOutCirc()
					  .setOnUpdate((Color value) =>
					  {
						  Background.color = value;
					  })
					  .setOnComplete(() =>
					  {
						  if (!toFull)
							  SaimonseiEvents.OnRealmImageRevealed?.Invoke(layerIndex, skipping);

						  onAlphaIn?.Invoke();
					  });
	}


	// 'fade out' alpha decrease
	private void AlphaOutBackgroundBW(bool toZero = false)
	{
		if (BackgroundBW == null)
			return;

		var backgroundColour = BackgroundBW.color;
		float targetTrans = backgroundColour.a - BackgroundBWAlphaDecrement;

		if (targetTrans < 0f || toZero)
			targetTrans = 0f;

		var targetColour = new Color(backgroundColour.r, backgroundColour.g, backgroundColour.b, targetTrans);

		// fade out B&W backGround
		LeanTween.value(BackgroundBW.gameObject, backgroundColour, targetColour, LayerFadeTime)
					  .setEaseOutCirc()
					  .setOnUpdate((Color value) =>
					  {
						  BackgroundBW.color = value;
					  });
	}


	private Image CreateLayerImage(StoryRealm realm, RealmLayer layer)
	{
		if (layer.LayerImage == null)       // layer has no image
			return null;

		var newLayerUI = Instantiate(LayerImagePrefab, Background.transform);
		var layerImage = newLayerUI.GetComponent<Image>();

		if (realm.UnveilLayers)
		{
			newLayerUI.transform.SetAsFirstSibling();       // for unveiling
			currentRealmLayers.Insert(0, layerImage);
			layerImage.color = Color.white;             // will be fading out
		}
		else
		{
			currentRealmLayers.Add(layerImage);
			layerImage.color = Color.clear;             // will be fading in
		}

		if (RealmSplashAtStart)
			layerImage.color = Color.clear;             // see InitLayerImages

		layerImage.enabled = realm.UnveilLayers;         // visible until 'unveiled', else hidden until revealed!
		layerImage.sprite = layer.LayerImage;

		return layerImage;
	}


	private void DeleteLayerImages()
	{
		foreach (Transform layerImage in Background.transform)
		{
			Destroy(layerImage.gameObject);
		}
	}

	private void HideRealmLayers()
	{
		foreach (var layerImage in currentRealmLayers)
		{
			layerImage.enabled = false;
		}

		currentRealmLayers.Clear();
	}


	// if RealmSplashAtStart, all layers are set to clear, to show realm background image
	// so if unveiling layers, they are set to white here
	private void OnHideGoddess()
	{
		if (RealmSplashAtStart)
		{
			foreach (var layerImage in currentRealmLayers)
			{
				if (currentRealm.UnveilLayers)
					layerImage.color = Color.white;             // will be fading out
				else
					layerImage.color = Color.clear;             // will be fading in
			}
		}
	}

	private void OnRealmStateChanged(Goddess.RealmState newState, bool skipping)
	{
        switch (newState)
        {
            case Goddess.RealmState.EnteredRealm:
                ProgressBar.gameObject.SetActive(false);
				break;
            case Goddess.RealmState.StoryTold:
				//ProgressBar.gameObject.SetActive(false);
                break;
            case Goddess.RealmState.LayerReveal:
				InstrumentsPanel.gameObject.SetActive(true);
				HilightCurrentInstrument();
				ProgressBar.gameObject.SetActive(true);
                break;
            case Goddess.RealmState.LearningRealmSong:
                ProgressBar.gameObject.SetActive(true);
				AlphaInBackground(currentRealm, false, true);
				AlphaOutBackgroundBW(true);

				LearnSongText.text = "";
				LearnSongText.enabled = false;

				//UpdateProgress(0);
				//ProgressFill.color = ScoreFillColour;
				//ProgressBackground.color = ScoreBackgroundColour;
				break;
            case Goddess.RealmState.RealmSongLearnt:
                ProgressBar.gameObject.SetActive(false);
                break;
            case Goddess.RealmState.LandHealthZero:
				FadeInstrument(false, instrumentFadeTime);

				LearnSongText.text = "";
				LearnSongText.enabled = false;
				break;
        }
    }

 //   private void OnEnterRealmLayer(StoryRealm realm, RealmLayer layer, Vector3[] gridCorners)
	//{
	//	//RealmLayerName.text = layer.SpellName;
	//}

    private void OnPlayCycleStart(ModeManager.GameMode gameMode)         // learn song cycle
	{
		UpdateProgress(0);
		RealmLayerName.text = "";
		realmFailed = false;

		UpdateProgress((float)realmPassScore);          // set to full
		PulseProgress();

		ProgressFill.color = ScoreFillColour;
		ProgressBackground.color = ScoreBackgroundColour;
	}


	private void OnAllRealmLayersRevealed()
	{
		PulseProgress();
		RealmLayerName.text = "";

		LearnSongText.text = currentRealm.LearnSongText;
		LearnSongText.enabled = true;

		// clear the mist
		//SaimonseiEvents.OnActivateMist?.Invoke(false);
	}

	private void Cleanup()
	{
		PocketCrystalsPrompt.enabled = false;
		SlowTimePrompt.enabled = false;

		DeleteLayerImages();

		NextLayersPanel.gameObject.SetActive(false);
		ClearLayerChoices();

		InstrumentsPanel.gameObject.SetActive(false);
	}


	private void OnStoryPlayQuit()
	{
		Cleanup();
	}

	private void OnRestartGame()
	{
		Cleanup();
	}

	private void OnRealmRevealStateChanged(RealmReveal.RevealState newState, StoryRealm realm, bool skippingTouchstones)
	{
		int layerIndex = realm.CurrentLayerIndex;
		int layerCount = realm.LayerCount;

		switch (newState)
        {
            case RealmReveal.RevealState.Start:
				break;
            case RealmReveal.RevealState.PlaySequence:
				FadeInstrument(true, instrumentFadeTime);
				break;

            case RealmReveal.RevealState.Capture:
				break;
            case RealmReveal.RevealState.TimeOut:
				break;

			case RealmReveal.RevealState.Success:
				if (SavedGameState.SpendCrystals && realm.CurrentLayer.SpendCoins)
				{
					int spent = SavedGameState.SpendCrystalsInPocket();

					if (spent > 0)
						SaimonseiEvents.OnCrystalsInPocketUpdated?.Invoke(SavedGameState.CrystalsInPocket, spent, false, true);
				}

				if (!skippingTouchstones)
					PlaySFX(SuccessAudio);

				if (currentRealm.UnveilLayers)
				{
					if (RealmSplashAtStart)
					{
						if (layerIndex > 0)      // don't remove last layer (don't want to go back to splash)
							FadeOutLayerImage(layerIndex);
                        else
							SaimonseiEvents.OnRealmImageRevealed?.Invoke(layerIndex, false);   // next in story
					}
					else
						FadeOutLayerImage(layerIndex);
				}
				else
				{
					AlphaInBackground(realm, false);
					AlphaOutBackgroundBW(false);
					//FadeInLayerImage(realm, false);
				}

				FadeInstrument(false, instrumentFadeTime);
				break;

			case RealmReveal.RevealState.Revealed:
				if (!skippingTouchstones)
					PlaySFX(RevealedAudio);

				UpdateLayerProgress(layerIndex, layerCount);

				if (skippingTouchstones)       // skipping touchstones bypasses Success state
				{
					AlphaInBackground(realm, true);
					AlphaOutBackgroundBW(false);
					//FadeInLayerImage(realm, true);
				}

				break;

			case RealmReveal.RevealState.WaitingPlayer:
				break;

            case RealmReveal.RevealState.Fail:
				if (!skippingTouchstones)
					PlaySFX(FailedAudio);

				FadeInstrument(false, instrumentFadeTime);
				break;
        }
    }

	private void OnSkipRealmLayer()
	{
		UpdateLayerProgress(currentRealm.CurrentLayerIndex, currentRealm.LayerCount);
		//FadeInLayerImage(currentRealm, true);
		AlphaInBackground(currentRealm, true);
		AlphaOutBackgroundBW(false);
	}

	private void UpdateLayerProgress(int layerIndex, int layerCount)
	{
		if (currentRealm.UnveilLayers)      // layerCount decreases
			UpdateProgress(((float)layerCount - (float)layerIndex) / (float)layerCount);
		else                                // layerCount increases
			UpdateProgress(((float)layerIndex + 1f) / (float)layerCount);
	}


	private void OnWaitForLayerChoice(List<LayerChoice> layerChoices)
	{
		// display player choices for next layer
		NextLayersPanel.gameObject.SetActive(false);

        // populate choice buttons
		ClearLayerChoices();
		BuildNextLayerChoices(layerChoices);
	}

	private void BuildNextLayerChoices(List<LayerChoice> layerChoices)
	{
		foreach (var layerChoice in layerChoices)
		{
			var newLayerUI = Instantiate(LayerChoicePrefab, NextLayers);
			newLayerUI.SetLayerChoice(layerChoice);
			newLayerUI.gameObject.SetActive(false);
		}

		StartCoroutine(FadeInLayerChoices());
	}

	private void OnRealmLayerChosen(LayerChoice chosenLayer)
	{
		NextLayersPanel.gameObject.SetActive(false);
		ClearLayerChoices();
	}

	private void ClearLayerChoices()
	{
		foreach (Transform layerChoice in NextLayers)
		{
			Destroy(layerChoice.gameObject);
		}
	}

	private IEnumerator FadeInLayerChoices()
	{
		PlaySFX(LayerChoicesAudio);

		NextLayersPanel.gameObject.SetActive(true);

		foreach (Transform layerChoice in NextLayers)
		{
			var layerChoiceUI = layerChoice.GetComponent<LayerChoiceUI>();

			var layerChoiceImage = layerChoice.GetComponent<Image>();
			var layerChoiceText = layerChoiceUI.ChoiceText;

			var layerImageColour = layerChoiceImage.color;
			var layerTextColour = layerChoiceText.color;

			layerChoiceImage.color = Color.clear;
			layerChoiceText.color = Color.clear;

			layerChoiceUI.ChoiceButton.interactable = false;
			layerChoiceUI.gameObject.SetActive(true);

			LeanTween.value(layerChoiceImage.gameObject, Color.clear, layerImageColour, layerChoiceFadeTime)
							.setEaseInSine()
							.setOnUpdate((Color c) => layerChoiceImage.color = c)
			                .setOnComplete(() =>
                            {
								PlaySFX(LayerChoiceAudio);
							});

            LeanTween.value(layerChoiceText.gameObject, Color.clear, layerTextColour, layerChoiceFadeTime)
				            .setEaseInSine()
				            .setOnUpdate((Color c) => layerChoiceText.color = c)
				            .setOnComplete(() =>
				            {
					            layerChoiceUI.ChoiceButton.interactable = true;
				            });

			yield return new WaitForSeconds(layerChoiceButtonDelay);
		}

		yield return null;
	}


	private void OnRealmRevealCapture(int countDownTime)
	{
		FadeInstrument(false, countDownTime, InstrumentTimeOut);
	}

	private void InstrumentTimeOut()
	{
        SaimonseiEvents.OnInstrumentTimeOut?.Invoke();
		currentRealm.PlaySFX(currentRealm.CurrentLayer.TimeoutAudio);
	}

	private void OnScoreUpdated(InstrumentScore score, ScoreKeeper.ScoreRating rating)
	{
        if (realmPassScore == 0)
			return;

		UpdateScore(score, rating == ScoreKeeper.ScoreRating.Final);    // sets realmFailed flag

        if (rating == ScoreKeeper.ScoreRating.Final)
            SaimonseiEvents.OnFinalScore?.Invoke(realmFailed);      // for story mode success/fail
    }

	private void UpdateScore(InstrumentScore score, bool isFinal)
	{
		float totalScore = score.TotalScore;

		//UpdateProgress((float)totalScore / (float)realmPassScore);
		UpdateProgress(((float)realmPassScore - (float)totalScore) / (float)realmPassScore);	// start full, gradually empty
		//ProgressFill.color = (totalScore < realmPassScore) ? ProgressFillColour : ProgressFullColour;

		// failed event / audio only once per song!
		var failed = (isFinal && score.RecordedNoteCount < score.TargetNoteCount) || (totalScore > realmPassScore);
		//Debug.Log("UpdateScore: RecordedNoteCount = " + score.RecordedNoteCount + " TargetNoteCount = " + score.TargetNoteCount + " totalScore = " + totalScore + " realmPassScore = " + realmPassScore);
		if (!realmFailed && failed)
		{
			realmFailed = true;

			UpdateProgress(0f);          // set to empty
			PulseProgress();

			SaimonseiEvents.OnLearnRealmSongFailed?.Invoke();
			currentRealm.PlaySFX(FailedAudio);
		}
	}

	private void OnLandHealthChanged(float newHealth, float maxHealth)
	{
		//Debug.Log("OnLandHealthChanged: newHealth " + newHealth);

        StartCoroutine(UpdateHealthCrystals(newHealth, maxHealth));
		LandHealthText.color = newHealth < SavedGameState.LandInitHealth ? LandSickColour : LandHealthyColour;
	}


	private void FadeOutLayerImage(int layerIndex)
	{
		// get layer image
		var layer = Background.transform.GetChild(layerIndex);

		if (layer == null)
		{
			Debug.LogError("FadeOutLayerImage missing index " + layerIndex);
			return;
		}

		var image = layer.GetComponent<Image>();
		if (image == null)      // layer has no image
		{
			SaimonseiEvents.OnRealmImageRevealed?.Invoke(layerIndex, false);
			return;
		}

		//image.color = Color.clear;
		//image.enabled = true;

		// fade in panel
		LeanTween.value(image.gameObject, RevealColour, Color.clear, LayerFadeTime)
					  .setEaseOutSine()
					  .setOnUpdate((Color value) =>
					  {
						  image.color = value;
					  })
					  .setOnComplete(() =>
                      {
                          SaimonseiEvents.OnRealmImageRevealed?.Invoke(layerIndex, false);
                      });
    }

	private void FadeInLayerImage(StoryRealm realm, bool skipping, Action onFade = null)
	{
		int layerIndex = realm.CurrentLayerIndex;

		// create layer image
		var image = CreateLayerImage(currentRealm, realm.CurrentLayer);

		if (image == null)    // layer has no image
		{
			SaimonseiEvents.OnRealmImageRevealed?.Invoke(layerIndex, skipping);
			onFade?.Invoke();
			return;
		}

		image.color = Color.clear;
		image.enabled = true;

		// fade in panel
		LeanTween.value(image.gameObject, Color.clear, RevealColour, LayerFadeTime)
					  .setEaseOutCirc()
					  .setOnUpdate((Color value) =>
					  {
						  image.color = value;
					  })
					  .setOnComplete(() =>
					  {
						  SaimonseiEvents.OnRealmImageRevealed?.Invoke(layerIndex, skipping);
						  onFade?.Invoke();
					  });
	}


	private void FadeInstrument(bool fadeIn, float fadeTime, Action onFade = null)
	{
		if (instrumentFadeTween != null)
			LeanTween.cancel(instrumentFadeTween.id);

		if (fadeIn)
		{
			InstrumentCloud.color = Color.clear;
			InstrumentImage.color = Color.clear;

			if (currentRealm.CurrentLayer.InstrumentImage != null)      // layer image overrides realm image
				InstrumentImage.sprite = currentRealm.CurrentLayer.InstrumentImage;
            else
				InstrumentImage.sprite = currentRealm.InstrumentImage;

			instrumentFadeTween = LeanTween.value(InstrumentImage.gameObject, Color.clear, Color.white, fadeTime)
						  .setEaseOutCubic()
						  .setOnUpdate((Color value) =>
						  {
							  InstrumentCloud.color = value;
							  InstrumentImage.color = value;
						  })
					      .setOnComplete(() =>
					      {
							  onFade?.Invoke();
						  });
		}
		else
		{
			instrumentFadeTween = LeanTween.value(InstrumentImage.gameObject, Color.white, Color.clear, fadeTime)
						.setEaseInQuad()
						.setOnUpdate((Color value) =>
						{
							InstrumentCloud.color = value;
							InstrumentImage.color = value;
						})
						.setOnComplete(() =>
						{
							onFade?.Invoke();
						});
		}
	}

	private void UpdateProgress(float newValue)
	{
		if (ProgressBar.value == newValue)
			return;

		if (newValue == 0)
			ProgressBar.image.color = ProgressFillColour;       // reset

		LeanTween.value(ProgressBar.gameObject, ProgressBar.value, newValue, progressUpdateTime)
			  .setEaseInCirc()
			  .setOnUpdate((float value) =>
			  {
				  ProgressBar.value = value;
			  });

		if (newValue != 0f)
			currentRealm.PlaySFX(ProgressAudio);
	}

	private void PulseProgress()
	{
		LeanTween.scale(ProgressBar.gameObject, Vector3.one * progressPulseScale, progressPulseTime)
					.setEase(LeanTweenType.easeInCubic)
                    .setLoopPingPong(1);
	}

	private void InitHealthCrystals()
	{
		for (int i = 0; i < MaxCrystals; i++)
		{
			var crystal = Instantiate(CrystalPrefab, LandHealthCrystals.transform);
			crystal.gameObject.SetActive(false);

			float gradientValue = (float)i / (float)(MaxCrystals - 1);
			crystal.SetColour(CrystalColours.Evaluate(gradientValue));

			crystal.SetNumber(i + 1);
		}

		activeCrystalCount = 0;
	}


	private void DeleteHealthCrystals()
	{
		foreach (Transform crystal in LandHealthCrystals.transform)
		{
			Destroy(crystal.gameObject);
		}

	}

	private IEnumerator UpdateHealthCrystals(float newHealth, float maxHealth)
	{
		var healthPerCrystal = maxHealth / (float)MaxCrystals;
		int activeCrystals = (int)(newHealth / healthPerCrystal);       // round down to int

		if (activeCrystals == activeCrystalCount)     // no change!
			yield break;

		//PulseHealthCrystals();

		bool crystalsIncreased = activeCrystals > activeCrystalCount;
		int counter = 1;

		activeCrystalCount = 0;

		foreach (Transform crystal in LandHealthCrystals.transform)
		{
			bool activate = counter <= activeCrystals;
			crystal.gameObject.SetActive(activate);

			if (activate)
			{
				activeCrystalCount++;

				var healthCrystal = crystal.GetComponent<HealthCrystal>();
				healthCrystal.PulseSparkles(crystalPulseScale, crystalPulseTime);

				PlaySFX(CrystalAudio);
			}
			else if (counter == 1)
			{
				// audio if no active crystals (ie. first crystal not active)
				PlaySFX(NoCrystalsAudio);
			}

			counter++;
			yield return new WaitForSeconds(crystalInterval);

			// audio on last active crystal
			if (activeCrystalCount > 0 && counter == activeCrystals + 1)
			{
				if (crystalsIncreased)
				{
					PlaySFX(CrystalsUpAudio);
				}
				else 
				{
					PlaySFX(CrystalsDownAudio);
				}
			}
		}

        yield return null;
	}


	private void PulseHealthCrystals()
	{
		LeanTween.scale(LandHealthCrystals.gameObject, Vector3.one * crystalPulseScale, crystalPulseTime)
					.setEase(LeanTweenType.easeInCubic)
					.setLoopPingPong(1);
	}


	private void OnHealthCrystalTouched(HealthCrystal crystal)
	{
		//if (!RealmReveal.StoryRealmsInPlay)
		//	return;

		if (pocketingCrystals)           // can't tap while still flashing after pocketing (messy)
			return;

		int crystalsToPocket = (activeCrystalCount - crystal.CrystalNumber) + 1;  // include touched crystal!

		if (crystalsFlashingFrom > 0)           // one touch at a time!
		{
            // 'pocket' the flashing crystals
			pocketingCrystals = true;
			PocketCrystalsPrompt.enabled = false;

			crystalsToPocket = (activeCrystalCount - crystalsFlashingFrom) + 1;         // as touched before
			SaimonseiEvents.OnHealthCrystalsPocketed?.Invoke(crystalsToPocket, MaxCrystals);
			return;
		}

		if (crystal.CrystalNumber == 1)         // can't pocket all crystals! (zero health)
			return;

		PocketCrystalsPrompt.text = "Tap Crystals Again to Pocket " + crystalsToPocket + "!";
		PocketCrystalsPrompt.enabled = true;

		FlashCrystals(crystal.CrystalNumber);
	}

    // update one at a time for visual effect
	private IEnumerator IncrementPocketCrystals(int totalCrystalsInPocket, int increment, bool reset, bool spent)
	{
		if (reset)
		{
			PocketCrystals.text = totalCrystalsInPocket.ToString();
			yield break;
		}

		var prevCount = totalCrystalsInPocket - (spent ? -increment : increment);

		for (int i = 0; i < increment; i++)
		{
			PocketCrystals.text = (prevCount + (spent ? -i : i) + (spent ? -1 : 1)).ToString();
			PlaySFX(PocketAudio);

			yield return new WaitForSeconds(updatePocketCrystalInterval);
		}

		yield return null;
	}

	private void FlashCrystals(int fromCrystalNumber)
	{
		if (crystalsFlashingFrom > 0)
			return;

		crystalsFlashingFrom = fromCrystalNumber;
		int pulseCount = 0;

		//Debug.Log("FlashCrystals: fromCrystalNumber = " + fromCrystalNumber + ", LandHealthCrystals.childCount = " + LandHealthCrystals.transform.childCount);
		for (int i = fromCrystalNumber-1; i < activeCrystalCount; i++)  
		{
			var crystalObj = LandHealthCrystals.transform.GetChild(i);      // zero index
			var crystal = crystalObj.GetComponent<HealthCrystal>();
			crystal.PulseSparkles(crystalPulseScale * crystalPocketScaleFactor, crystalPulseTime, NumCrystalFlashes);

			pulseCount++;
		}

		if (pulseCount > 0)
			StartCoroutine(CrystalFlashAudio(crystalPulseTime));
	}

    // audio when crystals are mid-pulse (ie. enlarged)
	private IEnumerator CrystalFlashAudio(float pulseTime)
	{
		yield return new WaitForSecondsRealtime(pulseTime);
		FlashAudio();

		for (int i = 0; i < NumCrystalFlashes - 1; i++)
		{
			PocketCrystalsPrompt.enabled = false;
			yield return new WaitForSecondsRealtime(pulseTime);

            if (! pocketingCrystals)        // prompt disabled on second tap
			    PocketCrystalsPrompt.enabled = true;

			yield return new WaitForSecondsRealtime(pulseTime);
			FlashAudio();
		}

		FlashCrystalsReset();       // re-enable tap detection
		yield return null;
	}

	private void FlashAudio()
	{
		PlaySFX(CrystalsPocketFlashAudio);
	}


	private void FlashCrystalsReset()
	{
		crystalsFlashingFrom = 0;
		pocketingCrystals = false;
		PocketCrystalsPrompt.enabled = false;
		PocketCrystalsPrompt.text = "Tap Crystals Again to Pocket!";
	}

	private void OnPocketClicked()
	{
		PocketButton.interactable = false;
		SlowTimePrompt.enabled = true;

		LeanTween.value(SlowTimePrompt.gameObject, Color.clear, Color.white, slowTimePromptTime)
			            .setEase(LeanTweenType.easeOutSine)
                        .setOnUpdate((Color c) => { SlowTimePrompt.color = c; })
			            .setLoopPingPong(1)
						.setOnComplete(() =>
                        {
                            SlowTimePrompt.enabled = false;
							PocketButton.interactable = true;
						});
	}

	private void OnCrystalsInPocketUpdated(int totalCrystalsInPocket, int increment, bool reset, bool spent)
	{
        // animate pocket crystals incrementing (or decrementing)
        StartCoroutine(IncrementPocketCrystals(totalCrystalsInPocket, increment, reset, spent));

		PocketCrystals.transform.localScale = Vector3.one;
		PocketImage.transform.localScale = Vector3.one;

		LeanTween.scale(PocketCrystals.gameObject, Vector3.one * pocketPulseScale, pocketPulseTime)
				    .setEase(LeanTweenType.easeInCubic)
				    .setLoopPingPong(1);

		LeanTween.scale(PocketImage.gameObject, Vector3.one * pocketPulseScale, pocketPulseTime)
					.setEase(LeanTweenType.easeInCubic)
					.setLoopPingPong(1);
	}


	public override void Show(bool visible)
	{
		UIPanel.SetActive(visible);
	}
}
