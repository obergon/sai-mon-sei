﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GoddessUI : UIPanel
{
    [Space]
    public GameObject UIPanel;

    public Image StoryPanel;
    public Text StoryText;

    [Space]
    public Image InstrumentPanel;
    public Text InstrumentText;

    [Space]
    public Slider ProgressBar;
    public Image ProgressFill;
    public Color ProgressColour;                // reveal layers / learning song
    public Color ProgressFullColour;            // learning song

    [Space]
    public Button BackButton;
    public Button LearnButton;
    public Text LearnButtonText;

    [Space]
    public AudioClip ProgressAudio;
    public AudioClip NewInstrumentAudio;

    [Header("Saved Data")]
    [Tooltip("Data saved between game sessions")]
    public SavedGameState SavedGameState;        // land health, pocket crystals, realm and layer attempts, completed flag, etc

    [Header("Saved Player State")]
    public PlayerSO PlayerData;                 // age, health

    //[Header("Reset saved data")]
    //public Button ResetButton;
    //public AudioClip ResetAudio;

    [Header("Toggle memes on/off")]
    public Toggle MemesToggle;
    public Text MemesOnOff;
    public AudioClip MemesAudio;

    [Header("Skip Realms")]
    public bool CanSkipRealms = true;        // skip to next realm
    public Button SkipButton;

    public Button CompleteAllButton;        // testing only

    private const float storyPause = 0.5f;
    private const float storyIntroTime = 1f;

    private const float pulseScale = 1.25f;
    private const float pulseTime = 0.25f;

    private float progressUpdateTime = 0.25f;       // slider 
    private float progressPulseScale = 1.75f;
    private float progressPulseTime = 0.25f;

    private bool realmCompleted = false;        // completed previously (can skip)


    public void OnEnable()
    {
        SaimonseiEvents.OnEnterRealm += OnEnterRealm;
        SaimonseiEvents.OnRealmStateChanged += OnRealmStateChanged;
        SaimonseiEvents.OnInstrumentAcquired += OnInstrumentAcquired;
        SaimonseiEvents.OnInstrumentAcquired += OnInstrumentAcquired;
        SaimonseiEvents.OnGameReset += OnGameReset;

        BackButton.onClick.AddListener(OnBackButton);
        LearnButton.onClick.AddListener(OnLearnButton);
        //ResetButton.onClick.AddListener(OnResetClicked);
        //CompleteAllButton.onClick.AddListener(OnCompleteAllClicked);

        MemesToggle.onValueChanged.AddListener(OnMemesToggled);

        ToggleMemes(PlayerData.DoMemes, true);     // init

        if (CanSkipRealms)
            SkipButton.onClick.AddListener(OnSkipClicked);
    }

    public void OnDisable()
    {
        SaimonseiEvents.OnEnterRealm -= OnEnterRealm;
        SaimonseiEvents.OnRealmStateChanged -= OnRealmStateChanged;
        SaimonseiEvents.OnInstrumentAcquired -= OnInstrumentAcquired;
        SaimonseiEvents.OnGameReset -= OnGameReset;

        BackButton.onClick.RemoveListener(OnBackButton);
        LearnButton.onClick.RemoveListener(OnLearnButton);      // start searching for pieces
        //ResetButton.onClick.RemoveListener(OnResetClicked);
        //CompleteAllButton.onClick.RemoveListener(OnCompleteAllClicked);

        MemesToggle.onValueChanged.RemoveListener(OnMemesToggled);

        if (CanSkipRealms)
            SkipButton.onClick.RemoveListener(OnSkipClicked);
    }


    private void OnEnterRealm(StoryRealm realm, int realmIndex, int realmCount)
    {
        InstrumentPanel.gameObject.SetActive(false);
        InstrumentText.text = "";

        UpdateProgress((float)realmIndex / (float)realmCount);
        TellStory(realm);

        realmCompleted = SavedGameState.RealmCompleted(realm.RealmName);

        SkipButton.gameObject.SetActive(CanSkipRealms);
        SkipButton.interactable = false;

        if (realm.LayerCount == 0)
            LearnButtonText.text = "LEARN SONG...";
        else
            LearnButtonText.text = "ENTER REALM...";

        //ResetButton.gameObject.SetActive(realmIndex == 0);      // reset from first realm only
    }

    private void OnInstrumentAcquired(string instrumentText)
    {
        //InstrumentPanel.gameObject.SetActive(true);
        InstrumentText.text = "The Goddess gave you " + instrumentText + "!";

        PlaySFX(NewInstrumentAudio);
    }

    private void OnRealmStateChanged(Goddess.RealmState newState, bool skipping)
    {
        switch (newState)
        {
            case Goddess.RealmState.EnteredRealm:
                LearnButton.interactable = false;
                //SkipButton.interactable = false;
                break;
            case Goddess.RealmState.StoryTold:
                LearnButton.interactable = true;
                PulseButton(LearnButton, pulseScale, pulseTime);
                SkipButton.interactable = CanSkipRealms && realmCompleted;
                break;
            case Goddess.RealmState.LayerReveal:
                break;
            case Goddess.RealmState.LearningRealmSong:
                break;
            case Goddess.RealmState.RealmSongLearnt:
                break;
            case Goddess.RealmState.LandHealthZero:
                break;
        }
    }

    private void OnBackButton()
    {
        SaimonseiEvents.OnStartMenu?.Invoke(); 
    }


    // unfold story - animate instrument 'dropping down'?
    private void TellStory(StoryRealm realm)
    {
        Color panelColour = StoryPanel.color;
        StoryPanel.color = Color.clear;
        StoryText.color = Color.clear;
		//StoryPanel.transform.localScale = Vector3.zero;

		StoryText.text = string.Format("{0}{1}{2}", "\"", realm.Story, "\"");
		//StoryText.text = realm.Story;

        // fade in panel
        LeanTween.value(StoryPanel.gameObject, Color.clear, panelColour, storyIntroTime)
                      .setEaseInCirc()
                      .setOnUpdate((Color value) =>
                      {
                          StoryPanel.color = value;
                      });

        //LeanTween.scale(StoryPanel.gameObject, Vector3.one, storyIntroTime)
        //                .setEaseInOutCirc();

        // fade in text
        LeanTween.value(StoryText.gameObject, Color.clear, realm.StoryColour, storyIntroTime)
                    .setEaseInCirc()
                    .setDelay(storyPause)
                    .setOnUpdate((Color value) =>
                        {
                            StoryText.color = value;
                        })
                    .setOnComplete(() =>
                    {
                        SaimonseiEvents.OnRealmStateChanged?.Invoke(Goddess.RealmState.StoryTold, false);
                    });
    }


    public void UpdateProgress(float newValue)
    {
        if (ProgressBar.value == newValue)
            return;

        if (newValue == 0)
            ProgressBar.image.color = ProgressColour;       // reset

        LeanTween.value(ProgressBar.gameObject, ProgressBar.value, newValue, progressUpdateTime)
              .setEaseInCirc()
              .setOnUpdate((float value) =>
              {
                  ProgressBar.value = value;
              });

        if (newValue != 0f)
            PlaySFX(ProgressAudio);
    }

    private void PulseProgress()
    {
        LeanTween.scale(ProgressBar.gameObject, Vector3.one * progressPulseScale, progressPulseTime)
                    .setEase(LeanTweenType.easeInCubic)
                    .setLoopPingPong(1);
    }

    private void OnLearnButton()
    {
        SaimonseiEvents.OnEnterLayers?.Invoke();        // skip straight to realm song if no layers!
    }


    //private void OnResetClicked()
    //{
    //    ResetGame();
    //}


    private void OnGameReset()
    {
        ResetGame();
    }

    private void ResetGame()
    {
        SavedGameState.InitGameState(false);
        PlayerData.InitPlayerData();

        SaimonseiEvents.OnUpdateLandHealth?.Invoke(SavedGameState.LandHealthPercent, false);

        if (SavedGameState.CrystalsInPocket != 0)
            SaimonseiEvents.OnCrystalsInPocketUpdated?.Invoke(SavedGameState.CrystalsInPocket, 0, true, false);

        //PlaySFX(ResetAudio);
    }

    //private void OnCompleteAllClicked()
    //{
    //    SaimonseiEvents.OnCompleteAllRealms?.Invoke();
    //    PlaySFX(CompleteAllAudio);
    //}


    private void OnMemesToggled(bool on)
    {
        ToggleMemes(on, false);
    }

    private void ToggleMemes(bool on, bool silent)
    {
        SaimonseiEvents.OnToggleMemes?.Invoke(on);
        MemesOnOff.text = on ? "MEMES ON" : "MEMES OFF";

        if (!silent)
            PlaySFX(MemesAudio);
    }


    private void OnSkipClicked()
    {
        // bypass entire realm
        if (CanSkipRealms)
            SaimonseiEvents.OnSkipRealm?.Invoke();
    }

    private void PulseButton(Button button, float scaleFactor, float scaleTime)
    {
        var startScale = button.transform.localScale;

        LeanTween.scale(button.gameObject, startScale * scaleFactor, scaleTime)
                                .setEase(LeanTweenType.easeOutSine)
                                .setOnComplete(() =>
                                {
                                    LeanTween.scale(button.gameObject, startScale, scaleTime / 2f)    // TODO: magic number!
                                            .setEase(LeanTweenType.easeInSine);
                                });
    }


    public override void Show(bool visible)
	{
        UIPanel.SetActive(visible);
	}
}
