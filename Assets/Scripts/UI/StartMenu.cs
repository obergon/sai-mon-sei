﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartMenu : UIPanel
{
    [Space]
    public Button CoreGameButton;
    public Button StoryModeButton;
    public Button QuitButton;

    public AudioClip MenuMusic;


    public void OnEnable()
    {
        CoreGameButton.onClick.AddListener(CoreGameMode);
        StoryModeButton.onClick.AddListener(StoryGameMode);
        QuitButton.onClick.AddListener(QuitGame);

        //TODO: temp - remove!
        //CoreGameButton.interactable = false;
    }

    public void OnDisable()
    {
        CoreGameButton.onClick.RemoveListener(CoreGameMode);
        StoryModeButton.onClick.RemoveListener(StoryGameMode);
        QuitButton.onClick.RemoveListener(QuitGame);
    }


    private void CoreGameMode()
    {
        SaimonseiEvents.OnGameModeSelected?.Invoke(ModeManager.GameMode.SaiMonSei);
    }

    private void StoryGameMode()
    {
        SaimonseiEvents.OnGameModeSelected?.Invoke(ModeManager.GameMode.StoryMode);
    }


    public override void Show(bool visible)
    {
        base.Show(visible);

        if (visible)
        {
            if (! IsBGMPlaying)
               PlayBGM();
        }
        else
        {
            StopBGM();
            //MenuMusic.Stop();
        }
    }


    private void QuitGame()
    {
        Application.Quit();
    }
}
