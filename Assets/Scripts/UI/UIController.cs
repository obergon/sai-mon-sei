﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;


public class UIController : MonoBehaviour
{
	public UIPanel StartMenuPanel;
	public UIPanel MainMenuPanel;       // leaderboard mode
	public UIPanel PlayAreaPanel;

	public UIPanel RealmPanel;          // story mode
	public UIPanel GoddessPanel;        // story mode
	public UIPanel PlayerPanel;         // age, health, memes

	public RealmUI RealmUI;             // story mode
	public GoddessUI GoddessUI;         // story mode
	public PlayerUI PlayerUI;           // age, health, memes

	public Image LivingGrid;            // shader
	public RawImage LivingGridVideo;       // render texture

	public Image Blackout;
	public Color BlackoutColour;
	public float FadeToBlackTime;

	public AudioSource UIAudioSource;
	public AudioClip FadeInAudio;

	[Header("Saved Player State")]
	public PlayerSO PlayerData;

	//public bool DoMemes;                // player age, health & meme choices

	public ParticleSystem Mist;
	public float MaxMistEmission;          // ie. when land health is zero
	public float StartMenuMistFactor;      // lighter mist on start menu

	private ParticleSystem.EmissionModule mistEmitter;
	private float emissionRate;                 // current rate over time constant
	private float storyMistEmissionFactor = 6f;      // rate reduced while goddess story

	public UIPanel CurrentPanel { get; private set; }


	private void OnEnable()
	{
		SaimonseiEvents.OnStartMenu += OnStartMenu;
		SaimonseiEvents.OnGameModeSelected += OnGameModeSelected;
		//SaimonseiEvents.OnActivateMist += ActivateMist;

		// leaderboard mode
		SaimonseiEvents.OnStartNewGame += OnStartNewGame;
		SaimonseiEvents.OnMainMenu += OnMainMenu;

		// story mode
		SaimonseiEvents.OnEnterRealm += OnEnterRealm;
		SaimonseiEvents.OnRealmStateChanged += OnRealmStateChanged;
		SaimonseiEvents.OnLandHealthChanged += OnLandHealthChanged;
		SaimonseiEvents.OnStoryPlayQuit += OnStoryPlayQuit;
		SaimonseiEvents.OnRestartGame += OnRestartGame;

		// memes
		SaimonseiEvents.OnToggleMemes += OnToggleMemes;
	}

	private void OnDisable()
	{
		SaimonseiEvents.OnStartMenu -= OnStartMenu;
		SaimonseiEvents.OnGameModeSelected -= OnGameModeSelected;
		//SaimonseiEvents.OnActivateMist -= ActivateMist;

		SaimonseiEvents.OnStartNewGame -= OnStartNewGame;
		SaimonseiEvents.OnMainMenu -= OnMainMenu;

		SaimonseiEvents.OnEnterRealm -= OnEnterRealm;
		SaimonseiEvents.OnRealmStateChanged -= OnRealmStateChanged;
		SaimonseiEvents.OnLandHealthChanged -= OnLandHealthChanged;
		SaimonseiEvents.OnStoryPlayQuit -= OnStoryPlayQuit;
		SaimonseiEvents.OnRestartGame -= OnRestartGame;

		// memes
		SaimonseiEvents.OnToggleMemes -= OnToggleMemes;
	}


	private void Start()
	{
		LeanTween.init(1600);

		StartMenuPanel.gameObject.SetActive(false);
		FadeToPanel(StartMenuPanel);

		PlayerPanel.Show(false);            // only if DoMemes

		mistEmitter = Mist.emission;
		SetMistEmission(MaxMistEmission * StartMenuMistFactor);

		//ActivateMist(true);
		Mist.Play();
	}


	private void OnGameModeSelected(ModeManager.GameMode mode)
	{
		switch (mode)
		{
			case ModeManager.GameMode.SaiMonSei:
				FadeToPanel(MainMenuPanel);
				RealmUI.gameObject.SetActive(false);
	            GoddessUI.gameObject.SetActive(false);
				PlayerUI.gameObject.SetActive(false);
				break;

			case ModeManager.GameMode.StoryMode:        // handled by Goddess
				SetMistEmission(MaxMistEmission);
				RealmUI.gameObject.SetActive(true);
				GoddessUI.gameObject.SetActive(true);
				PlayerUI.gameObject.SetActive(true);
				break;
		}
	}


	private void OnStartMenu()
	{
		FadeToPanel(StartMenuPanel, false, HideAll);
		SetMistEmission(emissionRate / storyMistEmissionFactor);
	}

	private void OnMainMenu()
	{
		FadeToPanel(MainMenuPanel);
	}

	//private void ActivateMist(bool turnOn)
	//{
	//    if (turnOn)
	//    {
	//        if (!Mist.isPlaying)
	//            Mist.Play();

	//        SetMistEmission(emissionRate);
	//    }
	//    else
	//        SetMistEmission(0);
	//    //Mist.Stop();
	//}


	// leaderboard mode
	private void OnStartNewGame(string songEventname, PlayArea.PlayMode mode)
	{
		FadeToPanel(PlayAreaPanel);
	}

	// story mode
	private void OnEnterRealm(StoryRealm realm, int realmIndex, int realmCount)
	{
		FadeToPanel(GoddessPanel, false, ShowRealm);
	}

	private void OnRealmStateChanged(Goddess.RealmState newState, bool skipping)
	{
		switch (newState)
		{
			case Goddess.RealmState.EnteredRealm:
				SetMistEmission(emissionRate / storyMistEmissionFactor);
				break;
			case Goddess.RealmState.StoryTold:
				SetMistEmission(emissionRate);      // full rate
				break;
			case Goddess.RealmState.LayerReveal:
				SetMistEmission(emissionRate);      // full rate
				FadeToPanel(PlayAreaPanel, false, HideGoddess);
				break;
			case Goddess.RealmState.LearningRealmSong:
				FadeToPanel(PlayAreaPanel, true, null);       // force fade 
				break;
			case Goddess.RealmState.RealmSongLearnt:
				break;
			case Goddess.RealmState.LandHealthZero:
				break;
		}
	}

	private void OnLandHealthChanged(float newHealth, float maxHealth)
	{
		// mist emission at max when land health is zero
		var percentHealth = (newHealth / maxHealth) * 100f;
		var percentMist = (100f - percentHealth) / 100f;

		SetMistEmission(MaxMistEmission * percentMist);
	}


	private void OnToggleMemes(bool isOn)
	{
		PlayerData.DoMemes = isOn;
	}


	private void HideMemes()
	{
		if (PlayerData.DoMemes)
			PlayerPanel.Show(false);
	}

	private void OnStoryPlayQuit()
	{
		HideMemes();
	}

	private void OnRestartGame()
	{
		HideMemes();
	}

	private void SetMistEmission(float rateOverTime)
	{
		mistEmitter.rateOverTime = rateOverTime;
		emissionRate = mistEmitter.rateOverTime.constant;
		//Debug.Log("SetMistEmission" + emissionRate);
	}


	private void ShowRealm()
	{
		RealmPanel.Show(true);              // realm persists under goddess / play area panels

		if (PlayerData.DoMemes)
			PlayerPanel.Show(false);

		LivingGrid.enabled = false;
		//LivingGridVideo.enabled = false;
	}

	private void HideRealm()
	{
		RealmPanel.Show(false);

		LivingGrid.enabled = true;
		//LivingGridVideo.enabled = true;
	}

	private void ShowGoddess()
	{
		GoddessPanel.Show(true);
	}

	private void HideGoddess()
	{
		GoddessPanel.Show(false);
		SaimonseiEvents.OnHideGoddess?.Invoke();

		if (PlayerData.DoMemes)
			PlayerPanel.Show(true);
	}

	private void ShowPlayArea()
	{
		PlayAreaPanel.Show(true);
	}

	private void HidePlayArea()
	{
		PlayAreaPanel.Show(false);
	}

	private void HideAll()
	{
		HideRealm();
		HideGoddess();
		HidePlayArea();
	}

	// fade to black

	private void FadeToPanel(UIPanel toPanel, bool force = false, Action OnFaded = null)
	{
		if (!force && CurrentPanel == toPanel)
			return;

		Blackout.color = Color.clear;
		Blackout.gameObject.SetActive(true);

		LeanTween.value(Blackout.gameObject, Color.clear, BlackoutColour, FadeToBlackTime / 2f)
							.setEase(LeanTweenType.easeOutQuart)
							.setOnUpdate((Color col) => Blackout.color = col)
							.setOnComplete(() =>
							{
								// switch panels while blacked out
								//if (hideCurrent && CurrentPanel != null)
								if (CurrentPanel != null)
									CurrentPanel.Show(false);

								CurrentPanel = toPanel;
								CurrentPanel.Show(true);

								PlaySFX(FadeInAudio);
								//if (FadeInAudio != null)
								//    AudioSource.PlayClipAtPoint(FadeInAudio, Vector3.zero);

								OnFaded?.Invoke();

								// clear blackout
								LeanTween.value(Blackout.gameObject, BlackoutColour, Color.clear, FadeToBlackTime / 2f)
											.setEase(LeanTweenType.easeInQuart)
											.setOnUpdate((Color col) => Blackout.color = col)
											.setOnComplete(() =>
											{
												Blackout.gameObject.SetActive(false);
											});
							});
	}

	private void PlaySFX(AudioClip sfx)
	{
		if (sfx == null)
			return;

		UIAudioSource.clip = sfx;
		UIAudioSource.Play();
	}
}
