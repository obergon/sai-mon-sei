﻿using System;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class Track
{
    public string Version = "1.0.0";

	public Vector2 GridSize = new Vector2(2, 2);         // x & y// TODO: was Vector2Int

	public List<TrackNote> Notes;


	public Track()
	{
		Notes = new List<TrackNote>();
	}

	// constructor from json
	public Track(string jsonString)
	{
		InitFromJson(jsonString);
	}

	public string ToJson(bool prettyPrint)
	{
		return JsonUtility.ToJson(this, prettyPrint);
	}

	public void InitFromJson(string jsonString)
	{
		Track track = JsonUtility.FromJson<Track>(jsonString);

		Version = track.Version;
		GridSize = track.GridSize;
		Notes = track.Notes;
	}

	public void AddNote(TrackNote note)
	{
		Notes.Add(note);
	}

	public TrackNote GetNoteByNumber(int noteNumber)
	{
		foreach (var trackNote in Notes)
		{
			if (trackNote.NoteNumber == noteNumber)
				return trackNote;
		}
		return null;
	}
}
