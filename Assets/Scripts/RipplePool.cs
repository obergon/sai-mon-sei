﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class RipplePool : MonoBehaviour
{
    public int PoolSize;
    public Ripple RipplePrefab;

    private List<Ripple> Pool = new List<Ripple>();

    private void Start()
    {
        InitPool();
    }

    private void InitPool()
    {
        for (int i = 0; i < PoolSize; i++)
        {
            CreateRipple();
        }
    }

    public Ripple GetRipple()
    {
        foreach (var ripple in Pool)
        {
            if (! ripple.gameObject.activeSelf)
            {
                ripple.gameObject.SetActive(true);
                return ripple;
            }
        }

        // no ripples available! create a new one
        var newRipple = CreateRipple();
        newRipple.gameObject.SetActive(true);
        return newRipple;
    }

    //public void PoolRipple(Ripple ripple)
    //{
    //    ripple.Reset();
    //}

    //private void DisableRipple()
    //{
    //    ripple.gameObject.SetActive(false);
    //}

    private Ripple CreateRipple()
    {
        var newRipple = Instantiate(RipplePrefab, transform);
        //newRipple.transform.localScale = Vector3.zero;
        newRipple.gameObject.SetActive(false);

        Pool.Add(newRipple);
        return newRipple;
    }

    public void ResetRipples()
    {
        foreach (var ripple in Pool)
        {
            ripple.Reset();         // shrink and pool
        }
    }
}
