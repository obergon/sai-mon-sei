﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    public List<AudioSource> AudioSources = new List<AudioSource>();
    //public List<SourceMixer> SourceMappings = new List<SourceMixer>();

    [Space]
    public float FadeTime = 1f;


    private void PlayAudio(AudioSource source, AudioClip clip, bool fade = false)
    {
        if (clip == null)
            return;

        source.clip = clip;

        if (fade)
            FadeAudioVolume(source, true);
        else
            source.Play();
    }

    private void StopAudio(AudioSource source, bool fade = false)
    {
        if (fade)
            FadeAudioVolume(source, false);
        else
            source.Stop();
    }

    private void FadeAudioVolume(AudioSource source, bool fadeIn)
    {
        if (fadeIn)
        {
            source.volume = 0;
            source.Play();
        }

        LeanTween.value((fadeIn ? 0f : 1f), (fadeIn ? 1f : 0f), FadeTime)
                                .setEaseOutQuad()
                                .setOnUpdate((float x) => source.volume = x)
                                .setOnComplete(() =>
                                {
                                    if (!fadeIn)
                                        source.Stop();
                                });
    }
}


//[Serializable]
//[CreateAssetMenu(fileName = "SourceMixer", menuName = "Audio/Source-Mixer Mapping")]
//public class SourceMixer : ScriptableObject
//{
//    public AudioSource Source;
//    public AudioMixer Mixer;
//}

[Serializable]
//[CreateAssetMenu(fileName = "ClipOutput", menuName = "Audio/Clip-Output Mapping")]
public class ClipOutput : MonoBehaviour
{
    public AudioClip Clip;
    public AudioSource Output;

    [Range(0f, 1f)]
    public float Volume;
}
