﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class StoryRealm : MonoBehaviour
{
    public TrackManager RealmSong;
    [Tooltip("Title for scoring purposes")]
    public string RealmSongTitle;           // for scoring

    public string RealmName;

    [Tooltip("Title for display purposes")]
    public string RealmSongName;            // for display

    [TextArea(maxLines: 10, minLines: 6)]
    public string Story;

    [TextArea(maxLines: 6, minLines: 2)]
    public string LearnSongText;

    [Tooltip("Name of instrument awarded by Goddess")]
    public string InstrumentText;

    [Tooltip("Image of instrument awarded by Goddess")]
    public Sprite InstrumentImage;

    public Sprite RealmBackgroundBW;
    public Sprite RealmBackground;
    public Color StoryColour;

    [Tooltip("Coloured filter for this Realm")]
    public Color FilterTint = Color.clear;

    [Tooltip("Story fade-in time")]
    public float StoryTextTime = 1f;

    public AudioClip RealmEntryAudio;

    [Header("Layers to Unveil / Reveal")]
    [Tooltip("Catalog of all Layers in this Realm (for internal tracking)")]
    public List<RealmLayer> RealmLayers = new List<RealmLayer>();

    [Space]
    [Tooltip("Unveil or Reveal layers?")]
    [HideInInspector]
    public bool UnveilLayers;

    [Tooltip("Message on Touchstone Sequence 'fail'")]
    [TextArea(maxLines: 10, minLines: 2)]
    public string FailMessage = "The Land has Suffered";

    [Tooltip("Message when Realm Song Learnt")]
    [TextArea(maxLines: 10, minLines: 2)]
    public string SongLearntMessage = "Success!";

    [Space]
    [Tooltip("Ambient Music audio source during this realm/layers")]
    public AudioSource RealmBGMAudioSource;
    public float BGMFadeTime = 1f;

    [Tooltip("Sound FX audio source during this realm/layers")]
    public AudioSource RealmSFXAudioSource;

    [Header("Learn Song")]

    public int LearnGridWidth;
    public int LearnGridHeight;

    [HideInInspector]
    public float Tolerance;

    [Tooltip("Score required to 'learn' song")]
    public int PassScore = 1000;

    [Tooltip("Land health increase % on song learnt")]
    public float LandHealth;

    [Tooltip("Land health decrease % on learn song 'fail'")]
    public float FailLandHealth = 5f;

 

    public int CurrentLayerIndex { get; private set; }

    public RealmLayer CurrentLayer { get { return LayerCount > 0 ? RealmLayers[ CurrentLayerIndex ] : null; }}
    public int LayerCount { get { return RealmLayers.Count; } }
    public bool CurrentLayerIsLast { get { return (UnveilLayers || LayerCount == 0) ? CurrentLayerIndex == 0 : CurrentLayerIndex == LayerCount-1; } }


    //[Header("Saved Data Asset")]
    //[Tooltip("Realm Asset Data")]
    //[Space]
    //public RealmSO RealmData;


    private void OnEnable()
    {
        SaimonseiEvents.OnEnterRealm += OnEnterRealm;
        SaimonseiEvents.OnRealmRevealStateChanged += OnRealmRevealStateChanged;
        SaimonseiEvents.OnRealmLayerChosen += OnRealmLayerChosen;
        SaimonseiEvents.OnSkipRealmLayer += OnSkipRealmLayer;
        SaimonseiEvents.OnStartMenu += OnStartMenu;
    }

    private void OnDisable()
    {
        SaimonseiEvents.OnEnterRealm -= OnEnterRealm;
        SaimonseiEvents.OnRealmRevealStateChanged -= OnRealmRevealStateChanged;
        SaimonseiEvents.OnRealmLayerChosen -= OnRealmLayerChosen;
        SaimonseiEvents.OnSkipRealmLayer -= OnSkipRealmLayer;
        SaimonseiEvents.OnStartMenu -= OnStartMenu;
    }


    private void OnEnterRealm(StoryRealm realm, int realmIndex, int realmCount)
    {
        if (realm.LayerCount == 0)
            CurrentLayerIndex = 0;
        else
            CurrentLayerIndex = UnveilLayers ? realm.RealmLayers.Count-1 : 0;

        realm.PlaySFX(realm.RealmEntryAudio);
    }

    private void OnRealmRevealStateChanged(RealmReveal.RevealState newState, StoryRealm realm, bool skippingTouchstones)
    {
        switch (newState)
        {
            case RealmReveal.RevealState.Start:
            case RealmReveal.RevealState.PlaySequence:
            case RealmReveal.RevealState.Capture:
                break;

            case RealmReveal.RevealState.Success:                   // layer image revealing
                SaimonseiEvents.OnUpdateLandHealth?.Invoke(CurrentLayer.SuccessLandHealth, true);
                break;

            case RealmReveal.RevealState.Revealed:                  // on to next layer to reveal!
                //Debug.Log("NEXT LAYER: " + newState);
                NextLayer();
                break;

            case RealmReveal.RevealState.WaitingPlayer:
                break;

            case RealmReveal.RevealState.TimeOut:
            case RealmReveal.RevealState.Fail:
                SaimonseiEvents.OnUpdateLandHealth?.Invoke(-CurrentLayer.FailLandHealth, true);
                break;

            case RealmReveal.RevealState.Touched:
                break;
        }

        //Debug.Log("StoryRealm.OnRealmRevealStateChanged " + newState + " CurrentLayerIndex " + CurrentLayerIndex);
    }

    private void NextLayer()
    {
        if (! CurrentLayer.IsLastLayer)
        {
            if (CurrentLayer.IsMultipleChoice)  
            {
                //Debug.Log("NextLayer: CHOICES - CurrentLayerIndex = " + CurrentLayerIndex + " Choices = " + CurrentLayer.NextLayerChoices.Count + " Layer = " + CurrentLayer.VeilName);
                SaimonseiEvents.OnWaitForLayerChoice?.Invoke(CurrentLayer.NextLayerChoices);
            }
            else                                                 // no choices...
            {
                CurrentLayerIndex = CurrentLayer.NextLayerChoices[0].TargetLayer.transform.GetSiblingIndex();
                //Debug.Log("NextLayer: NO CHOICES - CurrentLayerIndex = " + CurrentLayerIndex + " Layer = " + CurrentLayer.VeilName);
                SaimonseiEvents.OnPlayCurrentRealmLayer?.Invoke();
            }
        }
        else
        {
            // all layers of realm revealed!  enable learn song!
            SaimonseiEvents.OnAllRealmLayersRevealed?.Invoke();
        }
    }

    private void OnRealmLayerChosen(LayerChoice chosenLayer)
    {
        CurrentLayerIndex = chosenLayer.TargetLayer.transform.GetSiblingIndex();
        //Debug.Log("OnRealmLayerChosen: CurrentLayerIndex: " + CurrentLayerIndex + " CurrentLayer.SpellName = " + CurrentLayer.SpellName);

        SaimonseiEvents.OnPlayCurrentRealmLayer?.Invoke();

        if (chosenLayer.HealthPercent != 0f)
            SaimonseiEvents.OnUpdateLandHealth?.Invoke(chosenLayer.HealthPercent, true);
    }

    private void OnSkipRealmLayer()
    {
        NextLayer();
    }


    private void OnStartMenu()
    {
        StopBGM(true);
    }


    public void PlayBGM(bool fade)
    {
        if (fade)
            FadeBGMVolume(true);
        else
            RealmBGMAudioSource.Play();
    }

    public void StopBGM(bool fade)
    {
        if (fade)
            FadeBGMVolume(false);
        else
            RealmBGMAudioSource.Stop();
    }

    public void PlaySFX(AudioClip clip)
    {
        if (clip == null)
            return;

        RealmSFXAudioSource.clip = clip;
        RealmSFXAudioSource.Play();
    }

    private void FadeBGMVolume(bool fadeIn)
    {
        //Debug.Log("FadeBGMVolume: " + RealmName + " fadeIn = " + fadeIn);
        if (fadeIn)
        {
            RealmBGMAudioSource.volume = 0;
            RealmBGMAudioSource.Play();
        }

        LeanTween.value((fadeIn ? 0f : 1f), (fadeIn ? 1f : 0f), BGMFadeTime)
                                .setEaseOutQuad()
                                .setOnUpdate((float x) => RealmBGMAudioSource.volume = x)
                                .setOnComplete(() =>
                                {
                                    if (! fadeIn)
                                        RealmBGMAudioSource.Stop();
                                });
    }
}
