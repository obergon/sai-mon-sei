﻿


using System;
/// <summary>
/// Plays a note for the 
/// </summary>
public class InstrumentPlayer : FMODPlayer
{
    public void OnEnable()
    {
		InitBackingTrack();         // only one instrument (0)
		//InitInstruments();        // 4 extra instruments (1-4)
	}

	public void OnDisable()
	{
		Cleanup();
	}


	public void PlayNote(int noteNumber)
	{
		base.PlayNote(0, noteNumber);
	}

	public void StopNote(int noteNumber)
	{
		base.StopNote(0, noteNumber);
	}
}
