﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Effectively the Player. Bard playing on behalf of The Goddess
/// </summary>
public class Goddess : MonoBehaviour
{
    public List<StoryRealm> Realms = new List<StoryRealm>();            // must be at least one realm!

    public AudioSource GoddessSFXAudioSource;
    public AudioClip NewGameAudio;
    public AudioClip GameOverAudio;

    public float GameOverPause;

    private int currentRealmIndex = 0;
    private StoryRealm CurrentRealm { get { return Realms[ currentRealmIndex ]; } }

    private const float landMaxHealth = 100f;

    [Header("Saved Data ")]
    [Tooltip("Data saved between game sessions")]
    public SavedGameState SavedGameState;            // land health, pocket crystals, realm and layer attempts, completed flag, etc

    [Header("Dev hidden keys")]
    public KeyCode CompleteAllKey = KeyCode.RightArrow;         // + Command
    public KeyCode ResetKey = KeyCode.R;                        // + Command
    public AudioClip CompleteAllAudio;
    public AudioClip ResetAudio;

    public enum RealmState              // state during cycle through a realm
    {
        EnteredRealm,               // Goddess telling story, giving instrument - text / instrument animation
        StoryTold,                  // story told by goddess, instrument given - player ready to unveil layers (touchstones)
        LayerReveal,                // in play area - search mode
        LearningRealmSong,          // on click next after successful 'search' (in play area - learn mode)
        RealmSongLearnt,            // on click next after successfully played song -> enter next realm
        LandHealthZero              // restart game
    }

    public static RealmState CurrentRealmState;


    public void OnEnable()
    {
        SaimonseiEvents.OnGameModeSelected += OnGameModeSelected;
        SaimonseiEvents.OnStoryPlayQuit += OnStoryPlayQuit;

        SaimonseiEvents.OnRealmStateChanged += OnRealmStateChanged;
        SaimonseiEvents.OnUpdateLandHealth += OnUpdateLandHealth;
        SaimonseiEvents.OnHealthCrystalsPocketed += OnHealthCrystalsPocketed;

        SaimonseiEvents.OnRestartGame += OnRestartGame;
        SaimonseiEvents.OnSkipRealm += OnSkipRealm;
        SaimonseiEvents.OnEnterLayers += OnEnterLayers;

        SaimonseiEvents.OnCompleteAllRealms += OnCompleteAllRealms;         // testing only
    }

    public void OnDisable()
    {
        SaimonseiEvents.OnGameModeSelected -= OnGameModeSelected;
        SaimonseiEvents.OnStoryPlayQuit -= OnStoryPlayQuit;

        SaimonseiEvents.OnRealmStateChanged -= OnRealmStateChanged;
        SaimonseiEvents.OnUpdateLandHealth -= OnUpdateLandHealth;
        SaimonseiEvents.OnHealthCrystalsPocketed -= OnHealthCrystalsPocketed;

        SaimonseiEvents.OnRestartGame -= OnRestartGame;
        SaimonseiEvents.OnSkipRealm -= OnSkipRealm;
        SaimonseiEvents.OnEnterLayers -= OnEnterLayers;

        SaimonseiEvents.OnCompleteAllRealms -= OnCompleteAllRealms;         // testing only
    }


    // for testing only!!
    private void Update()
    {
        if ((Input.GetKey(KeyCode.RightCommand) || Input.GetKey(KeyCode.LeftCommand)) && Input.GetKeyDown(CompleteAllKey))
        {
            SaimonseiEvents.OnCompleteAllRealms?.Invoke();
            PlaySFX(CompleteAllAudio);
        }

        else if ((Input.GetKey(KeyCode.RightCommand) || Input.GetKey(KeyCode.LeftCommand)) && Input.GetKeyDown(ResetKey))
        {
            SaimonseiEvents.OnGameReset?.Invoke();
            PlaySFX(ResetAudio);
        }
    }

    private void OnEnterLayers()
    {
        // skip to realm song if no layers!
        if (CurrentRealm.LayerCount == 0)
            SaimonseiEvents.OnRealmStateChanged?.Invoke(RealmState.LearningRealmSong, false);
        else
            SaimonseiEvents.OnRealmStateChanged?.Invoke(RealmState.LayerReveal, false);      // -> playmode to find the pieces of sheet music
    }

    private void OnRealmStateChanged(RealmState newState, bool skipping)
    {
        CurrentRealmState = newState;

        switch (CurrentRealmState)
        {
            case RealmState.EnteredRealm:
                if (! skipping)
                    SavedGameState.IncrementRealmAttempts(CurrentRealm.RealmName);
                break;

            case RealmState.StoryTold:
                SaimonseiEvents.OnInstrumentAcquired?.Invoke(CurrentRealm.InstrumentText);
                break;

            case RealmState.LayerReveal:
                SaimonseiEvents.OnUpdateLandHealth?.Invoke(SavedGameState.LandHealthPercent, false);     // init mist
                RevealRealmLayers();
                break;

            case RealmState.LearningRealmSong:
                LearnSong();
                break;

            case RealmState.RealmSongLearnt:
                //// start the mist for next realm...
                //SaimonseiEvents.OnActivateMist(true);
                if (!skipping)
                {
                    SavedGameState.SetRealmCompleted(CurrentRealm.RealmName);
                    SaimonseiEvents.OnUpdateLandHealth?.Invoke(CurrentRealm.LandHealth, true);
                }
                EnterNextRealm();
                break;

            case RealmState.LandHealthZero:
                SavedGameState.ZeroHealthCount++;
                PlaySFX(GameOverAudio);
                //SavedGameState.ResetLandHealth();
                break;
        }
    }

    private void OnGameModeSelected(ModeManager.GameMode mode)
    {
        switch (mode)
        {
            case ModeManager.GameMode.SaiMonSei:      // handled by UI Controller
                break;

            case ModeManager.GameMode.StoryMode:
                Init(true);
                EnterCurrentRealm();

                PlaySFX(NewGameAudio);
                break;
        }

        SavedGameState.PlayCount++;
    }

    private void Init(bool resetHealth = false)
    {
        SaimonseiEvents.OnCrystalsInPocketUpdated?.Invoke(SavedGameState.CrystalsInPocket, 0, true, false);

        if (resetHealth || SavedGameState.LandHealthPercent == 0)       // player may not have restarted after land health zero
            SaimonseiEvents.OnUpdateLandHealth?.Invoke(SavedGameState.LandInitHealth, false);
        else
            SaimonseiEvents.OnUpdateLandHealth?.Invoke(SavedGameState.LandHealthPercent, false);

        InitRealms();
    }

    private void OnUpdateLandHealth(float newHealth, bool increment)
    {
        if (increment)
            SavedGameState.LandHealthPercent += newHealth;
        else
            SavedGameState.LandHealthPercent = newHealth;

        if (SavedGameState.LandHealthPercent > landMaxHealth)
            SavedGameState.LandHealthPercent = landMaxHealth;

        if (SavedGameState.LandHealthPercent <= 0)
        {
            SavedGameState.LandHealthPercent = 0;
            SaimonseiEvents.OnRealmStateChanged?.Invoke(RealmState.LandHealthZero, false);
        }

        SaimonseiEvents.OnLandHealthChanged?.Invoke(SavedGameState.LandHealthPercent, landMaxHealth);
    }


    // pocketing crystals reduces health (but increases 'score factor') 
    private void OnHealthCrystalsPocketed(int numCrystalsPocketed, int maxCrystals)
    {
        SavedGameState.CrystalsInPocket += numCrystalsPocketed;
        SaimonseiEvents.OnCrystalsInPocketUpdated?.Invoke(SavedGameState.CrystalsInPocket, numCrystalsPocketed, false, false);

        float healthPerCrystal = landMaxHealth / (float)maxCrystals;

        //Debug.Log("OnHealthCrystalsSpent: numCrystalsSpent = " + numCrystalsSpent + ", healthTaken = " + (numCrystalsSpent * healthPerCrystal) + ", currentLandHealth = " + currentLandHealth);
        SaimonseiEvents.OnUpdateLandHealth?.Invoke(-numCrystalsPocketed * healthPerCrystal, true);     // health (therefore crystals) reduced
    }

    private void OnRestartGame()
    {
        ReEnterFirstRealm();
    }

    private void OnSkipRealm()
    {
        EnterNextRealm();
    }

    private void EnterCurrentRealm()
    {
        CurrentRealm.gameObject.SetActive(true);        // start listening for OnRealmRevealStateChanged

        SaimonseiEvents.OnEnterRealm?.Invoke(CurrentRealm, currentRealmIndex, Realms.Count);
        SaimonseiEvents.OnRealmStateChanged?.Invoke(RealmState.EnteredRealm, false);

        //Debug.Log("EnterCurrentRealm: " + CurrentRealm.RealmName);
    }


    private void EnterNextRealm()
    {
        CurrentRealm.gameObject.SetActive(false);       // stop listening for OnRealmRevealStateChanged

        if (currentRealmIndex == Realms.Count - 1)      // already at end of realms 
            currentRealmIndex = 0;                      // completed all realms - return to first realm
        else
            currentRealmIndex++;

        EnterCurrentRealm();
    }

    // on restart after land 'died'...
    private void ReEnterFirstRealm()
    {
        CurrentRealm.gameObject.SetActive(false);       // stop listening for OnRealmRevealStateChanged

        Init(true);             // reset land health to init value
        EnterCurrentRealm();
    }

    private void InitRealms()
    {
        currentRealmIndex = 0;

        foreach (var realm in Realms)
        {
            realm.gameObject.SetActive(false);
        }
    }

    private void OnStoryPlayQuit()
    {
        // re-enter realm
        EnterCurrentRealm();
    }

    private void RevealRealmLayers()
    {
         SaimonseiEvents.OnStartNewGame?.Invoke(CurrentRealm.RealmSong.SongEventName, PlayArea.PlayMode.RevealLayers);
    }

    private void LearnSong()
    {
        SaimonseiEvents.OnSongSelected?.Invoke(CurrentRealm.RealmSongTitle, CurrentRealm.RealmSong.SongEventName, PlayArea.PlayMode.LearnSong, CurrentRealm.LearnGridWidth, CurrentRealm.LearnGridHeight, CurrentRealm.Tolerance);
        SaimonseiEvents.OnStartNewGame?.Invoke(CurrentRealm.RealmSong.SongEventName, PlayArea.PlayMode.LearnSong);
    }


    // testing only

    private void OnCompleteAllRealms()
    {
        SavedGameState.CompleteAllRealms(Realms);
    }


    public void PlaySFX(AudioClip clip)
    {
        if (clip == null)
            return;

        GoddessSFXAudioSource.clip = clip;
        GoddessSFXAudioSource.Play();
    }
}
