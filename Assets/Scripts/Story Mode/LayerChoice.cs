﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class LayerChoice
{
    [Tooltip("Text shown to player on choice button")]
    public string ChoiceText;

    [Tooltip("Layer to navigate to")]
    public RealmLayer TargetLayer;

    [Range(-100, 100)]
    [Tooltip("Percentage of Land Health increased/decreased by this choice")]
    public float HealthPercent = 0;
}


