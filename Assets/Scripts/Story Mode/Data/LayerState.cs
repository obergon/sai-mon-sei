﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class LayerState
{
    public string LayerName;

    public int AttemptCount;        // number of times started (touchstone sequence played)
    public bool Completed;          // successfully repeated touchstone sequence
}

