﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Story Mode only
/// </summary>
[CreateAssetMenu(fileName = "SavedGameState", menuName = "StoryMode/Saved Game State")]
public class SavedGameState : ScriptableObject
{
    [Tooltip("Name of Story (set of Realms)")]
    public string StoryName = "The Goddess and The Bard";

    [Tooltip("Number of times Story played")]
    public int PlayCount;

    [Header("Land Health")]
    [Range(1, 100)]
    [Tooltip("Initial Land Health percentage at start of game")]
    public float LandInitHealth = 5f;       // starts very sick

    [Tooltip("Current Land Health (translates to Crystals)")]
    public float LandHealthPercent;

    [Tooltip("Number of times Land Health reached zero")]
    public int ZeroHealthCount;

    [Header("Pocket")]
	public int CrystalsInPocket;
	[Tooltip("Crystals in pocket spent on successful touchstone sequence?")]
    [HideInInspector]
	public bool SpendCrystals;       // on successful touchstone sequence
	[Tooltip("Crystals that have been spent")]
    [HideInInspector]
    public int CrystalsSpent;

	[Tooltip("Factor by which pocketed crystals increase note intervals\n" +
                                        "The lower the number, the greater effect of each crystal on time")]
    public float PocketSpeedFactor = 5f;


    public float PocketedCrystalsSpeedFactor
    {
        get
        {
            if (PocketSpeedFactor > 0 && CrystalsInPocket > 0)
            {
                float crystalFactor = (float)CrystalsInPocket / PocketSpeedFactor;
                return 1f + crystalFactor;      // time will be multiplied by this factor
            }
            return 1f;
        }
    }

    [Header("Realms Played")]
    public List<RealmState> Realms = new List<RealmState>();


    public int SpendCrystalsInPocket()
    {
        int spent = CrystalsInPocket;
        CrystalsSpent += spent;
        CrystalsInPocket = 0;
        return spent;
    }

    public void IncrementRealmAttempts(string realmName)
    {
        if (string.IsNullOrEmpty(realmName))
            return;

        var realmState = GetRealm(realmName);
        realmState.AttemptCount++;
    }

    public void SetRealmCompleted(string realmName)
    {
        if (string.IsNullOrEmpty(realmName))
            return;

        var realmState = GetRealm(realmName);
        realmState.SongLearnt = true;
    }

    public bool RealmCompleted(string realmName)
    {
        if (string.IsNullOrEmpty(realmName))
            return false;

        var realm = GetRealm(realmName, false);
        if (realm == null)
            return false;

        return realm.SongLearnt;
    }


    public void IncrementLayerAttempts(string realmName, string layerName)
    {
        if (string.IsNullOrEmpty(realmName) || string.IsNullOrEmpty(layerName))
            return;

        var realmState = GetRealm(realmName);
        var layerState = realmState.GetLayer(layerName);
        layerState.AttemptCount++;
    }

    public void SetLayerCompleted(string realmName, string layerName)
    {
        if (string.IsNullOrEmpty(realmName) || string.IsNullOrEmpty(layerName))
            return;

        var realmState = GetRealm(realmName);
        var layerState = realmState.GetLayer(layerName);
        layerState.Completed = true;
    }

    public bool RealmLayerCompleted(string realmName, string layerName)
    {
        if (string.IsNullOrEmpty(realmName) || string.IsNullOrEmpty(layerName))
            return false;

        var realm = GetRealm(realmName, false);
        if (realm == null)
            return false;

        var layer = realm.GetLayer(layerName, false);
        if (layer == null)
            return false;

        return layer.Completed;
    }


    private RealmState GetRealm(string realmName, bool createNew = true)
    {
        if (string.IsNullOrEmpty(realmName))
            return null;

        foreach (var realmState in Realms)
        {
            if (realmState.RealmName == realmName)
                return realmState;
        }

        if (!createNew)
            return null;

        var newRealmState = new RealmState
        {
            RealmName = realmName
        };

        Realms.Add(newRealmState);
        return newRealmState;
    }

    public void ResetLandHealth()
    {
        LandHealthPercent = LandInitHealth;
        CrystalsInPocket = 0;
        CrystalsSpent = 0;

        SpendCrystals = false;
    }

    public void InitGameState(bool keepCrystals)
    {
        if (!keepCrystals)
            ResetLandHealth();

        PlayCount = 0;
        ZeroHealthCount = 0;

        Realms.Clear();
    }

    public void CompleteAllRealms(List<StoryRealm> realms)
    {
        foreach (var realm in realms)
        {
            foreach (var layer in realm.RealmLayers)
            {
                SetLayerCompleted(realm.RealmName, layer.VeilName);
            }

            SetRealmCompleted(realm.RealmName);
        }
    }

    public string AnalyticsData
    {
        get { return string.Format("PlayCount: {0} Health: {1}", PlayCount, LandHealthPercent); }
    }

    // JSON for Analytics
    public string ToJson(bool prettyPrint)
    {
        return JsonUtility.ToJson(this, prettyPrint);
    }
}
