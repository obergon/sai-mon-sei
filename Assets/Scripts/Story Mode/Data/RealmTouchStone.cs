﻿using System;
using UnityEngine;


/// <summary>
/// A RealmTouchStone represents a single point ('touchstone') in the sequence required to unlock (reveal) a RealmLayer
/// </summary>
[Serializable]
public class RealmTouchStone
{
    [Tooltip("FMOD InstrumentPlayer Note to play")]
    public int NoteNumber;

    [Tooltip("Interval (in beats) before next note is played")]
    public float IntervalBeats = 0.5f;      // time after note - expressed in beats

    [Tooltip("Duration of visual effect")]
    public float FlashTime = 0.5f;         // time for which GridPoint is expanded

    [Space]
    [Tooltip("AudioClip to play (if no FMOD note)")]
    public AudioClip InstrumentNote;

    [HideInInspector]
    public Vector2 GridCoords;                  // grid coordinates - random assigned at runtime
    //public Vector2 Position;                  // viewport coordinates (0-1, 0-1) // TODO: for manually placed (design-time) touch stones
}
