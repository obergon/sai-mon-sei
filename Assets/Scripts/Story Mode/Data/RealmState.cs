﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class RealmState
{
    public string RealmName;

    public int AttemptCount;        // number of times entered realm
    public bool SongLearnt;         // song learnt

    [Header("Layers Played")]
    public List<LayerState> Layers = new List<LayerState>();


    public LayerState GetLayer(string layerName, bool createNew = true)
    {
        if (string.IsNullOrEmpty(layerName))
            return null;

        foreach (var layerState in Layers)
        {
            if (layerState.LayerName == layerName)
                return layerState;
        }

        if (!createNew)
            return null;

        var newLayerState = new LayerState
        {
            LayerName = layerName
        };

        Layers.Add(newLayerState);
        return newLayerState;
    }
}
