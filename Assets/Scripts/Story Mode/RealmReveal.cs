﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// For Story Mode - reveal layers in a Realm (by learning touchstone sequence)
/// Handles OnTouchStart event for capturing sequence
/// </summary>
[RequireComponent(typeof(PlayArea))]         // core game loop functions / UI
public class RealmReveal : MonoBehaviour
{
    [Header("Saved Data")]
    [Tooltip("Data saved between game sessions")]
    public SavedGameState SavedGameState;               // land health, pocket crystals, realm and layer attempts, completed flag, etc

    [Tooltip("Tapping (inside grid area) does same as hitting Play button")]
    public bool TapToPlay = true;

    [TextArea(1, 3)]
    public string GameOverText;                     // land zero health

    private GridPoint[,] layerGrid;            // touchstone grid for current realm layer
    private Vector2 lastGridCoords = new Vector2(-1, -1);

    private Vector2 RandomGridCoords
    {
        // make sure not same as last grid coords
        get
        {
            Vector2 randomCoords;

            do
            {
                randomCoords = new Vector2(UnityEngine.Random.Range(0, layerGrid.GetLength(0)), UnityEngine.Random.Range(0, layerGrid.GetLength(1)));
            }
            while (randomCoords.x == lastGridCoords.x && randomCoords.y == lastGridCoords.y);

            // got a different one
            lastGridCoords = randomCoords;
            return randomCoords;
        }
    }

    private IEnumerator captureCountdownCoroutine = null;
    private int touchCaptureCountdown = 0;
    private int touchCount = 0;

    private float playPause = 0.5f;                 // before playing sequence
    private float captureFeedbackSpeed = 0.5f;       // speed of fade in message for touchstone feedback

    private string lastLayerOutroStory = "";

    private PlayArea playArea;                      // required component!
    private ModeManager.GameMode gameMode;          // this script only 'interested in' Story mode
    private StoryRealm currentRealm;

    private bool learnRealmSongFailed = false;

    private bool StoryRealmsInPlay;

    private bool canTouchToPlay = false;            // when play button enabled

    private int memesShowing = 0;                   // number of memes on-screen

    public enum RevealState
    {
        Start,
        PlaySequence,   // revealing touchstone sequence (PIN code) for current layer
        Capture,        // capturing player touchstone sequence (PIN code) for current layer
        Touched,        // touch registered - to prevent multiple
        TimeOut,        // capture not completed within time limit for layer
        Success,        // sequence successfully repeated
        Revealed,       // reveal layer after fade in/out
        WaitingPlayer,   // waiting for player to choose next layer -> PlaySequence
        Fail            // sequence not successfully repeated (try again)
    }

    private RevealState currentState;


    private void Awake()
    {
        playArea = GetComponent<PlayArea>();
    }

    private void OnEnable()
    {
        SaimonseiEvents.OnGameModeSelected += OnGameModeSelected;
        SaimonseiEvents.OnEnterRealm += OnEnterRealm;
        SaimonseiEvents.OnRealmStateChanged += OnRealmStateChanged;
        SaimonseiEvents.OnStartNewGame += OnStartNewGame;
        SaimonseiEvents.OnInitGrid += OnInitGrid;
        SaimonseiEvents.OnPlayEnabled += OnPlayEnabled;           // play button
        SaimonseiEvents.OnPlay += OnPlay;           // play button clicked

        SaimonseiEvents.OnTouchStart += OnTouchStart;                           // for touchstones (not learning song)
        SaimonseiEvents.OnPlayCurrentRealmLayer += PlayCurrentRealmLayer;       // touch stones
		SaimonseiEvents.OnWaitForLayerChoice += OnWaitForLayerChoice;
        SaimonseiEvents.OnFinalScore += OnFinalScore;

        SaimonseiEvents.OnLearnRealmSongFailed += OnLearnRealmSongFailed;
        SaimonseiEvents.OnRealmImageRevealed += OnRealmImageRevealed;
        SaimonseiEvents.OnInstrumentTimeOut += OnInstrumentTimeOut;

        SaimonseiEvents.OnMemeCountUpdated += OnMemeCountUpdated;

        SaimonseiEvents.OnLeaveGame += OnLeaveGame;
    }

    private void OnDisable()
    {
        SaimonseiEvents.OnGameModeSelected -= OnGameModeSelected;
        SaimonseiEvents.OnEnterRealm -= OnEnterRealm;
        SaimonseiEvents.OnRealmStateChanged -= OnRealmStateChanged;
        SaimonseiEvents.OnStartNewGame -= OnStartNewGame;
        SaimonseiEvents.OnInitGrid -= OnInitGrid;
        SaimonseiEvents.OnPlayEnabled -= OnPlayEnabled;
        SaimonseiEvents.OnPlay -= OnPlay;           // play button clicked

        SaimonseiEvents.OnTouchStart -= OnTouchStart;
        SaimonseiEvents.OnPlayCurrentRealmLayer -= PlayCurrentRealmLayer;
		SaimonseiEvents.OnWaitForLayerChoice -= OnWaitForLayerChoice;
        SaimonseiEvents.OnFinalScore -= OnFinalScore;

        SaimonseiEvents.OnLearnRealmSongFailed -= OnLearnRealmSongFailed;
        SaimonseiEvents.OnRealmImageRevealed -= OnRealmImageRevealed;
        SaimonseiEvents.OnInstrumentTimeOut -= OnInstrumentTimeOut;

        SaimonseiEvents.OnMemeCountUpdated -= OnMemeCountUpdated;

        SaimonseiEvents.OnLeaveGame -= OnLeaveGame;
    }


	private void OnGameModeSelected(ModeManager.GameMode mode)
    {
        gameMode = mode;
    }

    private void OnEnterRealm(StoryRealm realm, int realmIndex, int realmCount)
    {
        currentRealm = realm;
        learnRealmSongFailed = false;
        StoryRealmsInPlay = false;
    }


    private void OnLeaveGame(ulong clientID)
    {
        StoryRealmsInPlay = false;
    }

    // story mode
    private void OnRealmStateChanged(Goddess.RealmState newState, bool skipping)
    {
        switch (newState)
        {
            case Goddess.RealmState.EnteredRealm:
            case Goddess.RealmState.StoryTold:
                break;

            case Goddess.RealmState.LayerReveal:        // stepping through layers
                StoryRealmsInPlay = true;
                break;

            case Goddess.RealmState.LearningRealmSong:
                currentRealm.StopBGM(true);
                break;

            case Goddess.RealmState.RealmSongLearnt:
                break;

            case Goddess.RealmState.LandHealthZero:
                //playArea.FadeGameOver("The Land must be Spared from further Suffering... Tap to Try Again!", currentRealm.StoryTextTime);
                playArea.FadeGameOver(GameOverText, currentRealm.StoryTextTime);
                //playArea.FadeStory(GameOverTryAgain, currentRealm.StoryTextTime, false, currentRealm.StoryColour);
                lastLayerOutroStory = "";
                currentRealm.StopBGM(true);
                break;
        }
    }

    private void OnStartNewGame(string songEventName, PlayArea.PlayMode mode)
    {
        if (gameMode != ModeManager.GameMode.StoryMode)
            return;

        if (mode != PlayArea.PlayMode.RevealLayers)
            return;

        playArea.SetPlayMode(mode);
        PlayCurrentRealmLayer();
    }


    private void OnFinalScore(bool failed)
    {
        if (gameMode != ModeManager.GameMode.StoryMode)
            return;

        var songName = "'" + currentRealm.RealmSongName + "'";
        bool hasRealmSongMessage = !string.IsNullOrEmpty(currentRealm.SongLearntMessage);
        bool hasFailMessage = !string.IsNullOrEmpty(currentRealm.FailMessage);

        string message = failed ? ((hasFailMessage ? (currentRealm.FailMessage + "\n") : "") + "Tap to Try Again!")
                                : hasRealmSongMessage ? currentRealm.SongLearntMessage : ("You learnt the Song of The Realm!");

        playArea.FadeStory(message, currentRealm.StoryTextTime, learnRealmSongFailed, currentRealm.StoryColour);

        // disable song once learnt (to disable touch input)
        if (!failed)
            SaimonseiEvents.OnTrackStop?.Invoke(currentRealm.RealmSong.SongEventName);
    }

    private void PlayCurrentRealmLayer()
    {
        learnRealmSongFailed = false;

        // create 'touchstone' grid (no animation)
        var currentLayer = currentRealm.CurrentLayer;

        playArea.InitRevealGrid(currentLayer.SearchGridWidth, currentLayer.SearchGridHeight, currentRealm.Tolerance);  // no animation

        // combine outro of last layer (if present) with intro of current layer
        bool hasLastOutro = !string.IsNullOrEmpty(lastLayerOutroStory);
        var introStory = string.Format("{0}{1}{2}", (hasLastOutro ? lastLayerOutroStory : ""), (hasLastOutro ? "\n...\n" : ""), CurrentLayerIntro());
        playArea.FadeStory(introStory, currentRealm.StoryTextTime, true, currentRealm.StoryColour);

        SaimonseiEvents.OnEnterRealmLayer?.Invoke(currentRealm, currentLayer, playArea.GridCorners);

        currentRealm.PlaySFX(currentRealm.CurrentLayer.EntryAudio);

        // layer entry cost
        //Debug.Log("PlayCurrentRealmLayer: cost = " + currentLayer.EntryCostLandHealth);
        SaimonseiEvents.OnUpdateLandHealth?.Invoke(-currentLayer.EntryCostLandHealth, true);
    }

	private string CurrentLayerIntro()
    {
         return currentRealm.CurrentLayer.IntroStory;
    }

    private string CurrentLayerOutro()
    {
        bool hasOutro = !string.IsNullOrEmpty(currentRealm.CurrentLayer.OutroStory);
        return hasOutro ? (currentRealm.CurrentLayer.OutroStory + "\n\n") : "";
    }

    private void OnLearnRealmSongFailed()
    {
        learnRealmSongFailed = true;
        SaimonseiEvents.OnUpdateLandHealth?.Invoke(-currentRealm.CurrentLayer.FailLandHealth, true);
    }

    private void OnInitGrid(GridPoint[,] grid)
    {
        if (gameMode != ModeManager.GameMode.StoryMode)
            return;

        layerGrid = grid;
    }


    private void OnPlayEnabled(bool enabled)
    {
        canTouchToPlay = enabled;
    }

    // 'play' button clicked
    private void OnPlay()
    {
        if (gameMode != ModeManager.GameMode.StoryMode)
            return;

        if (memesShowing > 0)
            return;

        learnRealmSongFailed = false;

        //Debug.Log("OnPlay: Goddess.CurrentRealmState = " + Goddess.CurrentRealmState);

        if (Goddess.CurrentRealmState != Goddess.RealmState.LandHealthZero                // game restart on 'play'
                        && Goddess.CurrentRealmState != Goddess.RealmState.LearningRealmSong)      // handled by PlayArea / TrackManager
        {
            // bypass touchstone sequence according to flag or if there are no touchstones
            if (currentRealm.CurrentLayer.SkipTouchstones || currentRealm.CurrentLayer.TouchStones.Count == 0)
            {
                SetRevealState(RevealState.Revealed, true);

                if (!string.IsNullOrEmpty(currentRealm.RealmName) && !string.IsNullOrEmpty(currentRealm.CurrentLayer.VeilName))
                {
                    SavedGameState.IncrementLayerAttempts(currentRealm.RealmName, currentRealm.CurrentLayer.VeilName);
                    SavedGameState.SetLayerCompleted(currentRealm.RealmName, currentRealm.CurrentLayer.VeilName);
                }
            }
            else
                StartCoroutine(PlayRandomTouchSequence());
        }
    }

    private IEnumerator PlayRandomTouchSequence()
    {
        SetRevealState(RevealState.PlaySequence);

        if (!string.IsNullOrEmpty(currentRealm.RealmName) && !string.IsNullOrEmpty(currentRealm.CurrentLayer.VeilName))
            SavedGameState.IncrementLayerAttempts(currentRealm.RealmName, currentRealm.CurrentLayer.VeilName);

        yield return new WaitForSecondsRealtime(playPause);

        RealmTouchStone lastTouchStone = null;
        int touchStoneCount = 0;

        // set random coords, flash at that location and play note for each 'touchstone' in the layer
        foreach (var touchStone in currentRealm.CurrentLayer.TouchStones)
        {
            lastTouchStone = touchStone;
            touchStoneCount++;

            touchStone.GridCoords = RandomGridCoords;

            // play the touchstone note
            currentRealm.CurrentLayer.PlayTouchstoneNote(touchStone);

            // flash
            var gridPoint = layerGrid[(int)touchStone.GridCoords.x, (int)touchStone.GridCoords.y];
            gridPoint.gridDot.gameObject.SetActive(true);
            gridPoint.Expand(touchStone.FlashTime * 1000, true, true);    // milliseconds

            // interval before note / flash (not last touchstone)
            if (touchStoneCount < currentRealm.CurrentLayer.TouchStones.Count)
                yield return new WaitForSecondsRealtime(NoteIntervalSecs(touchStone.IntervalBeats) * SavedGameState.PocketedCrystalsSpeedFactor);
        }

        // wait for last crystal + a small margin
        float pauseTime = lastTouchStone.FlashTime + currentRealm.CurrentLayer.PauseAfterPlay;
        yield return new WaitForSecondsRealtime(pauseTime); 

        StopCaptureCountdown();     // if running
        StartCaptureCountdown();
        yield return null;
    }

    private float NoteIntervalSecs(float beats)
    {
        return (60f / currentRealm.CurrentLayer.BeatsPerMinute * beats);
    }

    // compare touched stones with those in the layer TouchStones sequence
    private void OnTouchStart(Vector2 touchPosition)
    {
        if (gameMode != ModeManager.GameMode.StoryMode)
            return;

        if (!StoryRealmsInPlay)
            return;

        if (memesShowing > 0)     // must make meme choice or wait for it to fade      
            return;

        var worldPosition = Camera.main.ScreenToWorldPoint(touchPosition);

        if (!playArea.TouchedInsideGrid(worldPosition))
            return;

        if (TapToPlay && canTouchToPlay)
        {
            SaimonseiEvents.OnPlay?.Invoke();
            return;
        }

        if (currentState == RevealState.WaitingPlayer)
            return;

        SaimonseiEvents.OnCreateRipple?.Invoke(worldPosition, currentState == RevealState.Capture ? Ripple.RippleType.RealmReveal : Ripple.RippleType.Invalid);

        if (currentState != RevealState.Capture)
            return;

        touchCount++;
        SetRevealState(RevealState.Touched);  // to capture one touch only!

        SaimonseiEvents.OnStopMemes?.Invoke(true);         // in case any just fading in...
        SaimonseiEvents.OnStopAgeing?.Invoke();

        float distanceFromGridPoint = 0f;
        var touchedStone = GridPoint.GetNearestGridPoint(layerGrid, touchPosition, currentRealm.CurrentLayer.MaxTouchDistance, out distanceFromGridPoint);

        if (touchedStone == null)     // not near enough to any grid point
        {
            SequenceCaptureFail(true);
            return;
        }

        if (!currentRealm.CurrentLayer.StoneIsInSequence((int)touchedStone.gridCoords.x, (int)touchedStone.gridCoords.y))
        {
            // touched stone not part of current sequence
            SequenceCaptureFail(true);
            return;
        }

        // get the touchstone in the sequence that should be touched next
        var targetTouchStone = currentRealm.CurrentLayer.TouchStones[touchCount - 1];

        // expand nearest grid stone to touch
        var touched = currentRealm.CurrentLayer.GetTouchStoneAt((int)touchedStone.gridCoords.x, (int)touchedStone.gridCoords.y, touchCount - 1);

        // play instrument note for the stone that was touched
        if (touched != null)
            currentRealm.CurrentLayer.PlayTouchstoneNote(touched);

        touchedStone.Expand(touched.FlashTime * 1000f, true, true);     // milliseconds

        // if stone touched not the next touchstone in sequence, fail
        var touchedCorrectStone = targetTouchStone.GridCoords.x == touchedStone.gridCoords.x && targetTouchStone.GridCoords.y == touchedStone.gridCoords.y;
        if (!touchedCorrectStone)       // not the right grid point!
        {
            SequenceCaptureFail(true);
            return;
        }

        if (touchCount == currentRealm.CurrentLayer.TouchStones.Count)
        {
            // last touch - got this far - must have succeeded with sequence!
            StopCaptureCountdown();
            SetRevealState(RevealState.Success);

            if (!string.IsNullOrEmpty(currentRealm.RealmName) && !string.IsNullOrEmpty(currentRealm.CurrentLayer.VeilName))
                SavedGameState.SetLayerCompleted(currentRealm.RealmName, currentRealm.CurrentLayer.VeilName);

            playArea.TouchFeedback("YES!", worldPosition);
        }
        else
        {
            // wait for next touch in touchstone sequence
            SetRevealState(RevealState.Capture);
        }
    }

    private void OnWaitForLayerChoice(List<LayerChoice> layerChoices)
    {
        SetRevealState(RevealState.WaitingPlayer);
    }

    private void OnRealmImageRevealed(int layerIndex, bool skipping)
    {
		if (!skipping)
		{
			StoryNext();
			SetRevealState(RevealState.Revealed);
		}
	}

    private void OnInstrumentTimeOut()
    {
        SetRevealState(RevealState.TimeOut);
    }

    private void StoryNext()
    {
        //Debug.Log("StoryNext '" + currentRealm.CurrentLayer.VeilName + "' CurrentLayerIndex = " + currentRealm.CurrentLayerIndex + " LayerCount = " + currentRealm.LayerCount);
        if (currentRealm.CurrentLayerIsLast)    // last layer - show its outro story
        {
            lastLayerOutroStory = "";

            //var outroStory = CurrentLayerOutro() + "\n...\n" + "You're Ready to Learn '" + currentRealm.RealmSongName + "'!!";
            var outroStory = CurrentLayerOutro(); // + "\n...\n" + "You're Ready to Learn The Song of The Realm!!";
            playArea.FadeStory(outroStory, currentRealm.StoryTextTime, false, currentRealm.StoryColour);
        }
        else             // if this is not the last layer, cache its outro story to combine with next layer's intro story
        {
            lastLayerOutroStory = CurrentLayerOutro(); // currentRealm.CurrentLayer.OutroStory;
            playArea.FadeStory("", currentRealm.StoryTextTime, false, currentRealm.StoryColour);
        }
    }


    private void SequenceCaptureFail(bool audio)
    {
        SetRevealState(RevealState.Fail);
        StopCaptureCountdown();
    }

    private void SetRevealState(RevealState state, bool skippingTouchstones = false)  //silent = false, bool fadeInLayerImage = false)
    {
        //Debug.Log("SetRevealState currentState = " + currentState + " new state = " + state);

        currentState = state;
        SaimonseiEvents.OnRealmRevealStateChanged?.Invoke(currentState, currentRealm, skippingTouchstones);

        string failMessage = currentRealm.CurrentLayer.FailMessage;
        if (!string.IsNullOrEmpty(failMessage))
            failMessage += "\n";

        switch (currentState)
        {
            case RevealState.Start:
                break;
            case RevealState.PlaySequence:
                break;
            case RevealState.Capture:
                break;
            case RevealState.Touched:
                break;
            case RevealState.TimeOut:
                playArea.FadeStory("Time's up!\n" + failMessage + "Tap to Try Again!", currentRealm.StoryTextTime * captureFeedbackSpeed, true, currentRealm.StoryColour);
                break;
            case RevealState.Success:
                break;
            case RevealState.Revealed:
                break;
            case RevealState.WaitingPlayer:
                break;
            case RevealState.Fail:
                playArea.FadeStory(failMessage + "Tap to Try Again!", currentRealm.StoryTextTime * captureFeedbackSpeed, true, currentRealm.StoryColour);
                break;
        }
    }

    private void StartCaptureCountdown()
    {
        playArea.ResetRevealGrid();     // shouldn't be necessary, but cleanup any 'lingering' touchstones
                
        touchCaptureCountdown = currentRealm.CurrentLayer.CaptureTimeLimit;     // seconds
        touchCount = 0;

        SetRevealState(RevealState.Capture);
        SaimonseiEvents.OnRealmRevealCapture?.Invoke(currentRealm.CurrentLayer.CaptureTimeLimit);

        captureCountdownCoroutine = CaptureCountdown();
        StartCoroutine(captureCountdownCoroutine);
    }


    private IEnumerator CaptureCountdown()
    {
        while (touchCaptureCountdown > 0)
        {
            SaimonseiEvents.OnRealmRevealCountdown?.Invoke(touchCaptureCountdown, currentRealm.CurrentLayer.FinalCountdown);

            yield return new WaitForSecondsRealtime(1f);
            touchCaptureCountdown--;
        }

        StopCaptureCountdown();
        yield return null;
    }


    private void StopCaptureCountdown()
    {
        if (captureCountdownCoroutine != null)
        {
            StopCoroutine(captureCountdownCoroutine);
            captureCountdownCoroutine = null;

            SaimonseiEvents.OnRealmRevealCountdown?.Invoke(0, 0);
        }
    }


    private void OnMemeCountUpdated(int memeCount)
    {
        //Debug.Log("OnMemeCountUpdated: " + memeCount);
        memesShowing = memeCount;
    }
}
