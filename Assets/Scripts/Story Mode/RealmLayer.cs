﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// A RealmLayer is revealed by repeated the 'PIN' code represented by TouchStones
/// Values set in Inspector (hence MonoBehaviour)
/// </summary>
[Serializable]
[RequireComponent(typeof(InstrumentPlayer))]        // to play touchstone notes!
public class RealmLayer : MonoBehaviour
{
    //[Tooltip("Name of spell cast via touchstones")]
    //public string SpellName;            // name of spell cast
    [Tooltip("Name of Layer - used for saving game progress (layers completed)")]
    public string VeilName;             // name of layer

    public Sprite LayerImage;     
    public Color Tint;  
    public Color StoryColour;

    [Tooltip("Does background image fade in on layer completion?")]
    public bool FadeInBackground = true;
    public float FadeInAlpha = 0.15f;

    [Tooltip("Name of instrument learnt in this layer")]
    public string InstrumentText;

    [Tooltip("Image of instrument learnt in this layer")]
    public Sprite InstrumentImage;

    [Tooltip("Story before layer revealed")]
    [TextArea(maxLines: 10, minLines: 2)]
    public string IntroStory;

    [Tooltip("Story after layer revealed...\nCarries over to show with next layer's intro")]
    [TextArea(maxLines: 10, minLines: 2)]
    public string OutroStory;

    [Tooltip("Message on Touchstone Sequence 'fail'")]
    [TextArea(maxLines: 10, minLines: 2)]
    public string FailMessage = "Those Crystals might help...";

    public AudioClip EntryAudio;
    public AudioClip TimeoutAudio;
    public AudioClip SuccessAudio;
    public AudioClip FailAudio;
    [Tooltip("Spend all pocketed coins on touchstone success?")]
    [HideInInspector]
    public bool SpendCoins = false;           // all pocket coins spent on touchstone success?

    [Tooltip("Touch location accuracy tolerance")]
    [HideInInspector]
    public float MaxTouchDistance;           // max distance from nearest touchstone

    [Header("Land Health Values")]
    [Tooltip("Land health % cost (decrease) to enter layer")]
    public float EntryCostLandHealth = 2f;

    [Tooltip("Land health increase % on layer 'success'")]
    public float SuccessLandHealth = 5f;

    [Tooltip("Land health decrease % on sequence 'fail'")]
    public float FailLandHealth = 1f;

    [Space]

    [Header("Player Multiple-Choice Options")]
    [Tooltip("List of layers player can choose to go to next")]
    public List<LayerChoice> NextLayerChoices = new List<LayerChoice>();    // player chooses next layer

    [Header("Touch Stones")]
    [Tooltip("Check to skip touchstone sequence")]
    public bool SkipTouchstones = false;
    [Tooltip("Layer BPM - for timing of touchstone interval beats")]
    public float BeatsPerMinute = 100f;
    [Tooltip("Sequence of notes for player to repeat")]
    public List<RealmTouchStone> TouchStones = new List<RealmTouchStone>();    // number of random GridPoints generated to unveil layer

    [Space]

    [Tooltip("Width of Touchstone grid")]
    public int SearchGridWidth = 2;
    [Tooltip("Height of Touchstone grid")]
    public int SearchGridHeight = 2;

    [Tooltip("Pause after sequence play before countdown starts")]
    [HideInInspector]
    public float PauseAfterPlay;
    [Tooltip("Repeat sequence time limit")]
    public int CaptureTimeLimit = 10;        // seconds
    [Tooltip("Display countdown for last few seconds")]
    public int FinalCountdown = 3;        // seconds


    public bool IsLastLayer { get { return NextLayerChoices.Count == 0; } }
    public bool IsMultipleChoice { get { return NextLayerChoices.Count > 1; } }


    private InstrumentPlayer instrumentPlayer;


    private void Start()
    {
        instrumentPlayer = GetComponent<InstrumentPlayer>();
    }

    public RealmTouchStone GetTouchStoneAt(int touchX, int touchY, int stoneIndex)
    {
        // there could be >1 stone at the same location...

        // so if the position of the stone at stoneIndex matches the touch position, return that stone!
        if (TouchStones[stoneIndex].GridCoords.x == touchX && TouchStones[stoneIndex].GridCoords.y == touchY)
            return TouchStones[stoneIndex];

        // else return the (first) stone at the touched location
        foreach (var touchStone in TouchStones)
        {
            if (touchStone.GridCoords.x == touchX && touchStone.GridCoords.y == touchY)
                return touchStone;
        }

        return null;
    }

    public bool StoneIsInSequence(int x, int y)
    {
        foreach (var touchStone in TouchStones)
        {
            if (touchStone.GridCoords.x == x && touchStone.GridCoords.y == y)
                return true;
        }

        return false;
    }


    public void PlayTouchstoneNote(RealmTouchStone touchstone)
    {
        if (touchstone.NoteNumber > 0 && instrumentPlayer != null)          // FMOD instrument
            instrumentPlayer.PlayNote(touchstone.NoteNumber);
        else if (touchstone.InstrumentNote != null)                         // AudioClip fallback
            AudioSource.PlayClipAtPoint(touchstone.InstrumentNote, Vector3.zero);
    }

    public void StopTouchstoneNote(RealmTouchStone touchstone)
    {
        if (touchstone.NoteNumber > 0 && instrumentPlayer != null)          // FMOD instrument
            instrumentPlayer.StopNote(touchstone.NoteNumber);
    }
}


