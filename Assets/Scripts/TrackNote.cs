﻿using System;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class TrackNote
{
    public string Version = "1.0.0";

	public string SongEventName;

	// data from TimelineInfo
	public int BeatNumber = 0;
	public int BarNumber = 0;
	public float BeatsPerMinute = 0;
	public int TimelinePosition = 0;        // ms

	public int InstrumentNumber;           // to lookup event instance
	public int NoteNumber;
	public int NoteDuration;				// 1/4 notes

	// game data
	public Vector2 GridCoords;			// x, y 

	public int NoteDurationMs;              // duration in ms

	public int TouchStartTime;              // ms from start
	public int TouchEndTime;                // ms from start

	public float TouchStartDistance;		// from gridPoint at GridCoords
	public float TouchEndDistance;          // from gridPoint at GridCoords

	public bool IsDudNote = false;
	public bool PlayedBack = false;			// recorded note has been played back
}
