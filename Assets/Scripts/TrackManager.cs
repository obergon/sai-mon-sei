﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


// handles the play, record, playback cycle of a track (aka 'song')
public class TrackManager : MonoBehaviour
{
	public ScoreKeeper ScoreKeeper;
	public float DudNoteDuration = 0.3f;          // time to playback dud notes

	private Track targetTrack;      // list of TrackNotes from FMOD timeline markers
	private Track recordedTrack;    // list of TrackNotes from player input

	private GridPoint[,] gridPoints;
	private Vector2 GridSize { get { return new Vector2(gridPoints.GetLength(0), gridPoints.GetLength(1)); } }
	private Vector2 lastGridCoords = new Vector2(-1, -1);

	private int storyNoteIndex = -1;            // incremented on each touch end (when scored)
	private List<InstrumentScore> StoryNoteScores = new List<InstrumentScore>();        // for each note - totalled at end

	private Vector2 RandomGridCoords
	{
		// make sure not same as last grid coords
		get
		{
			Vector2 randomCoords;

			do
			{
				randomCoords = new Vector2(UnityEngine.Random.Range(0, (int)GridSize.x), UnityEngine.Random.Range(0, (int)GridSize.y));
			}
			while (randomCoords.x == lastGridCoords.x && randomCoords.y == lastGridCoords.y);

			// got a different one
			lastGridCoords = randomCoords;
			return randomCoords;
		}
	}


	public string SongEventName { get { return fmodPlayer.SongEventName; } }
	//public string SongTitle { get; private set; }
	public int NumInstruments { get { return fmodPlayer.NumInstruments; } }

	public FMODPlayer fmodPlayer;                       // set in inspector
	public int TrackNumber;                         // used for seeding random grid points

	private int currentInstrumentNumber = 0;            // 0 == all instruments

	private Vector2 TouchPosition;
	private bool IsTouchValid { get { return (playMode == PlayMode.RecordInstrument) && (NetworkManager.IsMultiPlayerGame ? NetworkManager.IsLocalPlayersTurn : true); } }


	public enum PlayMode
	{
		Waiting,            // not player's turn
		PlayAll,            // play original - all instruments
		PlayInstrument,     // play original - 1 instrument
		RecordInstrument,   // play backing track, record players' input
		PlaybackInstrument, // play player's recording - 1 instrument
		PlaybackAll,        // play player's recording - all instruments
		Done                // next player / instrument / track
	}
	private PlayMode playMode;

	private DateTime playStartTime;
	private bool touching = false;

	// record mode
	private TrackNote recordingNote = null;                     // created on touch when recording a track note
	private GridPoint recordNearestGridPoint = null;            // nearest grid point to touch position when recording
	private int playHeadPosition = 0;                           // ms (for recording and playback)

	private float maxTouchDistance = 16f;                       // touch must be within this distance of a grid point to 'count'

	[Tooltip("Tolerance (in milliseconds) for early or late note touch timing")]
	private const float recordPlayHeadToleranceMs = 0.5f * 1000f;       // TODO: ?? play head position toleance for recording
	private const float playbackPlayHeadToleranceMs = 0.02f * 1000f;    // play head position tolerance for playback (fixed timestep)

	private bool inputAllowed = true;                                // breathing space on track end - no input allowed

	private ModeManager.GameMode gameMode;


	private void OnEnable()
	{
		SaimonseiEvents.OnInitGrid += OnInitGrid;
		SaimonseiEvents.OnSongSelected += OnSongSelected;           // song button clicked
		SaimonseiEvents.OnPlayCycleStart += OnPlayCycleStart;       // play button clicked
		SaimonseiEvents.OnTrackPlay += OnTrackPlay;
		SaimonseiEvents.OnTrackEnd += OnTrackEnd;
		SaimonseiEvents.OnBackingTrackStart += OnBackingStart;
		SaimonseiEvents.OnTimelineMarker += OnTimelineMarker;
		SaimonseiEvents.OnTimelineBeat += OnTimelineBeat;

		SaimonseiEvents.OnTouchStart += OnTouchStart;
		SaimonseiEvents.OnTouchMove += OnTouchMove;
		SaimonseiEvents.OnTouchEnd += OnTouchEnd;

		// multiplayer syncing
		SaimonseiEvents.OnSongSelectedNetwork += StartSong;             // full circle
		SaimonseiEvents.OnTouchStartNetwork += StartTouch;             // full circle
		SaimonseiEvents.OnTouchEndNetwork += EndTouch;             // full circle
		SaimonseiEvents.OnAnimateNoteNetwork += AnimateNote;             // full circle
	}

	private void OnDisable()
	{
		SaimonseiEvents.OnInitGrid -= OnInitGrid;
		SaimonseiEvents.OnSongSelected -= OnSongSelected;
		SaimonseiEvents.OnPlayCycleStart -= OnPlayCycleStart;
		SaimonseiEvents.OnTrackPlay -= OnTrackPlay;
		SaimonseiEvents.OnTrackEnd -= OnTrackEnd;
		SaimonseiEvents.OnBackingTrackStart -= OnBackingStart;
		SaimonseiEvents.OnTimelineMarker -= OnTimelineMarker;
		SaimonseiEvents.OnTimelineBeat -= OnTimelineBeat;

		SaimonseiEvents.OnTouchStart -= OnTouchStart;
		SaimonseiEvents.OnTouchMove -= OnTouchMove;
		SaimonseiEvents.OnTouchEnd -= OnTouchEnd;

		// multiplayer syncing
		SaimonseiEvents.OnSongSelectedNetwork -= StartSong;
		SaimonseiEvents.OnTouchStartNetwork -= StartTouch;         // full circle
		SaimonseiEvents.OnTouchEndNetwork -= EndTouch;             // full circle
		SaimonseiEvents.OnAnimateNoteNetwork -= AnimateNote;             // full circle
	}

	// acts as 'play head' in record and playback modes
	// fixed time interval
	private void FixedUpdate()
	{
		if (playMode == PlayMode.PlayAll || playMode == PlayMode.PlayInstrument)          // notes triggered by timeline markers
			return;

		// notes triggered by track markers
		playHeadPosition += (int)(Time.fixedDeltaTime * 1000f);         // ms

		if (playMode == PlayMode.PlaybackInstrument || playMode == PlayMode.PlaybackAll)
			PlaybackAtPlayHead();
	}


	// play recorded note
	private void PlaybackAtPlayHead()
	{
		if (!(playMode == PlayMode.PlaybackInstrument || playMode == PlayMode.PlaybackAll))
			return;

		var recordedNote = GetNoteAtPlayhead(recordedTrack, true);      // look for an unplayed recorded note near the current play head position

		if (recordedNote == null)             // no unplayed recorded note at play head position
			return;

		if (currentInstrumentNumber > 0 && recordedNote.InstrumentNumber != currentInstrumentNumber)
			return;

		StartCoroutine(PlayRecordedNote(recordedNote));

		// expand recorded grid point
		var gridPoint = gridPoints[(int)recordedNote.GridCoords.x, (int)recordedNote.GridCoords.y];
		gridPoint.Expand(recordedNote.NoteDurationMs, false);
	}

	private IEnumerator PlayRecordedNote(TrackNote recordedNote)
	{
		recordedNote.PlayedBack = true;         // to prevent it from playing multiple times as playhead moves

		if (recordedNote.IsDudNote)
		{
			SaimonseiEvents.OnDudNote?.Invoke();
			yield return new WaitForSecondsRealtime(DudNoteDuration);
		}
		else
		{
			SaimonseiEvents.OnPlayNote?.Invoke(recordedNote.SongEventName, recordedNote.InstrumentNumber, recordedNote.NoteNumber);
			yield return new WaitForSecondsRealtime(recordedNote.NoteDurationMs / 1000f);
			SaimonseiEvents.OnStopNote?.Invoke(recordedNote.SongEventName, recordedNote.InstrumentNumber, recordedNote.NoteNumber);
		}
		yield return null;
	}

	// look for an unplayed note that starts close to current play head position
	private TrackNote GetNoteAtPlayhead(Track track, bool checkUnplayed)
	{
		if (playMode == PlayMode.PlayAll || playMode == PlayMode.PlayInstrument)        // played from FMOD markers
			return null;

		//var playHeadTolerance = (playMode == PlayMode.Record) ? recordPlayHeadTolerance : playbackPlayHeadTolerance;

		if (playMode == PlayMode.RecordInstrument)
		{
			// look for a target marker that starts near (early or late) the current playhead position
			foreach (var trackNote in track.Notes)
			{
				if (currentInstrumentNumber > 0 && trackNote.InstrumentNumber != currentInstrumentNumber)
					continue;

				if (Mathf.Abs(playHeadPosition - trackNote.TimelinePosition) < recordPlayHeadToleranceMs)
				{
					//string earlyLate = playHeadPosition < trackNote.TimelinePosition ? "EARLY" : "LATE";
					//Debug.Log("GetNoteAtPlayhead: " + earlyLate + ",  timing diff " + (playHeadPosition - trackNote.TimelinePosition) + " recordPlayHeadToleranceMs = " + recordPlayHeadToleranceMs);
					return trackNote;
				}
			}
		}
		else if (playMode == PlayMode.PlaybackInstrument || playMode == PlayMode.PlaybackAll)
		{
			// look for a target note that starts on or before the current playhead position, within fixed update delta
			foreach (var trackNote in track.Notes)
			{
				//Debug.Log("GetNoteAtPlayhead: playHeadPosition " + playHeadPosition + " TimelinePosition = " + trackNote.TimelinePosition);

				if (checkUnplayed && trackNote.PlayedBack)
					continue;

				if (currentInstrumentNumber > 0 && trackNote.InstrumentNumber != currentInstrumentNumber)
					continue;

				if (trackNote.TimelinePosition - playHeadPosition <= playbackPlayHeadToleranceMs)
				{
					//Debug.Log("GetNoteAtPlayhead: playHeadPosition " + playHeadPosition + " TimelinePosition = " + trackNote.TimelinePosition);
					return trackNote;
				}
			}
		}

		return null;
	}

	private void ResetRecordingPlayedBack()
	{
		foreach (var trackNote in recordedTrack.Notes)
		{
			trackNote.PlayedBack = false;
		}
	}

	private void OnBackingStart(string songEventName, PlayMode mode, DateTime trackPlayStartTime)
	{
		if (songEventName != SongEventName)
			return;

		playMode = mode;
		playStartTime = trackPlayStartTime;
	}

	private void OnTrackEnd(string songEventName, PlayMode mode, FMODPlayer.TimelineInfo timelineInfo, int restBeats)
	{
		if (songEventName != SongEventName)
			return;

		StartCoroutine(EndTrack(timelineInfo, restBeats));
	}


	// 4 beat breathing space!
	private IEnumerator EndTrack(FMODPlayer.TimelineInfo timelineInfo, int restBeats)
	{
		ResetGrid();
		inputAllowed = false;

		yield return new WaitForSecondsRealtime(SecondsPerBeat(timelineInfo.timelineData) * restBeats * (3f / 4f));      // see PlayArea fade in (blacked out after 3 beats)  

		inputAllowed = true;

		switch (playMode)
		{
			case PlayMode.Waiting:           // not player's turn
				break;

			case PlayMode.PlayAll:            // play original - all instruments
				FirstInstrument();
				break;

			case PlayMode.PlayInstrument:     // play original - 1 instrument
				RecordInstrument();
				break;

			case PlayMode.RecordInstrument:             // play backing track, record players' input
				if (gameMode == ModeManager.GameMode.StoryMode)
				{
					if (NumInstruments == 1)
						PlayCycleDone();
					else if (currentInstrumentNumber < NumInstruments)
						NextInstrument();
					else
						PlayCycleDone();
				}
				else
				{
					if (NumInstruments == 1)
						PlaybackInstrument();
					else if (currentInstrumentNumber < NumInstruments)
						NextInstrument();
					else
						PlaybackAll();
				}
				break;

			case PlayMode.PlaybackInstrument:    // play player's recording - 1 instrument
				if (NumInstruments == 1)
					PlayCycleDone();
				else if (currentInstrumentNumber < NumInstruments)
					NextInstrument();
				else
					PlaybackAll();
				break;

			case PlayMode.PlaybackAll:        // play player's recording - all instruments
				PlayCycleDone();
				break;

			case PlayMode.Done:                // next player / instrument / track
			default:
				break;
		}

		yield return null;
	}


	private float SecondsPerBeat(TimelineData timelineData)
	{
		return 60f / timelineData.currentTempo;
	}

	private void AnimateTimelineNote(TimelineData timelineData)
	{
		//Debug.Log("AnimateTimelineNote: IsLocalPlayersTurn = " + NetworkManager.IsLocalPlayersTurn);

		if (NetworkManager.IsMultiPlayerGame)
		{
			var gridCoords = RandomGridCoords;

			if (NetworkManager.IsLocalPlayersTurn)
				SaimonseiEvents.OnPlayTimelineNoteLocal?.Invoke(gridCoords, timelineData); // logs player + instrument   // TODO: best way?

			if (NetworkManager.GetLocalPlayer.IsOwnedByServer)
				SaimonseiEvents.OnPlayTimelineNoteServer?.Invoke(gridCoords, timelineData);
		}
		else
		{
			AnimateNote(RandomGridCoords, timelineData);
		}
	}

	private void AnimateNote(Vector2 randomCoords, TimelineData timelineData)
	{
		var gridPoint = gridPoints[(int)randomCoords.x, (int)randomCoords.y];
		int noteDurationMs = 0;

		//Debug.Log("AnimateNote: " + randomCoords.x + "," + randomCoords.y + " noteDuration: " + timelineData.noteDuration + " currentTempo = " + timelineData.currentTempo);

		if (timelineData.currentTempo > 0)
		{
			var quarterBeatSeconds = SecondsPerBeat(timelineData) / 4f;
			noteDurationMs = (int)(timelineData.noteDuration * quarterBeatSeconds * 1000f);       // ms

			if (playMode != PlayMode.PlayAll)                   // TODO: in or out?
				gridPoint.Expand(noteDurationMs, false);               // scale grid button
		}

		if (playMode == PlayMode.PlayInstrument)
		{
			targetTrack.AddNote(new TrackNote
			{
				SongEventName = SongEventName,

				BeatNumber = timelineData.currentBeat,
				BarNumber = timelineData.currentBar,
				BeatsPerMinute = timelineData.currentTempo,
				TimelinePosition = timelineData.timelinePosition,     // ms

				InstrumentNumber = timelineData.instrumentNumber,
				NoteNumber = timelineData.noteNumber,
				NoteDuration = timelineData.noteDuration,               // 1/4 notes

				GridCoords = randomCoords,
				NoteDurationMs = noteDurationMs,
			});
		}
	}

	private void OnTimelineMarker(string songEventName, FMODPlayer.TimelineInfo timelineInfo)
	{
		if (songEventName != SongEventName)
			return;

		if (playMode != PlayMode.PlayAll && playMode != PlayMode.PlayInstrument)
			return;

		if (currentInstrumentNumber > 0 && timelineInfo.timelineData.instrumentNumber != currentInstrumentNumber)
			return;

		// if multi-player, driven by local player
		AnimateTimelineNote(timelineInfo.timelineData);
	}


	private void OnTimelineBeat(string songEventName, FMODPlayer.TimelineInfo timelineInfo, bool backingTrackStarted, PlayMode playMode)
	{
		if (songEventName != SongEventName)
			return;

		if (touching)
			SaimonseiEvents.OnCreateRipple?.Invoke(Camera.main.ScreenToWorldPoint(TouchPosition), IsTouchValid ? Ripple.RippleType.Valid : Ripple.RippleType.Invalid);
	}


	private void OnTrackPlay(string songEventName, PlayMode mode, int instrumentNumber)
	{
		if (songEventName != SongEventName)         // not this track!
			return;

		//bool changed = currentInstrumentNumber != instrumentNumber;
		currentInstrumentNumber = instrumentNumber;
		playMode = mode;            // TODO: here?

		storyNoteIndex = -1;
		//StoryNoteScores.Clear();

		SaimonseiEvents.OnInstrumentChanged?.Invoke(songEventName, currentInstrumentNumber, fmodPlayer.PlayerInstrumentName(currentInstrumentNumber), ShortInstrumentName(currentInstrumentNumber), gridPoints.Length);

		ModePrompt(mode);
	}


	private void ModePrompt(PlayMode mode)
	{
		switch (mode)
		{
			case PlayMode.Waiting:           // not player's turn
				break;

			case PlayMode.PlayAll:            // play original - all instruments
				// seed randomiser to get same results for the day, track amd instrument
				//UnityEngine.Random.InitState(DateTime.Now.Day + TrackNumber + currentInstrumentNumber);

				if (gameMode == ModeManager.GameMode.StoryMode)
					SaimonseiEvents.OnStatusUpdated?.Invoke("LISTEN TO SONG", NetworkManager.IsLocalPlayersTurn ? "Listen to Whole Song" : "Listen to Whole Song", mode);
				else
					SaimonseiEvents.OnStatusUpdated?.Invoke("[ WHOLE SONG ]", NetworkManager.IsLocalPlayersTurn ? "Listen to Whole Song" : "Listen to Whole Song", mode);
				break;

			case PlayMode.PlayInstrument:     // play original - 1 instrument
				// seed randomiser to get same results for the day, track amd instrument
				UnityEngine.Random.InitState(DateTime.Now.Day + TrackNumber + currentInstrumentNumber + storyNoteIndex);

				if (gameMode == ModeManager.GameMode.StoryMode)
					SaimonseiEvents.OnStatusUpdated?.Invoke("TIMING IS KEY...", "...", mode);
				else
					SaimonseiEvents.OnStatusUpdated?.Invoke("[ PLAY ]", NetworkManager.IsLocalPlayersTurn ? "Listen, Watch & Memorise!" : "...", mode);
				break;

			case PlayMode.RecordInstrument:             // play backing track, record players' input
				playHeadPosition = 0;       // ms from recording start
				if (gameMode == ModeManager.GameMode.StoryMode)
					SaimonseiEvents.OnStatusUpdated?.Invoke("...PLAY IN TIME!", NetworkManager.IsLocalPlayersTurn ? "Touch to Repeat!" : "...", mode);
				else
					SaimonseiEvents.OnStatusUpdated?.Invoke("[ RECORDING ]", NetworkManager.IsLocalPlayersTurn ? "Touch to Repeat!" : "...", mode);
				break;

			case PlayMode.PlaybackInstrument:           // play player's recording - 1 instrument
				playHeadPosition = 0;       // ms from playback start
				SaimonseiEvents.OnStatusUpdated?.Invoke("[ PLAYBACK ]", NetworkManager.IsLocalPlayersTurn ? "Your Recorded Track" : "Their Recorded Track", mode);
				break;

			case PlayMode.PlaybackAll:        // play player's recording - all instruments
				playHeadPosition = 0;       // ms from playback start
				SaimonseiEvents.OnStatusUpdated?.Invoke("[ PLAYBACK ]", NetworkManager.IsLocalPlayersTurn ? "All Recorded Tracks" : "All Recorded Tracks", mode);
				break;

			case PlayMode.Done:                // next player / instrument / track
			default:
				break;
		}
	}


	private void StartSong(string songTitle, string songEventName)
	{
		//    Debug.Log("StartSong!");

		//SongTitle = songTitle;

		recordingNote = null;
		recordNearestGridPoint = null;

		currentInstrumentNumber = 1;
	}

	// song selected (by host if multiplayer)
	private void OnSongSelected(string songTitle, string songEventName, PlayArea.PlayMode playMode, int gridWidth, int gridHeight, float tolerance)
	{
		StartSong(songTitle, songEventName);
	}

	//on 'play' button clicked
	private void OnPlayCycleStart(ModeManager.GameMode mode)
	{
		gameMode = mode;

		//Debug.Log("StartPlayCycle!");
		lastGridCoords = new Vector2(-1, -1);

		targetTrack = new Track { GridSize = this.GridSize };
		recordedTrack = new Track { GridSize = this.GridSize };

		if (NumInstruments > 1)
			PlayAllInstruments();
		else
			FirstInstrument();

		StoryNoteScores.Clear();
	}

	private void PlayAllInstruments()
	{
		//Debug.Log("PlayAllInstruments!");

		playMode = PlayMode.PlayAll;
		SaimonseiEvents.OnTrackPlay?.Invoke(SongEventName, playMode, 0);
	}


	private void FirstInstrument()
	{
		if (NetworkManager.IsMultiPlayerGame)
			NetworkManager.IncrementCurrentPlayer();

		//if (NetworkManager.IsMultiPlayerGame)
		CountdownToFirstInstrument();
		//else
		//PlayFirstInstrument();
	}

	private void CountdownToFirstInstrument()
	{
		// currentInstrumentNumber is zero
		SaimonseiEvents.OnNextPlayerCountdown?.Invoke(3, fmodPlayer.PlayerInstrumentName(1), PlayFirstInstrument);
	}

	private void PlayFirstInstrument()
	{
		//Debug.Log("PlayFirstInstrument!");

		currentInstrumentNumber = 1;

		playMode = PlayMode.PlayInstrument;
		SaimonseiEvents.OnTrackPlay?.Invoke(SongEventName, playMode, currentInstrumentNumber);
	}


	private void NextInstrument()
	{
		if (NetworkManager.IsMultiPlayerGame)
			NetworkManager.IncrementCurrentPlayer();

		CountdownToNextInstrument();
	}

	private void CountdownToNextInstrument()
	{
		if (currentInstrumentNumber < NumInstruments)       // current is not last (and shouldn't be here if it is!)
		{
			SaimonseiEvents.OnNextPlayerCountdown?.Invoke(3, fmodPlayer.PlayerInstrumentName(currentInstrumentNumber + 1), PlayNextInstrument);
		}
	}

	private void PlayNextInstrument()
	{
		currentInstrumentNumber++;

		playMode = PlayMode.PlayInstrument;
		SaimonseiEvents.OnTrackPlay?.Invoke(SongEventName, PlayMode.PlayInstrument, currentInstrumentNumber);
	}


	private void RecordInstrument()
	{
		playMode = PlayMode.RecordInstrument;
		SaimonseiEvents.OnTrackPlay?.Invoke(SongEventName, playMode, currentInstrumentNumber);
	}

	private void PlaybackInstrument()
	{
		playMode = PlayMode.PlaybackInstrument;
		SaimonseiEvents.OnTrackPlay?.Invoke(SongEventName, playMode, currentInstrumentNumber);
	}

	private void PlaybackAll()
	{
		currentInstrumentNumber = 0;

		ResetRecordingPlayedBack();         // reset played back flags in case instruments played back individually

		playMode = PlayMode.PlaybackAll;
		SaimonseiEvents.OnTrackPlay?.Invoke(SongEventName, playMode, currentInstrumentNumber);
	}


	private string ShortInstrumentName(int instrumentNumber)
	{
		if (instrumentNumber > 0)
		{
			string fullInstrumentName = fmodPlayer.GetInstrumentNameByNumber(instrumentNumber);
			string[] instrSplit = fullInstrumentName.Split('_');

			if (instrSplit.Length == 2)
				return instrSplit[1];
			else
				return fullInstrumentName;       // full event name
		}

		return "--";
	}

	private void PlayCycleDone()
	{
		var instrumentsPlayed = NetworkManager.IsMultiPlayerGame ? NetworkManager.GetLocalPlayer.InstrumentScores.Keys.ToList() : null;

		//Debug.Log("PlayCycleDone: instrumentsPlayed: " + instrumentsPlayed.Count + " InstrumentScores count = " + NetworkManager.GetLocalPlayer.InstrumentScores.Keys.ToList().Count);
		playMode = PlayMode.Done;
		SaimonseiEvents.OnStatusUpdated?.Invoke("", "", playMode);

		if (gameMode == ModeManager.GameMode.SaiMonSei)
		{
			var results = ScoreKeeper.CalculateInstrumentScores(NumInstruments, instrumentsPlayed, gridPoints.Length, targetTrack, recordedTrack);
			SaimonseiEvents.OnPlayCycleDone?.Invoke(SongEventName, results);
		}
		else if (gameMode == ModeManager.GameMode.StoryMode)
		{
			var totalStoryScore = TotalStoryInstrumentScore;
			totalStoryScore.TargetNoteCount = targetTrack.Notes.Count;

			List<InstrumentScore> finalScore = new List<InstrumentScore> { totalStoryScore };
			SaimonseiEvents.OnPlayCycleDone?.Invoke(SongEventName, finalScore);
		}
	}

	private void OnInitGrid(GridPoint[,] points)
	{
		//Debug.Log("OnInitGrid: " + points.Length);
		gridPoints = points;
	}

	private void ResetGrid()
	{
		foreach (var gridPoint in gridPoints)
		{
			gridPoint.ResetScale(false);
		}
	}

	private void OnTouchStart(Vector2 touchPosition)
	{
		if (!inputAllowed)
			return;

		if (NetworkManager.IsMultiPlayerGame)
		{
			if (NetworkManager.IsLocalPlayersTurn)
				SaimonseiEvents.OnStartTouchClient?.Invoke(Camera.main.ScreenToViewportPoint(touchPosition));
			else        // ripple feedback only
				SaimonseiEvents.OnCreateRipple?.Invoke(Camera.main.ScreenToWorldPoint(touchPosition), Ripple.RippleType.Invalid);
		}
		else
			StartTouch(touchPosition);
	}

	// touch detection only when recording
	private void StartTouch(Vector2 touchPosition)
	{
		if (touching)       // shouldn't happen!
			return;

		//Debug.Log("TrackManager.StartTouch: " + touchPosition + " playMode = " + playMode);
		touching = true;
		TouchPosition = touchPosition;
		SaimonseiEvents.OnCreateRipple?.Invoke(Camera.main.ScreenToWorldPoint(TouchPosition), IsTouchValid ? Ripple.RippleType.Valid : Ripple.RippleType.Invalid);

		if (playMode != PlayMode.RecordInstrument)
			return;

		//SaimonseiEvents.OnCreateRipple?.Invoke(Camera.main.ScreenToWorldPoint(TouchPosition), IsTouchValid ? Ripple.RippleType.Valid : Ripple.RippleType.Invalid);

		float distanceFromGridPoint = 0f;
		recordNearestGridPoint = GridPoint.GetNearestGridPoint(gridPoints, touchPosition, maxTouchDistance, out distanceFromGridPoint);

		if (recordNearestGridPoint == null)     // not near enough to any grid point
		{
			SaimonseiEvents.OnDudNote?.Invoke();
			//Debug.Log("Null recordNearestGridPoint!");
			return;
		}

		if (recordedTrack == null)
		{
			Debug.LogError("Null recordedTrack!");
			return;
		}

		// record the touch event as a TrackNote
		// completed and added to recordedTrack OnTouchEnd
		recordingNote = new TrackNote
		{
			SongEventName = this.SongEventName,

			InstrumentNumber = currentInstrumentNumber,
			GridCoords = recordNearestGridPoint.gridCoords,
			TouchStartDistance = distanceFromGridPoint,
			TouchStartTime = (int)DateTime.Now.Subtract(playStartTime).TotalMilliseconds
		};

		var targetNote = GetNoteAtPlayhead(targetTrack, false);       // look for a target note near the current play head position

		// if no target note starts close to current play head position
		// or nearest grid point to touch position is not the target one
		// record a dud note
		if (targetNote == null || recordNearestGridPoint != gridPoints[(int)targetNote.GridCoords.x, (int)targetNote.GridCoords.y])
		{
			recordingNote.IsDudNote = true;       // record a 'dud' note
		}

		recordingNote.TimelinePosition = playHeadPosition;    // ms

		// capture target note data
		if (targetNote != null)
		{
			recordingNote.BeatNumber = targetNote.BeatNumber;
			recordingNote.BarNumber = targetNote.BarNumber;
			recordingNote.BeatsPerMinute = targetNote.BeatsPerMinute;

			recordingNote.InstrumentNumber = targetNote.InstrumentNumber;
			recordingNote.NoteNumber = targetNote.NoteNumber;
			recordingNote.NoteDuration = targetNote.NoteDuration;           // 1/4 notes
		}

		// expand nearest grid point to touch
		recordNearestGridPoint.Expand();             // start scaling grid button

		if (recordingNote.IsDudNote)
		{
			SaimonseiEvents.OnDudNote?.Invoke();
		}
		else
		{
			SaimonseiEvents.OnPlayNote?.Invoke(SongEventName, recordingNote.InstrumentNumber, recordingNote.NoteNumber);
		}
	}


	private void OnTouchEnd(Vector2 touchPosition)      // screen position
	{
		if (NetworkManager.IsMultiPlayerGame)
		{
			if (NetworkManager.IsLocalPlayersTurn)
				SaimonseiEvents.OnEndTouchClient?.Invoke(Camera.main.ScreenToViewportPoint(touchPosition));
		}
		else
			EndTouch(touchPosition);
	}

	private void EndTouch(Vector2 touchPosition)
	{
		if (!touching)      // shouldn't happen!
			return;

		touching = false;

		if (playMode != PlayMode.RecordInstrument || recordingNote == null || recordNearestGridPoint == null)
			return;

		//SaimonseiEvents.OnCreateRipple?.Invoke(Camera.main.ScreenToWorldPoint(touchPosition), Ripple.RippleType.Score);

		recordingNote.TouchEndDistance = Vector2.Distance(touchPosition, recordNearestGridPoint.gridDot.transform.position);
		recordingNote.TouchEndTime = (int)DateTime.Now.Subtract(playStartTime).TotalMilliseconds;
		recordingNote.NoteDurationMs = recordingNote.TouchEndTime - recordingNote.TouchStartTime;

		recordedTrack.AddNote(recordingNote);

		if (recordNearestGridPoint != null)
			recordNearestGridPoint.ResetScale(false);

		if (!recordingNote.IsDudNote)
			SaimonseiEvents.OnStopNote?.Invoke(SongEventName, recordingNote.InstrumentNumber, recordingNote.NoteNumber);

		// score the touch
		if (NetworkManager.IsLocalPlayersTurn)      // true if !multiplayer
		{
			ScoreKeeper.ScoreRating rating;
			//InstrumentScore touchScore;

			if (gameMode == ModeManager.GameMode.StoryMode)
			{
				storyNoteIndex++;
				InstrumentScore touchScore = ScoreKeeper.ScoreStoryNote(storyNoteIndex, targetTrack, recordedTrack, out rating);

				StoryNoteScores.Add(touchScore);
				SaimonseiEvents.OnTouchScore?.Invoke(Camera.main.ScreenToWorldPoint(TouchPosition), recordingNote, TotalStoryInstrumentScore, rating);

				//Debug.Log("OnTouchEnd: running score = " + TotalStoryInstrumentScore.TotalScore);
			}
			else
			{
				InstrumentScore touchScore = ScoreKeeper.ScoreInstrument(recordingNote.InstrumentNumber, gridPoints.Length, targetTrack, recordedTrack, out rating);
				SaimonseiEvents.OnTouchScore?.Invoke(Camera.main.ScreenToWorldPoint(TouchPosition), recordingNote, touchScore, rating);
			}
		}
	}

	private InstrumentScore TotalStoryInstrumentScore
	{
		get
		{
			InstrumentScore totalScore = new InstrumentScore();

			foreach (var score in StoryNoteScores)
			{
				totalScore.TimingDifference += score.TimingDifference;
				totalScore.DurationDifference += score.DurationDifference;
				totalScore.LocationDifference += score.LocationDifference;
				totalScore.DudNoteCount += score.DudNoteCount;
				totalScore.RecordedNoteCount++;
			}

			return totalScore;
		}
	}

	private void OnTouchMove(Vector2 direction, Vector2 touchPosition)
	{
		if (!touching)      // shouldn't happen!
			return;

		TouchPosition = touchPosition;

		// TODO: pitch bend??
	}
}