﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using UnityEngine;


public class UdpDiscovery : MonoBehaviour
{
    public float BroadcastInterval;
	public float ListenInterval;
	public float ListenTimeOut;

    public int BroadcastPort;
    public int ListenPort;

    //public string Password = "SAIMONSEI";		// TODO: append to broadcast ip address for extra verification?

    private Coroutine broadcastCoroutine;
    private bool broadcasting = false;

    private Coroutine listenCoroutine;
    private bool listening = false;
    private DateTime listenStartTime;

    private UdpClient udpServer;		// broadcast
    private UdpClient udpClient;		// listen



	#region broadcasting

	// send out host ip address
	public void StartBroadcasting()
    {
        StopBroadcasting();
        broadcastCoroutine = StartCoroutine(Broadcast());
    }

    private IEnumerator Broadcast()
    {
        if (broadcasting)
            yield break;

        string ipAddress = "";

        try
        {
			ipAddress = LocalIPAddress;

            if (ipAddress == null)
            {
                Debug.LogError("null LocalIPAddress!");
                yield break;
            }
		}
		catch (Exception ex)
		{
			Debug.LogError("Broadcast: failed to resolve LocalIPAddress: " + ex.Message);
            yield break;
        }

		byte[] ipBytes = Encoding.ASCII.GetBytes(ipAddress);
		broadcasting = true;

		udpServer = new UdpClient();
        udpServer.EnableBroadcast = true;

        //Debug.Log("Broadcasting: " + Encoding.ASCII.GetString(ipBytes));

        while (broadcasting)
        {
			udpServer.Send(ipBytes, ipBytes.Length, new IPEndPoint(IPAddress.Broadcast, BroadcastPort));

			//Debug.Log("Broadcasting: " + Encoding.ASCII.GetString(ipBytes));
            yield return new WaitForSeconds(BroadcastInterval);
        }

        udpServer.Close();
		yield return null;
    }

    public void StopBroadcasting()
    {
        if (broadcastCoroutine != null)
        {
            StopCoroutine(broadcastCoroutine);
            broadcastCoroutine = null;
        }

        broadcasting = false;
		if (udpServer != null)
			udpServer.Close();
    }

    #endregion


    #region listening

    // listen for (receive) host ip address for NetworkPlayer to be spawned (MLAPI) - see OnHostIP event
    public void StartListening()
    {
        StopListening();
        listenCoroutine = StartCoroutine(Listen());
    }

    private IEnumerator Listen()
	{
        listening = true;
        listenStartTime = DateTime.Now;

        double elapsedSeconds = 0;

        udpClient = new UdpClient();
        udpClient.EnableBroadcast = true;
        udpClient.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true); // allows connections within the computer
        udpClient.Client.Bind(new IPEndPoint(IPAddress.Any, ListenPort));

        udpClient.BeginReceive(new AsyncCallback(ReceiveCallback), null);       // async - doesn't block
        //Debug.Log("BeginReceive - start timeout counter");

        //SaimonseiEvents.OnDiscoveryListenCountdown?.Invoke(0, (int)ListenTimeOut);

        // timeout if waiting too long for EndReceive...
        while (listening)
		{
            elapsedSeconds = (DateTime.Now - listenStartTime).TotalSeconds;
            //Debug.Log("Waiting to receive... " + elapsedSeconds);
            SaimonseiEvents.OnDiscoveryListenCountdown?.Invoke((int)elapsedSeconds, (int)ListenTimeOut);

            if (elapsedSeconds > ListenTimeOut)
            {
                StopListening();

                Debug.Log("Listen timeout!");
                SaimonseiEvents.OnDiscoveryListenTimeout?.Invoke();
				yield break;
            }

            yield return new WaitForSecondsRealtime(ListenInterval);


        }

        yield return null;
	}

	// async callback from BeginReceive()
    private void ReceiveCallback(IAsyncResult result)
    {
        //Debug.Log("ReceiveCallback");

        try
        {
            IPEndPoint receiveEndPoint = new IPEndPoint(IPAddress.Any, ListenPort);
            byte[] ipAddress = udpClient.EndReceive(result, ref receiveEndPoint);

            //Debug.Log("EndReceive ipAddress: " + Encoding.ASCII.GetString(ipAddress));

			// dispatch OnHostIp event invocation back to main thread
            Dispatcher.Invoke(() =>
            {
                SaimonseiEvents.OnHostIP?.Invoke(Encoding.ASCII.GetString(ipAddress));
                StopListening();          // stop listening once an ip address has been received
            });
        }
        catch
		{
            // callback called as a result of udpClient.Close() (via timeout) will throw exception...
            // (udpClient.Close() is only way to cancel the BeginReceive)
        }
	}

    public void StopListening()
	{
        listening = false;

        if (listenCoroutine != null)
        {
            StopCoroutine(listenCoroutine);
            listenCoroutine = null;
        }

        try
        {
			if (udpClient != null)
				udpClient.Close();
        }
        catch
        {
            // in case udpClient spits the dummy...
        }
	}

	#endregion



	public static string LocalIPAddress
    {
        get
        {
            if (!System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                Debug.Log("LocalIPAddress GetIsNetworkAvailable = false");
                return null;
            }

            var host = Dns.GetHostEntry(Dns.GetHostName());

            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork || ip.AddressFamily == AddressFamily.InterNetworkV6)
                {
                    return ip.ToString();
                }
            }

            Debug.LogError("No network adapters with an IPv4 or IPv6 address in the system!");
            return null;
        }
    }
}
