﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MLAPI;
using MLAPI.Connection;
using MLAPI.Security;
using MLAPI.Spawning;
using MLAPI.Transports.UNET;
using UnityEngine;


[DisallowMultipleComponent]
[RequireComponent(typeof(UnetTransport))]

// class to handle starting and stopping hosts and clients (ie. connecting NetworkPlayers to NetworkingManager)
public class NetworkManager : MonoBehaviour
{
	public string RoomPassword;

	public static Dictionary<ulong, NetworkedObject> SpawnedPlayersDict => SpawnManager.SpawnedObjects;
	//public List<NetworkedObject> SpawnedPlayers => SpawnManager.SpawnedObjectsList;

	public static List<NetworkPlayer> NetworkPlayers
	{
		get
		{
			List<NetworkPlayer> players = new List<NetworkPlayer>();

			foreach (var spawnedPlayer in SpawnManager.SpawnedObjectsList)
			{
				players.Add(spawnedPlayer.GetComponent<NetworkPlayer>());
			}
			return players;
		}
	}

    // assumes host is first player
    // clients sorted in alphabetical order to avoid duplicate hosted groups
	public static List<NetworkPlayer> ClientPlayers
	{
		get
		{
			List<NetworkPlayer> clients = new List<NetworkPlayer>();

			foreach (var playerObject in SpawnManager.SpawnedObjectsList)
			{
				var player = playerObject.GetComponent<NetworkPlayer>();

				if (player.PlayerNumber.Value == 1)       // host
					continue;

				clients.Add(player);
			}

			return clients.OrderBy((x) => x.PlayerName).ToList();
		}
	}

    // separated string of the client names for leaderboard group key
	public static string GroupPlayersKey
	{
		get
		{
			string players = "";
			int playerIndex = 0;

			foreach (var player in ClientPlayers)   // clients sorted in alphabetical order
			{
				players += player.PlayerName.Value;

				if (playerIndex < ClientPlayers.Count - 1)        // not last player
					players += LeaderboardManager.GroupNameSeparator;
			}
			return players;
		}
	}

	public static NetworkPlayer GetPlayerByNumber(int playerNumber)
    {
		return NetworkPlayers.Where(x => x.PlayerNumber.Value == playerNumber).FirstOrDefault();
	}

	public static NetworkPlayer GetPlayerByInstrument(int instrumentNumber)
	{
		return NetworkPlayers.Where(x => x.InstrumentScores.ContainsKey(instrumentNumber)).FirstOrDefault();
	}

    public static bool LocalPlayerPlayedInstrument(int instrumentNumber)
    {
		if (!IsMultiPlayerGame)
			return true;

		InstrumentScore score;

		foreach (var player in NetworkPlayers)
		{
			if (player.IsLocalPlayer && player.GetInstrumentScore(instrumentNumber, out score))
				return true;
		}

		return false;
    }

    public static int PlayerCount { get { return SpawnManager.SpawnedObjectsList != null ? SpawnManager.SpawnedObjectsList.Count : -1; } }
	public static int MaxPlayers = 4;
	public static bool IsValidPlayerCount { get { return PlayerCount > 1 && PlayerCount <= MaxPlayers; } }

	public static NetworkPlayer GameHost { get { return NetworkPlayers.Where(x => x.IsGameHost).FirstOrDefault(); } }

	public static bool IsMultiPlayerGame { get { return PlayerCount > 0; } }

	public static int CurrentPlayerNumber { get; private set; }

	public static NetworkedObject GetPlayer(ulong clientId)
	{
		return IsMultiPlayerGame ? SpawnManager.GetPlayerObject(clientId) : null;
	}

	private UnetTransport transport;
	private const ushort PortNumber = 7777;

	// Only runs on host and client
	public static NetworkPlayer GetLocalPlayer { get { return SpawnManager.GetLocalPlayerObject().GetComponent<NetworkPlayer>(); } }
	public static bool IsLocalPlayersTurn { get { return (! IsMultiPlayerGame) ? true : GetLocalPlayer.IsPlayersTurn; }}


    public static string PlayersTurnName
    {
        get
        {
			foreach (var spawnedPlayer in SpawnManager.SpawnedObjectsList)
			{
				var player = spawnedPlayer.GetComponent<NetworkPlayer>();

                //Debug.Log("PlayersTurnName: '" + player.PlayerName.Value + "' PlayerNumber = " + player.PlayerNumber.Value + " IsPlayersTurn = " + player.IsPlayersTurn);
                if (player.IsPlayersTurn)
					return player.PlayerName.Value;
			}
			return "Anon";
        }
    }

    // scores for all players
	public static List<InstrumentScore> GroupInstrumentScores
	{
		get
		{
			List<InstrumentScore> scores = new List<InstrumentScore>();

			foreach (var player in NetworkPlayers)
			{
				scores.AddRange(player.InstrumentScores.Values);
			}

			return scores;
		}
	}

	// Contains player object, owned objects, cryptography keys and more
	public static NetworkedClient GetClient(ulong clientId)
	{
		return IsMultiPlayerGame ? NetworkingManager.Singleton.ConnectedClients[clientId] : null;
	}


	private void Start()
	{
		NetworkingManager.Singleton.ConnectionApprovalCallback += ApprovalCheck;
		transport = GetComponent<UnetTransport>();
	}

	private void OnDestroy()
	{
		Disconnect();
		NetworkingManager.Singleton.ConnectionApprovalCallback -= ApprovalCheck;
	}


	private void OnEnable()
	{
		SaimonseiEvents.OnHostGame += OnHostGame;
		SaimonseiEvents.OnJoinGame += OnJoinGame;
		SaimonseiEvents.OnLeaveGame += OnLeaveGame;
	}

	private void OnDisable()
	{
		SaimonseiEvents.OnHostGame -= OnHostGame;
		SaimonseiEvents.OnJoinGame -= OnJoinGame;
		SaimonseiEvents.OnLeaveGame -= OnLeaveGame;
	}


	private void StartHost(ushort port = PortNumber)
	{
		if (NetworkingManager.Singleton.IsListening == true)
			return;

		try
		{
			transport.ConnectAddress = UdpDiscovery.LocalIPAddress;
		}
		catch (Exception ex)
		{
			Debug.LogError("StartHost: unable to resolve local IP address: " + ex.Message);
			return;
		}

		transport.ServerListenPort = port;

		Debug.Log("StartHost: Host IP = " + transport.ConnectAddress + " port = " + transport.ServerListenPort);
		NetworkingManager.Singleton.StartHost();		// spawns a NetworkPlayer
	}

	private void StartClient(string IP, ushort port = PortNumber)
	{
		if (NetworkingManager.Singleton.IsListening == true)
			return;

		transport.ConnectAddress = IP;
		transport.ConnectPort = port;

		Debug.Log("StartClient: IP = " + transport.ConnectAddress + " port = " + transport.ConnectPort);
		NetworkingManager.Singleton.StartClient();      // spawns a NetworkPlayer
	}


	private void ApprovalCheck(byte[] connectionData, ulong clientId, NetworkingManager.ConnectionApprovedDelegate callback)
	{
		// approval logic here
		byte[] password = Encoding.ASCII.GetBytes(RoomPassword);

		//Debug.Log("ApprovalCheck: connectionData = " + BitConverter.ToString(connectionData) + " RoomPassword " + BitConverter.ToString(password));

		bool approveConnection = BitConverter.ToString(connectionData) == BitConverter.ToString(password);
		bool createPlayerObject = true;

		//Debug.Log("ApprovalCheck: approveConnection = " + approveConnection + " clientId = " + clientId);

		ulong ? prefabHash = null; // SpawnManager.GetPrefabHashFromGenerator("MyPrefabHashGenerator"); // The prefab hash. Use null to use the default player prefab

		// if approved, the connection gets added. If it's false, the client is disconnected
		callback(createPlayerObject, prefabHash, approveConnection, Vector3.zero, Quaternion.identity);
	}

	private void OnHostGame()
	{
		//Debug.Log("OnHostGame - StartHost");
		StartHost();
	}

	private void OnJoinGame(string ipAddress)
	{
		// password for connection approval...
		NetworkingManager.Singleton.NetworkConfig.ConnectionData = Encoding.ASCII.GetBytes(RoomPassword);

		try
		{
			StartClient(ipAddress);
		}
		catch (Exception ex)
		{
			Debug.Log("OnJoinGame Exception: " + ex + " Message: " + ex.Message);
		}

		//Debug.Log("OnJoinGame: " + ipAddress);
	}

	private void OnLeaveGame(ulong clientID)
	{
		if (! IsMultiPlayerGame)
			return;

		if (GetLocalPlayer != null)
		{
			if (clientID == NetworkingManager.Singleton.LocalClientId)
			{
				Debug.Log("OnLeaveGame: OwnerClientId = " + GetLocalPlayer.OwnerClientId);
				Disconnect();

				if (GetLocalPlayer.IsOwnedByServer)
					SaimonseiEvents.OnHostEndedGame?.Invoke();
			}
		}
	}

	private void Disconnect()
	{
		if (NetworkingManager.Singleton.IsHost)
		{
			NetworkingManager.Singleton.StopHost();
		}
		else if (NetworkingManager.Singleton.IsClient)
		{
			NetworkingManager.Singleton.StopClient();
		}
		else if (NetworkingManager.Singleton.IsServer)
		{
			NetworkingManager.Singleton.StopServer();
		}
	}


    // static current player number
	public static void IncrementCurrentPlayer()
	{
		CurrentPlayerNumber++;

		if (CurrentPlayerNumber > PlayerCount)      // cycle round
			CurrentPlayerNumber = 1;

		SaimonseiEvents.OnCurrentPlayerChanged?.Invoke(CurrentPlayerNumber);
	}

	public static void ResetCurrentPlayer()
	{
		CurrentPlayerNumber = 0;
		SaimonseiEvents.OnCurrentPlayerChanged?.Invoke(CurrentPlayerNumber);
	}
}
