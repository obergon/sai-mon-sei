﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


// wraps a UdpDiscovery instance to handle host IP send and receive
// and client receiving of host IP address, so clients can 'find' the host
//
// effectively the 'lobby'
[DisallowMultipleComponent]
public class NetworkUI : MonoBehaviour
{
	//public bool UseUdpDiscovery = true;
	//public HostDiscovery Discovery;         // TODO: remove!  HostDiscovery uses UnityEngine.Networking...
	public UdpDiscovery UdpDiscovery;

	public Button HostGameButton;
	public Button JoinGameButton;
	public Button LeaveGameButton;
	//public Text NumNetworkPlayers;
	public Text DiscoveryStatus;
	public Text DiscoveryCountdown;

	public GameObject NetworkPlayers;       // scroll view content
	public PlayerButton PlayerPrefab;       // for scroll view content

	public Color LocalPlayerColour;
	public Color OtherPlayerColour;

	public PlayerProfile PlayerProfile;

	private NetworkPlayer localPlayer;          // is this useful?

	private bool listeningforHost = false;      // so can cancel if no host found

    private bool internetReachable;
	private bool localNetworkReachable;


	private void OnEnable()
	{
		internetReachable = (Application.internetReachability != NetworkReachability.NotReachable);
		localNetworkReachable = (Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork);

		ConfigNetworkButtons(false);

		SaimonseiEvents.OnPlayerNameSaveResult += OnPlayerNameSaved;
		SaimonseiEvents.OnHostIP += HostIPReceived;

		SaimonseiEvents.OnNetworkPlayerEnteredGame += OnNetworkPlayerEnteredGame;
		SaimonseiEvents.OnNetworkPlayerLeftGame += OnNetworkPlayerLeftGame;
		SaimonseiEvents.OnStartNewGame += OnStartNewGame;
		SaimonseiEvents.OnHostEndedGame += OnHostEndedGame;
		SaimonseiEvents.OnDiscoveryListenCountdown += OnDiscoveryListenCountdown;
		SaimonseiEvents.OnDiscoveryListenTimeout += OnDiscoveryListenTimeout;

		HostGameButton.onClick.AddListener(OnHostGameButtonClicked);
		JoinGameButton.onClick.AddListener(OnJoinGameButtonClicked);
		LeaveGameButton.onClick.AddListener(OnLeaveGameButtonClicked);
	}

    private void OnDisable()
	{
		SaimonseiEvents.OnPlayerNameSaveResult -= OnPlayerNameSaved;
		SaimonseiEvents.OnHostIP -= HostIPReceived;

		SaimonseiEvents.OnNetworkPlayerEnteredGame -= OnNetworkPlayerEnteredGame;
		SaimonseiEvents.OnNetworkPlayerLeftGame -= OnNetworkPlayerLeftGame;
		SaimonseiEvents.OnStartNewGame -= OnStartNewGame;
		SaimonseiEvents.OnHostEndedGame -= OnHostEndedGame;
		SaimonseiEvents.OnDiscoveryListenCountdown -= OnDiscoveryListenCountdown;
		SaimonseiEvents.OnDiscoveryListenTimeout -= OnDiscoveryListenTimeout;

		HostGameButton.onClick.RemoveListener(OnHostGameButtonClicked);
		JoinGameButton.onClick.RemoveListener(OnJoinGameButtonClicked);
		LeaveGameButton.onClick.RemoveListener(OnLeaveGameButtonClicked);
	}


    private void OnHostGameButtonClicked()
	{
		DisableNetworkButtons();
		BroadcastHostIP();

		SaimonseiEvents.OnHostGame?.Invoke();       // start host
	}

	// client
	private void OnJoinGameButtonClicked()
	{
		DisableNetworkButtons();
		StartDiscoveryListening();
	}

	private void OnLeaveGameButtonClicked()
	{
		DisableNetworkButtons();
		StopDiscoveryListening();
		SaimonseiEvents.OnLeaveGame?.Invoke(localPlayer.OwnerClientId);       // rpc to all (other) clients after disconnection

		// update UI immediately on local client (rpc will not reach us because we will be disconnected)
		OnNetworkPlayerLeftGame(localPlayer.OwnerClientId);
	}


	private void OnNetworkPlayerEnteredGame(NetworkPlayer player)
	{
		if (player.IsLocalPlayer)
		{
			ConfigNetworkButtons(true);
			localPlayer = player;
		}

        if (GetPlayerButton(player.OwnerClientId) == null)      // don't want duplicate buttons! (eg. host/server)
        {
            //Debug.Log("OnNetworkPlayerEnteredGame: PlayerName = " + player.PlayerName.Value + " ClientId = " + player.OwnerClientId);

            var playerButton = Instantiate(PlayerPrefab, NetworkPlayers.transform);     // scroll view content
            playerButton.NetworkPlayer = player;
            playerButton.ClientID = player.OwnerClientId;

			playerButton.transform.SetSiblingIndex((int)player.OwnerClientId);           // order of buttons
            playerButton.PlayerName.text = player.PlayerName.Value;

            playerButton.ButtonImage.color = player.IsLocalPlayer ? LocalPlayerColour : OtherPlayerColour;
            playerButton.HostImage.enabled = player.IsOwnedByServer;
        }
    }


	private void OnNetworkPlayerLeftGame(ulong clientID)
	{
		// remove player button from scrollview
		// if the local player left the game, enable the host/join buttons
		var playerButton = GetPlayerButton(clientID);

		if (playerButton != null && playerButton.ClientID == clientID)
		{
			Destroy(playerButton.gameObject);

			if (playerButton.NetworkPlayer.IsLocalPlayer)
				ConfigNetworkButtons(false);
		}
	}

	private PlayerButton GetPlayerButton(ulong clientID)
	{
		foreach (Transform playerTransform in NetworkPlayers.transform)
		{
			var playerButton = playerTransform.GetComponent<PlayerButton>();

			if (playerButton.ClientID == clientID)
				return playerButton;
		}

		return null;
	}


	private void OnDiscoveryListenCountdown(int elapsedSeconds, int timeoutSeconds)
	{
		DiscoveryCountdown.text = (timeoutSeconds - elapsedSeconds).ToString();
	}

	private void OnDiscoveryListenTimeout()
	{
		StopDiscoveryListening();
		EnableNetworkButtons();
	}

	private void OnHostEndedGame()
	{
		ClearPlayerButtons();
	}

	private void ClearPlayerButtons()
	{
		foreach (Transform playerButton in NetworkPlayers.transform)
		{
			Destroy(playerButton.gameObject);
		}
	}


    private void OnStartNewGame(string songEventName, PlayArea.PlayMode mode)
	{
		StopDiscoveryListening();
	}

	private void DisableNetworkButtons()
	{
		HostGameButton.interactable = false;
		JoinGameButton.interactable = false;
		LeaveGameButton.interactable = false;
	}

	private void EnableNetworkButtons()
	{
		HostGameButton.interactable = true;
		JoinGameButton.interactable = true;
		LeaveGameButton.interactable = true;
	}

	private void ConfigNetworkButtons(bool inGame)
	{
		bool nameSaved = !string.IsNullOrEmpty(PlayerProfile.PlayerName);

		HostGameButton.interactable = localNetworkReachable && !inGame && nameSaved;
		JoinGameButton.interactable = localNetworkReachable && !inGame && nameSaved;
		LeaveGameButton.interactable = localNetworkReachable && inGame && nameSaved;
	}


	// async callback
	private void OnPlayerNameSaved(string playerName, bool success)
	{
		if (success)
		{
			ConfigNetworkButtons(false);
		}
	}

	private void ConfigHostButton()
	{
		bool nameSaved = !string.IsNullOrEmpty(PlayerProfile.PlayerName);

		HostGameButton.interactable = localNetworkReachable && nameSaved && !listeningforHost; // && !Discovery.IsClient;     // can't host if listening for host broadcast
	}


	private void HostIPReceived(string hostIP)
	{
		//Debug.Log("HostIPReceived: " + hostIP);

		// join game immediately host ip received
		SaimonseiEvents.OnJoinGame?.Invoke(hostIP);

		StopDiscoveryListening();                  // stops listening and broadcasting
		ConfigHostButton();
	}

	private void StartDiscoveryListening()
	{
		//Debug.Log("StartDiscoveryListening");

		DiscoverHostIP();      // listen as client to host broadcasts 
		listeningforHost = true;

		ConfigHostButton();                 // can't host if listening for host broadcast
	}

	private void StopDiscoveryListening()
	{
		//Debug.Log("StopDiscoveryListening");

		StopDiscovery();       // stops listening and broadcasting
		listeningforHost = false;

		ConfigHostButton();
	}

	// broadcast for client to discover
	private void BroadcastHostIP()
	{
		//Debug.Log("BroadcastHostIP Discovery.StartAsServer");

		//if (UseUdpDiscovery)
		//{
			UdpDiscovery.StartBroadcasting();
		//}
		//else
		//{
		//	Discovery.Initialize();
		//	Discovery.StartAsServer();
		//}

		DiscoveryStatus.text = "Broadcasting...";
	}

	// start listening for host IP broadcast
	private void DiscoverHostIP()
	{
		//Debug.Log("DiscoverHostIP Discovery.StartAsClient");

		//if (UseUdpDiscovery)
		//{
			UdpDiscovery.StartListening();
		//}
		//else
		//{
		//	Discovery.Initialize();
		//	Discovery.StartAsClient();
		//}

		DiscoveryStatus.text = "Searching...";
	}

	private void StopDiscovery()
	{
		//if (UseUdpDiscovery)
		//{
			UdpDiscovery.StopBroadcasting();
			UdpDiscovery.StopListening();
		//}
		//else
		//{
		//	Debug.Log("StopDiscovery Discovery.IsRunning: " + Discovery.IsRunning);

		//	if (Discovery.IsRunning)
		//		Discovery.StopBroadcast();       // stops listening and broadcasting
		//}

		DiscoveryStatus.text = "";
		DiscoveryCountdown.text = "";
	}
}
