﻿
using UnityEngine;
using MLAPI;
using MLAPI.NetworkedVar;
using MLAPI.Messaging;
using MLAPI.NetworkedVar.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

public class NetworkPlayer : NetworkedBehaviour
{
    public NetworkedVar<string> PlayerName = new NetworkedVar<string>(new NetworkedVarSettings()
    {
        SendTickrate = 0f,
        SendChannel = "Unreliable",
        ReadPermission = NetworkedVarPermission.Everyone,
        WritePermission = NetworkedVarPermission.OwnerOnly,
    });

    public NetworkedVar<int> PlayerNumber = new NetworkedVar<int>(new NetworkedVarSettings()
    {
        SendTickrate = 0f,
        SendChannel = "Unreliable",
        ReadPermission = NetworkedVarPermission.Everyone,
        WritePermission = NetworkedVarPermission.OwnerOnly,
    });


	public bool IsPlayersTurn { get { return NetworkManager.CurrentPlayerNumber == PlayerNumber.Value; } }
	public bool IsGameHost { get { return PlayerNumber.Value == 1; } }

	private int CurrentInstrumentNumber;

    // instruments played by this player
    // instrument number, pb score
	public NetworkedDictionary<int, InstrumentScore> InstrumentScores = new NetworkedDictionary<int, InstrumentScore>(new NetworkedVarSettings()
	{
		SendTickrate = 0f,
		SendChannel = "Unreliable",
		ReadPermission = NetworkedVarPermission.Everyone,
		WritePermission = NetworkedVarPermission.Everyone,
	});

	// player's leaderboard scores for the current group and song - cached so each can check for new PB
	private List<LeaderboardInstrPlayerScores> groupSongPlayerScores = new List<LeaderboardInstrPlayerScores>();

    public PlayerProfile PlayerProfile;


	// invoked when the object is spawned and ready
	// all properties like IsLocalPlayer etc are guaranteed to be ready
	// client submits to server, which fires OnNetworkPlayerEnteredGame event on all clients - for UI etc.
	public override void NetworkStart()
	{
		base.NetworkStart();

        if (IsOwner)
        {
			PlayerNumber.Value = NetworkManager.PlayerCount; // (int)((OwnerClientId == 0) ? 1 : OwnerClientId);
			PlayerName.Value = PlayerProfile.PlayerName;
		}
		//Debug.Log("NetworkPlayer.NetworkStart PlayerNumber = " + PlayerNumber.Value + " PlayerName = '" + PlayerName.Value + "' IsServer = " + IsServer + " IsLocalPlayer = " + IsLocalPlayer);
        //Debug.Log("NetworkPlayer.NetworkStart clientID = " + OwnerClientId + " PlayerName = " + PlayerName.Value + " IsHost = " + IsHost + " IsClient = " + IsClient + " IsServer = " + IsServer + " IsLocalPlayer = " + IsLocalPlayer);

        InvokeServerRpc(SubmitClientJoinedGame, this);         // OnNetworkPlayerEnteredGame event on all clients

		if (IsServer)       // server drives games / play cycle
		{
            PlayerName.OnValueChanged += OnPlayerNameChanged;           // eg. rpc update player button
            PlayerNumber.OnValueChanged += OnPlayerNumberChanged;       // eg. rpc update player button

			NetworkingManager.Singleton.OnClientDisconnectCallback += OnClientDisconnected;     // callback runs on server and connected client only

			SaimonseiEvents.OnStartSongServer += OnStartSong;
			SaimonseiEvents.OnStartCycleServer += OnStartCycle;
			SaimonseiEvents.OnPlayTimelineNoteServer += OnPlayTimelineNote;
		}

		if (IsLocalPlayer)
		{
			SaimonseiEvents.OnInstrumentChanged += OnInstrumentChanged;
            SaimonseiEvents.OnPlayTimelineNoteLocal += OnPlayTimelineNoteLocal;     // TODO: better way?
            SaimonseiEvents.OnFinalGroupScore += OnFinalGroupScore;                 // update InstrumentScores for instruments this player played
		    SaimonseiEvents.OnGroupSongScoresRetrieved += OnGroupSongScoresRetrieved;         // leaderboard lookup
		}

		if (IsClient)       // ie. all players listening for player input - ClientRPC to localplayer only
		{
			SaimonseiEvents.OnStartTouchClient += OnStartTouch;
			SaimonseiEvents.OnEndTouchClient += OnEndTouch;
		}

        InstrumentScores.OnDictionaryChanged += OnInstrumentScoresChanged;
	}

    private void OnDestroy()
    {
		PlayerName.OnValueChanged -= OnPlayerNameChanged;
		PlayerNumber.OnValueChanged -= OnPlayerNumberChanged;

		//NetworkingManager.Singleton.OnClientDisconnectCallback -= OnClientDisconnected;     // callback runs on server and connected client only

		SaimonseiEvents.OnStartSongServer -= OnStartSong;
		SaimonseiEvents.OnStartCycleServer -= OnStartCycle;
		SaimonseiEvents.OnPlayTimelineNoteServer -= OnPlayTimelineNote;

		SaimonseiEvents.OnInstrumentChanged -= OnInstrumentChanged;
        SaimonseiEvents.OnStartTouchClient -= OnStartTouch;
        SaimonseiEvents.OnEndTouchClient -= OnEndTouch;
        SaimonseiEvents.OnPlayTimelineNoteLocal -= OnPlayTimelineNoteLocal;

        SaimonseiEvents.OnGroupSongScoresRetrieved -= OnGroupSongScoresRetrieved;
        SaimonseiEvents.OnFinalGroupScore -= OnFinalGroupScore;

		InstrumentScores.OnDictionaryChanged -= OnInstrumentScoresChanged;
	}


	// server only
	private void OnPlayerNameChanged(string previousValue, string newValue)
	{
		if (!IsServer)
			return;

		InvokeClientRpcOnEveryone(ApplyPlayerNameChanged, previousValue, newValue);
	}

	[ClientRPC]
	public void ApplyPlayerNameChanged(string previousValue, string newValue)
	{
		SaimonseiEvents.OnPlayerNameChanged?.Invoke(this, newValue);
	}


	// server only
	private void OnPlayerNumberChanged(int previousValue, int newValue)
	{
		if (!IsServer)
			return;

		InvokeClientRpcOnEveryone(ApplyPlayerNumberChanged, previousValue, newValue);
	}

	[ClientRPC]
	public void ApplyPlayerNumberChanged(int previousValue, int newValue)
	{
		SaimonseiEvents.OnPlayerNumberChanged?.Invoke(this, newValue);
	}


	private void OnInstrumentScoresChanged(NetworkedDictionaryEvent<int, InstrumentScore> changeEvent)
    {
        //Debug.Log("OnInstrumentScoresChanged: " + PlayerName.Value + " instrument = " + changeEvent.key + " TotalScore = " + ((changeEvent.value != null) ? changeEvent.value.TotalScore.ToString() : "null"));
    }


    // By default, ServerRPC’s can only be called if the local client
    // owns the object the ServerRPC sits on...
    // RequireOwnership = false disables this
    [ServerRPC(RequireOwnership = false)]
	public void SubmitClientJoinedGame(NetworkPlayer player)
	{
		if (!IsServer)
			return;

		InvokeClientRpcOnEveryone(ApplyClientJoinedGame, player);
	}

	[ClientRPC]
	public void ApplyClientJoinedGame(NetworkPlayer player)
	{
		SaimonseiEvents.OnNetworkPlayerEnteredGame?.Invoke(player);      // eg. add to UI / reactivate buttons etc.
	}


    // response to NetworkManager disconnected event
    // if server, apply to all clients (for UI)
    private void OnClientDisconnected(ulong clientID)
	{
		try
		{
			if (IsServer)
			{
				InvokeClientRpcOnEveryone(ApplyClientLeftGame, clientID);
				Debug.Log("Server OnClientDisconnected " + clientID);
			}
		}
		catch
		{
			// NetworkPlayer may already have been destroyed (eg. by host leaving)
			Debug.Log("NetworkPlayer.OnClientDisconnected NetworkPlayer for clientID " + clientID + " already destroyed!");
		}
	}

	[ClientRPC]
	public void ApplyClientLeftGame(ulong clientID)
	{
		//Debug.Log("ApplyClientLeftGame " + clientID);
		SaimonseiEvents.OnNetworkPlayerLeftGame?.Invoke(clientID);      // eg. remove from UI / reactivate buttons etc.
	}


	// server only
	private void OnStartSong(string songTitle, string songEventName)
	{
		if (!IsServer)
			return;

		InstrumentScores.Clear();
		InvokeClientRpcOnEveryone(ApplyStartSong, songTitle, songEventName);
	}


	[ClientRPC]
	public void ApplyStartSong(string songTitle, string songEventName)
	{
		if (!IsLocalPlayer)    // InvokeClientRpcOnEveryone runs on every NetworkPlayer!
			return;

        // get the current leaderboard scores for this group, song and player
        // OnHostSongScoresRetrieved handles result
        SaimonseiEvents.OnLookupGroupSongScores?.Invoke(NetworkManager.GameHost.PlayerName.Value, NetworkManager.GroupPlayersKey, songTitle);   // retrieve from db & enable after clearing existing entries

        //Debug.Log("ApplyStartSong IsLocalPlayer: " + IsLocalPlayer + " IsOwnedByServer: " + IsOwnedByServer + " OwnerClientId: " + OwnerClientId);
        SaimonseiEvents.OnSongSelectedNetwork?.Invoke(songTitle, songEventName);
	}


	private void OnGroupSongScoresRetrieved(string hostName, string clientNames, string songName, List<LeaderboardInstrPlayerScores> songScores)
	{
		if (!IsLocalPlayer)
			return;

		groupSongPlayerScores = songScores;
	}

    // gets this player's score for the current song and given instrument
	private int GroupSongInstrumentScore(string instrument)
	{
		foreach (var songScore in groupSongPlayerScores)
		{
			if (songScore.Instrument == instrument)
			{
				foreach (var instrScore in songScore.PlayerScores)
				{
					if (instrScore.Player == PlayerName.Value)
					{
						//Debug.Log("GroupSongInstrumentScore: instrument " + instrument + " score: " + instrScore.Score);
						return instrScore.Score;
					}
				}
			}
		}

		return InstrumentScore.MaxScore;
	}

	// server only
	private void OnStartCycle()
	{
		if (!IsServer)
			return;

		InvokeClientRpcOnEveryone(ApplyStartCycle);
	}

	[ClientRPC]
	public void ApplyStartCycle()
	{
		SaimonseiEvents.OnStartPlayCycleNetwork?.Invoke();
	}

	// local player only
	private void OnInstrumentChanged(string songEventName, int instrumentNumber, string playerInstrumentName, string shortInstrumentName, int numGridPoints)
	{
		if (!IsLocalPlayer)
			return;

		CurrentInstrumentNumber = instrumentNumber;
	}

    // keep track of which instrument(s) the local player played
    private void CurrentInstrumentPlayed()
    {
		if (!IsLocalPlayer)
			return;

		if (!InstrumentScores.ContainsKey(CurrentInstrumentNumber))
			InstrumentScores.Add(CurrentInstrumentNumber, new InstrumentScore());       // pb score updated by OnFinalGroupScore
	}

    // update NetworkedDictionary InstrumentScores with the instruments this player played
    // save to leaderboard db if new PB for this group/song/player
	// local player only
	private void OnFinalGroupScore(SongScore songScore)
	{
		if (!IsLocalPlayer)
			return;

        List<InstrumentScore> playerScoresToSave = new List<InstrumentScore>();       // list of scores for this player for leaderboard update

        foreach (var instrScore in songScore.InstrumentScores)
		{
            InstrumentScore score;

            // if this player played this instrument, update the NetworkedDict with their score
            if (GetInstrumentScore(instrScore.InstrumentNumber, out score))
            {
				//Debug.Log("OnFinalGroupScore: " + instrScore.InstrumentNumber + ", PlayerName " + instrScore.PlayerName + ", InstrumentName " + instrScore.InstrumentName + ", TotalScore " + instrScore.TotalScore + ", isNewPB " + instrScore.isNewPB);

                InstrumentScores[instrScore.InstrumentNumber] = instrScore;  // NetworkedDict

                // score for this player and this instrument
				int playerScore = GroupSongInstrumentScore(instrScore.InstrumentName);
				instrScore.isNewPB = false;

				// save score if it is the first for this instrument or if the new score is better (lower) than current
				if (playerScore == InstrumentScore.MaxScore || instrScore.TotalScore < playerScore)
				{
					instrScore.isNewPB = true;
					playerScoresToSave.Add(instrScore);
					//Debug.Log("OnFinalGroupScore: NEW PB! TotalScore: " + instrScore.TotalScore);
				}
			}
		}

		// save group scores to the leaderboard, for instruments this player played
		SaimonseiEvents.OnSaveGroupSongScore?.Invoke(songScore.SongName, playerScoresToSave);       // leaderboard db update
	}

    // get player's score for a given instrument for the current song
	public bool GetInstrumentScore(int instrumentNumber, out InstrumentScore score)
	{
		return InstrumentScores.TryGetValue(instrumentNumber, out score);
	}

	private void OnPlayTimelineNote(Vector2 randomCoords, TimelineData timelineData)
	{
		if (!IsServer)
			return;

		InvokeClientRpcOnEveryone(ApplyPlayNote, randomCoords, timelineData);
	}


    private void OnPlayTimelineNoteLocal(Vector2 randomCoords, TimelineData timelineData)
    {
        if (!IsLocalPlayer)
            return;

        // log instrument played by local player
        CurrentInstrumentPlayed();
    }

    // By default, ServerRPC’s can only be called if the local client
    // owns the object the ServerRPC sits on...
    // RequireOwnership = false disables this
    [ServerRPC(RequireOwnership = false)]
	public void SubmitPlayNote(Vector2 randomCoords, TimelineData timelineData)
	{
		if (!IsServer)
			return;

		InvokeClientRpcOnEveryone(ApplyPlayNote, randomCoords, timelineData);
	}

	[ClientRPC]
	public void ApplyPlayNote(Vector2 randomCoords, TimelineData timelineData)
	{
        //Debug.Log("ApplyPlayNote: '" + PlayerName.Value + "' noteDuration: " + timelineData.noteDuration + " IsLocalPlayer = " + IsLocalPlayer);

        if (!IsLocalPlayer)    // InvokeClientRpcOnEveryone runs on every NetworkPlayer!
			return;

		SaimonseiEvents.OnAnimateNoteNetwork?.Invoke(randomCoords, timelineData);
	}


    // local player only
    // touchPosition is viewport point ([0,0] to [1,1])
    private void OnStartTouch(Vector2 touchPosition)
    {
        //Debug.Log("OnStartTouch: '" + PlayerName.Value + "' IsLocalPlayer = " + IsLocalPlayer);

		// all clients call Server RPC .. only local player reacts (Client RPC) to update game 
		InvokeServerRpc(SubmitStartTouch, touchPosition);
    }

    // By default, ServerRPC’s can only be called if the local client
    // owns the object the ServerRPC sits on...
    // RequireOwnership = false disables this
    [ServerRPC(RequireOwnership = false)]
    public void SubmitStartTouch(Vector2 touchPosition)
    {
        //Debug.Log("OnStartTouch: '" + PlayerName.Value + "' IsLocalPlayer = " + IsLocalPlayer + "' IsServer = " + IsServer);

        if (!IsServer)
            return;

        // touchPosition is viewport point ([0,0] to [1,1])
        InvokeClientRpcOnEveryone(ApplyStartTouch, touchPosition);
    }

    [ClientRPC]
	public void ApplyStartTouch(Vector2 touchPosition)
	{
        if (!IsLocalPlayer)    // InvokeClientRpcOnEveryone runs on every NetworkPlayer!
            return;

		//Debug.Log("ApplyStartTouch: '" + PlayerName.Value + "' IsLocalPlayer = " + IsLocalPlayer + " IsServer = " + IsServer); // + " Players = " + NetworkManager.NetworkPlayers.Count);

		// touchPosition is viewport point ([0,0] to [1,1])
		SaimonseiEvents.OnTouchStartNetwork?.Invoke(Camera.main.ViewportToScreenPoint(touchPosition));
	}


    // localplayer only
    // touchPosition = viewport point ([0,0] to [1,1])
    private void OnEndTouch(Vector2 touchPosition)
    {
        // all clients call Server RPC .. only local player reacts (Client RPC) to update game 
        InvokeServerRpc(SubmitEndTouch, touchPosition);
    }


    // By default, ServerRPC’s can only be called if the local client
    // owns the object the ServerRPC sits on...
    // RequireOwnership = false disables this
    [ServerRPC(RequireOwnership = false)]
    public void SubmitEndTouch(Vector2 touchPosition)
    {
        if (!IsServer)
            return;

        // touchPosition = viewport point ([0,0] to [1,1])
        InvokeClientRpcOnEveryone(ApplyEndTouch, touchPosition);
    }

    [ClientRPC]
	public void ApplyEndTouch(Vector2 touchPosition)
	{
		if (!IsLocalPlayer)    // InvokeClientRpcOnEveryone runs on every NetworkPlayer!
			return;

		// touchPosition = viewport point ([0,0] to [1,1])
		SaimonseiEvents.OnTouchEndNetwork?.Invoke(Camera.main.ViewportToScreenPoint(touchPosition));
	}
}
