﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerButton : MonoBehaviour
{
    public NetworkPlayer NetworkPlayer { get; set; }

	public Button Button;
	public Image ButtonImage;
	public Image HostImage;
	public Image TurnImage;
	public Image LeftImage;
	public Text PlayerNumber;
	public Text PlayerName;

    public ulong ClientID;

    private void OnEnable()
    {
        SaimonseiEvents.OnPlayerNameChanged += OnPlayerNameChanged;         // unsure of NetworkedVar timing - might be after button instantiated?
        SaimonseiEvents.OnPlayerNumberChanged += OnPlayerNumberChanged;         // unsure of NetworkedVar timing - might be after button instantiated?
    }

    private void OnDisable()
	{
        SaimonseiEvents.OnPlayerNameChanged -= OnPlayerNameChanged;
        SaimonseiEvents.OnPlayerNumberChanged -= OnPlayerNumberChanged;
    }

    private void OnPlayerNameChanged(NetworkPlayer player, string playerName)
    {
        if (NetworkPlayer == null)          // may have already been destroyed (?)
            return;

        if (player.OwnerClientId == NetworkPlayer.OwnerClientId)
            PlayerName.text = player.PlayerName.Value;
    }

    private void OnPlayerNumberChanged(NetworkPlayer player, int playerNumber)
    {
        if (NetworkPlayer == null)          // may have already been destroyed (?)
            return;

        // TODO: reinstate player number?
        //if (player.OwnerClientId == NetworkPlayer.OwnerClientId)
        //    PlayerNumber.text = player.PlayerNumber.Value.ToString() + ".";
    }


    public void PlayerLeftGame()
	{
		Button.interactable = false;
		LeftImage.enabled = true;
		TurnImage.enabled = false;
	}
}
