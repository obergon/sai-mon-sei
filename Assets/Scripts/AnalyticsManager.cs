﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

//[RequireComponent(typeof(AnalyticsEventTracker))]
public class AnalyticsManager : MonoBehaviour
{
	public SavedGameState SavedGameState;

	//private AnalyticsEventTracker eventTracker;
	private StoryRealm currentRealm;


	//private void Start()
	//{
	//	eventTracker = GetComponent<AnalyticsEventTracker>();
	//}


	private void OnEnable()
	{
		SaimonseiEvents.OnGameModeSelected += OnGameModeSelected;
		SaimonseiEvents.OnEnterRealm += OnEnterRealm;
		SaimonseiEvents.OnRealmStateChanged += OnRealmStateChanged;
		SaimonseiEvents.OnRealmRevealStateChanged += OnRealmRevealStateChanged;
		SaimonseiEvents.OnStoryPlayQuit += OnStoryPlayQuit;
	}

	private void OnDisable()
	{
		SaimonseiEvents.OnGameModeSelected -= OnGameModeSelected;
		SaimonseiEvents.OnEnterRealm -= OnEnterRealm;
		SaimonseiEvents.OnRealmStateChanged -= OnRealmStateChanged;
		SaimonseiEvents.OnRealmRevealStateChanged -= OnRealmRevealStateChanged;
		SaimonseiEvents.OnStoryPlayQuit -= OnStoryPlayQuit;
	}


	private void OnGameModeSelected(ModeManager.GameMode mode)
	{
        AnalyticsEvent.Custom("GameModeSelected", new Dictionary<string, object>
                {
                    { mode.ToString(), SavedGameState.AnalyticsData }
                    //{ mode.ToString(), SavedGameState.ToJson(true) }
				});
    }

	private void OnEnterRealm(StoryRealm realm, int realmIndex, int realmCount)
	{
		currentRealm = realm;
	}

	private void OnRealmStateChanged(Goddess.RealmState newState, bool skipping)
	{
		switch (newState)
		{
			case Goddess.RealmState.EnteredRealm:
			case Goddess.RealmState.StoryTold:
			case Goddess.RealmState.LayerReveal:
			case Goddess.RealmState.LearningRealmSong:
				break;

			case Goddess.RealmState.RealmSongLearnt:
				AnalyticsEvent.Custom("RealmComplete", new Dictionary<string, object>
				{
					{ currentRealm.RealmName, SavedGameState.AnalyticsData }
					//{ currentRealm.RealmName, SavedGameState.ToJson(true) }
				});
				break;

			case Goddess.RealmState.LandHealthZero:
				break;
		}
	}

	private void OnRealmRevealStateChanged(RealmReveal.RevealState newState, StoryRealm realm, bool skippingTouchstones)
	{
		switch (newState)
		{
			case RealmReveal.RevealState.Start:
			case RealmReveal.RevealState.PlaySequence:
			case RealmReveal.RevealState.Capture:
			case RealmReveal.RevealState.Touched:
			case RealmReveal.RevealState.TimeOut:
				break;

			case RealmReveal.RevealState.Success:
				AnalyticsEvent.Custom("LayerComplete", new Dictionary<string, object>
				{
					{ currentRealm.CurrentLayer.VeilName, SavedGameState.AnalyticsData }
					//{ currentRealm.CurrentLayer.VeilName, SavedGameState.ToJson(true) }
				});
				break;

			case RealmReveal.RevealState.Revealed:
			case RealmReveal.RevealState.WaitingPlayer:
			case RealmReveal.RevealState.Fail:
				break;
		}
	}

	private void OnStoryPlayQuit()
	{
		AnalyticsEvent.Custom("StoryPlayQuit", new Dictionary<string, object>
				{
					{ "StoryQuit", SavedGameState.AnalyticsData }
					//{ "StoryQuit", SavedGameState.ToJson(true) }
				});
	}
}