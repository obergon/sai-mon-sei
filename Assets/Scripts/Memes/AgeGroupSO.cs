﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "AgeGroup", menuName = "Memes/Age Group")]
public class AgeGroupSO : ScriptableObject
{
    public string AgeRangeName;

    [Space]
    [Tooltip("Minimum age of range in years (inclusive)")]
    public int MinAge;      // years
    [Tooltip("Maximum age of range in years (exclusive)")]
    public int MaxAge;      // years


    public bool AgeWithinRange(int years)
    {
        if (MaxAge < MinAge)                // messed up age range!
            return true;

        if (MinAge == 0 && MaxAge == 0)     // not an age range!
            return true;

        return years >= MinAge && years < MaxAge;
    }
}
