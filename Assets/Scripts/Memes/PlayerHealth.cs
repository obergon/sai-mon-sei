﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class PlayerHealth
{
    public string HealthName;

    [Range(0, 100f)]
    public float CurrentValue;
}
