﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Meme", menuName = "Memes/Meme")]
public class MemeSO : ScriptableObject
{
    public string MemeText;
    public Sprite MemeImage;
    public Color MemeColour = Color.white;

    [Header("Impact on Player Health (percentage)")]
    public float[] LikeHealth = new float[ MemePlayer.NumHealthBars ];
    public float[] DislikeHealth = new float[ MemePlayer.NumHealthBars ];
    public float[] ShareHealth = new float[ MemePlayer.NumHealthBars ];
    public float[] MissHealth = new float[ MemePlayer.NumHealthBars ];

    [Header("Impact on Player Age (years)")]
    public float LikeAgeIncrement;          // years
    public float DislikeAgeIncrement;
    public float ShareAgeIncrement;
    public float MissAgeIncrement;

    [Header("Impact on Player Ageing Rate")]
    public float LikeAgeingRate;          // factor
    public float DislikeAgeingRate;
    public float ShareAgeingRate;
    public float MissAgeingRate;

    [Header("Impact on Land Health (percentage)")]
    public float LikeLandHealthIncrement;
    public float DislikeLandHealthIncrement;
    public float ShareLandHealthIncrement;
    public float MissLandHealthIncrement;

    [Header("Eligibility")]
    public List<AgeGroupSO> AgeGroups; 
    public List<string> RealmNames;         // TODO: RealmSO
    public List<string> LayerNames;         // TODO: LayerSO

    [Header("Audio")]
    [Tooltip("Sound played when meme has faded in")]
    public AudioClip ArriveAudio;
    [Tooltip("Sound played just before meme is destroyed")]
    public AudioClip DepartAudio;
    [Tooltip("Sound played while meme is fading in")]
    public AudioClip AnimateAudio;
    [Tooltip("Sound played when meme cancelled by player")]
    public AudioClip CancelledAudio;


    public bool IsEligibleForAge(float playerAge)
    {
        if (AgeGroups == null || AgeGroups.Count == 0)      // meme for all ages
            return true;

        foreach (var ageGroup in AgeGroups)
        {
            if (ageGroup.AgeWithinRange((int)playerAge))
                return true;
        }
        return false;
    }

    public bool IsEligibleForRealm(string realmName)
    {
        if (RealmNames == null || RealmNames.Count == 0)      // meme for all realms
            return true;

        foreach (var realm in RealmNames)
        {
            if (string.Compare(realm, realmName) == 0)
                return true;
        }
        return false;
    }

    public bool IsEligibleForLayer(string layerName)
    {
        if (LayerNames == null || LayerNames.Count == 0)      // meme for all layers
            return true;

        foreach (var layer in LayerNames)
        {
            if (string.Compare(layer, layerName) == 0)
                return true;
        }
        return false;
    }
}
