﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MemeUI : MonoBehaviour
{
    [Header("Meme Content")]
    public Text MemeText;
    public Image MemeImage;

    public CanvasGroup Canvas;

    [Header("Buttons")]
    public Button LikeButton;
    public Button DislikeButton;
    public Button ShareButton;
    public Button CancelButton;

    [Header("Audio")]
    public AudioSource MemeAudioSource;
    [Tooltip("Sound played when meme arrives")]
    public AudioClip MemeArriveAudio;
    [Tooltip("Sound played just before meme is destroyed")]
    public AudioClip MemeDepartAudio;
    [Tooltip("Sound played while meme is fading in")]
    public AudioClip MemeAnimateAudio;
    [Tooltip("Sound played when meme cancelled by player")]
    public AudioClip MemeCancelledAudio;

    [Tooltip("Can meme be cancelled while it's fading in?")]
    public bool CanCancel = true;                 // meme can be stopped while it's fading in

    [Tooltip("Fadeout / cancel")]
    public ParticleSystem FadeOutParticles;

    public MemeSO MemeData { get; private set; }

    private float fadeInTime = 1.25f;
    private float fadeOutTime = 0.25f;

    private float pulseScale = 1.25f;        // on arrival
    private float pulseTime = 0.05f;        // on arrival

    private int warpInTween;            // cancel if player cancels tween
    private int scaleInTween;           // cancel if player cancels tween
    private int fadeInTween;            // cancel if player reacts
    private int fadeOutTween;           // cancel if player reacts

    private bool playerReacted = false;      // a reaction button clicked - only one permitt

    private enum MemeState
    {
        FadingIn,
        FadedIn,
        FadingOut
    };

    private MemeState memeState;

    private void OnEnable()
    {
        LikeButton.onClick.AddListener(OnMemeLike);
        DislikeButton.onClick.AddListener(OnMemeDisike);
        ShareButton.onClick.AddListener(OnMemeShare);
        CancelButton.onClick.AddListener(OnMemeCancel);

        SaimonseiEvents.OnStopMemes += OnStopMemes;
	}

    private void OnDisable()
    {
        LikeButton.onClick.RemoveListener(OnMemeLike);
        DislikeButton.onClick.RemoveListener(OnMemeDisike);
        ShareButton.onClick.RemoveListener(OnMemeShare);
        CancelButton.onClick.RemoveListener(OnMemeCancel);

        SaimonseiEvents.OnStopMemes -= OnStopMemes;
    }


    public void WarpIn(float showTime)
    {
        PlayerCanReact(false);

        var endPosition = transform.position;
        transform.position = Vector3.zero;

        var endScale = transform.localScale;
        transform.localScale = Vector3.zero;

        warpInTween = LeanTween.move(gameObject, endPosition, fadeInTime)
                            .setEaseInQuart()
                            .setOnComplete(() =>
                            {
                                PlaySFX(MemeArriveAudio);
                                PlaySFX(MemeData.ArriveAudio);
                            }).id;

        scaleInTween = LeanTween.scale(gameObject, endScale, fadeInTime)
                            .setEaseInQuart()
                            .setOnComplete(() =>
                            {
                                    LeanTween.scale(gameObject, Vector3.one * pulseScale, pulseTime)
                                                .setEaseOutQuart()
                                                .setLoopPingPong(1);
                            }).id;

        FadeInOut(showTime);

        PlaySFX(MemeAnimateAudio);
        PlaySFX(MemeData.AnimateAudio);
    }

    private void FadeInOut(float showTime)
    {
        if (!CanCancel)              // meme 'showing' as soon as it starts fading in
        {
            SaimonseiEvents.OnMemeShowing?.Invoke(this);   // meme counter incremented so can't tap to play
        }

        memeState = MemeState.FadingIn;
        CancelButton.interactable = false;

        fadeInTween = LeanTween.value(gameObject, 0f, 1f, fadeInTime)
                                .setOnUpdate((float x) => Canvas.alpha = x)
                                .setEaseOutQuad()
                                .setOnComplete(() =>
                                {
                                    if (CanCancel)        // meme not 'showing' until fully faded in
                                    {
                                        SaimonseiEvents.OnMemeShowing?.Invoke(this);   // meme counter incremented so can't tap to play
                                        CancelButton.interactable = true;
                                    }
                                    memeState = MemeState.FadedIn;
                                    PlayerCanReact(true);

                                    fadeOutTween = FadeOut(showTime).id;        // tween can be cancelled
                                }).id;
    }

    private LTDescr FadeOut(float delayTime)
    {
        //Debug.Log("FadeOut: fadingOut = " + fadingOut);
        // cancel FadeInOut tweens when player clicks a reaction
        if (playerReacted)
            CancelTweens();

        //FadeOutParticles.Play();

        //var tween = LeanTween.value(gameObject, Canvas.alpha, 0f, fadeOutTime)
        //              .setDelay(delayTime)
        //              .setEaseInQuad()
        //              .setOnUpdate((float x) => Canvas.alpha = x)

        // set state after delay
        LeanTween.delayedCall(delayTime, OnFadeOut);

        //var tween = LeanTween.scale(gameObject, Vector3.zero, fadeOutTime)
        var tween = LeanTween.scaleY(gameObject, 0f, fadeOutTime)
                    .setDelay(delayTime)
                    .setEaseInElastic()          // screen 'flicker'
                    .setOnComplete(() =>
                    {
                        //SaimonseiEvents.OnMemeHidden?.Invoke();   // meme count decremented

                        if (!playerReacted)
                            SaimonseiEvents.OnMemeMissed?.Invoke(this);

                        PlaySFX(MemeDepartAudio);
                        PlaySFX(MemeData.DepartAudio);

                        Destroy(gameObject, 1f);
                        fadeInTween = 0;
                        fadeOutTween = 0;
                    });

        return tween;
    }

    private void OnFadeOut()
    {
        SaimonseiEvents.OnMemeHidden?.Invoke();   // meme count decremented

        memeState = MemeState.FadingOut;
        CancelButton.interactable = false;
    }

    private void CancelTweens()
    {
        if (warpInTween != 0)
            LeanTween.cancel(warpInTween);

        if (scaleInTween != 0)
            LeanTween.cancel(scaleInTween);

        if (fadeInTween != 0)
            LeanTween.cancel(fadeInTween);

        if (fadeOutTween != 0)
            LeanTween.cancel(fadeOutTween);
    }

    public void SetMemeData(MemeSO meme)
    {
        MemeData = meme;

        MemeText.text = meme.MemeText;
        MemeText.color = meme.MemeColour;
        MemeImage.sprite = meme.MemeImage;
    }

    private void PlayerCanReact(bool canReact)
    {
        LikeButton.interactable = canReact;
        DislikeButton.interactable = canReact;
        ShareButton.interactable = canReact;
    }

    private void OnStopMemes(bool onTouch)
    {
        //playerReacted = true;

        CancelTweens();
        FadeOut(0f);

        if (onTouch)
        {
            PlaySFX(MemeCancelledAudio);
            PlaySFX(MemeData.CancelledAudio);
        }
    }

    // reaction button clicks

    private void OnMemeLike()
    {
        if (MemeData == null || playerReacted)
            return;

        playerReacted = true;
        FadeOut(0f);
        SaimonseiEvents.OnMemeLiked?.Invoke(this);
    }

    private void OnMemeDisike()
    {
        if (MemeData == null || playerReacted)
            return;

        playerReacted = true;
        FadeOut(0f);
        SaimonseiEvents.OnMemeDisliked?.Invoke(this);
    }

    private void OnMemeShare()
    {
        if (MemeData == null || playerReacted)
            return;

        playerReacted = true;
        FadeOut(0f);
        SaimonseiEvents.OnMemeShared?.Invoke(this);
    }


    private void OnMemeCancel()
    {
        OnStopMemes(true);
        SaimonseiEvents.OnMemeCancelled?.Invoke(this);
    }

    private void PlaySFX(AudioClip sfx)
    {
        if (sfx == null)
            return;

        MemeAudioSource.clip = sfx;
        MemeAudioSource.Play();
    }
}
