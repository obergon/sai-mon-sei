﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Linq;
//using UnityEngine;


//public class MemeGenerator : MonoBehaviour
//{
//    public List<MemeSO> MemeLibrary = new List<MemeSO>();
//    public MemeUI MemePrefab;

//    public float MinMemeTime = 2f;
//    public float MaxMemeTime = 5f;

//    private bool generating = false;
//    private Coroutine generatorCoroutine;

//    private Vector3[] playGridCorners = null;       // for memes


//    //private void OnEnable()
//    //{
//    //    SaimonseiEvents.OnRealmStateChanged += OnRealmStateChanged;
//    //    SaimonseiEvents.OnEnterRealmLayer += OnEnterRealmLayer;
//    //}

//    //private void OnDisable()
//    //{
//    //    SaimonseiEvents.OnRealmStateChanged -= OnRealmStateChanged;
//    //    SaimonseiEvents.OnEnterRealmLayer -= OnEnterRealmLayer;
//    //}


//    private void OnEnterRealmLayer(StoryRealm realm, RealmLayer layer, Vector3[] gridCorners)
//    {
//        playGridCorners = gridCorners;
//        StartGenerating(realm, layer);
//    }

//    private void OnRealmStateChanged(Goddess.RealmState newState, bool skipping)
//    {
//        switch (newState)
//        {
//            case Goddess.RealmState.EnteredRealm:
//                break;
//            case Goddess.RealmState.StoryTold:
//                break;
//            case Goddess.RealmState.LayerReveal:
//                break;
//            case Goddess.RealmState.LearningRealmSong:
//                StopGenerating();
//                break;
//            case Goddess.RealmState.RealmSongLearnt:
//                StopGenerating();
//                break;
//            case Goddess.RealmState.LandHealthZero:
//                StopGenerating();
//                break;
//        }
//    }


//    // returns world coords
//    private Vector2 RandomInsideGrid()      // world space corner coords
//    {
//        if (playGridCorners == null)
//            return Vector3.zero;

//        //Debug.Log("RandomInsideGrid: x = " + playGridCorners[0].x + ", " + playGridCorners[1].x + " y = " + playGridCorners[0].y + ", " + playGridCorners[1].y);
//        var randomX = UnityEngine.Random.Range(playGridCorners[0].x, playGridCorners[2].x);
//        var randomY = UnityEngine.Random.Range(playGridCorners[0].y, playGridCorners[2].y);
//        return new Vector2(randomX, randomY);
//    }


//    private void StartGenerating(StoryRealm realm, RealmLayer layer)
//    {
//        StopGenerating();
//        generatorCoroutine = StartCoroutine(GenerateMemes(realm, layer));
//    }


//    private void StopGenerating()
//    {
//        if (generatorCoroutine != null)
//            StopCoroutine(generatorCoroutine);

//        generating = false;
//    }

//    private IEnumerator GenerateMemes(StoryRealm realm, RealmLayer layer)
//    {
//        if (generating)
//            yield break;

//        generating = true;

//        while (generating)
//        {
//            yield return new WaitForSeconds(5f);
//            GenerateMeme(Player.CurrentAge, realm.RealmName, layer.VeilName);
//        }

//        yield return null;
//    }

//    private MemeSO GetRandomMeme(float age, string realmName, string layerName)
//    {
//        //var eligable = MemeLibrary.Where(x => x.AgeGroups.Contains(ageGroup)).ToList();

//        //return eligable[Random.Range(0, eligable.Count - 1)];
//        return MemeLibrary[UnityEngine.Random.Range(0, MemeLibrary.Count - 1)];
//    }


//    public MemeUI GenerateMeme(float age, string realmName, string layerName)
//    {
//        var randomMeme = GetRandomMeme(age, realmName, layerName);
//        if (randomMeme == null)
//            return null;

//        float memeTime = UnityEngine.Random.Range(MinMemeTime, MaxMemeTime);
//        var newMeme = Instantiate(MemePrefab, transform);
//        newMeme.transform.position = RandomInsideGrid();

//        newMeme.SetMeme(randomMeme);
//        newMeme.FadeInOut(memeTime);

//        return newMeme;
//    }
//}
