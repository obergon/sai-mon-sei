﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "PlayerData", menuName = "Memes/Player Data")]
public class PlayerSO : ScriptableObject
{
    public float CurrentAge;              // age in years

    [Tooltip("Constant rate at which player ages (years per minute")]
    public float RateOfAgeing;

    public bool DoMemes = false;

    public PlayerHealth[] HealthBars = new PlayerHealth[ MemePlayer.NumHealthBars ];


    public void InitPlayerData()
    {
        CurrentAge = 0;

        foreach (var healthBar in HealthBars)
        {
            healthBar.CurrentValue = 0;
        }
    }
}
