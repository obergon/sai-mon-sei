﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Player age, health and meme generator
/// </summary>
public class PlayerUI : UIPanel
{
    [Space]
    public GameObject UIPanel;

    [Header("Health")]
	public List<PlayerHealthUI> healthBars = new List<PlayerHealthUI>();

	[Header("Age")]
	public Text CurrentAge;
	public Slider AgeSlider;

    [Space]
    [Tooltip("Only display memes eligible for age, realm and layer")]
    public bool CheckEligibility = false;

    [Header("Memes")]
    public List<MemeSO> MemeLibrary = new List<MemeSO>();
    public MemeUI MemePrefab;
    public Transform Memes;            // container

    [Header("Meme Timing")]
    public float MinMemeTime;              // time meme shows for
    public float MaxMemeTime;

    public float MinMemeInterval;          // time between memes
    public float MaxMemeInterval;

    [Tooltip("Health bar updated audio")]
    public AudioClip HealthBarAudio;

    [Header("Saved Player State")]
    public PlayerSO PlayerData;

    private float healthBarUpdateTime = 0.5f;       // age / health sliders 
    private float healthBarPulseScale = 1.2f;
    private float HelthBarPulseTime = 0.25f;

    private bool generatingMemes = false;
    private Coroutine generatorCoroutine;

    private Vector3[] playGridCorners = null;       // for memes
    private float memeMargin = 6f;                 // from edges of grid

    private int memesShowing = 0;


    private void OnEnable()
    {
        SaimonseiEvents.OnRealmStateChanged += OnRealmStateChanged;
        SaimonseiEvents.OnRealmRevealStateChanged += OnRealmRevealStateChanged;
        SaimonseiEvents.OnEnterRealmLayer += OnEnterRealmLayer;

        // memes
        SaimonseiEvents.OnMemeShowing += OnMemeShowing;
        SaimonseiEvents.OnMemeHidden += OnMemeHidden;
        SaimonseiEvents.OnMemeLiked += OnMemeLiked;
        SaimonseiEvents.OnMemeDisliked += OnMemeDisliked;
        SaimonseiEvents.OnMemeShared += OnMemeShared;
        SaimonseiEvents.OnMemeMissed += OnMemeMissed;

        SaimonseiEvents.OnIncrementAge += OnIncrementAge;
    }

    private void OnDisable()
    {
        SaimonseiEvents.OnRealmStateChanged -= OnRealmStateChanged;
        SaimonseiEvents.OnRealmRevealStateChanged -= OnRealmRevealStateChanged;
        SaimonseiEvents.OnEnterRealmLayer -= OnEnterRealmLayer;

        SaimonseiEvents.OnMemeShowing -= OnMemeShowing;
        SaimonseiEvents.OnMemeHidden -= OnMemeHidden;
        SaimonseiEvents.OnMemeLiked -= OnMemeLiked;
        SaimonseiEvents.OnMemeDisliked -= OnMemeDisliked;
        SaimonseiEvents.OnMemeShared -= OnMemeShared;
        SaimonseiEvents.OnMemeMissed -= OnMemeMissed;

        SaimonseiEvents.OnIncrementAge -= OnIncrementAge;
    }

    private void OnEnterRealmLayer(StoryRealm realm, RealmLayer layer, Vector3[] gridCorners)
    {
        playGridCorners = gridCorners;
        StartGenerating(realm, layer);
    }

    private void OnRealmStateChanged(Goddess.RealmState newState, bool skipping)
    {
        switch (newState)
        {
            case Goddess.RealmState.EnteredRealm:
                break;
            case Goddess.RealmState.StoryTold:
                break;
            case Goddess.RealmState.LayerReveal:
                break;
            case Goddess.RealmState.LearningRealmSong:
                StopGenerating();
                break;
            case Goddess.RealmState.RealmSongLearnt:
                StopGenerating();
                break;
            case Goddess.RealmState.LandHealthZero:
                StopGenerating();
                break;
        }
    }

    private void OnRealmRevealStateChanged(RealmReveal.RevealState newState, StoryRealm realm, bool skippingTouchstones)
    {
        switch (newState)
        {
            case RealmReveal.RevealState.Start:
                break;
            case RealmReveal.RevealState.PlaySequence:
                StopGenerating();
                break;
            case RealmReveal.RevealState.Capture:
                break;
            case RealmReveal.RevealState.Touched:
                break;
            case RealmReveal.RevealState.TimeOut:
                break;
            case RealmReveal.RevealState.Success:
                break;
            case RealmReveal.RevealState.Revealed:
                break;
            case RealmReveal.RevealState.WaitingPlayer:
                StopGenerating();
                break;
            case RealmReveal.RevealState.Fail:
                break;
        }
    }

    // returns world coords
    private Vector3 RandomInsideGrid()     
    {
        if (playGridCorners == null)    // world space corner coords
            return Vector3.zero;

        var randomX = UnityEngine.Random.Range(playGridCorners[0].x + memeMargin, playGridCorners[2].x - memeMargin);
        var randomY = UnityEngine.Random.Range(playGridCorners[0].y + memeMargin, playGridCorners[2].y - memeMargin);
        return new Vector3(randomX, randomY, 0f);
    }


    private void StartGenerating(StoryRealm realm, RealmLayer layer)
    {
        if (!PlayerData.DoMemes)
            return;

        generatorCoroutine = StartCoroutine(GenerateMemes(realm, layer));
    }

    private void StopGenerating()
    {
        if (!PlayerData.DoMemes)
            return;

        if (! generatingMemes)
            return;

        if (generatorCoroutine != null)
            StopCoroutine(generatorCoroutine);

        generatingMemes = false;
        SaimonseiEvents.OnStopMemes?.Invoke(false);
        SaimonseiEvents.OnStopAgeing?.Invoke();
    }

    private IEnumerator GenerateMemes(StoryRealm realm, RealmLayer layer)
    {
        if (!PlayerData.DoMemes)
            yield break;

        if (generatingMemes)
            yield break;

        generatingMemes = true;
        SaimonseiEvents.OnStartMemes?.Invoke();
        SaimonseiEvents.OnStartAgeing?.Invoke();

        while (generatingMemes)
        {
            yield return new WaitForSeconds(UnityEngine.Random.Range(MinMemeInterval, MaxMemeInterval));
            GenerateMeme(realm.RealmName, layer.VeilName);
        }

        yield return null;
    }

    private MemeSO GetRandomMeme(string realmName, string layerName)
    {
        if (CheckEligibility)
        {
            var eligible = MemeLibrary.Where(x => x.IsEligibleForAge(PlayerData.CurrentAge)
                                                && x.IsEligibleForRealm(realmName)
                                                && x.IsEligibleForLayer(layerName)).ToList();

            if (eligible.Count == 0)
                return null;

            return eligible[UnityEngine.Random.Range(0, eligible.Count)];
        }

        return MemeLibrary[UnityEngine.Random.Range(0, MemeLibrary.Count)];
    }

    public MemeUI GenerateMeme(string realmName, string layerName)
    {
        var randomMeme = GetRandomMeme(realmName, layerName);
        if (randomMeme == null)
            return null;

        float memeShowTime = UnityEngine.Random.Range(MinMemeTime, MaxMemeTime);
        var newMeme = Instantiate(MemePrefab, Memes);
        newMeme.Canvas.alpha = 0;
        newMeme.transform.position = RandomInsideGrid();

        newMeme.SetMemeData(randomMeme);
        newMeme.WarpIn(memeShowTime);
        //newMeme.FadeInOut(memeShowTime);

        return newMeme;
    }


    private void OnMemeShowing(MemeUI meme)
    {
        memesShowing++;
        SaimonseiEvents.OnMemeCountUpdated?.Invoke(memesShowing);
    }

    private void OnMemeHidden()
    {
        memesShowing--;

        if (memesShowing < 0)       // TODO: find out why this is happening (-1 when cancel memes that are fading in)
            memesShowing = 0;

        SaimonseiEvents.OnMemeCountUpdated?.Invoke(memesShowing);
    }

    // response to player reactions

    private void OnMemeLiked(MemeUI meme)
    {
        if (!PlayerData.DoMemes)
            return;

        //Debug.Log("'" + meme.MemeData.MemeText + "' Liked!");
        for (int i = 0; i < PlayerData.HealthBars.Length; i++)
        {
            UpdateHealthBar(i, meme.MemeData.LikeHealth[i], true);
        }

        UpdateAge(meme.MemeData.LikeAgeIncrement, true, meme.MemeData.LikeAgeingRate);

        // land health
        if (meme.MemeData.LikeLandHealthIncrement != 0)
            SaimonseiEvents.OnUpdateLandHealth?.Invoke(meme.MemeData.LikeLandHealthIncrement, true);
    }

    private void OnMemeDisliked(MemeUI meme)
    {
        if (!PlayerData.DoMemes)
            return;

        //Debug.Log("'" + meme.MemeData.MemeText + "' Disliked!");
        for (int i = 0; i < PlayerData.HealthBars.Length; i++)
        {
            UpdateHealthBar(i, meme.MemeData.DislikeHealth[i], true);
        }

        UpdateAge(meme.MemeData.DislikeAgeIncrement, true, meme.MemeData.DislikeAgeingRate);

        // land health
        if (meme.MemeData.DislikeLandHealthIncrement != 0)
            SaimonseiEvents.OnUpdateLandHealth?.Invoke(meme.MemeData.DislikeLandHealthIncrement, true);
    }

    private void OnMemeShared(MemeUI meme)
    {
        if (!PlayerData.DoMemes)
            return;

        //Debug.Log("'" + meme.MemeData.MemeText + "' Shared!");
        for (int i = 0; i < PlayerData.HealthBars.Length; i++)
        {
            UpdateHealthBar(i, meme.MemeData.ShareHealth[i], true);
        }

        UpdateAge(meme.MemeData.ShareAgeIncrement, true, meme.MemeData.ShareAgeingRate);

        // land health
        if (meme.MemeData.ShareLandHealthIncrement != 0)
            SaimonseiEvents.OnUpdateLandHealth?.Invoke(meme.MemeData.ShareLandHealthIncrement, true);
    }

    private void OnMemeMissed(MemeUI meme)
    {
        if (!PlayerData.DoMemes)
            return;

        //Debug.Log("'" + meme.MemeData.MemeText + "' Missed!");

        for (int i = 0; i < PlayerData.HealthBars.Length; i++)
        {
            UpdateHealthBar(i, meme.MemeData.MissHealth[i], true);
        }

        UpdateAge(meme.MemeData.MissAgeIncrement, true, meme.MemeData.MissAgeingRate);

        // land health
        if (meme.MemeData.MissLandHealthIncrement != 0)
            SaimonseiEvents.OnUpdateLandHealth?.Invoke(meme.MemeData.MissLandHealthIncrement, true);
    }


    // Ageing

    private void OnIncrementAge(float increment, float updateTime)
    {
        UpdateAge(increment, true, 0f, true, updateTime);
    }

    private void UpdateAge(float age, bool increment, float ageingRate, bool silent = false, float updateTime = 0f)
    {
        if (increment)
        {
            if (age == 0)
                return;

            if (PlayerData.CurrentAge == MemePlayer.MaxAge && age > 0)        // already maxed out!
                return;

            if (PlayerData.CurrentAge == 0f && age < 0)                       // already zero!
                return;
        }
        else if (PlayerData.CurrentAge == age)        // no change
            return;

        if (increment)
            PlayerData.CurrentAge += age;
        else
            PlayerData.CurrentAge = age;

        // clamp age
        if (PlayerData.CurrentAge > MemePlayer.MaxAge)
            PlayerData.CurrentAge = MemePlayer.MaxAge;
        else if (PlayerData.CurrentAge < 0f)
            PlayerData.CurrentAge = 0f;

        // rate of ageing factor
        if (ageingRate > 0)
            PlayerData.RateOfAgeing *= ageingRate;

        bool updated = AnimateHealthBar(AgeSlider, PlayerData.CurrentAge, silent, updateTime);

        if (updated && !silent)
            PulseHealthBar(AgeSlider);
    }

    // Health

    private void UpdateHealthBar(int healthBarIndex, float value, bool increment, bool silent = false)
    {
        if (increment)
        {
            if (value == 0)
                return;

            if (PlayerData.HealthBars[healthBarIndex].CurrentValue == MemePlayer.MaxHealth && value > 0)    // already maxed out!
                return;

            if (PlayerData.HealthBars[healthBarIndex].CurrentValue == 0f && value < 0)                      // already zero!
                return;
        }

        if (increment)
            PlayerData.HealthBars[healthBarIndex].CurrentValue += value;
        else
            PlayerData.HealthBars[healthBarIndex].CurrentValue = value;

        if (PlayerData.HealthBars[healthBarIndex].CurrentValue < 0)
            PlayerData.HealthBars[healthBarIndex].CurrentValue = 0;
        else if (PlayerData.HealthBars[healthBarIndex].CurrentValue > MemePlayer.MaxHealth)
            PlayerData.HealthBars[healthBarIndex].CurrentValue = MemePlayer.MaxHealth;

        bool updated = AnimateHealthBar(healthBars[healthBarIndex].HealthBar, PlayerData.HealthBars[healthBarIndex].CurrentValue, silent);
 
        if (updated && !silent)
            PulseHealthBar(healthBars[healthBarIndex].HealthBar);
    }

    private bool AnimateHealthBar(Slider healthBar, float newValue, bool silent = false, float updateTime = 0f)
    {
        if (healthBar.value == newValue)
            return false;

        if (updateTime > 0)
        {
            LeanTween.value(healthBar.gameObject, healthBar.value, newValue, updateTime)
                  .setEaseLinear()
                  .setOnUpdate((float value) =>
                  {
                      healthBar.value = value;
                  });
        }
        else
        {
            LeanTween.value(healthBar.gameObject, healthBar.value, newValue, healthBarUpdateTime)
                  .setEaseOutCirc()
                  .setOnUpdate((float value) =>
                  {
                      healthBar.value = value;
                  });
        }

        if (!silent && newValue != 0f) // && HealthBarAudio != null)
            PlaySFX(HealthBarAudio);
            //AudioSource.PlayClipAtPoint(HealthBarAudio, Vector3.zero);

        return true;
    }

    private void PulseHealthBar(Slider healthBar)
    {
        LeanTween.scale(healthBar.gameObject, Vector3.one * healthBarPulseScale, HelthBarPulseTime)
                    .setEase(LeanTweenType.easeInCubic)
                    .setLoopPingPong(1);
    }


    // init as per saved data
    private void InitHealthBars()
    {
        UpdateAge(PlayerData.CurrentAge, false, PlayerData.RateOfAgeing, true);

        for (int i = 0; i < PlayerData.HealthBars.Length; i++)
        {
            UpdateHealthBar(i, PlayerData.HealthBars[i].CurrentValue, false, true);
        }
    }


    public override void Show(bool visible)
	{
		UIPanel.SetActive(visible);

        if (visible)
            InitHealthBars();           // sync to saved data
    }
}
