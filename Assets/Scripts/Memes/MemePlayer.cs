﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Handles player ageing
/// </summary>
public class MemePlayer : MonoBehaviour
{
    [Space]
    [Tooltip("Seconds between each age update, when ageing")]
	public float AgeUpdateRate;

	[Header("Saved Player State")]
    public PlayerSO PlayerData;

    public const float MaxAge = 100f;              // age in years
    public const float MaxHealth = 100f;           // percent
    public const int NumHealthBars = 4;

    private Coroutine ageingCoroutine = null;
    private bool ageing = false;

    private void OnEnable()
    {
        SaimonseiEvents.OnStartAgeing += OnStartAgeing;
        SaimonseiEvents.OnStopAgeing += OnStopAgeing;
    }

    private void OnDisable()
    {
        SaimonseiEvents.OnStartAgeing -= OnStartAgeing;
        SaimonseiEvents.OnStopAgeing -= OnStopAgeing;
    }


    private void OnStartAgeing()
    {
        // start player ageing
        StartAgeing();
    }

    private void OnStopAgeing()
    {
        // stop player ageing
        StopAgeing();
    }

    private void StartAgeing()
    {
        ageingCoroutine = StartCoroutine(Age());
    }

    private void StopAgeing()
    {
        if (!ageing)
            return;

        if (ageingCoroutine != null)
            StopCoroutine(ageingCoroutine);

        ageing = false;
        SaimonseiEvents.OnAgeingStopped?.Invoke();
    }

    private IEnumerator Age()
    {
        if (ageing)
            yield break;

        ageing = true;
        SaimonseiEvents.OnAgeingStarted?.Invoke();

        while (ageing)
        {
            yield return new WaitForSeconds(AgeUpdateRate);

            // increment age
            var yearsPerSecond = PlayerData.RateOfAgeing / 60f;
            SaimonseiEvents.OnIncrementAge?.Invoke(yearsPerSecond * AgeUpdateRate, AgeUpdateRate);
        }

        yield return null;
    }


    //public PlayerAge GetPlayerAge()
    //{
    //    int years = (int)PlayerData.CurrentAge;
    //    int months = (int)((PlayerData.CurrentAge - years) * 12f);

    //    return new PlayerAge { Years = years, Months = months };
    //}
}


// for display purposes (not currently used)
//[Serializable]
//public class PlayerAge
//{
//    public int Years;
//    public int Months;
//}

