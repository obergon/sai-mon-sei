﻿

//// Decompiled with JetBrains decompiler
//// Type: UnityEngine.Networking.NetworkDiscovery
//// Assembly: UnityEngine.Networking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
//// MVID: 8B34E19C-EF53-416E-AE36-35C45BAFD2DE
//// Assembly location: C:\Users\Blake\sandbox\unity\test-project\Library\UnityAssemblies\UnityEngine.Networking.dll

//using System;
//using System.Collections.Generic;
//using UnityEngine;

//using UnityEngine.Networking;

///// <summary>
/////   Allows Unity games to find each other on a local network
/////   It can broadcast presence and listen for broadcasts
///// </summary>

//[DisallowMultipleComponent]
//public class NetworkingDiscovery : MonoBehaviour
//{
//	[SerializeField]
//	public int BroadcastPort = 7777;
//	[SerializeField]
//	private int BroadcastKey = 2222;
//	[SerializeField]
//	private int BroadcastVersion = 1;
//	[SerializeField]
//	private int BroadcastSubVersion = 1;
//	[SerializeField]
//	private int BroadcastInterval = 1000;

//	[SerializeField]
//	private string BroadcastData = "SAIMONSEI";

//	private int HostId = -1;
//	private const int MaxBroadcastMsgSize = 1024;

//	public bool IsRunning { get; private set; }
//	public bool IsServer { get; private set; }
//	public bool IsClient { get; private set; }

//	public Dictionary<string, NetworkBroadcastResult> BroadcastsReceived { get; private set; }

//	private byte[] MsgOutBuffer;
//	private byte[] MsgInBuffer;
//	private HostTopology DefaultTopology;


//	protected static byte[] StringToBytes(string str)
//	{
//		byte[] numArray = new byte[str.Length * 2];
//		Buffer.BlockCopy(str.ToCharArray(), 0, numArray, 0, numArray.Length);
//		return numArray;
//	}

//	protected static string BytesToString(byte[] bytes)
//	{
//		char[] chArray = new char[bytes.Length / 2];
//		Buffer.BlockCopy(bytes, 0, chArray, 0, bytes.Length);
//		return new string(chArray);
//	}

//	/// <summary>
//	///   <para>Initializes the NetworkDiscovery component.</para>
//	/// </summary>
//	/// <returns>
//	///   <para>Return true if the network port was available.</para>
//	/// </returns>
//	public bool Initialize()
//	{
//		if (BroadcastData.Length >= 1024)
//		{
//			Debug.LogError("NetworkDiscovery Initialize - data too large. max is " + 1024);
//			return false;
//		}
//		if (!NetworkTransport.IsStarted)
//			NetworkTransport.Init();


//		MsgOutBuffer = StringToBytes(BroadcastData);
//		MsgInBuffer = new byte[1024];
//		BroadcastsReceived = new Dictionary<string, NetworkBroadcastResult>();
//		ConnectionConfig defaultConfig = new ConnectionConfig();
//		int num = (int)defaultConfig.AddChannel(QosType.Unreliable);
//		DefaultTopology = new HostTopology(defaultConfig, 1);
//		if (IsServer)
//			StartAsServer();
//		if (IsClient)
//			StartAsClient();
//		return true;
//	}

//	/// <summary>
//	///   <para>Starts listening for broadcasts messages.</para>
//	/// </summary>
//	/// <returns>
//	///   <para>True is able to listen.</para>
//	/// </returns>
//	public bool StartAsClient()
//	{
//		if (HostId != -1 || IsRunning)
//		{
//			Debug.LogWarning("NetworkDiscovery StartAsClient already started");
//			return false;
//		}
//		HostId = NetworkTransport.AddHost(DefaultTopology, BroadcastPort);
//		if (HostId == -1)
//		{
//			Debug.LogError("NetworkDiscovery StartAsClient - addHost failed");
//			return false;
//		}
//		byte error;
//		NetworkTransport.SetBroadcastCredentials(HostId, BroadcastKey, BroadcastVersion, BroadcastSubVersion, out error);
//		IsRunning = true;
//		IsClient = true;
//		Debug.Log("StartAsClient Discovery listening");
//		return true;
//	}

//	/// <summary>
//	///   <para>Starts sending broadcast messages.</para>
//	/// </summary>
//	/// <returns>
//	///   <para>True is able to broadcast.</para>
//	/// </returns>
//	public bool StartAsServer()
//	{
//		if (HostId != -1 || IsRunning)
//		{
//			Debug.LogWarning("NetworkDiscovery StartAsServer already started");
//			return false;
//		}
//		HostId = NetworkTransport.AddHost(DefaultTopology, 0);
//		if (HostId == -1)
//		{
//			Debug.LogError("NetworkDiscovery StartAsServer - addHost failed");
//			return false;
//		}
//		byte error;
//		if (!NetworkTransport.StartBroadcastDiscovery(HostId, BroadcastPort, BroadcastKey, BroadcastVersion, BroadcastSubVersion, MsgOutBuffer, MsgOutBuffer.Length, BroadcastInterval, out error))
//		{
//			Debug.LogError("NetworkDiscovery StartBroadcast failed err: " + error);
//			return false;
//		}
//		IsRunning = true;
//		IsServer = true;
//		Debug.Log("StartAsServer Discovery broadcasting");
//		DontDestroyOnLoad(gameObject);
//		return true;
//	}

//	/// <summary>
//	///   <para>Stops listening and broadcasting.</para>
//	/// </summary>
//	public void StopBroadcast()
//	{
//		if (HostId == -1)
//		{
//			Debug.LogError("NetworkDiscovery StopBroadcast not initialized");
//		}
//		else if (!IsRunning)
//		{
//			Debug.LogWarning("NetworkDiscovery StopBroadcast not started");
//		}
//		else
//		{
//			if (IsServer)
//				NetworkTransport.StopBroadcastDiscovery();

//			NetworkTransport.RemoveHost(HostId);
//			HostId = -1;
//			IsRunning = false;
//			IsServer = false;
//			IsClient = false;
//			MsgInBuffer = (byte[])null;
//			BroadcastsReceived = (Dictionary<string, NetworkBroadcastResult>)null;

//			Debug.Log((object)"Stopped Discovery broadcasting");
//		}
//	}

//	private void Update()
//	{
//		if (HostId == -1 || IsServer)
//			return;

//		NetworkEventType fromHost;
//		do
//		{
//			int connectionId;
//			int channelId;
//			int receivedSize;
//			byte error;
//			//Debug.Log("Discovery Update: m_HostId = " + HostId);

//			fromHost = NetworkTransport.ReceiveFromHost(HostId, out connectionId, out channelId, MsgInBuffer, 1024, out receivedSize, out error);

//			if (fromHost == NetworkEventType.BroadcastEvent)
//			{
//				NetworkTransport.GetBroadcastConnectionMessage(HostId, MsgInBuffer, 1024, out receivedSize, out error);
//				string address;
//				int port;
//				NetworkTransport.GetBroadcastConnectionInfo(HostId, out address, out port, out error);
//				NetworkBroadcastResult networkBroadcastResult = new NetworkBroadcastResult
//				{
//					serverAddress = address,
//					broadcastData = new byte[receivedSize]
//				};
//				Buffer.BlockCopy(MsgInBuffer, 0, networkBroadcastResult.broadcastData, 0, receivedSize);
//				BroadcastsReceived[address] = networkBroadcastResult;

//				OnReceivedBroadcast(address, BytesToString(MsgInBuffer));		// virtual function - see HostDiscovery override
//			}
//		}
//		while (IsRunning && fromHost != NetworkEventType.Nothing);
//	}

//	private void OnDestroy()
//	{
//		if (IsServer && IsRunning && HostId != -1)
//		{
//			NetworkTransport.Shutdown();        // SM
//			NetworkTransport.StopBroadcastDiscovery();
//			NetworkTransport.RemoveHost(HostId);

//		}
//		if (!IsClient || !IsRunning || HostId == -1)
//			return;

//		NetworkTransport.Shutdown();        // SM
//		NetworkTransport.RemoveHost(HostId);
//	}

//	/// <summary>
//	///   <para>This is a virtual function that can be implemented to handle broadcast messages when running as a client.</para>
//	/// </summary>
//	/// <param name="fromAddress">The IP address of the server.</param>
//	/// <param name="data">The data broadcast by the server.</param>
//	public virtual void OnReceivedBroadcast(string fromAddress, string data)
//	{
//	}
//}

