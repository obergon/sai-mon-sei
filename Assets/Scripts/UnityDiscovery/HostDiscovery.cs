﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;


//// Unity networking version of discovery
//public class HostDiscovery : NetworkingDiscovery
//{
//	private string dataBroadcast = "SAIMONSEI";

//	// handle broadcast messages when running as a client
//	public override void OnReceivedBroadcast(string fromAddress, string data)
//	{
//		byte[] dataBroadcastBytes = StringToBytes(data);

//		if (data != BytesToString(dataBroadcastBytes))
//		{
//			Debug.Log("OnReceivedBroadcast: unknown broadcast data " + data);
//			return;
//		}

//		// IP address is in format ::ffff:192.etc

//		string[] ipSplit = fromAddress.Split(':');
//		string ipAddress = ipSplit[3];

//		base.OnReceivedBroadcast(fromAddress, data);

//		Debug.Log("OnReceivedBroadcast: " + ipAddress + " data: " + data);
//		SaimonseiEvents.OnHostIP?.Invoke(ipAddress);
//	}
//}
